��    ?     
  �        �     �     �     �     �                    >     G     d     �     �     �     �     �     �     �  
   �     �            +     	   ;     E     \     w     �     �     �     �     �     �     �          $  $   ,  *   Q  j   |  3   �  -     *   I     t     �     �     �     �  '   �     �     �  	   �     �               *     7     D     R     c     u     w     ~     �     �  !   �  +   �                 "      +      0      G      a      y   E   �   $   �   +   �   '   (!  #   P!     t!     {!     �!     �!     �!     �!     �!     �!     �!     �!     �!     �!     "     "     ("     0"     9"     E"  "   L"     o"     �"     �"     �"     �"     �"     �"     �"     �"     �"     �"     #     $#     :#  1   W#     �#     �#     �#     �#     �#  !   �#     �#     �#     �#  T   $  ?   Y$     �$  E   �$     �$     %     %     '%  	   :%     D%     I%     P%     X%     `%     o%     }%     �%     �%  �   �%  �   Y&     "'     1'     >'  
   E'     P'     U'     f'     r'  	   {'     �'     �'     �'  	   �'     �'     �'     �'     �'  5   �'  2   (  9   7(  4   q(  	   �(     �(     �(  ^   �(     )     $)     -)     9)     G)  	   P)     Z)     a)     f)     �)     �)  1   �)     �)     �)  t   �)     a*     r*     {*     �*     �*     �*     �*     �*     �*     �*     +     +     "+     /+     <+     C+     J+     V+  <   u+  �   �+     T,     \,      l,     �,     �,     �,  "   �,     �,     �,  	   �,     �,  	    -     
-  
   -     #-     *-  $   7-  -   \-  )   �-     �-     �-     �-  1   �-  "   .     4.     E.     W.     Y.     ^.     l.     s.     �.     �.     �.     �.     �.     �.     �.     �.     �.     /     $/     7/     @/     I/  6   O/     �/     �/     �/     �/  	   �/     �/     �/     �/     �/  "   �/     0     ,0     <0     O0  ;   [0  7   �0     �0     �0  8   �0  "  51  8  X2  	  �3     �4     �4     �4     �4     �4      5  	   5     5     45     ;5     G5     N5     [5     h5     v5     |5     �5  	   �5  	   �5  	   �5  	   �5     �5     �5  	   �5  
   �5     �5     �5  +   6     -6     ;6  	   Q6  l   [6     �6     �6     �6     �6  	   �6     �6  
   �6     7     '7      >7     _7     n7     �7     �7     �7     �7     �7  %  �7     �8     �8     �8     9     /9     F9     O9     n9     }9     �9     �9     �9  !   �9  !   �9  !   �9     :     %:     ,:     ;:     C:     J:  /   R:     �:     �:  #   �:     �:  #   �:     	;     (;     6;     L;     h;     {;     �;  
   �;  (   �;  0   �;  y   <  =   �<  0   �<  *   �<     =  
   0=     ;=     D=     J=  '   P=     x=     �=     �=     �=     �=     �=     �=     �=     
>     >     +>     =>     ?>  
   H>  &   S>  6   z>  0   �>  .   �>  
   ?     ?  	   (?     2?     9?     U?     t?     �?  O   �?  $   �?  +   @  '   7@  #   _@     �@     �@     �@     �@     �@     �@     �@     �@     �@     �@     A     A     3A     @A     PA     \A     iA     yA  '   �A     �A  !   �A     �A     �A     �A     �A     �A     B     B     "B     <B  *   YB  &   �B  *   �B  B   �B     C     ,C     8C     OC     UC  %   ^C     �C     �C     �C  �   �C  T   ,D  (   �D  V   �D     E     	E     !E     3E     ME     ^E     cE     kE     tE     |E     �E     �E     �E  	   �E  �   �E  �   tF     ?G     QG     _G  
   fG     qG     vG  
   �G  	   �G  
   �G     �G     �G     �G     �G     �G  	   �G     �G  
   �G  L   �G  D   GH  G   �H  A   �H     I     I     "I  |   )I     �I     �I     �I     �I  	   �I  
   �I     �I     �I  #   �I     "J  %   6J  8   \J     �J     �J  �   �J     "K     4K     <K     IK     XK     hK     ~K  $   �K     �K     �K  
   �K     �K     �K     �K     L     	L     L     !L  T   AL  �   �L     RM     [M  '   qM     �M     �M     �M  (   �M  	   �M  
   N     N     N  	   (N     2N     @N     MN     VN  1   fN  9   �N  7   �N     
O  	   &O  '   0O  P   XO     �O      �O  !   �O     P     P     	P      P     'P     @P     RP     lP     xP     �P     �P     �P     �P     �P     �P     
Q      Q  	   (Q     2Q  C   DQ     �Q  $   �Q     �Q     �Q  	   �Q     �Q     �Q     �Q     �Q     R      R     .R     =R     WR  8   fR  7   �R     �R     �R  N   S  2  VS  N  �T  %  �U     �V     W     =W     SW     jW     W  
   �W     �W     �W     �W  
   �W     �W     �W     X     /X     7X     ?X  
   HX  
   SX  
   ^X  
   iX     tX     xX     �X     �X     �X     �X  +   �X     Y     %Y     ;Y  �   IY  	   �Y  	   �Y     �Y     �Y  	   �Y     �Y     Z     Z     /Z     JZ     gZ     uZ     �Z     �Z     �Z     �Z     �Z                �   	   �   �   �   �   �   �             (      �   �          N           +          U   w   �          �       �       �   i         �   d   �   *   c   �   �   �   3  a   ]   �     !   <          �   M   �   �   �       '   �     <       g       �   �   S   �       /   	      u   �   �       �   �               �           6   $       �   H       �   I   "             E   �           �   �       �   z     m   �   �      �       �   =   B   �   9  5   ^   �   �   �   �   O   �       W   �           �     �       o       �       }   :   &     �   �           G          ?      �        5  (   L               �            "  #  j   C      �      �       7   �   Q   �           �   =      -     �   �       �     ?   ~   �      {   ;       �   �   �   �   f      1   1  ;  �   �   F          �   [           y         �   ,           �   �              �   +   �      4   
  �       �      �   k   �   �       X   -       �   9   /         #   l       �       2       �           )      �                     �       �   _   �   �   �   b   n            7      �           %         �   $      �   P   �   �   �   p   >  �   �   �   q   T       �   x   �           V      �        J   e          D   �     �   �   �   �   �     r           \       8         �           3   �            �       >   �       �           t   %           �           �      s         |       �               Z   .  0  �             :  @   6  2       .   8      !    
   ,    *  �      A   �           )       4  �   K   �   Y       h   0   �   �           �   '  v       `       �   R   &   �   �      % OM %% 1st Response no OM %s %% 2nd Response no OM %s %% Correct Total %s %% OM %s %d years %d month(s) %d day(s) %s shown (%) Correct (1st) without OM (%) Correct (2nd) without OM (%) Correct TOTAL --- 1st example word/sentence 2nd example word/sentence 3rd example word/sentence < Back About About TECS Accept Active Add Add 3 related sentences to the bank at once Add Block Add Collection To Bank Add Related Sentences (v3) Add To Bank Add all words to the bank Admin Comments: Administrator Age During Test: Allow/Disallow editing An example will follow Another example will follow Answer Counts Answers Application developed by John Murray Are you sure you want to delete this bank? Are you sure you want to delete this filter?
Any word or sentence using this filter will be set to neutral Are you sure you want to delete this from the bank? Are you sure you want to delete this program? Are you sure you want to delete this test? Auto Select Average Response Time for %s BAD DATA BANK Bank Bank '%s' does not exist on host system Bank Filter Bank Management Bank Name Bank Preview Border Height Border Thickness Border Width Break at end Break at end
 Break per filter Break per filter
 C Cancel Category Category of word or sentence Click to add a new custom item Click to remove the selected item Colour for words/sentences of the %s filter Comment Comments Continue Copy Copy the selected bank Copy the selected program Correct 1st Response %s Correct 2nd Response %s Correct in each sentence or word (count by first response categories) Could not create app banks directory Could not create app participants directory Could not create app programs directory Could not create app save directory Create Create New Bank Create Program Create new bank Current Bank Custom D.o.B DELETE BANK DELETE Filter DELETE INFO DELETE PROGRAM DELETE WORD/SENTENCE Date / Time Date of Test: Default Default: Default: %d Delete Delete selected word from the bank Delete the selected bank Delete the selected program DoB: Done E Edit Edit Existing Edit Filters Edit Phases Edit selected bank Edit selected program Edit selected word in the bank Enable Admin Password Enter administrator password Enter administrator password (3 attempts allowed) Example Counts Excel Colour Excel Colour Index Exit Export Export the bank to an Excel sheet Female Filter Filter by count Filter participant results by program name containing word(s) (note: case sensitive) Filter program matching specific word(s) (note: case sensitive) Filter the bank by word count Filter the bank searching for specific word(s) (note: case sensitive) Filters First Example Text First Response First answer (key) Font Size GUID Gender Gender: General Generate Excel Generate HTML ID ID: INVALID If you have any doubts, request help before continuing.




Remember that at the end of the task you will be asked to write as many sentences as you can remember.




If you understand, click to continue. If you have any doubts, request help before continuing.




Remember that at the end of the task you will be asked to write as many words as you can remember.




If you understand, click to continue. Import to Bank In Sequence
 In Use In Use: %d Info Info Text Colour Internal ID Interval Interval: Invalid bank
 Legend Lines Lines Min Lock Male Max Mean Response Time %s Modify global response timeout for this phase version Modify global rest interval for this phase version Modify global time between changes for this phase version Modify global time to respond for this phase version Move Down Move Up Name Need more words between '%d' and '%d' for filter '%s' require %d words but only found %d valid Neutral New Bank New Program No ID entered No break No break
 No. %s None Number of lines in the stimuli Number of words Number of words in the stimuli Numeric identifier for the participant (required) OK OM Omitted - Red text means the participant either did not respond at all or tried to respond outside the response time One last example Ordering Participant Participant # Participant ID Participant ID already exists Participant Info Participant date of birth Password Required Paste From Clipboard Percent Phase Phase Filter Phase Select Phase: Phases Please Wait Please enter the Num Lock test Please recall as many words and sentences in the box below.
 Please use the space below to check the numeric keypad
Enable or Disable the Num Lock key if the numbers are not written as you type

Please enter the number 456 Program Program Details Program Details - Participant %s Program Name Program Name: Program Valid Program is invalid or not selected Programs Random
 Randomise Referential Remaining Remaining: %d Remembered Remove Remove Block Remove selected word from the import Required %d of type filter '%s' only found %d Required %d words, currently has %d words Required To Select Reset Reset To Defaults Resets all global defaults for this phase version Response Time for sentence or word Response Timeout Response Timeout: S SPSS Save xls file Search Second Example Text Second Response Second answer (key) Select Select Bank Select Phase Selected Phase Selected Program Selected bank name Sentence (4 words) Sentence (5 words) Sentence (6 words) Settings Show All Shown Single click to select a program, double click to edit Start Stored Participant Records T TECS TOT OM %s TOTAL Correct (1st) TOTAL Correct (2nd) TOTAL OM Test Complete Test numeric keys are working here Text + Background Text Background Text Border Colour Text Colour The number of sentences that have appeared on the screen is The number of words that have appeared on the screen is Third Example Text This ID is already in use This is a default test string for the word/sentence bank This task is to count the number of sentences that appear on the screen and type the corresponding number on the keyboard.



Work fast because the sentences will disappear in a short time.



At the end of the task you will be asked to write as many complete sentences as you can remember. This task is to count the number of words in the sentences that appear on the screen and type the corresponding number of words on the keyboard.



Work fast because the sentences will disappear in a short time.



At the end of the task you will be asked to write as many complete sentences as you can remember. This task is to count the number of words that appear on the screen and type the corresponding number on the keyboard.



Work fast because words will disappear in a short time.



At the end of the task you will be asked to write as many words as you can remember. Time Between Changes Time Between Filter Changes: Time to Respond Time to Respond: Tutorial End Text Tutorial Intro Text Tutorials University of Seville Unlock Unspecified Update Use Defaults User Answers User Answers: VALID Valid Version Version 1 Version 2 Version 3 Version 4 View View selected program Wait Text Word Count Word Count %d Word or Sentence Word or Sentence already exists in the bank Word/Sentence Word/Sentence content Words Min Yellow background with a number means the participant answered in the response time but answered incorrectly bookshop computer estal  meeting motional  ounting  remembered the public have begun to arrive they always look at me they always look at me strangely they ignore me they laugh behind your back they look at me they mock me they see me they speak about you troop Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-18 00:09+0100
Last-Translator: 
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
   % OM %% aciertos 1ªR no OM %s %% aciertos 2ªR no OM %s %% aciertos totales %s %% OM %s %d años %d mes(es) %d día(s) %s mostrado(s) (%) AC 1ª sin OM (%) AC 2ª sin OM (%) aciertos TOT --- 1º ejemplo de palabra / oración 2º ejemplo de palabra / oración 3º ejemplo de palabra / oración < Volver Acerca Acerca de TECS Aceptar Activo Añadir Añadir 3 frases relacionadas al banco a la vez Agregar bloque Agregar la colección al banco Añadir oraciones relacionadas (v3) Añadir al banco Agregar todas las palabras al banco Comentarios del administrador: Administrador Edad durante el test: Permitir/no permitir editar Hagamos un ejemplo Hagamos otro ensayo Contador de respuestas Respuestas Aplicación desarrollada por John Murray ¿Está seguro de que desea eliminar este banco? ¿Está seguro de que desea eliminar este filtro?
Cualquier palabra o frase utilizando este filtro se cambiará a neutral ¿Está seguro de que desea eliminar este elemento del banco? ¿Está seguro de que desea borrar este archivo? ¿Está seguro de que desea eliminar esto? Auto-seleccionar Mean TR %s BAD DATA BANCO Banco Banco '%s' no existe en el sistema host Filtro de banco Administración del banco Nombre del banco Vista previa del banco Altura de borde Grosor del borde Ancho del borde Pausa al final Pausa al final
 Pausa por filtro Pausa por filtro
 C Cancelar Categoría Categorías de cada palabra u oración Haga clic para añadir un nuevo elemento personalizado Haga clic para eliminar el elemento seleccionado Color de las palabras / oraciones del filtro%s Comentario Comentarios Continuar Copiar Copia el banco seleccionado Copia el programa seleccionado Aciertos 1ªR %s Aciertos 2ªR %s Aciertos en cada oración o palabra (contar por categorías respiuesta primera) Could not create app banks directory Could not create app participants directory Could not create app programs directory Could not create app save directory Crear Crear nuevo banco Crear programa Nuevo banco Corriente Banco Personalizado F.d.N Eliminar banco ELIMINAR Filtro ELIMINAR INFO ELIMINAR PROGRAMA ELIMINAR PALABRA/ORACIÓN Fecha / Hora Fecha del Test: Por defecto Por defecto: Por defecto: %d Eliminar Eliminar palabra seleccionada del banco Eliminar el banco seleccionado Eliminar el programa seleccionado FdN: Hecho E Editar Editar Editar filtros Editar fases Editar banco seleccionado Editar programa seleccionado Editar la palabra seleccionada en el banco Habilitar contraseña de administrador Introduzca la contraseña de administrador Introduzca la contraseña de administrador (3 intentos permitidos) Cuentas de ejemplo Excel Color Índice de Color Excel Salir Exportar Exportar el banco a una hoja de Excel Femenino Filtro Filtrar por cuenta Filtrar resultados de participantes por nombre del programa que contienen la palabra (s) (nota: distingue mayúsculas y minúsculas) Filtrar el banco usando la(s) palabra(s) (nota: distingue mayúsculas y minúsculas) Filtrar el banco por número de palabras Filtrar el banco en busca de la palabra(s) (nota: distingue mayúsculas y minúsculas) Filtros Primer texto de ejemplo Respuesta primera Respuesta primera (tecla) Tamaño de texto GUID Género Género: General Generar Excel Generar HTML ID ID: INVÁLIDO Si tienes alguna duda consúltala antes de continuar.




Recuerda que al final de la tarea se te pedirá que escribas tantas oraciones como puedas recordar.




Si has entendido la tarea sigue adelante. Si tienes alguna duda consúltala antes de continuar.




Recuerda que al final de la tarea se te pedirá que escribas tantas palabras como puedas recordar.




Si has entendido la tarea sigue adelante. Importar al banco En secuencia
 En uso En uso: %d Info Color de texto info ID interno Intervalo Intervalo: Banco inválido
 Leyanda Líneas Mín líneas Bloquear Masculino Max Mean TR %s Modificar el tiempo de espera de respuesta global para esta versión de fase Modificar el intervalo de descanso global para esta versión de fase Modificar el tiempo global entre los cambios para esta versión de fase Modificar el tiempo global para responder a esta versión de fase Bajar Subir Nombre Se necesitan más palabras entre '%d' y '%d' filtro '%s' se requieren %d palabras pero sólo se han encontrado %d válido(s) Neutro Nuevo banco Nuevo programa ID no introducido Sin pausa Sin pausa
 Nº %s Ninguno Número de lineas de los estímulos Número de palabras Número de palabras de los estímulos Identificador numérico para el participante (requerido) OK OM Omitido - El texto en rojo significa que el participante no respondió en absoluto o trató de responder fuera del tiempo de respuesta Un último ensayo Ordenar Participante Participante # ID Participante El ID ya está en uso Participante Info Fecha de nacimiento del participante Contraseña requerida Pegar Porcentaje Fase Filtro de fase Selección de fase Fase: Fases Espere, por favor Introduce la prueba de Bloq Num Por favor escribe todas las palabras y frases que recuerdes en el cuadro siguiente.
 Por favor utilice el espacio de abajo para comprobar el teclado numérico
Active o desactive la tecla Bloq Num si los números no aparecen al teclear

por favor, introduzca el número 456 Programa Detalles del programa Detalles del programa - Participante %s Nombre de programa Nombre del programa: Programa válido Programa no es válido o no seleccionado Programas Aleatorio
 Aleatorizar Referencial Restantes Restantes: %d Recordado(s) Eliminar Eliminar bloque Quitar la palabra seleccionada de la importación %d se requieren de filtro '%s' sólo se han encontrado %d Se requieren %d palabras, actualmente tiene %d palabras Necesarios para seleccionar Reiniciar Restablecer los valores predeterminados Restablece todos los valores predeterminados globales para esta versión de fase TR oración o palabra Tiempo de espera de la respuesta Tiempo de espera de la respuesta: S SPSS Guardar archivo de xls Buscar Segundo texto de ejemplo Respuesta segunda Respuesta segunda (tecla) Seleccionar Selección de banco Selección de fase Selección de fase Selección de programa Selección del banco Oración (4 palabras) Oración (5 palabras) Oración (6 palabras) Ajustes Ver todos Total mostrado(s) Haga clic para seleccionar un programa, haga doble clic para editar Empezar Registros de participantes guardados T TECS TOT OM %s TOT aciertos (1ª) TOT aciertos (2ª) TOT OM Prueba completa Probar las teclas numéricas Texto + fondo Fondo de texto Color del borde del texto Color de texto El número de oraciones que han aparecido en pantalla es El número de palabras que han aparecido en pantalla es Tercer texto de ejemplo Este ID ya está en uso Esto es una cadena de prueba predeterminado para el banco de palabras o frases Esta tarea consiste en que cuentes el número de oraciones que aparecen en pantalla y teclees el número correspondiente en el teclado.



Trabaja rápido porque las oraciones desaparecerán en poco tiempo.



Al final de la tarea se te pedirá que escribas tantas oraciones completas como puedas recordar. Esta tarea consiste en que cuentes el número de palabras de las oraciones que aparecen en pantalla y teclees el número correspondiente de palabras en el teclado.



Trabaja rápido porque las oraciones desaparecerán en poco tiempo.



Al final de la tarea se te pedirá que escribas tantas oraciones completas como puedas recordar. Esta tarea consiste en que cuentes el número de palabras que aparecen en pantalla y teclees el número correspondiente en el teclado.



Trabaja rápido porque las palabras desaparecerán en poco tiempo.



Al final de la tarea se te pedirá que escribas tantas palabras como puedas recordar. Tiempo entre cambios de filtro Tiempo entre cambios de filtro: Tiempo para responder Tiempo para responder: Texto final tutorial Texto Intro tutorial Tutoriales Universidad de Sevilla Desbloquear No especificado Actualizar Usar valores por defecto Respuestas del participante Respuestas del participante: VÁLIDO Válido Versión Versión 1 Versión 2 Versión 3 Versión 4 Ver Ver programa seleccionado Texto durante la espera Número de palabras Número de palabras %d Palabra u oración La palabra u oración ya existe en el banco Palabra/Oración Oraciones y contenido Mín palabras Fondo amarillo con un número significa que el participante respondió en el tiempo de respuesta, pero respondió incorrectamente librería ordenador estal  reunión motional  ounting  recordado(s) el público ya empezó a llegar ellos me miran mal siempre Y ellos me miran mal siempre hablan de mí se ríen a tus espaldas ellos me miran mal me hacen burla ellos me miran siempre hablan de tí troop 