
setlocal ENABLEEXTENSIONS EnableDelayedExpansion

del pot_files.txt
del src_files.txt

cd ../App/src/

dir /b /s *.cpp >> ../../Translate/src_files.txt

cd ..
cd ..
cd Translate

for /f %%a in (src_files.txt) do (

	for %%i in ("%%a") do (
		set "filedrive=%%~di"
		set "filepath=%%~pi"
		set "filename=%%~ni"
		set "fileextension=%%~xi"
		
		set outfile=!filename!.pot
	)
	xgettext --c++ -s --keyword=_ -p ./po -o !outfile! %%a
)

cd po

del /q "AppComplete.pot"

dir /b /s *.pot >> ../pot_files.txt

REM msgcat "*.pot" > AppComplete.pot

msgcat -f ../pot_files.txt > AppComplete.pot

for %%i in (*.*) do if not "%%i"=="AppComplete.pot" del /q "%%i"

endlocal