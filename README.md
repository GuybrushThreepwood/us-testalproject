# README #

TECS (Testal Emotional Counting Stroop)

### What is this repository for? ###

* This project is part of the investigation into the development and validation of a prototype emotional counting stroop for the evaluation of the referential thought: supporting the process of early detection of psychosis

### How do I get set up? ###

* The project currently is built for 32/64 bit platforms on Windows and Mac OS (but should be easy to compile for unix based platforms)
* The application has two static library dependencies jsonbox and the wxWidgets library

### Who do I talk to? ###

* Repo owner / App developer - John Murray
* Principal Research Team (University of Seville): Rodríguez-Testal, J.F., Senín-Calderón, C. y Perona-Garcelán, S.