/**
	TECS
	ProjectMainFrame.cpp
    Purpose: Handles the apps main frame including menu bar and current panel view
*/

#include "ProjectMainFrame.h"
#include "DlgMenuAbout.h"
#include "DlgSettings.h"
#include "PanelPhaseRun.h"
#include "DlgPassword.h"
#include "System.h"

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
ProjectMainFrame::ProjectMainFrame( wxWindow* parent )
:
MainFrame( parent )
{
	wxString version(" v1.0 (2022-02)");
	this->SetTitle(_("TECS") + version);

	m_Config = 0;
	m_Config = new wxConfig("TECS");

	wxSize size = GetSize(); // get some default

	if (m_Config->Read(wxT("ui/frame.maximized"), 0L)) {
		// this only sets wxTopLevelWindow::m_maximizeOnShow
		Maximize(true);
	}
	// these won't overwrite the values if the keys are not present
	m_Config->Read(wxT("ui/frame.width"), &size.x);
	m_Config->Read(wxT("ui/frame.height"), &size.y);

	SetSize(size);
}

/////////////////////////////////////////////////////
/// CloseEvent
/// Desc: Event handler for when the program closes
/////////////////////////////////////////////////////
void ProjectMainFrame::CloseEvent(wxCloseEvent& event)
{
	// the changes will be written back automatically
	if( m_Config )
		delete m_Config;

	event.Skip();
}

/////////////////////////////////////////////////////
/// SizeEvent
/// Desc: Event handler for when the program frame changes size
/////////////////////////////////////////////////////
void ProjectMainFrame::SizeEvent(wxSizeEvent& event)
{
	if (m_Config)
	{
		m_Config->Write(wxT("ui/frame.maximized"), IsMaximized());
		if (!IsMaximized()) {
			m_Config->Write(wxT("ui/frame.width"), event.GetSize().x);
			m_Config->Write(wxT("ui/frame.height"), event.GetSize().y);
		}
	}

	event.Skip();
}

/////////////////////////////////////////////////////
/// IdleEvent
/// Desc: Event handler for when the program is idle (used for testing timer states)
/////////////////////////////////////////////////////
void ProjectMainFrame::IdleEvent(wxIdleEvent& WXUNUSED(event))
{
	ScreenList currentPanel = sys::AppData::Instance()->GetPanelId();
	wxPanel* pPanel = sys::AppData::Instance()->GetCurrentPanel();

	switch (currentPanel)
	{
		case SCREEN_EMPTY:
			break;
		case SCREEN_ENTRY:
			break;
		case SCREEN_ADMIN:
			break;
		case SCREEN_BANKSELECTION:
			break;
		case SCREEN_BANK:
			break;
		case SCREEN_PHASEPROGRAMS:
			break;
		case SCREEN_PARTICIPANTINFO:
			break;
		case SCREEN_PARTICIPANT:
			break;
		case SCREEN_TUTORIAL:
			break;
		case SCREEN_PHASERUN:
		{
			PanelPhaseRun* pPhaseRun = reinterpret_cast<PanelPhaseRun*>(pPanel);
			if (pPhaseRun != nullptr)
				pPhaseRun->DoIdleRefresh();
		}break;
		case SCREEN_QUESTIONS:
			break;
		default:
			break;
	}
}

/////////////////////////////////////////////////////
/// MenuSettingsClick
/// Desc: Menu event for showing the settings dialog
/////////////////////////////////////////////////////
void ProjectMainFrame::MenuSettingsClick(wxCommandEvent& WXUNUSED(event))
{
#ifdef _DEBUG
	DlgSettings* dlg = new DlgSettings(0);
	if (dlg->ShowModal() == RESPONSE_OK)
	{

	}

	dlg->Destroy();
	delete dlg;
#else
	if (sys::AppData::Instance()->GetAdminPasswordState())
	{
		DlgPassword* dlg = new DlgPassword(0);
		if (dlg->ShowModal() == RESPONSE_OK)
		{
			DlgSettings* dlgSettings = new DlgSettings(0);
			if (dlgSettings->ShowModal() == RESPONSE_OK)
			{

			}

			dlgSettings->Destroy();
			delete dlgSettings;
		}

		dlg->Destroy();
		delete dlg;
	}
	else
	{
		DlgSettings* dlgSettings = new DlgSettings(0);
		if (dlgSettings->ShowModal() == RESPONSE_OK)
		{

		}

		dlgSettings->Destroy();
		delete dlgSettings;
	}
#endif // _DEBUG
}

/////////////////////////////////////////////////////
/// MenuAboutClick
/// Desc: Menu event for showing the about dialog
/////////////////////////////////////////////////////
void ProjectMainFrame::MenuAboutClick(wxCommandEvent& WXUNUSED(event))
{
	DlgMenuAbout* dlg = new DlgMenuAbout(0);
	if (dlg->ShowModal() == RESPONSE_OK)
	{

	}

	dlg->Destroy();
	delete dlg;

}
