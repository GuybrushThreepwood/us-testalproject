/**
	TECS
	PanelBank.cpp
    Purpose: Panel for complete management of the word/sentence bank
*/

#include "PanelBank.h"
#include "DlgAddEditBank.h"
#include "DlgAddEditWithChildrenBank.h"
#include "DlgCustomEdit.h"
#include "DlgBankImport.h"
#include "System.h"

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
PanelBank::PanelBank( wxWindow* parent )
:
Bank( parent )
{
	// only allow row selection
	m_WordBankGrid->SetSelectionMode(wxGrid::wxGridSelectRows);

	// labels
	m_WordBankGrid->SetColLabelValue(0, _("Word/Sentence"));
	m_WordBankGrid->SetColLabelValue(1, _("Filter"));
	m_WordBankGrid->SetColLabelValue(2, _("Number of words"));

	// sort by filter
	m_WordBankGrid->SetSortingColumn(1);

	// disable cell highlight
	m_WordBankGrid->SetCellHighlightPenWidth(0);

	m_BankFilter = FILTER_ALL;
	m_StringFilter = "";
	m_WordCountFilter = -1;

	CreateGridFromBank();

	DoRefreshFilters();

	m_WordBankGrid->AutoSize();

	// paste check
	m_WordBankGrid->Connect(wxID_ANY, wxEVT_KEY_DOWN, wxKeyEventHandler(PanelBank::CopyPasteData), (wxObject*)NULL, this);
	m_WordBankGrid->GetGridWindow()->Connect(wxEVT_RIGHT_UP, wxMouseEventHandler(PanelBank::RightClick), NULL, this);

	// set the bank name as combo of name and guid
	BankFileInfo bf = sys::AppData::Instance()->GetSelectedBank();
	if (!bf.jsonData.isNull())
	{
		wxString str = wxString::Format("%s | guid: %s", bf.name, bf.guid);
		m_BankName->SetValue(str);
	}
}

/////////////////////////////////////////////////////
/// CreateGridFromBank
/// Desc: Clears and creates the current word bank into a wxGrid
/////////////////////////////////////////////////////
void PanelBank::CreateGridFromBank()
{
	// How many Cols/Rows
	int rows = m_WordBankGrid->GetNumberRows();

	// Delete all Cols/Rows
	if ( rows > 0 )
		m_WordBankGrid->DeleteRows(0, rows, true);

	std::map<wxString, BankData> bank = sys::AppData::Instance()->GetBank();

	DoBankFilter(bank, 0);

	m_WordBankGrid->InsertRows(0, bank.size());

	// populate
	int rowId = 0;
	for (const auto iter : bank)
	{
		m_WordBankGrid->SetCellValue(rowId, 0, iter.second.str);

		const FilterData& fd = sys::AppData::Instance()->FindFilter(iter.second.filterIndex);

		m_WordBankGrid->SetCellValue(rowId, 1, fd.name);
		m_WordBankGrid->SetCellValue(rowId, 2, wxString::Format("%d", (int)iter.second.wordCount));

		if (iter.second.hasGUID &&
			(iter.second.guid.size() > 0) &&
			(iter.second.guid != " "))
		{
			const int COLOUR_BG = 235;

			m_WordBankGrid->SetCellBackgroundColour(rowId, 0, wxColor(COLOUR_BG, COLOUR_BG, COLOUR_BG));
			m_WordBankGrid->SetCellBackgroundColour(rowId, 1, wxColor(COLOUR_BG, COLOUR_BG, COLOUR_BG));
			m_WordBankGrid->SetCellBackgroundColour(rowId, 2, wxColor(COLOUR_BG, COLOUR_BG, COLOUR_BG));
		}

		rowId++;
	}

	m_WordBankGrid->AutoSize();
	this->Layout();
}

/////////////////////////////////////////////////////
/// DoRefreshFilters
/// Desc: Populates the filter combo box with the stored filters
/////////////////////////////////////////////////////
void PanelBank::DoRefreshFilters()
{
	// populate the data
	m_ComboFilter->Clear();

	// populate from the current map
	m_FilterMap = sys::AppData::Instance()->GetFilters();
	for (auto val : m_FilterMap)
	{
		int index = m_ComboFilter->GetCount();
		m_ComboFilter->Insert(val.second.name, index);
	}
	// add ALL as first element
	m_ComboFilter->Insert(_("Show All"), 0);
	m_ComboFilter->SetSelection(0);
}

/////////////////////////////////////////////////////
/// DoBankFilter
/// Desc: Recursive method to filter word/sentences from the current bank 
/////////////////////////////////////////////////////
void PanelBank::DoBankFilter(std::map<wxString, BankData>& bank, int mode)
{
	if ( mode == 0) // filter by type
	{ 
		if (m_BankFilter != FILTER_ALL)
		{
			for (auto iter = bank.begin(); iter != bank.end();)
			{
				if (iter->second.filterIndex != m_BankFilter)
				{
					iter = bank.erase(iter);
					continue;
				}
				
				// next
				iter++;
			}
		}

		DoBankFilter(bank, 1);
	}
	if (mode == 1) // filter by search
	{
		if (m_StringFilter.Length() > 0 )
		{
			for (auto iter = bank.begin(); iter != bank.end();)
			{
				if (!iter->second.str.Contains(m_StringFilter))
				{
					iter = bank.erase(iter);
					continue;
				}
				
				// next
				iter++;
			}
		}

		DoBankFilter(bank, 2);
	}
	if (mode == 2) // filter by number
	{
		if (m_WordCountFilter > 0)
		{
			for (auto iter = bank.begin(); iter != bank.end();)
			{
				if (iter->second.wordCount != m_WordCountFilter)
				{
					iter = bank.erase(iter);
					continue;
				}
				
				// next
				iter++;
			}
		}
	}
}

/////////////////////////////////////////////////////
/// BtnEditFiltersClick
/// Desc: Button event to edit the filter list (not used)
/////////////////////////////////////////////////////
void PanelBank::BtnEditFiltersClick(wxCommandEvent& WXUNUSED(event))
{
	DlgCustomEdit* dlg = new DlgCustomEdit(0, CUSTOMMODE_FILTER);
	if (dlg->ShowModal() == RESPONSE_OK)
	{
		CreateGridFromBank();

		sys::AppData::Instance()->SaveSettings(sys::AppData::Instance()->defaultSettingsFile);

		DoRefreshFilters();
	}

	dlg->Destroy();
	delete dlg;
}

/////////////////////////////////////////////////////
/// BankFilterChange
/// Desc: Input event when the bank filter text is changed to filter out containing words in the bank
/////////////////////////////////////////////////////
void PanelBank::BankFilterChange(wxCommandEvent& WXUNUSED(event))
{
	int filterId = 0;

	int selectionIndex = m_ComboFilter->GetSelection();
	wxString str = m_ComboFilter->GetStringSelection();

	if (selectionIndex == 0 ||
		str.length() == 0 )
		filterId = 0;
	else
	{
		for (auto iter : m_FilterMap)
		{
			if (iter.second.name.IsSameAs(str))
			{
				filterId = iter.second.val;
				break;
			}
		}
	}

	if (filterId != m_BankFilter)
	{
		m_BankFilter = filterId;
		CreateGridFromBank();
	}
}

/////////////////////////////////////////////////////
/// BankSearchCancel
/// Desc: Cancel event for the search to clear the input box
/////////////////////////////////////////////////////
void PanelBank::BankSearchCancel(wxCommandEvent& WXUNUSED(event))
{
	if (m_Search->GetValue().Length() > 0)
	{
		m_Search->Clear();
		m_StringFilter.Clear();

		CreateGridFromBank();
	}
}

/////////////////////////////////////////////////////
/// BankSearchCancel
/// Desc: Button event to filter out containing words in the bank
/////////////////////////////////////////////////////
void PanelBank::BankSearch(wxCommandEvent& WXUNUSED(event))
{
	wxString str = m_Search->GetValue();

	if (m_StringFilter != str)
	{
		m_StringFilter = str;
		CreateGridFromBank();
	}
}

/////////////////////////////////////////////////////
/// WordCountKeyUp
/// Desc: Key event to filter the bank by word count
/////////////////////////////////////////////////////
void PanelBank::WordCountKeyUp(wxKeyEvent& WXUNUSED(event))
{
	wxString str = m_WordCount->GetValue();
	long value = -1;
	if (!str.ToLong(&value))
	{
		if (m_WordCountFilter != -1)
		{
			m_WordCountFilter = -1;
			CreateGridFromBank();
		}
	}
	else
	{
		m_WordCountFilter = value;
		CreateGridFromBank();
	}
}

/////////////////////////////////////////////////////
/// GridCellLeftClick
/// Desc: Event for left click to select a row in the word bank
/////////////////////////////////////////////////////
void PanelBank::GridCellLeftClick(wxGridEvent& event)
{
	m_WordBankGrid->ClearSelection();
	m_WordBankGrid->SelectRow(event.GetRow());
}

/////////////////////////////////////////////////////
/// GridCellLeftDoubleClick
/// Desc Event for double left click to edit a word/sentence in the bank
/////////////////////////////////////////////////////
void PanelBank::GridCellLeftDoubleClick(wxGridEvent& WXUNUSED(event))
{
	wxCommandEvent empty;
	BtnEditClick(empty);
}

/////////////////////////////////////////////////////
/// BtnAddClick
/// Desc: Button event to display the add dialog for the word bank
/////////////////////////////////////////////////////
void PanelBank::BtnAddClick(wxCommandEvent& WXUNUSED(event))
{
	DlgAddEditBank* dlg = new DlgAddEditBank(0);
	dlg->SetMode(MODE_ADD);
	dlg->Apply();
	if (dlg->ShowModal() == RESPONSE_OK)
	{
		CreateGridFromBank();

		sys::AppData::Instance()->SaveBank();
	}

	dlg->Destroy();
	delete dlg;
}

/////////////////////////////////////////////////////
/// BtnAddWithChildrenClick
/// Desc: Button event to display the add dialog for the word bank (v3 related sentences)
/////////////////////////////////////////////////////
void PanelBank::BtnAddWithChildrenClick(wxCommandEvent& WXUNUSED(event))
{
	DlgAddEditWithChildrenBank* dlg = new DlgAddEditWithChildrenBank(0);
	dlg->SetMode(MODE_ADD);
	dlg->Apply();
	if (dlg->ShowModal() == RESPONSE_OK)
	{
		CreateGridFromBank();

		sys::AppData::Instance()->SaveBank();
	}

	dlg->Destroy();
	delete dlg;
}

/////////////////////////////////////////////////////
/// BtnEditClick
/// Desc: Button event to edit a selected word/sentence (launches DlgAddEditBank)
/////////////////////////////////////////////////////
void PanelBank::BtnEditClick(wxCommandEvent& WXUNUSED(event))
{
	// get selected
	wxArrayInt arr = m_WordBankGrid->GetSelectedRows();
	
	int numSelected = arr.Count();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId >= 0 && rowId < 
			m_WordBankGrid->GetNumberRows())
		{
			BankData itemData;
			itemData.str = m_WordBankGrid->GetCellValue(rowId, 0);

			// find the item in the bank to check for a GUID
			const BankData& bd = sys::AppData::Instance()->GetFromBank(itemData.str);
			
			if( !bd.hasGUID )
			{
				// normal word/sentence
				DlgAddEditBank* dlg = new DlgAddEditBank(0);
				dlg->SetString(itemData.str);
				dlg->SetMode(MODE_EDIT);
				dlg->Apply();
				if (dlg->ShowModal() == RESPONSE_OK)
				{
					CreateGridFromBank();

					sys::AppData::Instance()->SaveBank();
				}

				dlg->Destroy();
				delete dlg;
			}
			else
			{
				// related sentence edit
				std::vector<BankData> list = sys::AppData::Instance()->GetMatchingGUID(bd.guid);

				if (list.size() == 3)
				{
					int validCount = 0;

					DlgAddEditWithChildrenBank* dlg = new DlgAddEditWithChildrenBank(0);

					for (std::size_t i = 0; i < list.size(); ++i)
					{
						int wordLength = sys::WordCount(list[i].str.ToStdWstring());

						if (wordLength == 4)
						{
							dlg->SetString(1, list[i].str);
							validCount++;
						}
						else
						if (wordLength == 5)
						{
							dlg->SetString(2, list[i].str);
							validCount++;
						}
						else
						if (wordLength == 6)
						{
							dlg->SetString(3, list[i].str);
							validCount++;
						}
					}

					if (validCount != 3)
					{
						dlg->Destroy();
						delete dlg;
					}
					else
					{
						dlg->SetMode(MODE_EDIT);
						dlg->Apply();
						if (dlg->ShowModal() == RESPONSE_OK)
						{
							CreateGridFromBank();

							sys::AppData::Instance()->SaveBank();
						}

						dlg->Destroy();
						delete dlg;
					}
				}
			}
		}
	}
}

/////////////////////////////////////////////////////
/// BtnDeleteClick
/// Desc: Button event to delete a selected word/sentence
/////////////////////////////////////////////////////
void PanelBank::BtnDeleteClick(wxCommandEvent& WXUNUSED(event))
{
	// get selected
	wxArrayInt arr = m_WordBankGrid->GetSelectedRows();

	int numSelected = arr.Count();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId >= 0 && 
			rowId < m_WordBankGrid->GetNumberRows())
		{
			BankData itemData;
			itemData.str = m_WordBankGrid->GetCellValue(rowId, 0);

			if (wxMessageBox(_("Are you sure you want to delete this from the bank?"), _("DELETE WORD/SENTENCE"), wxYES_NO) == wxYES)
			{
				if (sys::AppData::Instance()->RemoveFromBank(itemData.str))
				{
					CreateGridFromBank();

					sys::AppData::Instance()->SaveBank();
				}
			}
		}
	}
}

/////////////////////////////////////////////////////
/// BtnExportClick
/// Desc: Button event to export the current bank to an excel document
/////////////////////////////////////////////////////
void PanelBank::BtnExportClick(wxCommandEvent& WXUNUSED(event))
{
	wxFileDialog saveFileDialog(this, _("Save xls file"), "", "", "XLS files (*.xls)|*.xls", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveFileDialog.ShowModal() == wxID_CANCEL)
		return;

	std::string path = saveFileDialog.GetPath().ToStdString();

	std::map<wxString, BankData> bank = sys::AppData::Instance()->GetBank();

	ExcelFormat::BasicExcel xls;

	// create sheet 1 and get the associated BasicExcelWorksheet pointer
	xls.New(1);
	ExcelFormat::BasicExcelWorksheet* sheet = xls.GetWorksheet(0);
	sheet->Rename(_("BANK").ToStdWstring().c_str());

	ExcelFormat::XLSFormatManager fmt_mgr(xls);

	// Create a table
	ExcelFormat::ExcelFont font_bold;
	ExcelFormat::ExcelFont font_normal;
	font_bold._weight = FW_BOLD; // 700
	font_normal._weight = FW_NORMAL;

	ExcelFormat::CellFormat fmtBGGray(fmt_mgr);
	fmtBGGray.set_background(MAKE_COLOR2(23, 0));	// solid grey background
	fmtBGGray.set_font(font_bold);

	ExcelFormat::CellFormat fmt_bold(fmt_mgr);
	fmt_bold.set_font(font_bold);

	auto mainColourList = sys::AppData::Instance()->GetExcelColours();
	std::vector<ExcelFormat::CellFormat> excelFormatList;
	for (auto colVal : mainColourList)
	{
		ExcelFormat::CellFormat storedFilter(fmt_mgr, font_normal.set_color_index(colVal.idx));
		excelFormatList.push_back(storedFilter);
	}

	int row = 0;

	ExcelFormat::BasicExcelCell* cell = sheet->Cell(row, 0);
	cell->Set(_("Word/Sentence").ToStdWstring().c_str());
	cell->SetFormat(fmtBGGray);

	cell = sheet->Cell(row, 1);
	cell->Set(_("Filter").ToStdWstring().c_str());
	cell->SetFormat(fmtBGGray);

	row+=2;
	for ( auto item : bank ) 
	{
		

		auto fd = sys::AppData::Instance()->FindFilter(item.second.filterIndex);

		if (fd.val != -1)
		{
			ExcelFormat::CellFormat fmtFilter = excelFormatList[fd.colIndex];

			sheet->Cell(row, 0)->SetFormat(fmtFilter);
			sheet->Cell(row, 0)->Set(item.first.ToStdWstring().c_str());

			sheet->Cell(row, 1)->SetFormat(fmtFilter);
			sheet->Cell(row, 1)->Set(fd.name.ToStdWstring().c_str());

			row++;
		}
	}

	int s = m_WordBankGrid->GetColSize(0);

	sheet->SetColWidth(0, s * 50);
	s = m_WordBankGrid->GetColSize(1);
	sheet->SetColWidth(1, s * 50);

	xls.SaveAs(path.c_str());
}

/////////////////////////////////////////////////////
/// BtnBackClick
/// Desc: Button event to return to the admin panel
/////////////////////////////////////////////////////
void PanelBank::BtnBackClick(wxCommandEvent& WXUNUSED(event))
{
	sys::AppData::Instance()->ChangePanel(SCREEN_BANKSELECTION);
}

/////////////////////////////////////////////////////
/// CopyPasteData
/// Desc: Key event listening for copy/paste key strikes
/////////////////////////////////////////////////////
void PanelBank::CopyPasteData(wxKeyEvent& event)
{
	if ((event.GetUnicodeKey() == 'C') && (event.ControlDown() == true)) {

		//copy

	}
	else if ((event.GetUnicodeKey() == 'V') && (event.ControlDown() == true)) {

		// paste
		wxCommandEvent empty;
		Paste(empty);
	}
}

/////////////////////////////////////////////////////
/// Paste
/// Desc: Event callback for pasting from the clipboard
/////////////////////////////////////////////////////
void PanelBank::Paste(wxCommandEvent &WXUNUSED(event))
{
	if (wxTheClipboard->Open())
	{
		if (wxTheClipboard->IsSupported(wxDF_UNICODETEXT))
		{
			wxTextDataObject data;
			wxTheClipboard->GetData(data);

			wxString str = data.GetText();

			wxString curField;
			wxString curLine;

			bool validText = true;

			// go through the info in the clipboard and separate by newline
			auto currentBank = sys::AppData::Instance()->GetBank();
			do
			{
				curLine = str.BeforeFirst('\n');
				str = str.AfterFirst('\n');

				do
				{
					curField = curLine.BeforeFirst('\t');
					curLine = curLine.AfterFirst('\t');

					if (curField.Length() > 0)
					{
						// check if it exists already in the bank

						auto search = currentBank.find(curField);
						if (search != currentBank.end())
						{
							validText = false;
						}
					}

				} while (curLine.IsEmpty() == false);

			} while (str.IsEmpty() == false);

			// all valid we can show the import dialog
			if (validText)
			{
				DlgBankImport* dlg = new DlgBankImport(0, data.GetText());
				if (dlg->ShowModal() == RESPONSE_OK)
				{
					CreateGridFromBank();

					sys::AppData::Instance()->SaveBank();
				}

				dlg->Destroy();
				delete dlg;
			}
		}
		wxTheClipboard->Close();
	}
}

/////////////////////////////////////////////////////
/// RightClick
/// Desc: Right click anywhere on the word grid event
/////////////////////////////////////////////////////
void PanelBank::RightClick(wxMouseEvent& WXUNUSED(event))
{
	wxPoint point = wxGetMousePosition();//event.GetPosition();

	// If from keyboard
	if (point.x == -1 && point.y == -1) 
	{
		wxSize size = GetSize();
		point.x = size.x / 2;
		point.y = size.y / 2;
	}
	else 
	{
		//point = ScreenToClient(point);
		//point = ClientToScreen(point);
	}

	point.x = point.x - this->GetScreenPosition().x;
	point.y = point.y - this->GetScreenPosition().y;

	ShowContextMenu(point);
}

#define ID_CLIPBOARD_PASTE		100

/////////////////////////////////////////////////////
/// OnPopupClick
/// Desc: popup menu selection event
/////////////////////////////////////////////////////
void PanelBank::OnPopupClick(wxCommandEvent &event)
{
	//void *data = static_cast<wxMenu *>(event.GetEventObject())->GetClientData();
	switch (event.GetId()) 
	{
		case ID_CLIPBOARD_PASTE:
		{
			wxCommandEvent empty;
			Paste(empty);
		}break;
		default:
			break;
	}
}

/////////////////////////////////////////////////////
/// ShowContextMenu
/// Desc: Shows the paste context menu popup
/////////////////////////////////////////////////////
void PanelBank::ShowContextMenu(const wxPoint& pos)
{
	wxMenu menu;

	menu.Append(ID_CLIPBOARD_PASTE, _("Paste From Clipboard"));
	menu.Connect(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(PanelBank::OnPopupClick), NULL, this);

	wxPoint p = pos;
	//p.y = p.y + 100;
	PopupMenu(&menu, p);
}
