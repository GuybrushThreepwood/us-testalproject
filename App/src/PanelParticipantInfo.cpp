/**
	TECS
	PanelParticipantInfo.cpp
    Purpose: Panel shown for reviewing previously run programs by participants
*/

#include "PanelParticipantInfo.h"
#include "DlgProgramView.h"
#include "System.h"

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
PanelParticipantInfo::PanelParticipantInfo(wxWindow* parent)
:
ParticipantInfo( parent )
{
	// only allow row selection
	m_ParticipantGrid->SetSelectionMode(wxGrid::wxGridSelectRows);

	// labels
	m_ParticipantGrid->SetColLabelValue(0, _("Participant #"));
	m_ParticipantGrid->SetColLabelValue(1, _("Program Name"));
	m_ParticipantGrid->SetColLabelValue(2, _("Version"));
	m_ParticipantGrid->SetColLabelValue(3, _("Date / Time"));

	// sort by filter
	m_ParticipantGrid->SetSortingColumn(1);

	// disable cell highlight
	m_ParticipantGrid->SetCellHighlightPenWidth(0);

	m_PhaseFilter = VERSION_ALL;
	m_StringFilter = "";

	CreateGridFromPrograms();

	DoRefreshPhases();

	m_ParticipantGrid->AutoSize();
}

/////////////////////////////////////////////////////
/// CreateGridFromPrograms
/// Desc: Populates the wxGrid with the current programs in the file system
/////////////////////////////////////////////////////
void PanelParticipantInfo::CreateGridFromPrograms()
{
	m_FileMap.clear();

	// get all the files
	wxArrayString fileList;
	std::size_t totalFiles = wxDir::GetAllFiles(sys::AppData::Instance()->GetParticipantsDirectory(), &fileList);
	if (totalFiles > 0)
	{
		for (auto fileIter : fileList)
		{
			ParticipantFileInfo fileInfo;
			fileInfo.filename = fileIter;
			
			std::ifstream t(fileIter.ToStdString());
			std::stringstream buffer;
			buffer << t.rdbuf();

			std::string bufferStr = buffer.str();
			fileInfo.jsonData.loadFromString(bufferStr);
			//fileInfo.jsonData.loadFromFile(fileIter.ToStdString());

			if (!fileInfo.jsonData.isNull())
			{
				JsonBox::Value participantId = fileInfo.jsonData["ID"];
				JsonBox::Value gender = fileInfo.jsonData["Gender"];
				JsonBox::Value dobDate = fileInfo.jsonData["DOB"];
				JsonBox::Value date = fileInfo.jsonData["Date"];
				JsonBox::Value versionId = fileInfo.jsonData["VersionId"];
				JsonBox::Value programData = fileInfo.jsonData["ProgramInfo"];

				if (participantId.isString() &&
					date.isString() &&
					versionId.isInteger() &&
					programData.isObject() )
				{
					fileInfo.participantId = participantId.getString();
					fileInfo.versionId = versionId.getInteger();

					wxString str = date.getString();
					long long val = -1;
					if (str.ToLongLong(&val))
					{
						fileInfo.dateTime = wxDateTime((wxLongLong)val);
					}

					if (programData.isObject())
					{
						JsonBox::Object programObj = programData.getObject();
						JsonBox::Value name = programObj["Name"];
						if (name.isString())
						{
							fileInfo.programName = name.getString();
						}
					}
					// do the insert
					m_FileMap.insert(std::make_pair(fileIter, fileInfo));
				}
			}
		}
	}

	// How many Cols/Rows
	int rows = m_ParticipantGrid->GetNumberRows();

	// Delete all Cols/Rows
	if (rows > 0)
		m_ParticipantGrid->DeleteRows(0, rows, true);

	// stored ready to filter
	m_FilteredFileMap.clear();

	m_FilteredFileMap = m_FileMap;

	DoPhaseFilter(m_FilteredFileMap, 0);

	m_ParticipantGrid->InsertRows(0, m_FilteredFileMap.size());

	// populate
	int rowId = 0;
	for (const auto iter : m_FilteredFileMap)
	{
		m_ParticipantGrid->SetCellValue(rowId, 0, wxString::Format("%s", iter.second.participantId ));
		m_ParticipantGrid->SetCellValue(rowId, 1, iter.second.programName);

		const PhaseData& pd = sys::AppData::Instance()->FindPhase((int)iter.second.versionId);
		if (pd.val == -1)
			m_ParticipantGrid->SetCellValue(rowId, 2, wxString::Format("%d", (int)iter.second.versionId));
		else
			m_ParticipantGrid->SetCellValue(rowId, 2, pd.name);

		wxString dateStr = iter.second.dateTime.FormatISODate();
		wxString timeStr = iter.second.dateTime.FormatISOTime();
		m_ParticipantGrid->SetCellValue(rowId, 3, wxString::Format("%s @ %s", dateStr, timeStr));

		rowId++;
	}

	m_ParticipantGrid->AutoSize();
	this->Layout();
}


/////////////////////////////////////////////////////
/// DoRefreshPhases
/// Desc: Updates the filter combobox with the stored filters
/////////////////////////////////////////////////////
void PanelParticipantInfo::DoRefreshPhases()
{
	// populate the data
	m_ComboFilter->Clear();

	// populate from the current map
	m_PhaseMap = sys::AppData::Instance()->GetPhases();
	for (auto val : m_PhaseMap)
	{
		int index = m_ComboFilter->GetCount();
		m_ComboFilter->Insert(val.second.name, index);
	}
	// add ALL as first element
	m_ComboFilter->Insert(_("Show All"), 0);
	m_ComboFilter->SetSelection(0);
}

/////////////////////////////////////////////////////
/// DoPhaseFilter
/// Desc: Recursive filter of the participant list based on the selected phase filter
/////////////////////////////////////////////////////
void PanelParticipantInfo::DoPhaseFilter(std::map<wxString, ParticipantFileInfo>& phases, int mode)
{
	if (mode == 0) // filter by type
	{
		if (m_PhaseFilter != FILTER_ALL)
		{
			for (auto iter = phases.begin(); iter != phases.end();)
			{
				if (iter->second.versionId != m_PhaseFilter)
				{
					iter = phases.erase(iter);
					continue;
				}
				
				// next
				iter++;
			}
		}

		DoPhaseFilter(phases, 1);
	}
	if (mode == 1) // filter by search
	{
		if (m_StringFilter.Length() > 0)
		{
			for (auto iter = phases.begin(); iter != phases.end();)
			{
				if (!iter->second.programName.Contains(m_StringFilter))
				{
					iter = phases.erase(iter);
					continue;
				}
				
				// next
				iter++;
			}
		}
	}

}

/////////////////////////////////////////////////////
/// PhaseFilterChange
/// Desc: Combobox event when the phase filter is changed to update the participant list
/////////////////////////////////////////////////////
void PanelParticipantInfo::PhaseFilterChange(wxCommandEvent& WXUNUSED(event))
{
	int phaseId = 0;

	int selectionIndex = m_ComboFilter->GetSelection();
	wxString str = m_ComboFilter->GetStringSelection();

	if (selectionIndex == 0 ||
		str.length() == 0)
		phaseId = 0;
	else
	{
		for (auto iter : m_PhaseMap)
		{
			if (iter.second.name.IsSameAs(str))
			{
				phaseId = iter.second.val;
				break;
			}
		}
	}

	if (phaseId != m_PhaseFilter)
	{
		m_PhaseFilter = phaseId;
		CreateGridFromPrograms();
	}
}

/////////////////////////////////////////////////////
/// IdSearchCancel
/// Desc: Cancel event to clear the search field
/////////////////////////////////////////////////////
void PanelParticipantInfo::IdSearchCancel(wxCommandEvent& WXUNUSED(event))
{
	if (m_Search->GetValue().Length() > 0)
	{
		m_Search->Clear();
		m_StringFilter.Clear();

		CreateGridFromPrograms();
	}
}

/////////////////////////////////////////////////////
/// IdSearch
/// Desc: Search event to find a participant by the ID value
/////////////////////////////////////////////////////
void PanelParticipantInfo::IdSearch(wxCommandEvent& WXUNUSED(event))
{
	wxString str = m_Search->GetValue();

	if (m_StringFilter != str)
	{
		m_StringFilter = str;
		CreateGridFromPrograms();
	}
}

/////////////////////////////////////////////////////
/// GridCellLeftClick
/// Desc: Event for left clicking to select a row in the participant grid
/////////////////////////////////////////////////////
void PanelParticipantInfo::GridCellLeftClick(wxGridEvent& event)
{
	m_ParticipantGrid->ClearSelection();
	m_ParticipantGrid->SelectRow(event.GetRow());
}

/////////////////////////////////////////////////////
/// GridCellLeftDoubleClick
/// Desc: Event for double clicking a row in the grid (launches the view dialog)
/////////////////////////////////////////////////////
void PanelParticipantInfo::GridCellLeftDoubleClick(wxGridEvent& WXUNUSED(event))
{
	wxCommandEvent empty;
	BtnViewClick(empty);
}

/////////////////////////////////////////////////////
/// BtnViewClick
/// Desc: Button event to view the participant details for a previously run program
/////////////////////////////////////////////////////
void PanelParticipantInfo::BtnViewClick(wxCommandEvent& WXUNUSED(event))
{
	// get selected
	wxArrayInt arr = m_ParticipantGrid->GetSelectedRows();

	int numSelected = arr.Count();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId >= 0 && rowId <
			m_ParticipantGrid->GetNumberRows())
		{
			wxString idVal = m_ParticipantGrid->GetCellValue(rowId, 0);
			//long val = -1;
			//if (idVal.ToLong(&val))
			//{
			//
			//}
			for (const auto iter : m_FilteredFileMap)
			{
				if (idVal == iter.second.participantId)
				{
					DlgProgramView* dlg = new DlgProgramView(0);

					dlg->SetFile(iter.second);
					dlg->Apply();
					if (dlg->ShowModal() == RESPONSE_OK)
					{
						CreateGridFromPrograms();
					}

					dlg->Destroy();
					delete dlg;
					break;
				}
			}
		}
	}
}

/////////////////////////////////////////////////////
/// BtnDeleteClick
/// Desc: Button event to delete a selected participant program
/////////////////////////////////////////////////////
void PanelParticipantInfo::BtnDeleteClick(wxCommandEvent& WXUNUSED(event))
{
	// get selected
	wxArrayInt arr = m_ParticipantGrid->GetSelectedRows();

	int numSelected = arr.Count();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId >= 0 &&
			rowId < m_ParticipantGrid->GetNumberRows())
		{
			wxString idVal = m_ParticipantGrid->GetCellValue(rowId, 0);
			//long val = -1;
			//if (idVal.ToLong(&val))
			//{
			//
			//}

			if (wxMessageBox(_("Are you sure you want to delete this test?"), _("DELETE INFO"), wxYES_NO) == wxYES)
			{
				for (const auto iter : m_FilteredFileMap)
				{
					if (idVal == iter.second.participantId)
					{
						// delete this
						if (wxRemoveFile(iter.second.filename))
						{
							CreateGridFromPrograms();

							DoRefreshPhases();

							m_ParticipantGrid->AutoSize();
							this->Layout();

							break;
						}
					}
				}
			}
		}
	}
}

/////////////////////////////////////////////////////
/// BtnBackClick
/// Desc: Button event for switching back to the admin selection panel
/////////////////////////////////////////////////////
void PanelParticipantInfo::BtnBackClick(wxCommandEvent& WXUNUSED(event))
{
	sys::AppData::Instance()->ChangePanel(SCREEN_ADMIN);
}
