/**
	TECS
	DlgAddEditWithChildrenBank.cpp
    Purpose: Handles all the callbacks for adding or editing words/sentences in the bank for version 3 (which requires related sentences)
*/

#include "DlgAddEditWithChildrenBank.h"
#include "System.h"

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
DlgAddEditWithChildrenBank::DlgAddEditWithChildrenBank( wxWindow* parent )
:
AddEditWithChildrenBankDlg( parent )
{
	m_ComboFilter->Clear();
	m_FilterMap.clear();

	const auto& filterMap = sys::AppData::Instance()->GetFilters();
	for (auto val : filterMap)
	{
		int index = m_ComboFilter->GetCount();

		m_ComboFilter->Insert(val.second.name, index);

		// make this map match the selection box
		m_FilterMap.insert(std::make_pair(val.second.val, index));
	}

	m_ComboFilter->SetSelection(0);

	m_Mode = MODE_ADD;
	m_BtnAddEdit->Disable();
}

/////////////////////////////////////////////////////
/// Apply
/// Desc: Called manually after construction to populate or modify UI elements for either ADD-ing or EDIT-ing
/////////////////////////////////////////////////////
void DlgAddEditWithChildrenBank::Apply()
{
	if (m_Mode == MODE_ADD)
	{
		this->SetTitle(_("Add Collection To Bank"));
		m_BtnAddEdit->SetLabelText(_("Add"));

		m_ComboFilter->SetSelection(0);

		m_CurrentGUID = sys::AppData::Instance()->GetGUIDAsString();
	}
	else if (m_Mode == MODE_EDIT)
	{
		// SENTENCE 1
		{
			const BankData& bd = sys::AppData::Instance()->GetFromBank(m_String1);

			m_CurrentGUID = bd.guid;

			m_InputWordOrSentence1->SetValue(m_String1);

			std::wstring str = m_String1.ToStdWstring();

			// match the bank index to the correct
			auto properIndex = m_FilterMap.find(bd.filterIndex);
			if (properIndex != m_FilterMap.end())
				m_ComboFilter->SetSelection(properIndex->second);
			else
				wxASSERT(0); // filter doesn't exist

			std::size_t wordCount = sys::WordCount(str);

			if (m_String1.size() == 0)
				m_TxtMessage1->SetLabel("");
			else
				m_TxtMessage1->SetLabel(wxString::Format(_("Word Count %d"), (int)wordCount));
		}
		// SENTENCE 2
		{
			const BankData& bd = sys::AppData::Instance()->GetFromBank(m_String2);

			m_InputWordOrSentence2->SetValue(m_String2);

			std::wstring str = m_String2.ToStdWstring();

			// match the bank index to the correct
			auto properIndex = m_FilterMap.find(bd.filterIndex);
			if (properIndex != m_FilterMap.end())
				m_ComboFilter->SetSelection(properIndex->second);
			else
				wxASSERT(0); // filter doesn't exist

			std::size_t wordCount = sys::WordCount(str);

			if (m_String2.size() == 0)
				m_TxtMessage2->SetLabel("");
			else
				m_TxtMessage2->SetLabel(wxString::Format(_("Word Count %d"), (int)wordCount));
		}
		// SENTENCE 3
		{
			const BankData& bd = sys::AppData::Instance()->GetFromBank(m_String3);

			m_InputWordOrSentence3->SetValue(m_String3);

			std::wstring str = m_String3.ToStdWstring();

			// match the bank index to the correct
			auto properIndex = m_FilterMap.find(bd.filterIndex);
			if (properIndex != m_FilterMap.end())
				m_ComboFilter->SetSelection(properIndex->second);
			else
				wxASSERT(0); // filter doesn't exist

			std::size_t wordCount = sys::WordCount(str);

			if (m_String3.size() == 0)
				m_TxtMessage3->SetLabel("");
			else
				m_TxtMessage3->SetLabel(wxString::Format(_("Word Count %d"), (int)wordCount));
		}

		this->SetTitle(_("Edit Existing"));
		m_BtnAddEdit->SetLabelText(_("Update"));
	}

	bool disableButton1 = true;
	bool disableButton2 = true;
	bool disableButton3 = true;

	disableButton1 = CheckValues(1, 4, m_InputWordOrSentence1, m_TxtMessage1, bMessageSizer1);
	disableButton2 = CheckValues(2, 5, m_InputWordOrSentence2, m_TxtMessage2, bMessageSizer2);
	disableButton3 = CheckValues(3, 6, m_InputWordOrSentence3, m_TxtMessage3, bMessageSizer3);

	if (disableButton1 || disableButton2 || disableButton3)
		m_BtnAddEdit->Disable();
	else
		m_BtnAddEdit->Enable();
}

/////////////////////////////////////////////////////
/// CheckValues
/// Desc: Checks the bank for pre-existing word/sentence and updates the UI
/// Return: boolean (true = not valid | false = valid)
/////////////////////////////////////////////////////
bool DlgAddEditWithChildrenBank::CheckValues( int num, std::size_t requiredWordCount, wxTextCtrl* txtCtrl, wxStaticText* txtMessage, wxBoxSizer* sizerContainer )
{
	bool disableButton = true;

	wxString str = txtCtrl->GetValue();

	auto currentBank = sys::AppData::Instance()->GetBank();
	auto search = currentBank.find(str);

	if (m_Mode == MODE_ADD)
	{
		if (search == currentBank.end())
		{
			// doesn't exist
			std::wstring strCounter = str.ToStdWstring();
			std::size_t wordCount = sys::WordCount(strCounter);

			if (str.size() == 0)
			{
				txtMessage->SetLabel("");

				//m_BtnAddEdit->Disable();
				disableButton = true;
			}
			else 
			if (requiredWordCount != wordCount)
			{
				txtMessage->SetLabel(wxString::Format(_("Required %d words, currently has %d words"), (int)requiredWordCount, (int)wordCount));

				//m_BtnAddEdit->Disable();
				disableButton = true;
			}
			else
			{
				txtMessage->SetLabel(wxString::Format(_("Word Count %d"), (int)wordCount));

				//m_BtnAddEdit->Enable();
				disableButton = false;
			}
		}
		else
		{
			// already exists
			txtMessage->SetLabel(_("Word or Sentence already exists in the bank"));

			//m_BtnAddEdit->Disable();
			disableButton = true;
		}
	}
	else if (m_Mode == MODE_EDIT)
	{
		if (search == currentBank.end())
		{
			// doesn't exist
			std::wstring strCounter = str.ToStdWstring();
			std::size_t wordCount = sys::WordCount(strCounter);

			if (str.size() == 0)
			{
				txtMessage->SetLabel("");

				//m_BtnAddEdit->Disable();
				disableButton = true;
			}
			else
			if (requiredWordCount != wordCount)
			{
				txtMessage->SetLabel(wxString::Format(_("Required %d words, currently has %d words"), (int)requiredWordCount, (int)wordCount));

				//m_BtnAddEdit->Disable();
				disableButton = true;
			}
			else
			{
				txtMessage->SetLabel(wxString::Format(_("Word Count %d"), (int)wordCount));

				//m_BtnAddEdit->Enable();
				disableButton = false;
			}
		}
		else
		{
			if (num == 1)
			{
				// already exists
				if (m_OriginalString1 != str)
				{
					txtMessage->SetLabel(_("Word or Sentence already exists in the bank"));

					//m_BtnAddEdit->Disable();
					disableButton = true;
				}
				else
				{
					std::wstring strCounter = str.ToStdWstring();
					std::size_t wordCount = sys::WordCount(strCounter);

					if (requiredWordCount != wordCount)
					{
						txtMessage->SetLabel(wxString::Format(_("Required %d words, currently has %d words"), (int)requiredWordCount, (int)wordCount));

						//m_BtnAddEdit->Disable();
						disableButton = true;
					}
					else
					{
						txtMessage->SetLabel(wxString::Format(_("Word Count %d"), (int)wordCount));

						//m_BtnAddEdit->Enable();
						disableButton = false;
					}
				}
			}
			else
			if (num == 2)
			{
				// already exists
				if (m_OriginalString2 != str)
				{
					txtMessage->SetLabel(_("Word or Sentence already exists in the bank"));

					//m_BtnAddEdit->Disable();
					disableButton = true;
				}
				else
				{
					std::wstring strCounter = str.ToStdWstring();
					std::size_t wordCount = sys::WordCount(strCounter);

					if (requiredWordCount != wordCount)
					{
						txtMessage->SetLabel(wxString::Format(_("Required %d words, currently has %d words"), (int)requiredWordCount, (int)wordCount));

						//m_BtnAddEdit->Disable();
						disableButton = true;
					}
					else
					{
						txtMessage->SetLabel(wxString::Format(_("Word Count %d"), (int)wordCount));

						//m_BtnAddEdit->Enable();
						disableButton = false;
					}
				}
			}
			else
			if (num == 3)
			{
				// already exists
				if (m_OriginalString3 != str)
				{
					txtMessage->SetLabel(_("Word or Sentence already exists in the bank"));

					//m_BtnAddEdit->Disable();
					disableButton = true;
				}
				else
				{
					std::wstring strCounter = str.ToStdWstring();
					std::size_t wordCount = sys::WordCount(strCounter);

					if (requiredWordCount != wordCount)
					{
						txtMessage->SetLabel(wxString::Format(_("Required %d words, currently has %d words"), (int)requiredWordCount, (int)wordCount));

						//m_BtnAddEdit->Disable();
						disableButton = true;
					}
					else
					{
						txtMessage->SetLabel(wxString::Format(_("Word Count %d"), (int)wordCount));

						//m_BtnAddEdit->Enable();
						disableButton = false;
					}
				}
			}
		}
	}
	sizerContainer->Layout();

	return disableButton;
}

/////////////////////////////////////////////////////
/// KeyUpEvent
/// Desc: Key event for the input box (will check the bank for existing)
/////////////////////////////////////////////////////
void DlgAddEditWithChildrenBank::KeyUpEvent( wxKeyEvent& WXUNUSED(event))
{
	bool disableButton1 = true;
	bool disableButton2 = true;
	bool disableButton3 = true;

	disableButton1 = CheckValues(1, 4, m_InputWordOrSentence1, m_TxtMessage1, bMessageSizer1);
	disableButton2 = CheckValues(2, 5, m_InputWordOrSentence2, m_TxtMessage2, bMessageSizer2);
	disableButton3 = CheckValues(3, 6, m_InputWordOrSentence3, m_TxtMessage3, bMessageSizer3);

	if(disableButton1 || disableButton2 || disableButton3)
		m_BtnAddEdit->Disable();
	else
		m_BtnAddEdit->Enable();
}

/////////////////////////////////////////////////////
/// BtnCancelClick
/// Desc: Button callback for cancelling the dialog (nothing saved/changed)
/////////////////////////////////////////////////////
void DlgAddEditWithChildrenBank::BtnCancelClick( wxCommandEvent& WXUNUSED(event))
{
	this->EndModal(RESPONSE_CANCEL);
}

/////////////////////////////////////////////////////
/// BtnAddEditClick
/// Desc: Button callback for adding to or updating the bank
/////////////////////////////////////////////////////
void DlgAddEditWithChildrenBank::BtnAddEditClick( wxCommandEvent& WXUNUSED(event))
{
	if (m_Mode == MODE_ADD)
	{
		// sentence 1 
		{
			wxString str = m_InputWordOrSentence1->GetValue();
			wxString selectionStr = m_ComboFilter->GetStringSelection();

			int filterId = 0;

			if (selectionStr.length() == 0)
				filterId = 0;
			else
			{
				const FilterData& fd = sys::AppData::Instance()->FindFilter(selectionStr);

				if (fd.val != -1)
					filterId = fd.val;
			}

			BankData bd;
			bd.str = str;
			bd.filterIndex = filterId;
			bd.wordCount = sys::WordCount(str.ToStdWstring());
			bd.hasGUID = true;
			bd.guid = m_CurrentGUID;

			sys::AppData::Instance()->AddToBank(bd);
		}

		// sentence 2 
		{
			wxString str = m_InputWordOrSentence2->GetValue();
			wxString selectionStr = m_ComboFilter->GetStringSelection();

			int filterId = 0;

			if (selectionStr.length() == 0)
				filterId = 0;
			else
			{
				const FilterData& fd = sys::AppData::Instance()->FindFilter(selectionStr);

				if (fd.val != -1)
					filterId = fd.val;
			}

			BankData bd;
			bd.str = str;
			bd.filterIndex = filterId;
			bd.wordCount = sys::WordCount(str.ToStdWstring());
			bd.hasGUID = true;
			bd.guid = m_CurrentGUID;

			sys::AppData::Instance()->AddToBank(bd);
		}

		// sentence 3 
		{
			wxString str = m_InputWordOrSentence3->GetValue();
			wxString selectionStr = m_ComboFilter->GetStringSelection();

			int filterId = 0;

			if (selectionStr.length() == 0)
				filterId = 0;
			else
			{
				const FilterData& fd = sys::AppData::Instance()->FindFilter(selectionStr);

				if (fd.val != -1)
					filterId = fd.val;
			}

			BankData bd;
			bd.str = str;
			bd.filterIndex = filterId;
			bd.wordCount = sys::WordCount(str.ToStdWstring());
			bd.hasGUID = true;
			bd.guid = m_CurrentGUID;

			sys::AppData::Instance()->AddToBank(bd);
		}
	}
	else if (m_Mode == MODE_EDIT)
	{
		// SENTENCE 1
		{
			wxString str = m_InputWordOrSentence1->GetValue();
			wxString selectionStr = m_ComboFilter->GetStringSelection();

			int filterId = 0;

			if (selectionStr.length() == 0)
				filterId = 0;
			else
			{
				const FilterData& fd = sys::AppData::Instance()->FindFilter(selectionStr);

				if (fd.val != -1)
					filterId = fd.val;
			}

			BankData bd;
			bd.str = str;
			bd.filterIndex = filterId;
			bd.wordCount = sys::WordCount(str.ToStdWstring());
			bd.hasGUID = true;
			bd.guid = m_CurrentGUID;

			sys::AppData::Instance()->UpdateBank(m_OriginalString1, bd);

			m_String1 = str;
		}

		// SENTENCE 2
		{
			wxString str = m_InputWordOrSentence2->GetValue();
			wxString selectionStr = m_ComboFilter->GetStringSelection();

			int filterId = 0;

			if (selectionStr.length() == 0)
				filterId = 0;
			else
			{
				const FilterData& fd = sys::AppData::Instance()->FindFilter(selectionStr);

				if (fd.val != -1)
					filterId = fd.val;
			}

			BankData bd;
			bd.str = str;
			bd.filterIndex = filterId;
			bd.wordCount = sys::WordCount(str.ToStdWstring());
			bd.hasGUID = true;
			bd.guid = m_CurrentGUID;

			sys::AppData::Instance()->UpdateBank(m_OriginalString2, bd);

			m_String2 = str;
		}

		// SENTENCE 3
		{
			wxString str = m_InputWordOrSentence3->GetValue();
			wxString selectionStr = m_ComboFilter->GetStringSelection();

			int filterId = 0;

			if (selectionStr.length() == 0)
				filterId = 0;
			else
			{
				const FilterData& fd = sys::AppData::Instance()->FindFilter(selectionStr);

				if (fd.val != -1)
					filterId = fd.val;
			}

			BankData bd;
			bd.str = str;
			bd.filterIndex = filterId;
			bd.wordCount = sys::WordCount(str.ToStdWstring());
			bd.hasGUID = true;
			bd.guid = m_CurrentGUID;

			sys::AppData::Instance()->UpdateBank(m_OriginalString3, bd);

			m_String3 = str;
		}
	}

	this->EndModal(RESPONSE_OK);
}
