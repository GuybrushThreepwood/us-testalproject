/**
	TECS
	PanelPhaseRun.cpp
    Purpose: Runs a program fullscreen based on the program selected during participant entry
*/

#include "PanelPhaseRun.h"
#include "PanelQuestions.h"
#include "System.h"

namespace
{
	const int ORIGINAL_WIDTH = 784;
	const int ORIGINAL_HEIGHT = 541;
}

wxBEGIN_EVENT_TABLE(PanelPhaseRun, wxPanel)
	EVT_TIMER(TIMER_ID, PanelPhaseRun::OnTimer)
wxEND_EVENT_TABLE()

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
PanelPhaseRun::PanelPhaseRun( wxWindow* parent )
:
PhaseRun( parent ),
m_Timer(this, TIMER_ID),
m_NumLinePrints(1),
m_KeyCheckValue(-1)
{
	m_Initialised = false;

	m_Count = 0;

	m_CurrentWidth = 1;//this->GetSize().GetWidth();
	m_CurrentHeight = 1;//this->GetSize().GetHeight();

	m_ScaleW = 1.0f;
	m_ScaleH = 1.0f;

	m_CurrentFontSize = sys::AppData::Instance()->GetTextFontSize();
	m_CurrentSmallFontSize = m_CurrentFontSize -3;

	this->SetFocus();

	// store the program
	m_RunData = sys::AppData::Instance()->GetParticipantData();
	m_Version = m_RunData.selectedVersionId;
	m_RunData.response.clear();

	// prepare the first block, first word
	m_RunBlock = 0;
	m_RunIndex = 0;
	ProgramBlock pb = m_RunData.program.at(m_RunBlock);
	BankData bd = pb.words.at(m_RunIndex);
	m_WordOrSentence = bd.str;
	m_LastFilterId = bd.filterIndex;

	if (m_Version == VERSION_ONE ||
		m_Version == VERSION_TWO )
	{
		m_GeneratedLineCount = sys::RandInt(m_RunData.phaseData.linesCountMin, m_RunData.phaseData.linesCountMax);
	}
	else
	if (m_Version == VERSION_THREE)
	{
		m_GeneratedLineCount = 1;
	}
	else
	if (m_Version == VERSION_FOUR)
	{
		m_GeneratedLineCount = 1;
	}

	m_DidFirstRespond = false;
	m_DidSecondRespond = false;

	m_BorderSpacing = sys::AppData::Instance()->GetTextBorderWidth();

	m_BorderAttr.GetTextBoxAttr().GetLeftPadding().SetValue(m_BorderSpacing, wxTEXT_ATTR_UNITS_PIXELS);
	m_BorderAttr.GetTextBoxAttr().GetRightPadding().SetValue(m_BorderSpacing, wxTEXT_ATTR_UNITS_PIXELS);

	m_BorderAttr.GetTextBoxAttr().GetTopPadding().SetValue(sys::AppData::Instance()->GetTextBorderHeight(), wxTEXT_ATTR_UNITS_PIXELS);
	m_BorderAttr.GetTextBoxAttr().GetBottomPadding().SetValue(sys::AppData::Instance()->GetTextBorderHeight(), wxTEXT_ATTR_UNITS_PIXELS);

	m_BorderAttr.GetTextBoxAttr().GetBorder().SetColour(sys::AppData::Instance()->GetTextBorderColour());
	m_BorderAttr.GetTextBoxAttr().GetBorder().SetWidth(sys::AppData::Instance()->GetTextBorderThickness(), wxTEXT_ATTR_UNITS_PIXELS);
	m_BorderAttr.GetTextBoxAttr().GetBorder().SetStyle(wxTEXT_BOX_ATTR_BORDER_SOLID);

	m_LastResponse.didRespond = false;

	m_LastResponse.hasFirstRespose = false;
	m_LastResponse.firstResponseTime = -1;
	m_LastResponse.firstResponseValue = -1;

	m_LastResponse.hasSecondResponse = false;
	m_LastResponse.secondResponseTime = -1;
	m_LastResponse.secondResponseValue = -1;

	m_State = RUN_STATE_INTERVAL;
	m_Timer.Start(m_RunData.phaseData.timeInterval, true);
	m_StopWatch.Pause();

	this->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
	this->SetCursor(wxCURSOR_BLANK);

	m_Initialised = true;
	m_FirstIdle = true;
}

/////////////////////////////////////////////////////
/// DoText
/// Desc: Based on the current state, sets up the rich text view
/////////////////////////////////////////////////////
void PanelPhaseRun::DoText()
{
	m_TxtTutorial->Clear();

	m_TxtTutorial->Disable();

	m_TxtTutorial->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
	m_TxtTutorial->BeginAlignment(wxTEXT_ALIGNMENT_CENTRE);
	m_TxtTutorial->BeginBold();
	m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextColour());
	m_TxtTutorial->BeginFontSize(m_CurrentFontSize);

	if (m_State == RUN_STATE_SHOW)
	{
		switch (m_Version)
		{
		case VERSION_ONE:
		case VERSION_TWO:
		{
			DoTextVersion1OR2();
		}break;

		case VERSION_THREE:
		case VERSION_FOUR:
		{
			DoTextVersion3OR4();
		}break;


		default:
			break;
		}
	}
	else
	if (m_State == RUN_STATE_REST)
	{
		m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextInfoColour());

		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();

		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetWaitText());

		m_TxtTutorial->LineBreak();
	}
	else
	if (m_State == RUN_STATE_END)
	{
		m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextInfoColour());

		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();

		m_TxtTutorial->WriteText(_("Test Complete"));
		
		m_TxtTutorial->LineBreak();
	}

	m_TxtTutorial->EndBold();
	m_TxtTutorial->SetScrollbar(wxVERTICAL, 0, 0, 0);
}

/////////////////////////////////////////////////////
/// DoTextVersion1OR2
/// Desc: Formats the rich text for version 1 or 2
/////////////////////////////////////////////////////
void PanelPhaseRun::DoTextVersion1OR2()
{
	if (m_GeneratedLineCount == 4)
	{
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();

		wxRichTextBox* textBox = m_TxtTutorial->WriteTextBox(m_BorderAttr);
		m_TxtTutorial->SetFocusObject(textBox);

		m_TxtTutorial->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
		m_TxtTutorial->BeginAlignment(wxTEXT_ALIGNMENT_CENTRE);
		m_TxtTutorial->BeginBold();
		m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextColour());
		m_TxtTutorial->BeginFontSize(m_CurrentFontSize);

		m_TxtTutorial->WriteText(m_WordOrSentence);
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(m_WordOrSentence);
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(m_WordOrSentence);
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(m_WordOrSentence);
		m_TxtTutorial->LineBreak();

		m_TxtTutorial->SetFocusObject(0);

		m_NumLinePrints = 4;
	}
	else
	if (m_GeneratedLineCount == 5)
	{
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();

		wxRichTextBox* textBox = m_TxtTutorial->WriteTextBox(m_BorderAttr);
		m_TxtTutorial->SetFocusObject(textBox);

		m_TxtTutorial->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
		m_TxtTutorial->BeginAlignment(wxTEXT_ALIGNMENT_CENTRE);
		m_TxtTutorial->BeginBold();
		m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextColour());
		m_TxtTutorial->BeginFontSize(m_CurrentFontSize);

		m_TxtTutorial->WriteText(m_WordOrSentence);
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(m_WordOrSentence);
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(m_WordOrSentence);
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(m_WordOrSentence);
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(m_WordOrSentence);
		m_TxtTutorial->LineBreak();

		m_TxtTutorial->SetFocusObject(0);

		m_NumLinePrints = 5;
	}
	else
	if (m_GeneratedLineCount == 6)
	{
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();

		wxRichTextBox* textBox = m_TxtTutorial->WriteTextBox(m_BorderAttr);
		m_TxtTutorial->SetFocusObject(textBox);

		m_TxtTutorial->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
		m_TxtTutorial->BeginAlignment(wxTEXT_ALIGNMENT_CENTRE);
		m_TxtTutorial->BeginBold();
		m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextColour());
		m_TxtTutorial->BeginFontSize(m_CurrentFontSize);

		m_TxtTutorial->WriteText(m_WordOrSentence);
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(m_WordOrSentence);
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(m_WordOrSentence);
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(m_WordOrSentence);
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(m_WordOrSentence);
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(m_WordOrSentence);
		m_TxtTutorial->LineBreak();

		m_TxtTutorial->SetFocusObject(0);

		m_NumLinePrints = 6;
	}
	
	m_LastTextLine = m_WordOrSentence.ToStdString();
	m_KeyCheckValue = m_NumLinePrints;

}

/////////////////////////////////////////////////////
/// DoTextVersion3OR4
/// Desc: Formats the rich text for version 3 or 4
/////////////////////////////////////////////////////
void PanelPhaseRun::DoTextVersion3OR4()
{
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();

	wxRichTextBox* textBox = m_TxtTutorial->WriteTextBox(m_BorderAttr);
	m_TxtTutorial->SetFocusObject(textBox);

	m_TxtTutorial->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
	m_TxtTutorial->BeginAlignment(wxTEXT_ALIGNMENT_CENTRE);
	m_TxtTutorial->BeginBold();
	m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextColour());
	m_TxtTutorial->BeginFontSize(m_CurrentFontSize);

	m_TxtTutorial->WriteText(m_WordOrSentence);

	m_TxtTutorial->SetFocusObject(0);
	
	m_TxtTutorial->LineBreak();

	m_NumLinePrints = 1;
	
	m_LastTextLine = m_WordOrSentence.ToStdString();
	m_KeyCheckValue = sys::WordCount(m_WordOrSentence.ToStdWstring());
}


/////////////////////////////////////////////////////
/// DoIdleRefresh
/// Desc: Method called during the app idle frames (used for set up and resizing)
/////////////////////////////////////////////////////
void PanelPhaseRun::DoIdleRefresh()
{
	if (m_Initialised &&
		m_FirstIdle)
	{
		m_TxtTutorial->Show();
		bRootSizer->Layout();

		m_FirstIdle = false;
	}
}

/////////////////////////////////////////////////////
/// KeyUpEvent
/// Desc: Key event during the test to store user responses
/////////////////////////////////////////////////////
void PanelPhaseRun::KeyUpEvent(wxKeyEvent& event)
{
	wxChar quitKey = event.GetUnicodeKey();
	if (quitKey == WXK_ESCAPE)
	{
		m_RunData.program.clear();
		m_RunData.response.clear();
		sys::AppData::Instance()->SetParticipantData(m_RunData);

		sys::AppData::Instance()->GetMainFrame()->ShowFullScreen(false);
		sys::AppData::Instance()->ChangePanel(SCREEN_PARTICIPANT);
		return;
	}

	wxChar uc = event.GetUnicodeKey();
	int c = event.GetKeyCode();

	if (uc != WXK_NONE)
	{
		// It's a "normal" character. Notice that this includes
		// control characters in 1..31 range, e.g. WXK_RETURN or
		// WXK_BACK, so check for them explicitly.
		if (uc >= 32)
		{
			// 48-57 0-to-9
			if (uc >= 48 && uc <= 57)
			{
                int realVal = std::abs(48 - uc);

				if (m_State == RUN_STATE_SHOW)
				{
					int time = (int)m_StopWatch.Time();

					if (time < m_RunData.phaseData.timeToRespond)
					{
						if (!m_DidFirstRespond)
						{
							m_LastResponse.didRespond = true;

							m_LastResponse.firstResponseTime = time;
							m_LastResponse.firstResponseValue = realVal;
							m_LastResponse.hasFirstRespose = true;
							
							m_DidFirstRespond = true;
						}
						else
						if (!m_DidSecondRespond)
						{
							m_LastResponse.secondResponseTime = time;
							m_LastResponse.secondResponseValue = realVal;
							m_LastResponse.hasSecondResponse = true;

							m_DidSecondRespond = true;
						}
					}
				}

				/*if (realVal == m_KeyCheckValue)
				{
					if (m_State == RUN_STATE_SHOW &&
						!m_DidRespond )
					{
						int time = (int)m_StopWatch.Time();

						if (time < m_RunData.phaseData.timeToRespond)
						{
							m_DidRespond = true;
							m_StopWatch.Pause();
							m_LastResponse.didRespond = true;
							m_LastResponse.responseTime = time;
						}
					}
				}
				else
				{
					if (m_State == RUN_STATE_SHOW &&
						!m_DidRespond &&
						!m_DidBadAnswer)
					{
						int time = (int)m_StopWatch.Time();

						//if (time < m_RunData.phaseData.timeToRespond)
						{
							m_DidBadAnswer = true;
							m_LastResponse.badAnswer = true;
							m_LastResponse.badResponseTime = time;
							m_LastResponse.badResponseVal = realVal;
						}
					}
				}*/
			}
			else
			{
				if (c >= 48 && c <= 57)
				{
                    int realVal = std::abs(48 - c);

					if (m_State == RUN_STATE_SHOW)
					{
						int time = (int)m_StopWatch.Time();

						if (time < m_RunData.phaseData.timeToRespond)
						{
							if (!m_DidFirstRespond)
							{
								m_LastResponse.didRespond = true;

								m_LastResponse.firstResponseTime = time;
								m_LastResponse.firstResponseValue = realVal;
								m_LastResponse.hasFirstRespose = true;

								m_DidFirstRespond = true;
							}
							else
							if (!m_DidSecondRespond)
							{
								m_LastResponse.secondResponseTime = time;
								m_LastResponse.secondResponseValue = realVal;
								m_LastResponse.hasSecondResponse = true;

								m_DidSecondRespond = true;
							}
						}
					}

					/*if (realVal == m_KeyCheckValue)
					{
						if (m_State == RUN_STATE_SHOW &&
							!m_DidRespond)
						{
							int time = (int)m_StopWatch.Time();

							if (time < m_RunData.phaseData.timeToRespond)
							{
								m_DidRespond = true;
								m_StopWatch.Pause();
								m_LastResponse.didRespond = true;
								m_LastResponse.responseTime = time;
							}
						}
					}
					else
					{
						if (m_State == RUN_STATE_SHOW &&
							!m_DidRespond &&
							!m_DidBadAnswer)
						{
							int time = (int)m_StopWatch.Time();

							//if (time < m_RunData.phaseData.timeToRespond)
							{
								m_DidBadAnswer = true;
								m_LastResponse.badAnswer = true;
								m_LastResponse.badResponseTime = time;
								m_LastResponse.badResponseVal = realVal;
							}
						}
					}*/
				}
			}
		}
		else
		{
			// It's a control character (or wide ascii)
		}
	}
	else // No Unicode equivalent.
	{
		int realVal = -1;

		// It's a special key, deal with all the known ones:
		switch (event.GetKeyCode())
		{
			case WXK_NUMPAD0:
			case WXK_NUMPAD1:
			case WXK_NUMPAD2:
			case WXK_NUMPAD3:
			case WXK_NUMPAD4:
			case WXK_NUMPAD5:
			case WXK_NUMPAD6:
			case WXK_NUMPAD7:
			case WXK_NUMPAD8:
			case WXK_NUMPAD9:
			{
				realVal = event.GetKeyCode() - WXK_NUMPAD0;

				if (m_State == RUN_STATE_SHOW)
				{
					int time = (int)m_StopWatch.Time();

					if (time < m_RunData.phaseData.timeToRespond)
					{
						if (!m_DidFirstRespond)
						{
							m_LastResponse.didRespond = true;

							m_LastResponse.firstResponseTime = time;
							m_LastResponse.firstResponseValue = realVal;
							m_LastResponse.hasFirstRespose = true;

							m_DidFirstRespond = true;
						}
						else
						if (!m_DidSecondRespond)
						{
							m_LastResponse.secondResponseTime = time;
							m_LastResponse.secondResponseValue = realVal;
							m_LastResponse.hasSecondResponse = true;

							m_DidSecondRespond = true;
						}
					}
				}

				/*if (realVal == m_KeyCheckValue)
				{
					if (m_State == RUN_STATE_SHOW &&
						!m_DidRespond )
					{
						int time = (int)m_StopWatch.Time();

						if (time < m_RunData.phaseData.timeToRespond)
						{
							m_DidRespond = true;
							m_StopWatch.Pause();
							m_LastResponse.didRespond = true;
							m_LastResponse.responseTime = time;
						}
					}
				}
				else
				{
					if (m_State == RUN_STATE_SHOW &&
						!m_DidRespond &&
						!m_DidBadAnswer)
					{
						int time = (int)m_StopWatch.Time();

						//if (time < m_RunData.phaseData.timeToRespond)
						{
							m_DidBadAnswer = true;
							m_LastResponse.badAnswer = true;
							m_LastResponse.badResponseTime = time;
							m_LastResponse.badResponseVal = realVal;
						}
					}
				}*/

			}break;
		}
	}
}

/////////////////////////////////////////////////////
/// OnTextAreaResize
/// Desc: Resize event to keep the text correctly scaled
/////////////////////////////////////////////////////
void PanelPhaseRun::OnTextAreaResize(wxSizeEvent& event)
{
	if (m_Initialised)
	{
		ResizePanel(event);

		DoText();
	}
}

/////////////////////////////////////////////////////
/// ResizePanel
/// Desc: Resize event to keep the text correctly scaled
/////////////////////////////////////////////////////
void PanelPhaseRun::ResizePanel(wxSizeEvent& event)
{
	int w = this->GetSize().GetWidth();
	int h = this->GetSize().GetHeight();

	if (m_CurrentWidth != w)
	{
		m_ScaleW = (float)w / (float)ORIGINAL_WIDTH;

		m_CurrentWidth = w;
	}

	if (m_CurrentHeight != h)
	{
		m_ScaleH = (float)h / (float)ORIGINAL_HEIGHT;

		m_CurrentHeight = h;
	}

	float m_FinalScale = (m_ScaleW + m_ScaleH) * 0.5f;

	event.Skip();

	m_CurrentFontSize = (int)((float)sys::AppData::Instance()->GetTextFontSize() * m_FinalScale);
	m_CurrentSmallFontSize = (int)((float)(sys::AppData::Instance()->GetTextFontSize()-3) * m_FinalScale);

	m_BorderSpacing = (int)((float)sys::AppData::Instance()->GetTextBorderWidth() * m_ScaleW);

	m_BorderAttr.GetTextBoxAttr().GetLeftPadding().SetValue(m_BorderSpacing, wxTEXT_ATTR_UNITS_PIXELS);
	m_BorderAttr.GetTextBoxAttr().GetRightPadding().SetValue(m_BorderSpacing, wxTEXT_ATTR_UNITS_PIXELS);

	DoText();
}

/////////////////////////////////////////////////////
/// BtnContinueClick
/// Desc: Button event for handling the end of the test
/////////////////////////////////////////////////////
void PanelPhaseRun::BtnContinueClick(wxCommandEvent& WXUNUSED(event))
{
	sys::AppData::Instance()->GetMainFrame()->ShowFullScreen(false);
	sys::AppData::Instance()->ChangePanel(SCREEN_QUESTIONS);
}

/////////////////////////////////////////////////////
/// OnTimer
/// Desc: Timer event to determine what state to change to during the program
/////////////////////////////////////////////////////
void PanelPhaseRun::OnTimer(wxTimerEvent& WXUNUSED(event))
{
	switch (m_State)
	{
		case RUN_STATE_SHOW:
		{
			// add response data
			ProgramBlock pb = m_RunData.program.at(m_RunBlock);
			BankData bd = pb.words.at(m_RunIndex);

			m_LastResponse.data = bd;
			m_LastResponse.didRespond = m_DidFirstRespond;

			if (m_Version == VERSION_ONE ||
				m_Version == VERSION_TWO)
			{
				m_LastResponse.lineOrWordCount = m_GeneratedLineCount;
			}
			else
				m_LastResponse.lineOrWordCount = sys::WordCount(m_WordOrSentence.ToStdWstring());

			// add to the response list
			m_RunData.response.push_back(m_LastResponse);

			// check to move to the word in the block
			if (m_RunIndex + 1 < (int)pb.words.size())
			{
				m_RunIndex++;
				bd = pb.words.at(m_RunIndex);
				m_WordOrSentence = bd.str;

				// generate another random number of lines
				if (m_Version == VERSION_ONE ||
					m_Version == VERSION_TWO)
				{
					m_GeneratedLineCount = sys::RandInt(m_RunData.phaseData.linesCountMin, m_RunData.phaseData.linesCountMax);
				}

				if (pb.breakType == BREAKTYPE_PERFILTER &&
					bd.filterIndex != m_LastFilterId) // filter change, need to rest
				{
					// move to the rest state
					m_TxtTutorial->Clear();

					m_State = RUN_STATE_REST;
					m_Timer.Start(m_RunData.phaseData.timeBetweenFilterChanges, true);
					m_StopWatch.Pause();

					m_LastFilterId = bd.filterIndex;

					DoText();
				}
				else
				{
					// move to the interval
					m_TxtTutorial->Clear();

					m_State = RUN_STATE_INTERVAL;
					m_Timer.Start(m_RunData.phaseData.timeInterval, true);
					m_StopWatch.Pause();
				}
			}
			else
			{
				// end of the block list
				if (m_RunBlock + 1 < (int)m_RunData.program.size())
				{
					// store this last block break
					BreakType blockBreakType = pb.breakType;

					// now go to new block
					m_RunBlock++;
					m_RunIndex = 0;
					pb = m_RunData.program.at(m_RunBlock);
					bd = pb.words.at(m_RunIndex);
					m_WordOrSentence = bd.str;

					// generate another random number of lines
					if (m_Version == VERSION_ONE ||
						m_Version == VERSION_TWO)
					{
						m_GeneratedLineCount = sys::RandInt(m_RunData.phaseData.linesCountMin, m_RunData.phaseData.linesCountMax);
					}

					// block should rest
					if (blockBreakType != BREAKTYPE_NOBREAK )
					{
						// move to the rest state
						m_TxtTutorial->Clear();

						m_State = RUN_STATE_REST;
						m_Timer.Start(m_RunData.phaseData.timeBetweenFilterChanges, true);
						m_StopWatch.Pause();

						m_LastFilterId = bd.filterIndex;

						DoText();
					}
					else
					{
						// move to the interval
						m_TxtTutorial->Clear();

						m_State = RUN_STATE_INTERVAL;
						m_Timer.Start(m_RunData.phaseData.timeInterval, true);
						m_StopWatch.Pause();
					}

					/*if (bd.filterIndex != m_LastFilterId) // filter change, need to rest
					{
						// move to the rest state
						m_TxtTutorial->Clear();

						m_State = RUN_STATE_REST;
						m_Timer.Start(m_RunData.phaseData.timeBetweenFilterChanges, true);
						m_StopWatch.Pause();

						m_LastFilterId = bd.filterIndex;

						DoText();
					}
					else
					{
						// move to the interval
						m_TxtTutorial->Clear();

						m_State = RUN_STATE_INTERVAL;
						m_Timer.Start(m_RunData.phaseData.timeInterval, true);
						m_StopWatch.Pause();
					}*/
				}
				else
				{
					// all blocks complete, go to the end
					m_TxtTutorial->Clear();

					m_State = RUN_STATE_END;

					m_BtnContinue->Show();
					bRootSizer->Layout();
					this->SetCursor(wxCURSOR_ARROW);

					DoText();

					// store response
					sys::AppData::Instance()->SetParticipantData(m_RunData);
				}
			}

			m_DidFirstRespond = false;
			m_DidSecondRespond = false;
			m_LastResponse.didRespond = false;
			m_LastResponse.hasFirstRespose = false;
			m_LastResponse.firstResponseTime = -1;
			m_LastResponse.firstResponseValue = -1;

			m_DidSecondRespond = false;
			m_LastResponse.hasSecondResponse = false;
			m_LastResponse.secondResponseTime = -1;
			m_LastResponse.secondResponseValue = -1;
	
		}break;
		case RUN_STATE_INTERVAL:
		{
			m_State = RUN_STATE_SHOW;
			m_Timer.Start(m_RunData.phaseData.timeResponseTimeout, true);
			m_StopWatch.Start(0);

			DoText();
		}break;
		case RUN_STATE_REST:
		{
			// move to the interval
			m_TxtTutorial->Clear();

			m_State = RUN_STATE_INTERVAL;
			m_Timer.Start(m_RunData.phaseData.timeInterval, true);
			m_StopWatch.Pause();
		}break;
		case RUN_STATE_END:
		{
			
		}break;
		default:
			break;
	}
}
