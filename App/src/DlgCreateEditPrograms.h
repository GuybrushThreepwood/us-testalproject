#ifndef __DlgCreateEditPrograms__
#define __DlgCreateEditPrograms__

/**
@file
Subclass of CreateEditProgramsDlg, which is generated by wxFormBuilder.
*/

#include "Common.h"
#include "TECSApp.h"

//// end generated include

/** Implementing CreateEditProgramsDlg */
class DlgCreateEditPrograms : public CreateEditProgramsDlg
{
	public:
		struct StoredBlockData
		{
			wxBoxSizer* filterboxSizer;
			wxCheckBox* activeCheckbox;
			wxStaticText* filterName;
			wxSpinCtrl* spinValueControl;

			wxBoxSizer* filterInfoboxSizer;
			wxStaticText* textInUse;
			wxStaticText* textRemaining;

			FilterData fd;
		};

		struct SelectionData
		{
			int filterId;
			std::vector<BankData> list;
		};

		/** Constructor */
		DlgCreateEditPrograms( wxWindow* parent );

		void SetMode(ModeType modeType = MODE_ADD)	{ m_Mode = modeType; }
		void SetEditFile(const wxString& filename) { m_EditFilename = filename; }
		void CheckValues();

	private:

		void CreateGridFromBank();
		void DoRefreshFilters();

		void CreateFilterData(wxBoxSizer* useSizer, wxScrolledWindow* scroller, std::vector<StoredInputData>& list, bool readOnly);
		void CreateOrderData();

		void SetCurrentSelection();
		void CheckSelectionCounts();
		void UpdateTimeValues();

		bool CanModifyCell(int row);
		void LoadFromFile();

	protected:
		virtual void EventInitDialog(wxInitDialogEvent& event);

		virtual void SettingsKeyUp(wxKeyEvent& event);
		void DynamicSettingsKeyUp(wxKeyEvent& event);
		
		void BeginDrag(wxListEvent& event);
		void OnDragEnd(wxMouseEvent& event);
		void OnDragQuit(wxMouseEvent& event);

		virtual void PageChanged(wxNotebookEvent& event);
		virtual void PageChanging(wxNotebookEvent& event);

		virtual void ComboBankChange(wxCommandEvent& event);
		virtual void BankFilterChange(wxCommandEvent& event);
		virtual void ComboPhaseChange(wxCommandEvent& event);

		virtual void ChangeDefaultCheckBox(wxCommandEvent& event);
		virtual void GridCellLeftClick(wxGridEvent& event);
		virtual void GridCellLeftDoubleClick(wxGridEvent& event);
		virtual void GridRangeSelect(wxGridRangeSelectEvent& event);

		virtual void BtnAddClick(wxCommandEvent& event);
		virtual void BtnAddWithChildrenClick(wxCommandEvent& event);
		virtual void BtnEditClick(wxCommandEvent& event);
		virtual void BtnDeleteClick(wxCommandEvent& event);

		virtual void BtnCancelClick(wxCommandEvent& event);
		virtual void BtnCreateEditClick(wxCommandEvent& event);

		virtual void OrderGridCellLeftClick(wxGridEvent& event);
		virtual void BtnRemoveOrderClick(wxCommandEvent& event);
		virtual void BtnMoveUpOrderClick(wxCommandEvent& event);
		virtual void BtnMoveDownOrderClick(wxCommandEvent& event);
		//virtual void OrderChoiceBox(wxCommandEvent& event);
		
		virtual void AddBlockClick(wxCommandEvent& event);
		virtual void BlockBreakChoice(wxCommandEvent& event);
		void BlockCheckBox(wxCommandEvent& event);
		void BlockCountChange(wxSpinEvent& event);
		void BlockCountChangeTxt(wxCommandEvent& event);

		void CopyPasteData(wxKeyEvent& event);
		void Paste(wxCommandEvent &event);
		void RightClick(wxMouseEvent& event);
		void OnPopupClick(wxCommandEvent &event);
		void ShowContextMenu(const wxPoint& pos);
	//// end generated class members

	private:
		ModeType m_Mode;

		wxDateTime m_CreationDate;
		wxString m_EditFilename;

		int m_BankFilter;
		std::map<int, int> m_OrderFilterMap;

		int m_PreviousBankSelection;
		std::map<wxString, BankFileInfo> m_BankListMap;
		std::map<int, BankFileInfo> m_BankFileComboMap;

		std::map<int, FilterData> m_FilterMap;

		int m_PreviousSelection;
		std::map<int, int> m_PhaseMap;

		wxString m_ProgramName;
		wxString m_Comments;

		int m_CurrentVersionSelected;

		bool m_UseTimeDefaults;
		int m_TimeToRespond;
		int m_ResponseTimeout;
		int m_Interval;
		int m_TimeBetweenChanges;

		int m_WordCountMin;
		int m_WordCountMax;
		int m_LinesCountMin;
		int m_LinesCountMax;

		// dynamic lists
		std::vector<StoredInputData> m_SettingsCountList;
		std::vector<StoredInputData> m_SelectionCountList;

		//
		std::map< int, SelectionData > m_BankSelectionMap;

		// ordering ifo
		int m_ListDragIndex;
		std::vector<OrderBlock> m_OrderData;

		// order dynamic data
		std::vector<StoredBlockData> m_OrderingDynamic;
};

#endif // __DlgCreateEditPrograms__
