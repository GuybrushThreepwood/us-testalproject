/**
	TECS
	PanelTutorial.cpp
    Purpose: Fullscreen tutorial run with example of how a selected program works
*/

#include "PanelTutorial.h"
#include "System.h"

namespace
{
	const int ORIGINAL_WIDTH = 784;
	const int ORIGINAL_HEIGHT = 541;
}

wxBEGIN_EVENT_TABLE(PanelTutorial, wxPanel)
	EVT_TIMER(TUT_EXAMPLE_ID, PanelTutorial::OnExampleTimer)
	EVT_TIMER(TUT_BREAK_ID, PanelTutorial::OnBreakTimer)
	EVT_TIMER(TUT_PREPARESHOW_ID, PanelTutorial::OnPrepareShowTimer)
wxEND_EVENT_TABLE()

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
PanelTutorial::PanelTutorial( wxWindow* parent )
:
Tutorial( parent ),
m_ExampleTimer(this, TUT_EXAMPLE_ID),
m_BreakTimer(this, TUT_BREAK_ID),
m_PrepareShowTimer(this, TUT_PREPARESHOW_ID),
m_TutorialComplete(false),
m_NumLinePrints(1),
m_ShowingWaitText(false),
m_ShowExamplePrepare(false),
m_KeyCheckValue(-1)
{
	
	m_CurrentWidth = this->GetSize().GetWidth();
	m_CurrentHeight = this->GetSize().GetHeight();

	m_ScaleW = 1.0f;
	m_ScaleH = 1.0f;

	m_CurrentFontSize = sys::AppData::Instance()->GetTextFontSize();
	m_CurrentSmallFontSize = sys::AppData::Instance()->GetTextFontSize()-3;

	m_RunData = sys::AppData::Instance()->GetParticipantData();
	m_Version = m_RunData.selectedVersionId;
	m_CurrentState = TUTORIAL_STATE_INITIAL;

	m_BorderSpacing = sys::AppData::Instance()->GetTextBorderWidth();

	m_BorderAttr.GetTextBoxAttr().GetLeftPadding().SetValue(m_BorderSpacing, wxTEXT_ATTR_UNITS_PIXELS);
	m_BorderAttr.GetTextBoxAttr().GetRightPadding().SetValue(m_BorderSpacing, wxTEXT_ATTR_UNITS_PIXELS);

	m_BorderAttr.GetTextBoxAttr().GetTopPadding().SetValue(sys::AppData::Instance()->GetTextBorderHeight(), wxTEXT_ATTR_UNITS_PIXELS);
	m_BorderAttr.GetTextBoxAttr().GetBottomPadding().SetValue(sys::AppData::Instance()->GetTextBorderHeight(), wxTEXT_ATTR_UNITS_PIXELS);

	m_BorderAttr.GetTextBoxAttr().GetBorder().SetColour(sys::AppData::Instance()->GetTextBorderColour());
	m_BorderAttr.GetTextBoxAttr().GetBorder().SetWidth(sys::AppData::Instance()->GetTextBorderThickness(), wxTEXT_ATTR_UNITS_PIXELS);
	m_BorderAttr.GetTextBoxAttr().GetBorder().SetStyle(wxTEXT_BOX_ATTR_BORDER_SOLID);

	SetupState();

	sys::AppData::Instance()->GetMainFrame()->ShowFullScreen(true);

	this->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
	this->SetFocus();
}

/////////////////////////////////////////////////////
/// OnExampleTimer
/// Desc: Timer event called when tutorial text has finished showing for the required time
/////////////////////////////////////////////////////
void PanelTutorial::OnExampleTimer(wxTimerEvent& WXUNUSED(event))
{
	if (m_CurrentState >= TUTORIAL_EXAMPLE_1 ||
		m_CurrentState <= TUTORIAL_EXAMPLE_3)
	{
		m_ShowingWaitText = true;

		DoBreakText();

		m_ExampleTimer.Stop();

		m_BreakTimer.Start(m_RunData.phaseData.timeBetweenFilterChanges, true);
	}
}

/////////////////////////////////////////////////////
/// OnBreakTimer
/// Desc: Timer event for the end of the rest time between showing tutorial text
/////////////////////////////////////////////////////
void PanelTutorial::OnBreakTimer(wxTimerEvent& WXUNUSED(event))
{
	m_ShowingWaitText = false;

	if (m_ExampleNumber == 1)
	{
		m_ExampleNumber = 2;
		m_ShowExamplePrepare = true;
		DoPrepare();

		m_PrepareShowTimer.Start(m_RunData.phaseData.timeBetweenFilterChanges, true);
	}
	else
	if (m_ExampleNumber == 2)
	{
		m_ExampleNumber = 3;
		m_ShowExamplePrepare = true;
		DoPrepare();

		m_PrepareShowTimer.Start(m_RunData.phaseData.timeBetweenFilterChanges, true);
	}
	else
	if (m_ExampleNumber == 3)
	{
		m_ShowExamplePrepare = false;
		wxTimerEvent empty;
		OnPrepareShowTimer(empty);
	}
}

/////////////////////////////////////////////////////
/// OnPrepareShowTimer
/// Desc: Event timer which ends after showing information text about which example is about to appear
/////////////////////////////////////////////////////
void PanelTutorial::OnPrepareShowTimer(wxTimerEvent& WXUNUSED(event))
{
	m_ShowExamplePrepare = false;

	if (m_CurrentState == TUTORIAL_STATE_INITIAL)
	{
		m_CurrentState = TUTORIAL_EXAMPLE_1;
		SetupState();

		m_BreakTimer.Stop();
		m_ExampleTimer.Start(m_RunData.phaseData.timeResponseTimeout, true);
	}
	else
	if (m_CurrentState == TUTORIAL_EXAMPLE_1)
	{
		m_CurrentState = TUTORIAL_EXAMPLE_2;
		SetupState();

		m_BreakTimer.Stop();
		m_ExampleTimer.Start(m_RunData.phaseData.timeResponseTimeout, true);
	}
	else
	if (m_CurrentState == TUTORIAL_EXAMPLE_2)
	{
		m_CurrentState = TUTORIAL_EXAMPLE_3;
		SetupState();

		m_BreakTimer.Stop();
		m_ExampleTimer.Start(m_RunData.phaseData.timeResponseTimeout, true);
	}
	else
	if (m_CurrentState == TUTORIAL_EXAMPLE_3)
	{
		m_CurrentState = TUTORIAL_END;
		SetupState();

		m_ExampleTimer.Stop();
		m_BreakTimer.Stop();
		m_PrepareShowTimer.Stop();

		m_TutorialComplete = true;

		m_BtnContinue->Show();
		this->SetCursor(wxCURSOR_ARROW);
		this->Layout();
	}
}

/////////////////////////////////////////////////////
/// KeyUpEvent
/// Desc: Main key event which determines what the user pressed during the tutorial text
/////////////////////////////////////////////////////
void PanelTutorial::KeyUpEvent(wxKeyEvent& event)
{
	wxChar quitKey = event.GetUnicodeKey();
	if (quitKey == WXK_ESCAPE)
	{
		m_RunData.program.clear();
		m_RunData.response.clear();
		sys::AppData::Instance()->SetParticipantData(m_RunData);

		sys::AppData::Instance()->GetMainFrame()->ShowFullScreen(false);
		sys::AppData::Instance()->ChangePanel(SCREEN_PARTICIPANT);
		return;
	}

	if (!m_ShowingWaitText &&
		!m_ShowExamplePrepare &&
		(m_CurrentState >= TUTORIAL_EXAMPLE_1 && m_CurrentState <= TUTORIAL_EXAMPLE_3))
	{
		wxChar uc = event.GetUnicodeKey();
		int c = event.GetKeyCode();

		if (uc != WXK_NONE)
		{
			// It's a "normal" character. Notice that this includes
			// control characters in 1..31 range, e.g. WXK_RETURN or
			// WXK_BACK, so check for them explicitly.
			if (uc >= 32)
			{
				// 48-57 0-to-9
				if (uc >= 48 && uc <= 57)
				{
                    int realVal = std::abs(48 - uc);
					if (realVal == m_KeyCheckValue)
					{
						m_ExampleTimer.Stop();

						wxTimerEvent empty;
						OnExampleTimer(empty);
					}
				}
				else
				{
					if (c >= 48 && c <= 57)
					{
                        int realVal = std::abs(48 - c);
						if (realVal == m_KeyCheckValue)
						{
							m_ExampleTimer.Stop();

							wxTimerEvent empty;
							OnExampleTimer(empty);
						}
					}
				}
			}
			else
			{
				// It's a control character
			}
		}
		else // No Unicode equivalent.
		{
			int realVal = -1;
			// It's a special key, deal with all the known ones:
			switch (event.GetKeyCode())
			{
				case WXK_NUMPAD0:
				case WXK_NUMPAD1:
				case WXK_NUMPAD2:
				case WXK_NUMPAD3:
				case WXK_NUMPAD4:
				case WXK_NUMPAD5:
				case WXK_NUMPAD6:
				case WXK_NUMPAD7:
				case WXK_NUMPAD8:
				case WXK_NUMPAD9:
				{
					realVal = event.GetKeyCode() - WXK_NUMPAD0;
					if (realVal == m_KeyCheckValue)
					{
						m_ExampleTimer.Stop();

						wxTimerEvent empty;
						OnExampleTimer(empty);
					}
				}break;
			}
		}
	}
}

/////////////////////////////////////////////////////
/// ResizePanel
/// Desc: Resize event which makes sure all elements are correctly sized and scaled based in the original resolution
/////////////////////////////////////////////////////
void PanelTutorial::ResizePanel(wxSizeEvent& event)
{
	int w = this->GetSize().GetWidth();
	int h = this->GetSize().GetHeight();

	if (m_CurrentWidth != w)
	{
		m_ScaleW = (float)w / (float)ORIGINAL_WIDTH;

		m_CurrentWidth = w;
	}

	if (m_CurrentHeight != h)
	{
		m_ScaleH = (float)h / (float)ORIGINAL_HEIGHT;

		m_CurrentHeight = h;
	}

	float m_FinalScale = (m_ScaleW + m_ScaleH) * 0.5f;

	event.Skip();

	m_CurrentFontSize = (int)((float)sys::AppData::Instance()->GetTextFontSize() * m_FinalScale);
	m_CurrentSmallFontSize = (int)((float)(sys::AppData::Instance()->GetTextFontSize()-3) * m_FinalScale);

	m_BorderSpacing = (int)((float)sys::AppData::Instance()->GetTextBorderWidth() * m_ScaleW);

	m_BorderAttr.GetTextBoxAttr().GetLeftPadding().SetValue(m_BorderSpacing, wxTEXT_ATTR_UNITS_PIXELS);
	m_BorderAttr.GetTextBoxAttr().GetRightPadding().SetValue(m_BorderSpacing, wxTEXT_ATTR_UNITS_PIXELS);

	if (m_ShowingWaitText)
		DoBreakText();
	else if (m_ShowExamplePrepare)
		DoPrepare();
	else
		SetupState();

}

/////////////////////////////////////////////////////
/// BtnContinueClick
/// Desc: Button event to start a tutorial or start the full program
/////////////////////////////////////////////////////
void PanelTutorial::BtnContinueClick(wxCommandEvent& WXUNUSED(event))
{
	if (m_TutorialComplete)
	{
		StoredScaleData data;

		data.currentFontSize = m_CurrentFontSize;
		data.currentHeight = m_CurrentHeight;
		data.currentSmallFontSize = m_CurrentSmallFontSize;
		data.currentWidth = m_CurrentWidth;
		data.scaleH = m_ScaleH;
		data.scaleW = m_ScaleW;

		this->SetCursor(wxCURSOR_BLANK);

		sys::AppData::Instance()->SetStoredData(data);
		sys::AppData::Instance()->ChangePanel(SCREEN_PHASERUN);
	}
	else
	{
		// prepare the tutorial state
		m_BtnContinue->Hide();
		this->SetCursor(wxCURSOR_BLANK);

		if (m_CurrentState == TUTORIAL_STATE_INITIAL)
		{
			m_ExampleNumber = 1;
			m_ShowExamplePrepare = true;
			DoPrepare();

			m_PrepareShowTimer.Start(m_RunData.phaseData.timeBetweenFilterChanges, true);
		}
	}

}

/////////////////////////////////////////////////////
/// SetupState
/// Desc: Prepares the text based on the current tutorial state
/////////////////////////////////////////////////////
void PanelTutorial::SetupState()
{
	m_TxtTutorial->Clear();

	m_TxtTutorial->Disable();

	m_TxtTutorial->SetFocusObject(0);
	m_TxtTutorial->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
	m_TxtTutorial->BeginAlignment(wxTEXT_ALIGNMENT_CENTRE);
	m_TxtTutorial->BeginBold();
	m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextInfoColour());
	m_TxtTutorial->BeginFontSize(m_CurrentFontSize);

	switch (m_CurrentState)
	{
		case TUTORIAL_STATE_INITIAL:
		{
			if (m_Version == VERSION_ONE)
			{
				DoTutorialLayout1();
			}
			else
			if (m_Version == VERSION_TWO)
			{
				DoTutorialLayout2();
			}
			else
			if (m_Version == VERSION_THREE)
			{
				DoTutorialLayout3();
			}
			else
			if (m_Version == VERSION_FOUR)
			{
				DoTutorialLayout4();
			}
		}break;
		case TUTORIAL_EXAMPLE_1:
		{
			if (m_Version == VERSION_ONE)
			{
				DoLayoutVersion1(1);
			}
			else
			if (m_Version == VERSION_TWO)
			{
				DoLayoutVersion2(1);
			}
			else
			if (m_Version == VERSION_THREE)
			{
				DoLayoutVersion3(1);
			}
			else
			if (m_Version == VERSION_FOUR)
			{
				DoLayoutVersion4(1);
			}
		}break;
		case TUTORIAL_EXAMPLE_2:
		{
			if (m_Version == VERSION_ONE)
			{
				DoLayoutVersion1(2);
			}
			else
			if (m_Version == VERSION_TWO)
			{
				DoLayoutVersion2(2);
			}
			else
			if (m_Version == VERSION_THREE)
			{
				DoLayoutVersion3(2);
			}
			else
			if (m_Version == VERSION_FOUR)
			{
				DoLayoutVersion4(2);
			}
		}break;
		case TUTORIAL_EXAMPLE_3:
		{
			if (m_Version == VERSION_ONE)
			{
				DoLayoutVersion1(3);
			}
			else
			if (m_Version == VERSION_TWO)
			{
				DoLayoutVersion2(3);
			}
			else
			if (m_Version == VERSION_THREE)
			{
				DoLayoutVersion3(3);
			}
			else
			if (m_Version == VERSION_FOUR)
			{
				DoLayoutVersion4(3);
			}
		}break;
		case TUTORIAL_END:
		{
			DoEndTutorial();
		}break;
		default:
			wxASSERT(0);
		break;
	}

	m_TxtTutorial->EndBold();
	m_TxtTutorial->SetScrollbar(wxVERTICAL, 0, 0, 0);
}

/////////////////////////////////////////////////////
/// DoLayoutVersion1
/// Desc: Method to draw the version 1 tutorial text
/////////////////////////////////////////////////////
void PanelTutorial::DoLayoutVersion1(int example)
{
	if (example == 1)
	{
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();

		wxRichTextBox* textBox = m_TxtTutorial->WriteTextBox(m_BorderAttr);
		m_TxtTutorial->SetFocusObject(textBox);

		m_TxtTutorial->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
		m_TxtTutorial->BeginAlignment(wxTEXT_ALIGNMENT_CENTRE);
		m_TxtTutorial->BeginBold();
		m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextColour());
		m_TxtTutorial->BeginFontSize(m_CurrentFontSize);

		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut1Ex1());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut1Ex1());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut1Ex1());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut1Ex1());
		m_TxtTutorial->LineBreak();

		m_TxtTutorial->SetFocusObject(0);

		m_LastTextLine = sys::AppData::Instance()->GetTut1Ex1();
		m_NumLinePrints = 4;
	}
	else
	if (example == 2)
	{
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();

		wxRichTextBox* textBox = m_TxtTutorial->WriteTextBox(m_BorderAttr);
		m_TxtTutorial->SetFocusObject(textBox);

		m_TxtTutorial->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
		m_TxtTutorial->BeginAlignment(wxTEXT_ALIGNMENT_CENTRE);
		m_TxtTutorial->BeginBold();
		m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextColour());
		m_TxtTutorial->BeginFontSize(m_CurrentFontSize);

		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut1Ex2());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut1Ex2());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut1Ex2());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut1Ex2());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut1Ex2());
		m_TxtTutorial->LineBreak();

		m_TxtTutorial->SetFocusObject(0);

		m_LastTextLine = sys::AppData::Instance()->GetTut1Ex2();
		m_NumLinePrints = 5;
	}
	else
	if (example == 3)
	{
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();

		wxRichTextBox* textBox = m_TxtTutorial->WriteTextBox(m_BorderAttr);
		m_TxtTutorial->SetFocusObject(textBox);

		m_TxtTutorial->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
		m_TxtTutorial->BeginAlignment(wxTEXT_ALIGNMENT_CENTRE);
		m_TxtTutorial->BeginBold();
		m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextColour());
		m_TxtTutorial->BeginFontSize(m_CurrentFontSize);

		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut1Ex3());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut1Ex3());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut1Ex3());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut1Ex3());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut1Ex3());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut1Ex3());
		m_TxtTutorial->LineBreak();

		m_TxtTutorial->SetFocusObject(0);

		m_LastTextLine = sys::AppData::Instance()->GetTut1Ex3();
		m_NumLinePrints = 6;
	}

	m_KeyCheckValue = m_NumLinePrints;
}

/////////////////////////////////////////////////////
/// DoLayoutVersion2
/// Desc: Method to draw the version 2 tutorial text
/////////////////////////////////////////////////////
void PanelTutorial::DoLayoutVersion2(int example)
{
	if (example == 1)
	{
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();

		wxRichTextBox* textBox = m_TxtTutorial->WriteTextBox(m_BorderAttr);
		m_TxtTutorial->SetFocusObject(textBox);

		m_TxtTutorial->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
		m_TxtTutorial->BeginAlignment(wxTEXT_ALIGNMENT_CENTRE);
		m_TxtTutorial->BeginBold();
		m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextColour());
		m_TxtTutorial->BeginFontSize(m_CurrentFontSize);

		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut2Ex1());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut2Ex1());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut2Ex1());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut2Ex1());
		m_TxtTutorial->LineBreak();

		m_TxtTutorial->SetFocusObject(0);

		m_LastTextLine = sys::AppData::Instance()->GetTut2Ex1();
		m_NumLinePrints = 4;
	}
	else
	if (example == 2)
	{
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();

		wxRichTextBox* textBox = m_TxtTutorial->WriteTextBox(m_BorderAttr);
		m_TxtTutorial->SetFocusObject(textBox);

		m_TxtTutorial->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
		m_TxtTutorial->BeginAlignment(wxTEXT_ALIGNMENT_CENTRE);
		m_TxtTutorial->BeginBold();
		m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextColour());
		m_TxtTutorial->BeginFontSize(m_CurrentFontSize);

		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut2Ex2());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut2Ex2());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut2Ex2());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut2Ex2());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut2Ex2());
		m_TxtTutorial->LineBreak();

		m_TxtTutorial->SetFocusObject(0);

		m_LastTextLine = sys::AppData::Instance()->GetTut2Ex2();
		m_NumLinePrints = 5;
	}
	else
	if (example == 3)
	{
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->LineBreak();

		wxRichTextBox* textBox = m_TxtTutorial->WriteTextBox(m_BorderAttr);
		m_TxtTutorial->SetFocusObject(textBox);

		m_TxtTutorial->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
		m_TxtTutorial->BeginAlignment(wxTEXT_ALIGNMENT_CENTRE);
		m_TxtTutorial->BeginBold();
		m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextColour());
		m_TxtTutorial->BeginFontSize(m_CurrentFontSize);

		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut2Ex3());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut2Ex3());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut2Ex3());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut2Ex3());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut2Ex3());
		m_TxtTutorial->LineBreak();
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut2Ex3());
		m_TxtTutorial->LineBreak();

		m_TxtTutorial->SetFocusObject(0);

		m_LastTextLine = sys::AppData::Instance()->GetTut2Ex3();
		m_NumLinePrints = 6;
	}

	m_KeyCheckValue = m_NumLinePrints;
}

/////////////////////////////////////////////////////
/// DoLayoutVersion3
/// Desc: Method to draw the version 3 tutorial text
/////////////////////////////////////////////////////
void PanelTutorial::DoLayoutVersion3(int example)
{
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();

	wxRichTextBox* textBox = m_TxtTutorial->WriteTextBox(m_BorderAttr);
	m_TxtTutorial->SetFocusObject(textBox);

	m_TxtTutorial->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
	m_TxtTutorial->BeginAlignment(wxTEXT_ALIGNMENT_CENTRE);
	m_TxtTutorial->BeginBold();
	m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextColour());
	m_TxtTutorial->BeginFontSize(m_CurrentFontSize);

	if (example == 1)
	{
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut3Ex1());
		m_LastTextLine = sys::AppData::Instance()->GetTut3Ex1();
		m_NumLinePrints = 1;
	}
	else
	if (example == 2)
	{
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut3Ex2());
		m_LastTextLine = sys::AppData::Instance()->GetTut3Ex2();
		m_NumLinePrints = 1;
	}
	else
	if (example == 3)
	{
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut3Ex3());
		m_LastTextLine = sys::AppData::Instance()->GetTut3Ex3();
		m_NumLinePrints = 1;
	}

	m_TxtTutorial->LineBreak();

	m_TxtTutorial->SetFocusObject(0);

	m_KeyCheckValue = sys::WordCount(m_LastTextLine);
}

/////////////////////////////////////////////////////
/// DoLayoutVersion4
/// Desc: Method to draw the version 4 tutorial text
/////////////////////////////////////////////////////
void PanelTutorial::DoLayoutVersion4(int example)
{
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();

	wxRichTextBox* textBox = m_TxtTutorial->WriteTextBox(m_BorderAttr);
	m_TxtTutorial->SetFocusObject(textBox);

	m_TxtTutorial->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
	m_TxtTutorial->BeginAlignment(wxTEXT_ALIGNMENT_CENTRE);
	m_TxtTutorial->BeginBold();
	m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextColour());
	m_TxtTutorial->BeginFontSize(m_CurrentFontSize);

	if (example == 1)
	{
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut4Ex1());
		m_LastTextLine = sys::AppData::Instance()->GetTut4Ex1();
	}
	else
	if (example == 2)
	{
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut4Ex2());
		m_LastTextLine = sys::AppData::Instance()->GetTut4Ex2();
	}
	else
	if (example == 3)
	{
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTut4Ex3());
		m_LastTextLine = sys::AppData::Instance()->GetTut4Ex3();
	}

	m_TxtTutorial->LineBreak();

	m_TxtTutorial->SetFocusObject(0);

	m_NumLinePrints = 1;
	m_KeyCheckValue = sys::WordCount(m_LastTextLine);
}

/////////////////////////////////////////////////////
/// DoTutorialLayout1
/// Desc: Sets up the version 1 tutorial text print
/////////////////////////////////////////////////////
void PanelTutorial::DoTutorialLayout1()
{
	DoTutorialTextPrint();
}

/////////////////////////////////////////////////////
/// DoTutorialLayout2
/// Desc: Sets up the version 2 tutorial text print
/////////////////////////////////////////////////////
void PanelTutorial::DoTutorialLayout2()
{
	m_TxtTutorial->LineBreak();
	DoTutorialTextPrint();
}

/////////////////////////////////////////////////////
/// DoTutorialLayout3
/// Desc: Sets up the version 3 tutorial text print
/////////////////////////////////////////////////////
void PanelTutorial::DoTutorialLayout3()
{
	m_TxtTutorial->LineBreak();
	DoTutorialTextPrint();
}

/////////////////////////////////////////////////////
/// DoTutorialLayout4
/// Desc: Sets up the version 4 tutorial text print
/////////////////////////////////////////////////////
void PanelTutorial::DoTutorialLayout4()
{
	m_TxtTutorial->LineBreak();
	DoTutorialTextPrint();
}

/////////////////////////////////////////////////////
/// DoTutorialTextPrint
/// Desc: Draws the tutorial text for the specific version
/////////////////////////////////////////////////////
void PanelTutorial::DoTutorialTextPrint()
{
	if ( m_Version == 1)
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTutTxtVersion1());
	else
	if (m_Version == 2)
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTutTxtVersion2());
	else
	if (m_Version == 3 || m_Version == 4)
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTutTxtVersion3And4());

	m_TxtTutorial->LineBreak();
}

/////////////////////////////////////////////////////
/// DoPrepare
/// Desc: Draws the get ready text before showing an example
/////////////////////////////////////////////////////
void PanelTutorial::DoPrepare()
{
	m_TxtTutorial->Clear();

	m_TxtTutorial->Disable();

	m_TxtTutorial->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
	m_TxtTutorial->BeginAlignment(wxTEXT_ALIGNMENT_CENTRE);
	m_TxtTutorial->BeginBold();
	m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextInfoColour());
	m_TxtTutorial->BeginFontSize(m_CurrentSmallFontSize);
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();

	if (m_ExampleNumber == 1)
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTxtEx1());
	else
	if (m_ExampleNumber == 2)
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTxtEx2());
	else
	if (m_ExampleNumber == 3)
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetTxtEx3());

	m_TxtTutorial->LineBreak();
}

/////////////////////////////////////////////////////
/// DoBreakText
/// Desc: Draws the information text after showing an example regarding how many words or lines were shown
/////////////////////////////////////////////////////
void PanelTutorial::DoBreakText()
{
	m_TxtTutorial->Clear();

	m_TxtTutorial->Disable();

	m_TxtTutorial->SetBackgroundColour(sys::AppData::Instance()->GetBGColour());
	m_TxtTutorial->BeginAlignment(wxTEXT_ALIGNMENT_CENTRE);
	m_TxtTutorial->BeginBold();
	m_TxtTutorial->BeginTextColour(sys::AppData::Instance()->GetTextInfoColour());
	m_TxtTutorial->BeginFontSize(m_CurrentSmallFontSize);
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	int wordCount = sys::WordCount(m_LastTextLine);

	if (m_Version == VERSION_ONE )
		m_TxtTutorial->WriteText(wxString::Format("%s %d", sys::AppData::Instance()->GetInputText1(), m_NumLinePrints));
	else
	if (m_Version == VERSION_TWO)
		m_TxtTutorial->WriteText(wxString::Format("%s %d", sys::AppData::Instance()->GetInputText2(), m_NumLinePrints));
	else
		m_TxtTutorial->WriteText(wxString::Format("%s %d", sys::AppData::Instance()->GetInputText1(), wordCount));

	m_TxtTutorial->LineBreak();
}

/////////////////////////////////////////////////////
/// DoEndTutorial
/// Desc: Shows the information text at the end of a tutorial
/////////////////////////////////////////////////////
void PanelTutorial::DoEndTutorial()
{
	m_TxtTutorial->LineBreak();
	m_TxtTutorial->LineBreak();
	if (m_Version == VERSION_ONE)
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetEndingText1());
	else
		m_TxtTutorial->WriteText(sys::AppData::Instance()->GetEndingText2());
	m_TxtTutorial->LineBreak();
}
