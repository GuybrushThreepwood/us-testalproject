#ifndef __PanelPhasePrograms__
#define __PanelPhasePrograms__

/**
@file
Subclass of PhasePrograms, which is generated by wxFormBuilder.
*/

#include "Common.h"
#include "TECSApp.h"

//// end generated include

/** Implementing PhasePrograms */
class PanelPhasePrograms : public PhasePrograms
{
	public:
		/** Constructor */
		PanelPhasePrograms( wxWindow* parent );
	//// end generated class members

	private:
		void CreateGridFromPrograms();
		void DoRefreshPhases();
		void DoPhaseFilter(std::map<wxString, ProgramFileInfo>& phases, int mode);

		bool CheckProgramValidity(ProgramFileInfo& pf);

	protected:
		// Handlers for PhasePrograms events.
		virtual void PhaseFilterChange(wxCommandEvent& event);
		virtual void GridCellLeftDoubleClick(wxGridEvent& event);
		virtual void BtnEditPhasesClick(wxCommandEvent& event);
		virtual void BtnCreateClick(wxCommandEvent& event);
		virtual void BtnEditClick(wxCommandEvent& event);
		virtual void BtnCopyClick(wxCommandEvent& event);
		virtual void BtnDeleteClick(wxCommandEvent& event);
		virtual void BtnBackClick(wxCommandEvent& event);

	private:
		int m_PhaseFilter;
		std::map<int, PhaseData> m_PhaseMap;

		std::map<wxString, ProgramFileInfo> m_FileMap;
		std::map<wxString, ProgramFileInfo> m_FilteredFileMap;
};

#endif // __PanelPhasePrograms__
