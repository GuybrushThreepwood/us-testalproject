/**
	TECS
	PanelQuestions.cpp
    Purpose: Panel shown after a participant has run a program and is asked for information on what they remember
*/

#include "PanelQuestions.h"
#include "System.h"
#include "DlgPassword.h"

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
PanelQuestions::PanelQuestions( wxWindow* parent )
:
Questions( parent )
{
	m_InputText->SetFocus();

	m_CurrentData = sys::AppData::Instance()->GetParticipantData();

	m_StringFilter.Clear();

	m_ProgramData->InsertColumn(0, _("Word/Sentence"));
	m_ProgramData->InsertColumn(1, _("Filter"));


	m_ProgramCounts.clear();
	for (auto resp : m_CurrentData.response)
	{
		const FilterData fd = sys::AppData::Instance()->FindFilter(resp.data.filterIndex);

		auto mapFind = m_ProgramCounts.find(fd.val);
		if (mapFind == m_ProgramCounts.end())
		{
			// insert a new pair
			m_ProgramCounts.insert(std::make_pair(fd.val, 1));
		}
		else
		{
			// update
            int oldVal = m_ProgramCounts[fd.val];
            oldVal += 1;
			m_ProgramCounts[fd.val] = oldVal;
		}
	}

	CreateProgramDataList();

	m_AnswersDynamic.clear();

	// dynamically create correct answer counter info
	auto filterMap = sys::AppData::Instance()->GetFilters();
	for (auto iter = filterMap.begin(); iter != filterMap.end();)
	{
		StoredCorrectData answerData;

		answerData.fd = iter->second;
		auto mapFilterCounts = m_ProgramCounts.find(answerData.fd.val);
		
		answerData.filterboxSizer = new wxBoxSizer(wxHORIZONTAL);

		answerData.filterName = new wxStaticText(m_CorrectValuesScroll, wxID_ANY, wxString::Format(_("%s shown"), answerData.fd.name), wxDefaultPosition, wxDefaultSize, 0);
		answerData.filterName->Wrap(-1);
		answerData.filterboxSizer->Add(answerData.filterName, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

		answerData.shownVal = new wxTextCtrl(m_CorrectValuesScroll, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0);
		answerData.shownVal->SetMaxSize(wxSize(55, -1));
		if (mapFilterCounts != m_ProgramCounts.end())
			answerData.shownVal->SetValue(wxString::Format("%d", mapFilterCounts->second));
		else
			answerData.shownVal->SetValue(wxString::Format("0"));
		answerData.shownVal->Disable();
		answerData.filterboxSizer->Add(answerData.shownVal, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);


		answerData.rememberedCorrectly = new wxStaticText(m_CorrectValuesScroll, wxID_ANY, wxString::Format(_("remembered")), wxDefaultPosition, wxDefaultSize, 0);
		answerData.rememberedCorrectly->Wrap(-1);
		answerData.filterboxSizer->Add(answerData.rememberedCorrectly, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

		answerData.spinValueControl = new wxSpinCtrl(m_CorrectValuesScroll, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 100, 0);
		answerData.spinValueControl->SetMaxSize(wxSize(55, -1));
		
		if (mapFilterCounts != m_ProgramCounts.end())
			answerData.spinValueControl->SetRange(0, mapFilterCounts->second);
		else
			answerData.spinValueControl->SetRange(0, 0);

		answerData.spinValueControl->Connect(wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler(PanelQuestions::DynamicCountChange), NULL, this);
		answerData.spinValueControl->Connect(wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler(PanelQuestions::DynamicCountChangeTxt), NULL, this);

		answerData.filterboxSizer->Add(answerData.spinValueControl, 1, wxALL | wxEXPAND, 5);


		answerData.percentage = new wxStaticText(m_CorrectValuesScroll, wxID_ANY, wxString("0%"), wxDefaultPosition, wxDefaultSize, 0);
		answerData.percentage->Wrap(-1);
		answerData.filterboxSizer->Add(answerData.percentage, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

		// add
		bFilterWindowSizer->Add(answerData.filterboxSizer, 0, wxEXPAND, 5);

		// add to the list
		m_AnswersDynamic.push_back(answerData);

		// next
		iter++;
	}

	bFilterWindowSizer->Layout();

	// disable the checkbox which will update all values
	for (auto item : m_AnswersDynamic)
		item.spinValueControl->SetValue(0);

	m_CheckPanel->Hide();
	m_ProgramPanel->Hide();
	this->Layout();

	m_UserInput = true;
}

/////////////////////////////////////////////////////
/// CreateProgramDataList
/// Desc: Clears and create the program data list 
/////////////////////////////////////////////////////
void PanelQuestions::CreateProgramDataList()
{
	int index = 0;
	m_ProgramData->DeleteAllItems();

	for (auto resp : m_CurrentData.response)
	{
		if (m_StringFilter.Length() > 0 )
		{
			if (resp.data.str.Contains(m_StringFilter)) // filter by string
			{
				m_ProgramData->InsertItem(index, resp.data.str, -1);
				const FilterData fd = sys::AppData::Instance()->FindFilter(resp.data.filterIndex);

				m_ProgramData->SetItem(index, 1, wxString::Format("%s", fd.name), -1);

				index++;
			}
		}
		else
		{
			m_ProgramData->InsertItem(index, resp.data.str, -1);
			const FilterData fd = sys::AppData::Instance()->FindFilter(resp.data.filterIndex);

			m_ProgramData->SetItem(index, 1, wxString::Format("%s", fd.name), -1);

			index++;
		}
	}

	int i = 0;
	for (i = 0; i < m_ProgramData->GetColumnCount(); ++i)
	{
		m_ProgramData->SetColumnWidth(i, wxLIST_AUTOSIZE_USEHEADER);
		int wh = m_ProgramData->GetColumnWidth(i);
		m_ProgramData->SetColumnWidth(i, wxLIST_AUTOSIZE);
		int wc = m_ProgramData->GetColumnWidth(i);
		if (wh > wc)
			m_ProgramData->SetColumnWidth(i, wxLIST_AUTOSIZE_USEHEADER);
	}
}

/////////////////////////////////////////////////////
/// DynamicCountChangeTxt
/// Desc: SpinCtrl event for the dynamic filter values
/////////////////////////////////////////////////////
void PanelQuestions::DynamicCountChangeTxt(wxCommandEvent& event)
{
	wxSpinEvent e;
	e.SetEventObject(event.GetEventObject());
	DynamicCountChange(e);
}

/////////////////////////////////////////////////////
/// DynamicCountChange
/// Desc: SpinCtrl event for the dynamic filter values
/////////////////////////////////////////////////////
void PanelQuestions::DynamicCountChange(wxSpinEvent& event)
{
	wxSpinCtrl* pObj = reinterpret_cast<wxSpinCtrl *>(event.GetEventObject());
	for (auto item : m_AnswersDynamic)
	{
		if (pObj == item.spinValueControl) // find the data
		{
			// get the values
			int valCorrect = item.spinValueControl->GetValue();
			wxString tmpStr = item.shownVal->GetValue();
			long tmpVal = -1;
			if (tmpStr.ToLong(&tmpVal))
			{
				int valShown = (int)tmpVal;

				if (valShown == 0)
				{
					item.percentage->SetLabel(wxString("0%"));
				}
				else
				{
					float percent = (float)(std::abs(100.0f * ((float)valCorrect / (float)valShown)));
					item.percentage->SetLabel(wxString::Format("%.2f%%", percent));
				}
			}
			else
			{
				item.percentage->SetLabel(wxString("0%"));
			}
			// done
			return;
		}
	}
}

/////////////////////////////////////////////////////
/// ProgramSearchCancel
/// Desc: Clears the search filter
/////////////////////////////////////////////////////
void PanelQuestions::ProgramSearchCancel(wxCommandEvent& WXUNUSED(event))
{
	if (m_ProgramSearch->GetValue().Length() > 0)
	{
		m_ProgramSearch->Clear();
		m_StringFilter.Clear();

		CreateProgramDataList();
	}
}

/////////////////////////////////////////////////////
/// ProgramSearch
/// Desc: 
/////////////////////////////////////////////////////
void PanelQuestions::ProgramSearch(wxCommandEvent& WXUNUSED(event))
{
	wxString str = m_ProgramSearch->GetValue();

	if (m_StringFilter != str)
	{
		m_StringFilter = str;
		CreateProgramDataList();
	}
}

/////////////////////////////////////////////////////
/// ProgramSearchEnter
/// Desc: 
/////////////////////////////////////////////////////
void PanelQuestions::ProgramSearchEnter(wxCommandEvent& WXUNUSED(event))
{
	wxString str = m_ProgramSearch->GetValue();

	if (m_StringFilter != str)
	{
		m_StringFilter = str;
		CreateProgramDataList();
	}
}

/////////////////////////////////////////////////////
/// BtnDoneClick
/// Desc: Button event for completed test (2 stages - 1st is user comments complete, 2nd is admin comments complete)
/////////////////////////////////////////////////////
void PanelQuestions::BtnDoneClick(wxCommandEvent& WXUNUSED(event))
{
	if (m_UserInput)
	{
		/*wxString userAnswers = m_InputText->GetValue();
		wxString curField;
		wxString curLine;

		// process to separate words by newline 
		do
		{
			curLine = userAnswers.BeforeFirst('\n');
			userAnswers = userAnswers.AfterFirst('\n');

			do
			{
				curField = curLine.BeforeFirst('\t');
				curLine = curLine.AfterFirst('\t');

				if (curField.Length() > 0)
				{ 
					m_AnswerStringList.push_back(curField);
				}

			} while (curLine.IsEmpty() == false);

		} while (userAnswers.IsEmpty() == false);

		// put it back into the user text
		m_InputText->Clear();
		for (auto userStr : m_AnswerStringList)
		{
			wxString a = m_InputText->GetValue();
			m_InputText->SetValue( a + userStr + "\n" );
		}*/

#ifdef _DEBUG
		// first click enables the admin comments section
		m_TxtQuestion->Hide();
		m_CheckPanel->Show();
		m_ProgramPanel->Show();
		bFilterWindowSizer->Layout();
		this->Layout();

		m_UserInput = false;
		m_InputText->Disable();

		m_InputMark->SetFocus();
#else
		if (sys::AppData::Instance()->GetAdminPasswordState())
		{
			DlgPassword* dlg = new DlgPassword(0);
			if (dlg->ShowModal() == RESPONSE_OK)
			{
				// first click enables the admin comments section
				m_TxtQuestion->Hide();
				m_CheckPanel->Show();
				m_ProgramPanel->Show();
				bFilterWindowSizer->Layout();
				this->Layout();

				m_UserInput = false;
				m_InputText->Disable();

				m_InputMark->SetFocus();
			}

			dlg->Destroy();
			delete dlg;
		}
		else
		{
			// first click enables the admin comments section
			m_TxtQuestion->Hide();
			m_CheckPanel->Show();
			m_ProgramPanel->Show();
			bFilterWindowSizer->Layout();
			this->Layout();

			m_UserInput = false;
			m_InputText->Disable();

			m_InputMark->SetFocus();
		}
#endif // _DEBUG
	}
	else
	{
		// second click saves all and returns to the entry panel
		JsonBox::Object outObj;

		ParticipantData pd = sys::AppData::Instance()->GetParticipantData();

		wxString idStr = pd.strId;//wxString::Format("%s", pd.participantId);
		std::string fullPath = sys::AppData::Instance()->GetParticipantsDirectory().ToStdString() + std::string("/") + std::string("Participant-") + std::string(idStr.ToStdString()) + std::string(".json");

		outObj["ID"] = JsonBox::Value(pd.strId.ToStdString());

		wxLongLong value = pd.dateOfBirth.GetValue();
		wxString numericStr = value.ToString();
		wxString dateStr = pd.dateOfBirth.FormatISODate();
		outObj["DOB"] = JsonBox::Value(numericStr.ToStdString());

		value = wxDateTime::Now().GetValue();
		numericStr = value.ToString();
		outObj["Date"] = JsonBox::Value(numericStr.ToStdString());
		outObj["VersionId"] = JsonBox::Value(pd.selectedVersionId);
		outObj["Gender"] = JsonBox::Value(pd.genderId);

		JsonBox::Object programObj;
		programObj["Name"] = JsonBox::Value(pd.programName.ToStdString());
		programObj["LinesMin"] = JsonBox::Value(pd.phaseData.linesCountMin);
		programObj["LinesMax"] = JsonBox::Value(pd.phaseData.linesCountMax);
		programObj["WordCountMin"] = JsonBox::Value(pd.phaseData.wordCountMin);
		programObj["WordCountMax"] = JsonBox::Value(pd.phaseData.wordCountMax);

		programObj["TimeToRespond"] = JsonBox::Value(pd.phaseData.timeToRespond);
		programObj["TimeResponseTimeout"] = JsonBox::Value(pd.phaseData.timeResponseTimeout);
		programObj["TimeInterval"] = JsonBox::Value(pd.phaseData.timeInterval);
		programObj["TimeBetweenFilterChanges"] = JsonBox::Value(pd.phaseData.timeBetweenFilterChanges);

		JsonBox::Array programArr;
		int index = 0;
		for (auto resp : pd.response)
		{
			JsonBox::Object progItem;
			progItem["Index"] = JsonBox::Value(index);
			progItem["StringData"] = JsonBox::Value(resp.data.str.ToStdString());
			progItem["FilterType"] = JsonBox::Value(resp.data.filterIndex);
			progItem["LineOrWordCount"] = JsonBox::Value(resp.lineOrWordCount);

			progItem["HasFirstResponse"] = JsonBox::Value(resp.hasFirstRespose);
			progItem["FirstResponseTime"] = JsonBox::Value(resp.firstResponseTime);
			progItem["FirstResponseValue"] = JsonBox::Value(resp.firstResponseValue);

			progItem["HasSecondResponse"] = JsonBox::Value(resp.hasSecondResponse);
			progItem["SecondResponseTime"] = JsonBox::Value(resp.secondResponseTime);
			progItem["SecondResponseValue"] = JsonBox::Value(resp.secondResponseValue);

			//progItem["ResponseTime"] = JsonBox::Value(resp.responseTime);
			//progItem["BadResponseTime"] = JsonBox::Value(resp.badResponseTime);
			//progItem["BadResponseVal"] = JsonBox::Value(resp.badResponseVal);

			// add default filters
			programArr.push_back(progItem);

			index++;
		}
		programObj["Program"] = JsonBox::Value(programArr);
		outObj["ProgramInfo"] = JsonBox::Value(programObj);

		outObj["Answers"] = JsonBox::Value(m_InputText->GetValue().ToStdString());

		JsonBox::Array answerArr;
		for (auto answeredCount : m_AnswersDynamic)
		{
			JsonBox::Object answerItem;
			// get the values
			int valCorrect = answeredCount.spinValueControl->GetValue();
			wxString tmpStr = answeredCount.shownVal->GetValue();
			long tmpVal = -1;
			if (tmpStr.ToLong(&tmpVal))
			{

			}
			else
				tmpVal = 0;

			answerItem["FilterType"] = JsonBox::Value(answeredCount.fd.val);
			answerItem["ShownCount"] = JsonBox::Value((int)tmpVal);
			answerItem["RememberedCount"] = JsonBox::Value(valCorrect);

			// add to answer count array
			answerArr.push_back(answerItem);
		}
		outObj["AnswerCounts"] = JsonBox::Value(answerArr);
		outObj["Comments"] = JsonBox::Value(m_InputMark->GetValue().ToStdString());

		JsonBox::Value v(outObj);

		v.writeToFile(fullPath);

		pd.genderId = -1;
		pd.program.clear();
		pd.response.clear();
		sys::AppData::Instance()->SetParticipantData(pd);
		sys::AppData::Instance()->ChangePanel(SCREEN_ENTRY);
	}
}
