/**
	TECS
	DlgSettings.cpp
    Purpose: Dialog for modifying the default settings of the application
*/

#include "DlgSettings.h"
#include "System.h"

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
DlgSettings::DlgSettings( wxWindow* parent )
:
SettingsDlg( parent )
{

}

/////////////////////////////////////////////////////
/// EventInitDialog
/// Desc: Called after construction to populate or modify UI elements
/////////////////////////////////////////////////////
void DlgSettings::EventInitDialog(wxInitDialogEvent& WXUNUSED(event))
{
	this->SetTitle(_("Settings"));

	// phase list
	m_ComboPhase->Clear();
	m_ComboTutPhase->Clear();

	// populate from the current map
	auto phaseMap = sys::AppData::Instance()->GetPhases();
	for (auto val : phaseMap)
	{
		int index = m_ComboPhase->GetCount();
		m_ComboPhase->Insert(val.second.name, index);
		m_ComboTutPhase->Insert(val.second.name, index);

		// make this map match the selection box
		m_PhaseMap.insert(std::make_pair(index, val.second.val));
	}
	m_PreviousComboSelection = -1;
	m_ComboPhase->SetSelection(0);
	m_ComboTutPhase->SetSelection(0);

	m_CheckUseAdminPass->SetValue(sys::AppData::Instance()->GetAdminPasswordState());

	// colours
	wxColour col = sys::AppData::Instance()->GetBGColour();
	m_PickerTextBG->SetColour(col);
	col = sys::AppData::Instance()->GetTextColour();
	m_PickerText->SetColour(col);
	col = sys::AppData::Instance()->GetTextBorderColour();
	m_PickerBorder->SetColour(col);
	col = sys::AppData::Instance()->GetTextInfoColour();
	m_PickerInfoText->SetColour(col);

	int fontSize = sys::AppData::Instance()->GetTextFontSize();
	m_FontSizeControl->SetRange(5, 200);
	m_FontSizeControl->SetValue(fontSize);

	// borders
	int borderThickness = sys::AppData::Instance()->GetTextBorderThickness();
	m_BorderThicknessCtrl->SetValue(borderThickness);

	int borderWidth = sys::AppData::Instance()->GetTextBorderWidth();
	m_BorderWidthCtrl->SetRange(0, 300);
	m_BorderWidthCtrl->SetValue(borderWidth);


	int borderHeight = sys::AppData::Instance()->GetTextBorderHeight();
	m_BorderHeightCtrl->SetRange(0, 300);
	m_BorderHeightCtrl->SetValue(borderHeight);

	m_SettingsTabs->SetSelection(0);

	wxCommandEvent empty;
	OnChangePhase(empty);	
	
	wxNotebookEvent eventPage;
	PageChanging(eventPage);
}

/////////////////////////////////////////////////////
/// PageChanged
/// Desc: Event callback for when the tab page is changed
/////////////////////////////////////////////////////
void DlgSettings::PageChanged(wxNotebookEvent& WXUNUSED(event))
{
	for (auto item : m_SettingsCountList)
	{
		// want to get the previous value and set it to the label
		if (item.input)
			item.input->SetValue(wxString::Format("%d", item.val));
	}

	bSettingsScrollBoxSizer->Layout();

	this->Layout();
}

/////////////////////////////////////////////////////
/// PageChanging
/// Desc: Event callback for when the tab page is changing
/////////////////////////////////////////////////////
void DlgSettings::PageChanging(wxNotebookEvent& WXUNUSED(event))
{
	CreateFilterData(bSettingsScrollBoxSizer, m_SettingsFilterScroll, m_SettingsCountList, false);

	bSettingsScrollBoxSizer->Layout();
	bSettingsScrollBoxSizer->RecalcSizes();

	this->Layout();
}

/////////////////////////////////////////////////////
/// CheckChangeAdminUse
/// Desc: Event callback for changing the check state for admin password usage
/////////////////////////////////////////////////////
void DlgSettings::CheckChangeAdminUse(wxCommandEvent& WXUNUSED(event))
{
	sys::AppData::Instance()->SetAdminPasswordState(m_CheckUseAdminPass->GetValue());
}

/////////////////////////////////////////////////////
/// DynamicSettingsKeyUp
/// Desc: Event for key up on the dynamic input boxes created for the default filter settings
/////////////////////////////////////////////////////
void DlgSettings::DynamicSettingsKeyUp(wxKeyEvent& event)
{
	wxTextCtrl* ctrl = reinterpret_cast<wxTextCtrl*>(event.GetEventObject());
	for (auto& item : m_SettingsCountList)
	{
		// check the control matches and update the val
		if (item.input == ctrl)
		{
			wxString str = item.input->GetValue();
			long val = -1;
			if (str.Length() != 0)
			{
				if (str.ToLong(&val))
				{
					item.val = (int)val;
				}
			}
			else
			{

			}
		}
	}
}

/////////////////////////////////////////////////////
/// CreateFilterData
/// Desc: Generates a box sizer containing all the current available filter values
/////////////////////////////////////////////////////
void DlgSettings::CreateFilterData(wxBoxSizer* useSizer, wxScrolledWindow* scroller, std::vector<StoredInputData>& list, bool readOnly)
{
	bool isListRefresh = false;
	if (list.size() > 0)
		isListRefresh = true;

	// before clearing store previous values
	if (isListRefresh)
	{
		auto filterMap = sys::AppData::Instance()->GetFilters();
		for (auto iter = filterMap.begin(); iter != filterMap.end(); ++iter)
		{
			// find and replace
			for (auto& item : list)
			{
				// want to get the previous value and set it to the label
				if (item.fd.val == iter->second.val)
				{
					wxString lastVal = item.input->GetValue();
					long convert = -1;
					if (lastVal.ToLong(&convert))
						item.val = (int)convert;
				}
			}
		}
	}

	// now clear
	useSizer->Clear(true);

	auto filterMap = sys::AppData::Instance()->GetFilters();
	for (auto iter = filterMap.begin(); iter != filterMap.end();)
	{
		wxBoxSizer* bNewSizer;
		bNewSizer = new wxBoxSizer(wxHORIZONTAL);

		wxBoxSizer* bLeftSide;
		bLeftSide = new wxBoxSizer(wxHORIZONTAL);

		wxString label = wxString::Format(_("No. %s"), iter->second.name);
		wxStaticText* leftStaticText = new wxStaticText(scroller, wxID_ANY, label, wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
		leftStaticText->Wrap(-1);


		bLeftSide->Add(leftStaticText, 0, wxALIGN_CENTER | wxALL, 5);

		wxTextCtrl* leftTextCtrl = new wxTextCtrl(scroller, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0);
		leftTextCtrl->SetMaxSize(wxSize(75, -1));
		leftTextCtrl->Connect(wxEVT_KEY_UP, wxKeyEventHandler(DlgSettings::DynamicSettingsKeyUp), NULL, this);

		// store
		if (isListRefresh)
		{
			// find and replace
			for (auto& item : list)
			{
				// want to get the previous value and set it to the label
				if (item.fd.val == iter->second.val)
				{
					// update control ptr
					item.input = leftTextCtrl;
					leftTextCtrl->SetValue(wxString::Format("%d", item.val));
					break;
				}
			}
		}
		else
		{
			// create new
			StoredInputData inputStore;
			inputStore.input = leftTextCtrl;
			inputStore.fd = iter->second;
			inputStore.val = 0;
			leftTextCtrl->SetValue(wxString::Format("%d", inputStore.val));
			//wxIntegerValidator<int> leftValCheck(&inputStore.val);
			//leftTextCtrl->SetValidator(leftValCheck);
			list.push_back(inputStore);
		}

		leftTextCtrl->Enable(!readOnly);
		bLeftSide->Add(leftTextCtrl, 1, wxALL, 5);

		bNewSizer->Add(bLeftSide, 1, wxEXPAND, 5);

		// next
		iter++;

		wxBoxSizer* bRightSide;
		bRightSide = new wxBoxSizer(wxHORIZONTAL);

		if (iter != filterMap.end())
		{
			label = wxString::Format(_("No. %s"), iter->second.name);
			wxStaticText* rightStaticText = new wxStaticText(scroller, wxID_ANY, label, wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
			rightStaticText->Wrap(-1);

			bRightSide->Add(rightStaticText, 0, wxALIGN_CENTER | wxALL, 5);

			wxTextCtrl* rightTextCtrl = new wxTextCtrl(scroller, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0);
			rightTextCtrl->SetMaxSize(wxSize(75, -1));
			rightTextCtrl->Connect(wxEVT_KEY_UP, wxKeyEventHandler(DlgSettings::DynamicSettingsKeyUp), NULL, this);

			// store
			if (isListRefresh)
			{
				// find and replace
				for (auto& item : list)
				{
					// want to get the previous value and set it to the label
					if (item.fd.val == iter->second.val)
					{
						// update control ptr
						item.input = rightTextCtrl;
						rightTextCtrl->SetValue(wxString::Format("%d", item.val));
						break;
					}
				}
			}
			else
			{
				// create new
				StoredInputData inputStore;
				inputStore.input = rightTextCtrl;
				inputStore.fd = iter->second;
				inputStore.val = 0;
				rightTextCtrl->SetValue(wxString::Format("%d", inputStore.val));
				//wxIntegerValidator<int> rightValCheck(&inputStore.val);
				//rightTextCtrl->SetValidator(leftValCheck);
				list.push_back(inputStore);
			}

			rightTextCtrl->Enable(!readOnly);
			bRightSide->Add(rightTextCtrl, 1, wxALL, 5);

			// next
			iter++;
		}
		else
			bRightSide->Add(0, 0, 1, wxEXPAND, 5); // spacer

		bNewSizer->Add(bRightSide, 1, wxEXPAND, 5);


		// add to the scroll window
		useSizer->Add(bNewSizer, 0, wxEXPAND, 5);
	}
}

/////////////////////////////////////////////////////
/// OnChangePhase
/// Desc: Combobox event callback for when either the time or tutorial combo box is changed
/////////////////////////////////////////////////////
void DlgSettings::OnChangePhase(wxCommandEvent& event)
{
	wxChoice* choice = reinterpret_cast<wxChoice*>(event.GetEventObject());
	int sel = 0;
	if (choice == m_ComboPhase)
	{
		sel = m_ComboPhase->GetSelection();
		m_ComboTutPhase->SetSelection(sel);
	}
	else
	if (choice == m_ComboTutPhase)
	{
		sel = m_ComboTutPhase->GetSelection();
		m_ComboPhase->SetSelection(sel);
	}

	// save previous data if need be
	if (m_PreviousComboSelection != -1)
	{
		// store
		auto previousSelection = m_PhaseMap.find(m_PreviousComboSelection);
		if (previousSelection != m_PhaseMap.end())
		{
			const PhaseData pd = sys::AppData::Instance()->FindPhase(previousSelection->second);
			if (pd.val != -1)
			{
				if (pd.val == VERSION_ONE)
				{
					sys::AppData::Instance()->SetTutTxtVersion1(m_TextCtrlTutorial->GetValue());

					sys::AppData::Instance()->SetTut1Ex1(m_TextControlEx1->GetValue());
					sys::AppData::Instance()->SetTut1Ex2(m_TextControlEx2->GetValue());
					sys::AppData::Instance()->SetTut1Ex3(m_TextControlEx3->GetValue());

				}
				else
				if (pd.val == VERSION_TWO)
				{
					sys::AppData::Instance()->SetTutTxtVersion2(m_TextCtrlTutorial->GetValue());

					sys::AppData::Instance()->SetTut2Ex1(m_TextControlEx1->GetValue());
					sys::AppData::Instance()->SetTut2Ex2(m_TextControlEx2->GetValue());
					sys::AppData::Instance()->SetTut2Ex3(m_TextControlEx3->GetValue());
				}
				else
				if (pd.val == VERSION_THREE || pd.val == VERSION_FOUR)
				{
					sys::AppData::Instance()->SetTutTxtVersion3And4(m_TextCtrlTutorial->GetValue());

					if (pd.val == VERSION_THREE)
					{
						sys::AppData::Instance()->SetTut3Ex1(m_TextControlEx1->GetValue());
						sys::AppData::Instance()->SetTut3Ex2(m_TextControlEx2->GetValue());
						sys::AppData::Instance()->SetTut3Ex3(m_TextControlEx3->GetValue());
					}
					else if (pd.val == VERSION_FOUR)
					{
						sys::AppData::Instance()->SetTut4Ex1(m_TextControlEx1->GetValue());
						sys::AppData::Instance()->SetTut4Ex2(m_TextControlEx2->GetValue());
						sys::AppData::Instance()->SetTut4Ex3(m_TextControlEx3->GetValue());
					}
				}

				sys::AppData::Instance()->SetTxtEx1(m_TextControlTut1->GetValue());
				sys::AppData::Instance()->SetTxtEx2(m_TextControlTut2->GetValue());
				sys::AppData::Instance()->SetTxtEx3(m_TextControlTut3->GetValue());

				if (pd.val != VERSION_TWO)
					sys::AppData::Instance()->SetInputText1(m_TextControlCounts->GetValue());
				else
					sys::AppData::Instance()->SetInputText2(m_TextControlCounts->GetValue());

				if (pd.val == VERSION_ONE)
					sys::AppData::Instance()->SetEndingText1(m_TextCtrlTutorialEnd->GetValue());
				else
					sys::AppData::Instance()->SetEndingText2(m_TextCtrlTutorialEnd->GetValue());

				sys::AppData::Instance()->SetWaitText(m_TextControlWait->GetValue());
			}
		}

		// store
	}
	else
		m_PreviousComboSelection = 0;

	auto itemSelected = m_PhaseMap.find(sel);
	if (itemSelected != m_PhaseMap.end())
	{
		const PhaseData pd = sys::AppData::Instance()->FindPhase(itemSelected->second);
		if (pd.val != -1)
		{
			// update the values
			if( pd.userTimeToRespond != pd.timeToRespond )
				m_InputTimeToRespond->SetValue(wxString::Format("%d", (int)pd.userTimeToRespond));
			else
				m_InputTimeToRespond->SetValue(wxString::Format("%d", (int)pd.timeToRespond));

			if (pd.userTimeResponseTimeout != pd.timeResponseTimeout)
				m_InputTimeResponseTimeout->SetValue(wxString::Format("%d", (int)pd.userTimeResponseTimeout));
			else
				m_InputTimeResponseTimeout->SetValue(wxString::Format("%d", (int)pd.timeResponseTimeout));

			if (pd.userTimeInterval != pd.timeInterval)
				m_InputTimeInterval->SetValue(wxString::Format("%d", (int)pd.userTimeInterval));
			else
				m_InputTimeInterval->SetValue(wxString::Format("%d", (int)pd.timeInterval));

			if (pd.userTimeBetweenFilterChanges != pd.timeBetweenFilterChanges)
				m_InputTimeBetweenChanges->SetValue(wxString::Format("%d", (int)pd.userTimeBetweenFilterChanges));
			else
				m_InputTimeBetweenChanges->SetValue(wxString::Format("%d", (int)pd.timeBetweenFilterChanges));

			// update the default values
			m_DefTimeToRespond->SetLabel(wxString::Format(_("Default: %d"), (int)pd.timeToRespond));
			m_DefTimeToRespond->Refresh();
			m_DefResponseTimeout->SetLabel(wxString::Format(_("Default: %d"), (int)pd.timeResponseTimeout));
			m_DefResponseTimeout->Refresh();
			m_DefInterval->SetLabel(wxString::Format(_("Default: %d"), (int)pd.timeInterval));
			m_DefInterval->Refresh();
			m_DefTimeBetweenChanges->SetLabel(wxString::Format(_("Default: %d"), (int)pd.timeBetweenFilterChanges));
			m_DefTimeBetweenChanges->Refresh();

			if (pd.val == VERSION_ONE)
			{
				m_TextCtrlTutorial->SetValue(sys::AppData::Instance()->GetTutTxtVersion1());

				m_TextControlEx1->SetValue(sys::AppData::Instance()->GetTut1Ex1());
				m_TextControlEx2->SetValue(sys::AppData::Instance()->GetTut1Ex2());
				m_TextControlEx3->SetValue(sys::AppData::Instance()->GetTut1Ex3());
			}
			else
			if (pd.val == VERSION_TWO)
			{
				m_TextCtrlTutorial->SetValue(sys::AppData::Instance()->GetTutTxtVersion2());

				m_TextControlEx1->SetValue(sys::AppData::Instance()->GetTut2Ex1());
				m_TextControlEx2->SetValue(sys::AppData::Instance()->GetTut2Ex2());
				m_TextControlEx3->SetValue(sys::AppData::Instance()->GetTut2Ex3());
			}
			else
			if (pd.val == VERSION_THREE || pd.val == VERSION_FOUR)
			{
				m_TextCtrlTutorial->SetValue(sys::AppData::Instance()->GetTutTxtVersion3And4());

				if (pd.val == VERSION_THREE)
				{
					m_TextControlEx1->SetValue(sys::AppData::Instance()->GetTut3Ex1());
					m_TextControlEx2->SetValue(sys::AppData::Instance()->GetTut3Ex2());
					m_TextControlEx3->SetValue(sys::AppData::Instance()->GetTut3Ex3());
				}
				else if (pd.val == VERSION_FOUR)
				{
					m_TextControlEx1->SetValue(sys::AppData::Instance()->GetTut4Ex1());
					m_TextControlEx2->SetValue(sys::AppData::Instance()->GetTut4Ex2());
					m_TextControlEx3->SetValue(sys::AppData::Instance()->GetTut4Ex3());
				}
			}

			m_TextControlTut1->SetValue(sys::AppData::Instance()->GetTxtEx1());
			m_TextControlTut2->SetValue(sys::AppData::Instance()->GetTxtEx2());
			m_TextControlTut3->SetValue(sys::AppData::Instance()->GetTxtEx3());

			if (pd.val != VERSION_TWO)
				m_TextControlCounts->SetValue(sys::AppData::Instance()->GetInputText1());
			else
				m_TextControlCounts->SetValue(sys::AppData::Instance()->GetInputText2());

			if (pd.val == VERSION_ONE)
				m_TextCtrlTutorialEnd->SetValue(sys::AppData::Instance()->GetEndingText1());
			else
				m_TextCtrlTutorialEnd->SetValue(sys::AppData::Instance()->GetEndingText2());

			m_TextControlWait->SetValue(sys::AppData::Instance()->GetWaitText());
		}


	}

	m_PreviousComboSelection = sel;
}

/////////////////////////////////////////////////////
/// PhaseValChange
/// Desc: Key event when a time value is changed
/////////////////////////////////////////////////////
void DlgSettings::PhaseValChange(wxKeyEvent& WXUNUSED(event))
{
	int sel = m_ComboPhase->GetSelection();

	auto itemSelected = m_PhaseMap.find(sel);
	if (itemSelected != m_PhaseMap.end())
	{
		PhaseData pd = sys::AppData::Instance()->FindPhase(itemSelected->second);

		// update all
		long intVal = 0;
		wxString strVal;
		
		// time to respond
		strVal = m_InputTimeToRespond->GetValue();
		strVal.ToLong(&intVal);

		if (pd.timeToRespond != (int)intVal)
			pd.userTimeToRespond = (int)intVal;

		// response timeout
		strVal = m_InputTimeResponseTimeout->GetValue();
		strVal.ToLong(&intVal);

		if (pd.timeResponseTimeout != (int)intVal)
			pd.userTimeResponseTimeout = (int)intVal;

		// time interval
		strVal = m_InputTimeInterval->GetValue();
		strVal.ToLong(&intVal);

		if (pd.timeInterval != (int)intVal)
			pd.userTimeInterval = (int)intVal;

		// time between changes
		strVal = m_InputTimeBetweenChanges->GetValue();
		strVal.ToLong(&intVal);

		if (pd.timeBetweenFilterChanges != (int)intVal)
			pd.userTimeBetweenFilterChanges = (int)intVal;

		sys::AppData::Instance()->UpdatePhase(pd.name, pd);
	}
}

/////////////////////////////////////////////////////
/// BtnResetTimesClick
/// Desc: Button event when all program times want to be reset to defaults
/////////////////////////////////////////////////////
void DlgSettings::BtnResetTimesClick(wxCommandEvent& WXUNUSED(event))
{
	int sel = m_ComboPhase->GetSelection();

	auto itemSelected = m_PhaseMap.find(sel);
	if (itemSelected != m_PhaseMap.end())
	{
		PhaseData pd = sys::AppData::Instance()->FindPhase(itemSelected->second);
		if (pd.val != -1)
		{
			// update the values
			pd.userTimeToRespond = pd.timeToRespond;
			m_InputTimeToRespond->SetValue(wxString::Format("%d", (int)pd.timeToRespond));

			pd.userTimeResponseTimeout = pd.timeResponseTimeout;
			m_InputTimeResponseTimeout->SetValue(wxString::Format("%d", (int)pd.timeResponseTimeout));

			pd.userTimeInterval = pd.timeInterval;
			m_InputTimeInterval->SetValue(wxString::Format("%d", (int)pd.timeInterval));

			pd.userTimeBetweenFilterChanges = pd.timeBetweenFilterChanges;
			m_InputTimeBetweenChanges->SetValue(wxString::Format("%d", (int)pd.timeBetweenFilterChanges));

			// update the default values
			m_DefTimeToRespond->SetLabel(wxString::Format("Default: %d", (int)pd.timeToRespond));
			m_DefTimeToRespond->Refresh();
			m_DefResponseTimeout->SetLabel(wxString::Format("Default: %d", (int)pd.timeResponseTimeout));
			m_DefResponseTimeout->Refresh();
			m_DefInterval->SetLabel(wxString::Format("Default: %d", (int)pd.timeInterval));
			m_DefInterval->Refresh();
			m_DefTimeBetweenChanges->SetLabel(wxString::Format("Default: %d", (int)pd.timeBetweenFilterChanges));
			m_DefTimeBetweenChanges->Refresh();

			sys::AppData::Instance()->UpdatePhase(pd.name, pd);
		}
	}
}

/////////////////////////////////////////////////////
/// OnBGColChange
/// Desc: Color event change for the text BG
/////////////////////////////////////////////////////
void DlgSettings::OnBGColChange(wxColourPickerEvent& WXUNUSED(event))
{
	wxColour col = m_PickerTextBG->GetColour();

	sys::AppData::Instance()->SetBGColour(col);

}

/////////////////////////////////////////////////////
/// OnTextColChange
/// Desc: Color event change for the program text
/////////////////////////////////////////////////////
void DlgSettings::OnTextColChange(wxColourPickerEvent& WXUNUSED(event))
{
	wxColour col = m_PickerText->GetColour();

	sys::AppData::Instance()->SetTextColour(col);

}

/////////////////////////////////////////////////////
/// OnTextBorderColChange
/// Desc: Color event change for the text border
/////////////////////////////////////////////////////
void DlgSettings::OnTextBorderColChange(wxColourPickerEvent& WXUNUSED(event))
{
	wxColour col = m_PickerBorder->GetColour();

	sys::AppData::Instance()->SetTextBorderColour(col);
}

/////////////////////////////////////////////////////
/// OnTextBorderColChange
/// Desc: Color event change for the tutorial/info text
/////////////////////////////////////////////////////
void DlgSettings::OnTextInfoColChange(wxColourPickerEvent& WXUNUSED(event))
{
	wxColour col = m_PickerInfoText->GetColour();

	sys::AppData::Instance()->SetTextInfoColour(col);
}

/////////////////////////////////////////////////////
/// BtnResetBGClick
/// Desc: Button event to reset the BG color
/////////////////////////////////////////////////////
void DlgSettings::BtnResetBGClick(wxCommandEvent& WXUNUSED(event))
{
	wxColour col;
	col.Set(0, 0, 0);

	m_PickerTextBG->SetColour(col);
	sys::AppData::Instance()->SetBGColour(col);
}

/////////////////////////////////////////////////////
/// BtnResetTextClick
/// Desc: Button event to reset the text color
/////////////////////////////////////////////////////
void DlgSettings::BtnResetTextClick(wxCommandEvent& WXUNUSED(event))
{
	wxColour col;
	col.Set(255, 255, 255);

	m_PickerText->SetColour(col);
	sys::AppData::Instance()->SetTextColour(col);
}

/////////////////////////////////////////////////////
/// BtnResetTextBorderClick
/// Desc: Button event to reset the text border color
/////////////////////////////////////////////////////
void DlgSettings::BtnResetTextBorderClick(wxCommandEvent& WXUNUSED(event))
{
	wxColour col;
	col.Set(255, 255, 255);

	m_PickerBorder->SetColour(col);
	sys::AppData::Instance()->SetTextBorderColour(col);
}

/////////////////////////////////////////////////////
/// BtnResetTextInfoClick
/// Desc: Button event to reset the tutorial/info text color
/////////////////////////////////////////////////////
void DlgSettings::BtnResetTextInfoClick(wxCommandEvent& WXUNUSED(event))
{
	wxColour col;
	col.Set(255, 255, 255);

	m_PickerInfoText->SetColour(col);
	sys::AppData::Instance()->SetTextInfoColour(col);
}

/////////////////////////////////////////////////////
/// OnChangeFontSize
/// Desc: SpinCtrl event for changing the value
/////////////////////////////////////////////////////
void DlgSettings::OnChangeFontSize(wxSpinEvent& WXUNUSED(event))
{
	int val = m_FontSizeControl->GetValue();

	sys::AppData::Instance()->SetTextFontSize(val);
}

/////////////////////////////////////////////////////
/// OnBorderThicknessChange
/// Desc: SpinCtrl event for changing the border thickness value
/////////////////////////////////////////////////////
void DlgSettings::OnBorderThicknessChange(wxSpinEvent& WXUNUSED(event))
{
	int val = m_BorderThicknessCtrl->GetValue();

	sys::AppData::Instance()->SetTextBorderThickness(val);
}

/////////////////////////////////////////////////////
/// OnBorderThicknessChangeTxt
/// Desc: SpinCtrl event for changing the border thickness value
/////////////////////////////////////////////////////
void DlgSettings::OnBorderThicknessChangeTxt(wxCommandEvent& WXUNUSED(event))
{
	wxSpinEvent e;
	OnBorderThicknessChange(e);
}

/////////////////////////////////////////////////////
/// BtnResetTextBorderThicknessClick
/// Desc: Button event for resetting the border thickness
/////////////////////////////////////////////////////
void DlgSettings::BtnResetTextBorderThicknessClick(wxCommandEvent& WXUNUSED(event))
{
	m_BorderThicknessCtrl->SetValue(ORIGINAL_BORDER_THICKNESS);
	sys::AppData::Instance()->SetTextBorderThickness(ORIGINAL_BORDER_THICKNESS);
}

/////////////////////////////////////////////////////
/// OnBorderWidthChange
/// Desc: SpinCtrl event for changing the border width value
/////////////////////////////////////////////////////
void DlgSettings::OnBorderWidthChange(wxSpinEvent& WXUNUSED(event))
{
	int val = m_BorderWidthCtrl->GetValue();

	sys::AppData::Instance()->SetTextBorderWidth(val);
}

/////////////////////////////////////////////////////
/// OnBorderWidthChangeTxt
/// Desc: SpinCtrl event for changing the border width value
/////////////////////////////////////////////////////
void DlgSettings::OnBorderWidthChangeTxt(wxCommandEvent& WXUNUSED(event))
{
	wxSpinEvent e;
	OnBorderWidthChange(e);
}

/////////////////////////////////////////////////////
/// BtnResetTextBorderWidthClick
/// Desc: Button event for resetting the border width
/////////////////////////////////////////////////////
void DlgSettings::BtnResetTextBorderWidthClick(wxCommandEvent& WXUNUSED(event))
{
	m_BorderWidthCtrl->SetValue(ORIGINAL_BORDER_SPACE_W);
	sys::AppData::Instance()->SetTextBorderWidth(ORIGINAL_BORDER_SPACE_W);
}

/////////////////////////////////////////////////////
/// OnBorderHeightChange
/// Desc: SpinCtrl event for changing the border height value
/////////////////////////////////////////////////////
void DlgSettings::OnBorderHeightChange(wxSpinEvent& WXUNUSED(event))
{
	int val = m_BorderHeightCtrl->GetValue();

	sys::AppData::Instance()->SetTextBorderHeight(val);
}

/////////////////////////////////////////////////////
/// OnBorderHeightChangeTxt
/// Desc: SpinCtrl event for changing the border height value
/////////////////////////////////////////////////////
void DlgSettings::OnBorderHeightChangeTxt(wxCommandEvent& WXUNUSED(event))
{
	wxSpinEvent e;
	OnBorderHeightChange(e);
}


/////////////////////////////////////////////////////
/// BtnResetTextBorderWidthClick
/// Desc: Button event for resetting the border height
/////////////////////////////////////////////////////
void DlgSettings::BtnResetTextBorderHeightClick(wxCommandEvent& WXUNUSED(event))
{
	m_BorderHeightCtrl->SetValue(ORIGINAL_BORDER_SPACE_H);
	sys::AppData::Instance()->SetTextBorderHeight(ORIGINAL_BORDER_SPACE_H);
}

/////////////////////////////////////////////////////
/// BtnDoneClick
/// Desc: Button event for saving/accepting changes and closing the dialog
/////////////////////////////////////////////////////
void DlgSettings::BtnDoneClick(wxCommandEvent& WXUNUSED(event))
{
	int sel = m_ComboPhase->GetSelection();
	auto itemSelection = m_PhaseMap.find(sel);
	if (itemSelection != m_PhaseMap.end())
	{
		const PhaseData pd = sys::AppData::Instance()->FindPhase(itemSelection->second);
		if (pd.val != -1)
		{
			if (pd.val == VERSION_ONE)
			{
				sys::AppData::Instance()->SetTutTxtVersion1(m_TextCtrlTutorial->GetValue());

				sys::AppData::Instance()->SetTut1Ex1(m_TextControlEx1->GetValue());
				sys::AppData::Instance()->SetTut1Ex2(m_TextControlEx2->GetValue());
				sys::AppData::Instance()->SetTut1Ex3(m_TextControlEx3->GetValue());
			}
			else
			if (pd.val == VERSION_TWO)
			{
				sys::AppData::Instance()->SetTutTxtVersion2(m_TextCtrlTutorial->GetValue());

				sys::AppData::Instance()->SetTut2Ex1(m_TextControlEx1->GetValue());
				sys::AppData::Instance()->SetTut2Ex2(m_TextControlEx2->GetValue());
				sys::AppData::Instance()->SetTut2Ex3(m_TextControlEx3->GetValue());
			}
			else
			if (pd.val == VERSION_THREE || pd.val == VERSION_FOUR)
			{
				sys::AppData::Instance()->SetTutTxtVersion3And4(m_TextCtrlTutorial->GetValue());

				if (pd.val == VERSION_THREE)
				{
					sys::AppData::Instance()->SetTut3Ex1(m_TextControlEx1->GetValue());
					sys::AppData::Instance()->SetTut3Ex2(m_TextControlEx2->GetValue());
					sys::AppData::Instance()->SetTut3Ex3(m_TextControlEx3->GetValue());
				}
				else if (pd.val == VERSION_FOUR)
				{
					sys::AppData::Instance()->SetTut4Ex1(m_TextControlEx1->GetValue());
					sys::AppData::Instance()->SetTut4Ex2(m_TextControlEx2->GetValue());
					sys::AppData::Instance()->SetTut4Ex3(m_TextControlEx3->GetValue());
				}
			}

			sys::AppData::Instance()->SetTxtEx1(m_TextControlTut1->GetValue());
			sys::AppData::Instance()->SetTxtEx2(m_TextControlTut2->GetValue());
			sys::AppData::Instance()->SetTxtEx3(m_TextControlTut3->GetValue());

			if (pd.val != VERSION_TWO)
				sys::AppData::Instance()->SetInputText1(m_TextControlCounts->GetValue());
			else
				sys::AppData::Instance()->SetInputText2(m_TextControlCounts->GetValue());

			if (pd.val == VERSION_ONE)
				sys::AppData::Instance()->SetEndingText1(m_TextCtrlTutorialEnd->GetValue());
			else
				sys::AppData::Instance()->SetEndingText2(m_TextCtrlTutorialEnd->GetValue());

			sys::AppData::Instance()->SetWaitText(m_TextControlWait->GetValue());
		}
	}

	sys::AppData::Instance()->SetAdminPasswordState(m_CheckUseAdminPass->GetValue());

	sys::AppData::Instance()->SetBGColour( m_PickerTextBG->GetColour() );
	sys::AppData::Instance()->SetTextColour( m_PickerText->GetColour() );
	sys::AppData::Instance()->SetTextBorderColour(m_PickerBorder->GetColour());
	sys::AppData::Instance()->SetTextInfoColour(m_PickerInfoText->GetColour());

	sys::AppData::Instance()->SetTextBorderThickness(m_BorderThicknessCtrl->GetValue());
	sys::AppData::Instance()->SetTextBorderWidth(m_BorderWidthCtrl->GetValue());
	sys::AppData::Instance()->SetTextBorderHeight(m_BorderHeightCtrl->GetValue());

	sys::AppData::Instance()->SaveSettings(sys::AppData::Instance()->defaultSettingsFile);

	this->EndModal(RESPONSE_OK);
}
