#ifndef __PanelParticipant__
#define __PanelParticipant__

/**
@file
Subclass of Participant, which is generated by wxFormBuilder.
*/

#include "Common.h"
#include "TECSApp.h"

//// end generated include

/** Implementing Participant */
class PanelParticipant : public Participant
{
	public:
		/** Constructor */
		PanelParticipant( wxWindow* parent );
	//// end generated class members

	private:
		bool CheckProgramValidity(ProgramFileInfo& pf);

	protected:
		virtual void InputIdKeyUp(wxKeyEvent& event);
		virtual void NumTestKeyUp(wxKeyEvent& event);
		virtual void OnChangePhase(wxCommandEvent& event);
		virtual void BtnBackClick(wxCommandEvent& event);
		virtual void BtnStartClick(wxCommandEvent& event);

	private:
		std::map<wxString, ParticipantFileInfo> m_FileMap;
		std::map<int, int> m_PhaseMap;

		std::map<wxString, ProgramFileInfo> m_ProgramFileMap;
		std::map<wxString, ProgramFileInfo> m_FilteredProgramFileMap;

		bool m_ValidProgram;
};

#endif // __PanelParticipant__
