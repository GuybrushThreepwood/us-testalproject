///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1-0-g8feb16b3)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "TECSApp.h"

///////////////////////////////////////////////////////////////////////////

MainFrame::MainFrame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( 800,600 ), wxDefaultSize );

	m_MenuBarMain = new wxMenuBar( 0 );
	m_MenuSettings = new wxMenu();
	wxMenuItem* m_BtnSettings;
	m_BtnSettings = new wxMenuItem( m_MenuSettings, wxID_ANY, wxString( _("Settings") ) , wxEmptyString, wxITEM_NORMAL );
	m_MenuSettings->Append( m_BtnSettings );

	m_MenuBarMain->Append( m_MenuSettings, _("Settings") );

	m_MenuAbout = new wxMenu();
	wxMenuItem* m_BtnMenuAbout;
	m_BtnMenuAbout = new wxMenuItem( m_MenuAbout, wxID_ABOUT, wxString( _("About TECS") ) , wxEmptyString, wxITEM_NORMAL );
	m_MenuAbout->Append( m_BtnMenuAbout );

	m_MenuBarMain->Append( m_MenuAbout, _("About") );

	this->SetMenuBar( m_MenuBarMain );

	bMainFrameSizer = new wxBoxSizer( wxVERTICAL );


	this->SetSizer( bMainFrameSizer );
	this->Layout();
	m_StatusBarMain = this->CreateStatusBar( 1, wxSTB_SIZEGRIP, wxID_ANY );
	m_StatusBarMain->Hide();


	this->Centre( wxBOTH );

	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( MainFrame::CloseEvent ) );
	this->Connect( wxEVT_IDLE, wxIdleEventHandler( MainFrame::IdleEvent ) );
	this->Connect( wxEVT_SIZE, wxSizeEventHandler( MainFrame::SizeEvent ) );
	m_MenuSettings->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrame::MenuSettingsClick ), this, m_BtnSettings->GetId());
	m_MenuAbout->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrame::MenuAboutClick ), this, m_BtnMenuAbout->GetId());
}

MainFrame::~MainFrame()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( MainFrame::CloseEvent ) );
	this->Disconnect( wxEVT_IDLE, wxIdleEventHandler( MainFrame::IdleEvent ) );
	this->Disconnect( wxEVT_SIZE, wxSizeEventHandler( MainFrame::SizeEvent ) );

}

PhasePrograms::PhasePrograms( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* bRootSizer;
	bRootSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer21;
	bSizer21 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtPhaseFilter = new wxStaticText( this, wxID_ANY, _("Phase Filter"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtPhaseFilter->Wrap( -1 );
	bSizer21->Add( m_TxtPhaseFilter, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	wxArrayString m_ComboPhaseChoices;
	m_ComboPhase = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_ComboPhaseChoices, 0 );
	m_ComboPhase->SetSelection( 0 );
	bSizer21->Add( m_ComboPhase, 1, wxALL, 5 );

	m_BtnEditPhases = new wxButton( this, wxID_ANY, _("Edit Phases"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnEditPhases->Hide();

	bSizer21->Add( m_BtnEditPhases, 0, wxALL, 5 );


	bRootSizer->Add( bSizer21, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer63;
	bSizer63 = new wxBoxSizer( wxVERTICAL );

	m_PhaseProgramList = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	// Grid
	m_PhaseProgramList->CreateGrid( 0, 5 );
	m_PhaseProgramList->EnableEditing( false );
	m_PhaseProgramList->EnableGridLines( true );
	m_PhaseProgramList->EnableDragGridSize( false );
	m_PhaseProgramList->SetMargins( 0, 0 );

	// Columns
	m_PhaseProgramList->AutoSizeColumns();
	m_PhaseProgramList->EnableDragColMove( false );
	m_PhaseProgramList->EnableDragColSize( true );
	m_PhaseProgramList->SetColLabelSize( 30 );
	m_PhaseProgramList->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	m_PhaseProgramList->EnableDragRowSize( true );
	m_PhaseProgramList->SetRowLabelSize( 80 );
	m_PhaseProgramList->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	m_PhaseProgramList->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	m_PhaseProgramList->SetToolTip( _("Single click to select a program, double click to edit") );

	bSizer63->Add( m_PhaseProgramList, 1, wxALL|wxEXPAND, 5 );


	bRootSizer->Add( bSizer63, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer20;
	bSizer20 = new wxBoxSizer( wxHORIZONTAL );

	m_BtnCreate = new wxButton( this, wxID_ANY, _("Create"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer20->Add( m_BtnCreate, 0, wxALL, 5 );


	bSizer20->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnEdit = new wxButton( this, wxID_ANY, _("Edit"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnEdit->SetToolTip( _("Edit selected program") );

	bSizer20->Add( m_BtnEdit, 0, wxALL, 5 );


	bSizer20->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnCopy = new wxButton( this, wxID_ANY, _("Copy"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnCopy->SetToolTip( _("Copy the selected program") );

	bSizer20->Add( m_BtnCopy, 0, wxALL, 5 );


	bSizer20->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnDelete = new wxButton( this, wxID_ANY, _("Delete"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnDelete->SetToolTip( _("Delete the selected program") );

	bSizer20->Add( m_BtnDelete, 0, wxALL, 5 );


	bRootSizer->Add( bSizer20, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer22;
	bSizer22 = new wxBoxSizer( wxVERTICAL );


	bSizer22->Add( 0, 0, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer23;
	bSizer23 = new wxBoxSizer( wxHORIZONTAL );

	m_BtnBack = new wxButton( this, wxID_ANY, _("< Back"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer23->Add( m_BtnBack, 0, wxALL, 5 );


	bSizer22->Add( bSizer23, 1, wxEXPAND, 5 );


	bRootSizer->Add( bSizer22, 0, wxEXPAND, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();

	// Connect Events
	m_ComboPhase->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( PhasePrograms::PhaseFilterChange ), NULL, this );
	m_BtnEditPhases->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( PhasePrograms::BtnEditPhasesClick ), NULL, this );
	m_PhaseProgramList->Connect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( PhasePrograms::GridCellLeftDoubleClick ), NULL, this );
	m_BtnCreate->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( PhasePrograms::BtnCreateClick ), NULL, this );
	m_BtnEdit->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( PhasePrograms::BtnEditClick ), NULL, this );
	m_BtnCopy->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( PhasePrograms::BtnCopyClick ), NULL, this );
	m_BtnDelete->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( PhasePrograms::BtnDeleteClick ), NULL, this );
	m_BtnBack->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( PhasePrograms::BtnBackClick ), NULL, this );
}

PhasePrograms::~PhasePrograms()
{
	// Disconnect Events
	m_ComboPhase->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( PhasePrograms::PhaseFilterChange ), NULL, this );
	m_BtnEditPhases->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( PhasePrograms::BtnEditPhasesClick ), NULL, this );
	m_PhaseProgramList->Disconnect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( PhasePrograms::GridCellLeftDoubleClick ), NULL, this );
	m_BtnCreate->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( PhasePrograms::BtnCreateClick ), NULL, this );
	m_BtnEdit->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( PhasePrograms::BtnEditClick ), NULL, this );
	m_BtnCopy->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( PhasePrograms::BtnCopyClick ), NULL, this );
	m_BtnDelete->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( PhasePrograms::BtnDeleteClick ), NULL, this );
	m_BtnBack->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( PhasePrograms::BtnBackClick ), NULL, this );

}

Bank::Bank( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	bRootSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer41;
	bSizer41 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer4221;
	bSizer4221 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtBankName = new wxStaticText( this, wxID_ANY, _("Current Bank"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT|wxALIGN_CENTER_HORIZONTAL );
	m_TxtBankName->Wrap( -1 );
	bSizer4221->Add( m_TxtBankName, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_BankName = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_BankName->Enable( false );
	m_BankName->SetToolTip( _("Selected bank name") );

	bSizer4221->Add( m_BankName, 1, wxALL|wxEXPAND, 5 );


	bSizer41->Add( bSizer4221, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer42;
	bSizer42 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtFilter = new wxStaticText( this, wxID_ANY, _("Bank Filter"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT );
	m_TxtFilter->Wrap( -1 );
	bSizer42->Add( m_TxtFilter, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	wxArrayString m_ComboFilterChoices;
	m_ComboFilter = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_ComboFilterChoices, 0 );
	m_ComboFilter->SetSelection( 0 );
	bSizer42->Add( m_ComboFilter, 1, wxALL, 5 );

	m_BtnEditFilters = new wxButton( this, wxID_ANY, _("Edit Filters"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer42->Add( m_BtnEditFilters, 0, wxALL, 5 );


	bSizer41->Add( bSizer42, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer421;
	bSizer421 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtSearch = new wxStaticText( this, wxID_ANY, _("Search"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT );
	m_TxtSearch->Wrap( -1 );
	bSizer421->Add( m_TxtSearch, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_Search = new wxSearchCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	#ifndef __WXMAC__
	m_Search->ShowSearchButton( false );
	#endif
	m_Search->ShowCancelButton( true );
	m_Search->SetToolTip( _("Filter the bank searching for specific word(s) (note: case sensitive)") );

	bSizer421->Add( m_Search, 1, wxALL|wxEXPAND, 5 );


	bSizer41->Add( bSizer421, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer422;
	bSizer422 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtFilterCount = new wxStaticText( this, wxID_ANY, _("Filter by count"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT|wxALIGN_CENTER_HORIZONTAL );
	m_TxtFilterCount->Wrap( -1 );
	bSizer422->Add( m_TxtFilterCount, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_WordCount = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_WordCount->SetToolTip( _("Filter the bank by word count") );

	m_WordCount->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &m_ValWordCount ) );

	bSizer422->Add( m_WordCount, 1, wxALL|wxEXPAND, 5 );


	bSizer41->Add( bSizer422, 0, wxEXPAND, 5 );


	bRootSizer->Add( bSizer41, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer60;
	bSizer60 = new wxBoxSizer( wxVERTICAL );

	m_WordBankGrid = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	// Grid
	m_WordBankGrid->CreateGrid( 0, 3 );
	m_WordBankGrid->EnableEditing( false );
	m_WordBankGrid->EnableGridLines( true );
	m_WordBankGrid->EnableDragGridSize( false );
	m_WordBankGrid->SetMargins( 0, 0 );

	// Columns
	m_WordBankGrid->AutoSizeColumns();
	m_WordBankGrid->EnableDragColMove( false );
	m_WordBankGrid->EnableDragColSize( true );
	m_WordBankGrid->SetColLabelSize( 30 );
	m_WordBankGrid->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	m_WordBankGrid->AutoSizeRows();
	m_WordBankGrid->EnableDragRowSize( true );
	m_WordBankGrid->SetRowLabelSize( 80 );
	m_WordBankGrid->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	m_WordBankGrid->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	bSizer60->Add( m_WordBankGrid, 1, wxALL|wxEXPAND, 5 );


	bRootSizer->Add( bSizer60, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer34;
	bSizer34 = new wxBoxSizer( wxHORIZONTAL );

	m_BtnAdd = new wxButton( this, wxID_ANY, _("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer34->Add( m_BtnAdd, 0, wxALL, 5 );

	m_BtnAddWithChildren = new wxButton( this, wxID_ANY, _("Add Related Sentences (v3)"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnAddWithChildren->SetToolTip( _("Add 3 related sentences to the bank at once") );

	bSizer34->Add( m_BtnAddWithChildren, 0, wxALL, 5 );

	m_BtnEdit = new wxButton( this, wxID_ANY, _("Edit"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnEdit->SetToolTip( _("Edit selected word in the bank") );

	bSizer34->Add( m_BtnEdit, 0, wxALL, 5 );


	bSizer34->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnDelete = new wxButton( this, wxID_ANY, _("Delete"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnDelete->SetToolTip( _("Delete selected word from the bank") );

	bSizer34->Add( m_BtnDelete, 0, wxALL, 5 );


	bSizer34->Add( 0, 0, 1, wxEXPAND, 5 );


	bSizer34->Add( 0, 0, 1, wxEXPAND, 5 );


	bSizer34->Add( 0, 0, 1, wxEXPAND, 5 );


	bSizer34->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnExport = new wxButton( this, wxID_ANY, _("Export"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnExport->SetToolTip( _("Export the bank to an Excel sheet") );

	bSizer34->Add( m_BtnExport, 0, wxALL, 5 );


	bRootSizer->Add( bSizer34, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer22;
	bSizer22 = new wxBoxSizer( wxVERTICAL );


	bSizer22->Add( 0, 0, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer23;
	bSizer23 = new wxBoxSizer( wxHORIZONTAL );

	m_BtnBack = new wxButton( this, wxID_ANY, _("< Back"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer23->Add( m_BtnBack, 0, wxALL, 5 );


	bSizer22->Add( bSizer23, 1, wxEXPAND, 5 );


	bRootSizer->Add( bSizer22, 0, wxEXPAND, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();

	// Connect Events
	m_BankName->Connect( wxEVT_KEY_UP, wxKeyEventHandler( Bank::WordCountKeyUp ), NULL, this );
	m_ComboFilter->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Bank::BankFilterChange ), NULL, this );
	m_BtnEditFilters->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Bank::BtnEditFiltersClick ), NULL, this );
	m_Search->Connect( wxEVT_COMMAND_SEARCHCTRL_CANCEL_BTN, wxCommandEventHandler( Bank::BankSearchCancel ), NULL, this );
	m_Search->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( Bank::BankSearch ), NULL, this );
	m_WordCount->Connect( wxEVT_KEY_UP, wxKeyEventHandler( Bank::WordCountKeyUp ), NULL, this );
	m_WordBankGrid->Connect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( Bank::GridCellLeftClick ), NULL, this );
	m_WordBankGrid->Connect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( Bank::GridCellLeftDoubleClick ), NULL, this );
	m_WordBankGrid->Connect( wxEVT_GRID_RANGE_SELECT, wxGridRangeSelectEventHandler( Bank::GridRangeSelect ), NULL, this );
	m_BtnAdd->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Bank::BtnAddClick ), NULL, this );
	m_BtnAddWithChildren->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Bank::BtnAddWithChildrenClick ), NULL, this );
	m_BtnEdit->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Bank::BtnEditClick ), NULL, this );
	m_BtnDelete->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Bank::BtnDeleteClick ), NULL, this );
	m_BtnExport->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Bank::BtnExportClick ), NULL, this );
	m_BtnBack->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Bank::BtnBackClick ), NULL, this );
}

Bank::~Bank()
{
	// Disconnect Events
	m_BankName->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( Bank::WordCountKeyUp ), NULL, this );
	m_ComboFilter->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Bank::BankFilterChange ), NULL, this );
	m_BtnEditFilters->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Bank::BtnEditFiltersClick ), NULL, this );
	m_Search->Disconnect( wxEVT_COMMAND_SEARCHCTRL_CANCEL_BTN, wxCommandEventHandler( Bank::BankSearchCancel ), NULL, this );
	m_Search->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( Bank::BankSearch ), NULL, this );
	m_WordCount->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( Bank::WordCountKeyUp ), NULL, this );
	m_WordBankGrid->Disconnect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( Bank::GridCellLeftClick ), NULL, this );
	m_WordBankGrid->Disconnect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( Bank::GridCellLeftDoubleClick ), NULL, this );
	m_WordBankGrid->Disconnect( wxEVT_GRID_RANGE_SELECT, wxGridRangeSelectEventHandler( Bank::GridRangeSelect ), NULL, this );
	m_BtnAdd->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Bank::BtnAddClick ), NULL, this );
	m_BtnAddWithChildren->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Bank::BtnAddWithChildrenClick ), NULL, this );
	m_BtnEdit->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Bank::BtnEditClick ), NULL, this );
	m_BtnDelete->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Bank::BtnDeleteClick ), NULL, this );
	m_BtnExport->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Bank::BtnExportClick ), NULL, this );
	m_BtnBack->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Bank::BtnBackClick ), NULL, this );

}

BankSelection::BankSelection( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* bRootSizer;
	bRootSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer181;
	bSizer181 = new wxBoxSizer( wxVERTICAL );

	m_TxtSelectBank = new wxStaticText( this, wxID_ANY, _("Select Bank"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtSelectBank->Wrap( -1 );
	bSizer181->Add( m_TxtSelectBank, 0, wxALL, 5 );

	wxBoxSizer* bSizer189;
	bSizer189 = new wxBoxSizer( wxVERTICAL );

	m_BankFileList = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	// Grid
	m_BankFileList->CreateGrid( 0, 4 );
	m_BankFileList->EnableEditing( false );
	m_BankFileList->EnableGridLines( true );
	m_BankFileList->EnableDragGridSize( false );
	m_BankFileList->SetMargins( 0, 0 );

	// Columns
	m_BankFileList->AutoSizeColumns();
	m_BankFileList->EnableDragColMove( false );
	m_BankFileList->EnableDragColSize( true );
	m_BankFileList->SetColLabelSize( 30 );
	m_BankFileList->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	m_BankFileList->AutoSizeRows();
	m_BankFileList->EnableDragRowSize( true );
	m_BankFileList->SetRowLabelSize( 80 );
	m_BankFileList->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	m_BankFileList->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	bSizer189->Add( m_BankFileList, 1, wxALL|wxEXPAND, 5 );


	bSizer181->Add( bSizer189, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer20;
	bSizer20 = new wxBoxSizer( wxHORIZONTAL );

	m_BtnCreate = new wxButton( this, wxID_ANY, _("Create"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnCreate->SetToolTip( _("Create new bank") );

	bSizer20->Add( m_BtnCreate, 0, wxALL, 5 );


	bSizer20->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnEdit = new wxButton( this, wxID_ANY, _("Edit"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnEdit->SetToolTip( _("Edit selected bank") );

	bSizer20->Add( m_BtnEdit, 0, wxALL, 5 );


	bSizer20->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnCopy = new wxButton( this, wxID_ANY, _("Copy"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnCopy->SetToolTip( _("Copy the selected bank") );

	bSizer20->Add( m_BtnCopy, 0, wxALL, 5 );


	bSizer20->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnDelete = new wxButton( this, wxID_ANY, _("Delete"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnDelete->SetToolTip( _("Delete the selected bank") );

	bSizer20->Add( m_BtnDelete, 0, wxALL, 5 );


	bSizer181->Add( bSizer20, 0, wxEXPAND, 5 );


	bRootSizer->Add( bSizer181, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer182;
	bSizer182 = new wxBoxSizer( wxVERTICAL );

	m_TxtBankPreview = new wxStaticText( this, wxID_ANY, _("Bank Preview"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtBankPreview->Wrap( -1 );
	bSizer182->Add( m_TxtBankPreview, 0, wxALL, 5 );

	wxBoxSizer* bSizer421;
	bSizer421 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtSearch = new wxStaticText( this, wxID_ANY, _("Search"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT );
	m_TxtSearch->Wrap( -1 );
	bSizer421->Add( m_TxtSearch, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_Search = new wxSearchCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	#ifndef __WXMAC__
	m_Search->ShowSearchButton( false );
	#endif
	m_Search->ShowCancelButton( true );
	m_Search->SetToolTip( _("Filter the bank searching for specific word(s) (note: case sensitive)") );

	bSizer421->Add( m_Search, 1, wxALL|wxEXPAND, 5 );


	bSizer182->Add( bSizer421, 0, wxEXPAND, 5 );

	m_BankPreview = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	// Grid
	m_BankPreview->CreateGrid( 0, 3 );
	m_BankPreview->EnableEditing( false );
	m_BankPreview->EnableGridLines( true );
	m_BankPreview->EnableDragGridSize( false );
	m_BankPreview->SetMargins( 0, 0 );

	// Columns
	m_BankPreview->AutoSizeColumns();
	m_BankPreview->EnableDragColMove( false );
	m_BankPreview->EnableDragColSize( true );
	m_BankPreview->SetColLabelSize( 30 );
	m_BankPreview->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	m_BankPreview->AutoSizeRows();
	m_BankPreview->EnableDragRowSize( true );
	m_BankPreview->SetRowLabelSize( 80 );
	m_BankPreview->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	m_BankPreview->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	bSizer182->Add( m_BankPreview, 1, wxALL|wxEXPAND, 5 );


	bRootSizer->Add( bSizer182, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer402111;
	bSizer402111 = new wxBoxSizer( wxHORIZONTAL );

	m_BtnBack = new wxButton( this, wxID_ANY, _("< Back"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer402111->Add( m_BtnBack, 0, wxALL, 5 );


	bSizer402111->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnSelectBank = new wxButton( this, wxID_ANY, _("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer402111->Add( m_BtnSelectBank, 0, wxALL, 5 );


	bRootSizer->Add( bSizer402111, 0, wxEXPAND, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();

	// Connect Events
	m_BankFileList->Connect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( BankSelection::FileGridCellLeftClick ), NULL, this );
	m_BankFileList->Connect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( BankSelection::FileGridCellLeftDoubleClick ), NULL, this );
	m_BtnCreate->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankSelection::BtnCreateClick ), NULL, this );
	m_BtnEdit->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankSelection::BtnEditClick ), NULL, this );
	m_BtnCopy->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankSelection::BtnCopyClick ), NULL, this );
	m_BtnDelete->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankSelection::BtnDeleteClick ), NULL, this );
	m_Search->Connect( wxEVT_COMMAND_SEARCHCTRL_CANCEL_BTN, wxCommandEventHandler( BankSelection::BankSearchCancel ), NULL, this );
	m_Search->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BankSelection::BankSearch ), NULL, this );
	m_BankPreview->Connect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( BankSelection::BankGridCellLeftClick ), NULL, this );
	m_BtnBack->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankSelection::BtnBackClick ), NULL, this );
	m_BtnSelectBank->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankSelection::BtnSelectClick ), NULL, this );
}

BankSelection::~BankSelection()
{
	// Disconnect Events
	m_BankFileList->Disconnect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( BankSelection::FileGridCellLeftClick ), NULL, this );
	m_BankFileList->Disconnect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( BankSelection::FileGridCellLeftDoubleClick ), NULL, this );
	m_BtnCreate->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankSelection::BtnCreateClick ), NULL, this );
	m_BtnEdit->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankSelection::BtnEditClick ), NULL, this );
	m_BtnCopy->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankSelection::BtnCopyClick ), NULL, this );
	m_BtnDelete->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankSelection::BtnDeleteClick ), NULL, this );
	m_Search->Disconnect( wxEVT_COMMAND_SEARCHCTRL_CANCEL_BTN, wxCommandEventHandler( BankSelection::BankSearchCancel ), NULL, this );
	m_Search->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BankSelection::BankSearch ), NULL, this );
	m_BankPreview->Disconnect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( BankSelection::BankGridCellLeftClick ), NULL, this );
	m_BtnBack->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankSelection::BtnBackClick ), NULL, this );
	m_BtnSelectBank->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankSelection::BtnSelectClick ), NULL, this );

}

ParticipantInfo::ParticipantInfo( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* bRootSizer;
	bRootSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer41;
	bSizer41 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer42;
	bSizer42 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtFilter = new wxStaticText( this, wxID_ANY, _("Phase Filter"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT );
	m_TxtFilter->Wrap( -1 );
	bSizer42->Add( m_TxtFilter, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	wxArrayString m_ComboFilterChoices;
	m_ComboFilter = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_ComboFilterChoices, 0 );
	m_ComboFilter->SetSelection( 0 );
	bSizer42->Add( m_ComboFilter, 1, wxALL, 5 );


	bSizer41->Add( bSizer42, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer421;
	bSizer421 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtSearch = new wxStaticText( this, wxID_ANY, _("Search"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT );
	m_TxtSearch->Wrap( -1 );
	bSizer421->Add( m_TxtSearch, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_Search = new wxSearchCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	#ifndef __WXMAC__
	m_Search->ShowSearchButton( true );
	#endif
	m_Search->ShowCancelButton( true );
	m_Search->SetToolTip( _("Filter participant results by program name containing word(s) (note: case sensitive)") );

	bSizer421->Add( m_Search, 1, wxALL|wxEXPAND, 5 );


	bSizer41->Add( bSizer421, 0, wxEXPAND, 5 );


	bRootSizer->Add( bSizer41, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer61;
	bSizer61 = new wxBoxSizer( wxVERTICAL );

	m_ParticipantGrid = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	// Grid
	m_ParticipantGrid->CreateGrid( 0, 4 );
	m_ParticipantGrid->EnableEditing( false );
	m_ParticipantGrid->EnableGridLines( true );
	m_ParticipantGrid->EnableDragGridSize( false );
	m_ParticipantGrid->SetMargins( 0, 0 );

	// Columns
	m_ParticipantGrid->AutoSizeColumns();
	m_ParticipantGrid->EnableDragColMove( false );
	m_ParticipantGrid->EnableDragColSize( true );
	m_ParticipantGrid->SetColLabelSize( 30 );
	m_ParticipantGrid->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	m_ParticipantGrid->AutoSizeRows();
	m_ParticipantGrid->EnableDragRowSize( true );
	m_ParticipantGrid->SetRowLabelSize( 80 );
	m_ParticipantGrid->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	m_ParticipantGrid->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	bSizer61->Add( m_ParticipantGrid, 1, wxALL|wxEXPAND, 5 );


	bRootSizer->Add( bSizer61, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer34;
	bSizer34 = new wxBoxSizer( wxHORIZONTAL );

	m_BtnView = new wxButton( this, wxID_ANY, _("View"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnView->SetToolTip( _("View selected program") );

	bSizer34->Add( m_BtnView, 0, wxALL, 5 );


	bSizer34->Add( 0, 0, 1, wxEXPAND, 5 );


	bSizer34->Add( 0, 0, 1, wxEXPAND, 5 );


	bSizer34->Add( 0, 0, 1, wxEXPAND, 5 );


	bSizer34->Add( 0, 0, 1, wxEXPAND, 5 );


	bSizer34->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnDelete = new wxButton( this, wxID_ANY, _("Delete"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer34->Add( m_BtnDelete, 0, wxALL, 5 );


	bRootSizer->Add( bSizer34, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer22;
	bSizer22 = new wxBoxSizer( wxVERTICAL );


	bSizer22->Add( 0, 0, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer23;
	bSizer23 = new wxBoxSizer( wxHORIZONTAL );

	m_BtnBack = new wxButton( this, wxID_ANY, _("< Back"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer23->Add( m_BtnBack, 0, wxALL, 5 );


	bSizer22->Add( bSizer23, 1, wxEXPAND, 5 );


	bRootSizer->Add( bSizer22, 0, wxEXPAND, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();

	// Connect Events
	m_ComboFilter->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( ParticipantInfo::PhaseFilterChange ), NULL, this );
	m_Search->Connect( wxEVT_COMMAND_SEARCHCTRL_CANCEL_BTN, wxCommandEventHandler( ParticipantInfo::IdSearchCancel ), NULL, this );
	m_Search->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( ParticipantInfo::IdSearch ), NULL, this );
	m_ParticipantGrid->Connect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( ParticipantInfo::GridCellLeftClick ), NULL, this );
	m_ParticipantGrid->Connect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( ParticipantInfo::GridCellLeftDoubleClick ), NULL, this );
	m_ParticipantGrid->Connect( wxEVT_GRID_RANGE_SELECT, wxGridRangeSelectEventHandler( ParticipantInfo::GridRangeSelect ), NULL, this );
	m_BtnView->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ParticipantInfo::BtnViewClick ), NULL, this );
	m_BtnDelete->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ParticipantInfo::BtnDeleteClick ), NULL, this );
	m_BtnBack->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ParticipantInfo::BtnBackClick ), NULL, this );
}

ParticipantInfo::~ParticipantInfo()
{
	// Disconnect Events
	m_ComboFilter->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( ParticipantInfo::PhaseFilterChange ), NULL, this );
	m_Search->Disconnect( wxEVT_COMMAND_SEARCHCTRL_CANCEL_BTN, wxCommandEventHandler( ParticipantInfo::IdSearchCancel ), NULL, this );
	m_Search->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( ParticipantInfo::IdSearch ), NULL, this );
	m_ParticipantGrid->Disconnect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( ParticipantInfo::GridCellLeftClick ), NULL, this );
	m_ParticipantGrid->Disconnect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( ParticipantInfo::GridCellLeftDoubleClick ), NULL, this );
	m_ParticipantGrid->Disconnect( wxEVT_GRID_RANGE_SELECT, wxGridRangeSelectEventHandler( ParticipantInfo::GridRangeSelect ), NULL, this );
	m_BtnView->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ParticipantInfo::BtnViewClick ), NULL, this );
	m_BtnDelete->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ParticipantInfo::BtnDeleteClick ), NULL, this );
	m_BtnBack->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ParticipantInfo::BtnBackClick ), NULL, this );

}

Tutorial::Tutorial( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	this->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_INFOTEXT ) );

	bRootSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer62;
	bSizer62 = new wxBoxSizer( wxVERTICAL );

	m_TxtTutorial = new wxRichTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY|wxWANTS_CHARS|wxBORDER_NONE );
	bSizer62->Add( m_TxtTutorial, 1, wxEXPAND | wxALL, 0 );


	bRootSizer->Add( bSizer62, 1, wxEXPAND, 5 );

	m_BtnContinue = new wxButton( this, wxID_ANY, _("Continue"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnContinue->SetMinSize( wxSize( 200,80 ) );

	bRootSizer->Add( m_BtnContinue, 0, wxALIGN_RIGHT|wxALL, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();

	// Connect Events
	this->Connect( wxEVT_KEY_UP, wxKeyEventHandler( Tutorial::KeyUpEvent ) );
	this->Connect( wxEVT_SIZE, wxSizeEventHandler( Tutorial::ResizePanel ) );
	m_BtnContinue->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Tutorial::BtnContinueClick ), NULL, this );
	m_BtnContinue->Connect( wxEVT_KEY_UP, wxKeyEventHandler( Tutorial::KeyUpEvent ), NULL, this );
}

Tutorial::~Tutorial()
{
	// Disconnect Events
	this->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( Tutorial::KeyUpEvent ) );
	this->Disconnect( wxEVT_SIZE, wxSizeEventHandler( Tutorial::ResizePanel ) );
	m_BtnContinue->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Tutorial::BtnContinueClick ), NULL, this );
	m_BtnContinue->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( Tutorial::KeyUpEvent ), NULL, this );

}

PhaseRun::PhaseRun( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	this->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_INFOTEXT ) );

	bRootSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer62;
	bSizer62 = new wxBoxSizer( wxVERTICAL );

	m_TxtTutorial = new wxRichTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY|wxWANTS_CHARS|wxBORDER_NONE );
	m_TxtTutorial->Hide();

	bSizer62->Add( m_TxtTutorial, 1, wxEXPAND | wxALL, 0 );


	bRootSizer->Add( bSizer62, 1, wxEXPAND, 5 );

	m_BtnContinue = new wxButton( this, wxID_ANY, _("Continue"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnContinue->Hide();
	m_BtnContinue->SetMinSize( wxSize( 200,80 ) );

	bRootSizer->Add( m_BtnContinue, 0, wxALIGN_RIGHT|wxALL, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();

	// Connect Events
	this->Connect( wxEVT_KEY_UP, wxKeyEventHandler( PhaseRun::KeyUpEvent ) );
	this->Connect( wxEVT_SIZE, wxSizeEventHandler( PhaseRun::ResizePanel ) );
	m_TxtTutorial->Connect( wxEVT_SIZE, wxSizeEventHandler( PhaseRun::OnTextAreaResize ), NULL, this );
	m_BtnContinue->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( PhaseRun::BtnContinueClick ), NULL, this );
	m_BtnContinue->Connect( wxEVT_KEY_UP, wxKeyEventHandler( PhaseRun::KeyUpEvent ), NULL, this );
}

PhaseRun::~PhaseRun()
{
	// Disconnect Events
	this->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( PhaseRun::KeyUpEvent ) );
	this->Disconnect( wxEVT_SIZE, wxSizeEventHandler( PhaseRun::ResizePanel ) );
	m_TxtTutorial->Disconnect( wxEVT_SIZE, wxSizeEventHandler( PhaseRun::OnTextAreaResize ), NULL, this );
	m_BtnContinue->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( PhaseRun::BtnContinueClick ), NULL, this );
	m_BtnContinue->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( PhaseRun::KeyUpEvent ), NULL, this );

}

Entry::Entry( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* bRootSizer;
	bRootSizer = new wxBoxSizer( wxVERTICAL );


	bRootSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnAdmin = new wxButton( this, wxID_ANY, _("Administrator"), wxDefaultPosition, wxDefaultSize, 0 );
	bRootSizer->Add( m_BtnAdmin, 1, wxALL|wxEXPAND, 5 );


	bRootSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnParticipant = new wxButton( this, wxID_ANY, _("Participant"), wxDefaultPosition, wxDefaultSize, 0 );
	bRootSizer->Add( m_BtnParticipant, 1, wxALL|wxEXPAND, 5 );


	bRootSizer->Add( 0, 0, 1, wxEXPAND, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();

	// Connect Events
	m_BtnAdmin->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Entry::BtnAdminClick ), NULL, this );
	m_BtnParticipant->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Entry::BtnParticipantClick ), NULL, this );
}

Entry::~Entry()
{
	// Disconnect Events
	m_BtnAdmin->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Entry::BtnAdminClick ), NULL, this );
	m_BtnParticipant->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Entry::BtnParticipantClick ), NULL, this );

}

Admin::Admin( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* bRootSizer;
	bRootSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bTopSizer;
	bTopSizer = new wxBoxSizer( wxVERTICAL );


	bTopSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnBank = new wxButton( this, wxID_ANY, _("Bank Management"), wxDefaultPosition, wxDefaultSize, 0 );
	bTopSizer->Add( m_BtnBank, 1, wxALL|wxEXPAND, 5 );


	bTopSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnPhasePrograms = new wxButton( this, wxID_ANY, _("Programs"), wxDefaultPosition, wxDefaultSize, 0 );
	bTopSizer->Add( m_BtnPhasePrograms, 1, wxALL|wxEXPAND, 5 );


	bTopSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnParticipantInfo = new wxButton( this, wxID_ANY, _("Stored Participant Records"), wxDefaultPosition, wxDefaultSize, 0 );
	bTopSizer->Add( m_BtnParticipantInfo, 1, wxALL|wxEXPAND, 5 );


	bTopSizer->Add( 0, 0, 1, wxEXPAND, 5 );


	bRootSizer->Add( bTopSizer, 1, wxEXPAND, 5 );

	wxBoxSizer* bBottomSizer;
	bBottomSizer = new wxBoxSizer( wxVERTICAL );

	m_BtnBack = new wxButton( this, wxID_ANY, _("< Back"), wxDefaultPosition, wxDefaultSize, 0 );
	bBottomSizer->Add( m_BtnBack, 0, wxALL, 5 );


	bRootSizer->Add( bBottomSizer, 0, wxEXPAND, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();

	// Connect Events
	m_BtnBank->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Admin::BtnBankManageClick ), NULL, this );
	m_BtnPhasePrograms->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Admin::BtnPhaseProgramClick ), NULL, this );
	m_BtnParticipantInfo->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Admin::BtnParticipantInfoButtonClick ), NULL, this );
	m_BtnBack->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Admin::BtnBackClick ), NULL, this );
}

Admin::~Admin()
{
	// Disconnect Events
	m_BtnBank->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Admin::BtnBankManageClick ), NULL, this );
	m_BtnPhasePrograms->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Admin::BtnPhaseProgramClick ), NULL, this );
	m_BtnParticipantInfo->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Admin::BtnParticipantInfoButtonClick ), NULL, this );
	m_BtnBack->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Admin::BtnBackClick ), NULL, this );

}

Questions::Questions( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* bRootSizer;
	bRootSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer174;
	bSizer174 = new wxBoxSizer( wxHORIZONTAL );

	wxBoxSizer* bUserInfo;
	bUserInfo = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bInformSizer;
	bInformSizer = new wxBoxSizer( wxVERTICAL );

	m_TxtQuestion = new wxStaticText( this, wxID_ANY, _("Please recall as many words and sentences in the box below.\n"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtQuestion->Wrap( -1 );
	m_TxtQuestion->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );

	bInformSizer->Add( m_TxtQuestion, 0, wxALL|wxEXPAND, 5 );


	bUserInfo->Add( bInformSizer, 0, wxEXPAND, 5 );

	wxBoxSizer* bUserInputSizer;
	bUserInputSizer = new wxBoxSizer( wxVERTICAL );

	m_InputText = new wxRichTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0|wxVSCROLL|wxHSCROLL|wxWANTS_CHARS|wxBORDER_NONE );
	bUserInputSizer->Add( m_InputText, 1, wxEXPAND | wxALL, 5 );


	bUserInfo->Add( bUserInputSizer, 1, wxEXPAND, 5 );


	bSizer174->Add( bUserInfo, 1, wxEXPAND, 5 );

	wxBoxSizer* bDataListSizer;
	bDataListSizer = new wxBoxSizer( wxVERTICAL );

	m_ProgramPanel = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer176;
	bSizer176 = new wxBoxSizer( wxVERTICAL );

	m_ProgramSearch = new wxSearchCtrl( m_ProgramPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	#ifndef __WXMAC__
	m_ProgramSearch->ShowSearchButton( false );
	#endif
	m_ProgramSearch->ShowCancelButton( true );
	m_ProgramSearch->SetToolTip( _("Filter program matching specific word(s) (note: case sensitive)") );

	bSizer176->Add( m_ProgramSearch, 0, wxALL|wxEXPAND, 2 );

	m_TxtAnswersTitle = new wxStaticText( m_ProgramPanel, wxID_ANY, _("Program"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtAnswersTitle->Wrap( -1 );
	bSizer176->Add( m_TxtAnswersTitle, 0, wxALL, 5 );

	m_ProgramData = new wxListCtrl( m_ProgramPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_HRULES|wxLC_REPORT|wxLC_SINGLE_SEL );
	bSizer176->Add( m_ProgramData, 1, wxEXPAND, 5 );


	m_ProgramPanel->SetSizer( bSizer176 );
	m_ProgramPanel->Layout();
	bSizer176->Fit( m_ProgramPanel );
	bDataListSizer->Add( m_ProgramPanel, 1, wxEXPAND | wxALL, 2 );


	bSizer174->Add( bDataListSizer, 1, wxEXPAND, 2 );


	bRootSizer->Add( bSizer174, 1, wxEXPAND, 5 );

	bMarkSizer = new wxBoxSizer( wxHORIZONTAL );

	m_CheckPanel = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer95;
	bSizer95 = new wxBoxSizer( wxVERTICAL );

	m_EvaluationTab = new wxNotebook( m_CheckPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	m_AnswerPage = new wxPanel( m_EvaluationTab, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bCountersRootSizer;
	bCountersRootSizer = new wxBoxSizer( wxVERTICAL );

	bCountersRootSizer->SetMinSize( wxSize( 200,-1 ) );
	m_CorrectValuesScroll = new wxScrolledWindow( m_AnswerPage, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxBORDER_SIMPLE );
	m_CorrectValuesScroll->SetScrollRate( 5, 5 );
	bFilterWindowSizer = new wxBoxSizer( wxVERTICAL );


	m_CorrectValuesScroll->SetSizer( bFilterWindowSizer );
	m_CorrectValuesScroll->Layout();
	bFilterWindowSizer->Fit( m_CorrectValuesScroll );
	bCountersRootSizer->Add( m_CorrectValuesScroll, 1, wxEXPAND | wxALL, 0 );


	m_AnswerPage->SetSizer( bCountersRootSizer );
	m_AnswerPage->Layout();
	bCountersRootSizer->Fit( m_AnswerPage );
	m_EvaluationTab->AddPage( m_AnswerPage, _("Answers"), false );
	m_CommentsPage = new wxPanel( m_EvaluationTab, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bCommentSizer;
	bCommentSizer = new wxBoxSizer( wxVERTICAL );

	bCommentSizer->SetMinSize( wxSize( 200,-1 ) );
	m_InputMark = new wxRichTextCtrl( m_CommentsPage, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0|wxVSCROLL|wxHSCROLL|wxWANTS_CHARS|wxBORDER_NONE );
	bCommentSizer->Add( m_InputMark, 1, wxEXPAND, 5 );


	m_CommentsPage->SetSizer( bCommentSizer );
	m_CommentsPage->Layout();
	bCommentSizer->Fit( m_CommentsPage );
	m_EvaluationTab->AddPage( m_CommentsPage, _("Comments"), false );

	bSizer95->Add( m_EvaluationTab, 1, wxEXPAND | wxALL, 0 );


	m_CheckPanel->SetSizer( bSizer95 );
	m_CheckPanel->Layout();
	bSizer95->Fit( m_CheckPanel );
	bMarkSizer->Add( m_CheckPanel, 1, wxEXPAND | wxALL, 0 );


	bRootSizer->Add( bMarkSizer, 1, wxEXPAND, 5 );

	wxBoxSizer* bBtnGrpSizer;
	bBtnGrpSizer = new wxBoxSizer( wxHORIZONTAL );


	bBtnGrpSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnEnd = new wxButton( this, wxID_ANY, _("Done"), wxDefaultPosition, wxDefaultSize, 0 );
	bBtnGrpSizer->Add( m_BtnEnd, 0, wxALL, 5 );


	bRootSizer->Add( bBtnGrpSizer, 0, wxEXPAND, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();
	bRootSizer->Fit( this );

	// Connect Events
	m_ProgramSearch->Connect( wxEVT_COMMAND_SEARCHCTRL_CANCEL_BTN, wxCommandEventHandler( Questions::ProgramSearchCancel ), NULL, this );
	m_ProgramSearch->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( Questions::ProgramSearch ), NULL, this );
	m_BtnEnd->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Questions::BtnDoneClick ), NULL, this );
}

Questions::~Questions()
{
	// Disconnect Events
	m_ProgramSearch->Disconnect( wxEVT_COMMAND_SEARCHCTRL_CANCEL_BTN, wxCommandEventHandler( Questions::ProgramSearchCancel ), NULL, this );
	m_ProgramSearch->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( Questions::ProgramSearch ), NULL, this );
	m_BtnEnd->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Questions::BtnDoneClick ), NULL, this );

}

Participant::Participant( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* bRootSizer;
	bRootSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer58;
	bSizer58 = new wxBoxSizer( wxVERTICAL );


	bSizer58->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtParticipantTitle = new wxStaticText( this, wxID_ANY, _("Participant Info"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtParticipantTitle->Wrap( -1 );
	m_TxtParticipantTitle->SetFont( wxFont( 30, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );

	bSizer58->Add( m_TxtParticipantTitle, 1, wxALIGN_CENTER|wxALIGN_CENTER_VERTICAL|wxALL, 5 );


	bSizer58->Add( 0, 0, 1, wxEXPAND, 5 );


	bRootSizer->Add( bSizer58, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer115;
	bSizer115 = new wxBoxSizer( wxHORIZONTAL );


	bSizer115->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtIDWarn = new wxStaticText( this, wxID_ANY, _("This ID is already in use"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER_HORIZONTAL );
	m_TxtIDWarn->Wrap( -1 );
	bSizer115->Add( m_TxtIDWarn, 1, wxALL|wxEXPAND, 5 );


	bSizer115->Add( 0, 0, 1, wxEXPAND, 5 );


	bRootSizer->Add( bSizer115, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer4021;
	bSizer4021 = new wxBoxSizer( wxHORIZONTAL );


	bSizer4021->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtPhaseSelect = new wxStaticText( this, wxID_ANY, _("Selected Phase"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtPhaseSelect->Wrap( -1 );
	bSizer4021->Add( m_TxtPhaseSelect, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	wxArrayString m_ComboPhaseChoices;
	m_ComboPhase = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_ComboPhaseChoices, 0 );
	m_ComboPhase->SetSelection( 0 );
	bSizer4021->Add( m_ComboPhase, 1, wxALL, 5 );


	bSizer4021->Add( 0, 0, 1, wxEXPAND, 5 );


	bRootSizer->Add( bSizer4021, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer40211;
	bSizer40211 = new wxBoxSizer( wxHORIZONTAL );


	bSizer40211->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtPhaseProgram = new wxStaticText( this, wxID_ANY, _("Selected Program"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtPhaseProgram->Wrap( -1 );
	bSizer40211->Add( m_TxtPhaseProgram, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	wxArrayString m_ComboProgramChoices;
	m_ComboProgram = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_ComboProgramChoices, 0 );
	m_ComboProgram->SetSelection( 0 );
	bSizer40211->Add( m_ComboProgram, 1, wxALL, 5 );


	bSizer40211->Add( 0, 0, 1, wxEXPAND, 5 );


	bRootSizer->Add( bSizer40211, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer40;
	bSizer40 = new wxBoxSizer( wxHORIZONTAL );


	bSizer40->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtIdentifier = new wxStaticText( this, wxID_ANY, _("ID"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtIdentifier->Wrap( -1 );
	bSizer40->Add( m_TxtIdentifier, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputId = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputId->SetToolTip( _("Numeric identifier for the participant (required)") );

	m_InputId->SetValidator( wxTextValidator( wxFILTER_NONE, &m_ValInputId ) );

	bSizer40->Add( m_InputId, 1, wxALL, 5 );


	bSizer40->Add( 0, 0, 1, wxEXPAND, 5 );


	bRootSizer->Add( bSizer40, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer401;
	bSizer401 = new wxBoxSizer( wxHORIZONTAL );


	bSizer401->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtDOB = new wxStaticText( this, wxID_ANY, _("D.o.B"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtDOB->Wrap( -1 );
	bSizer401->Add( m_TxtDOB, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_DateDOB = new wxDatePickerCtrl( this, wxID_ANY, wxDefaultDateTime, wxDefaultPosition, wxDefaultSize, wxDP_DROPDOWN|wxDP_SHOWCENTURY );
	m_DateDOB->SetToolTip( _("Participant date of birth") );

	bSizer401->Add( m_DateDOB, 1, wxALL, 5 );


	bSizer401->Add( 0, 0, 1, wxEXPAND, 5 );


	bRootSizer->Add( bSizer401, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer402;
	bSizer402 = new wxBoxSizer( wxHORIZONTAL );


	bSizer402->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtGender = new wxStaticText( this, wxID_ANY, _("Gender"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtGender->Wrap( -1 );
	bSizer402->Add( m_TxtGender, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	wxArrayString m_ComboGenderChoices;
	m_ComboGender = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_ComboGenderChoices, 0 );
	m_ComboGender->SetSelection( 0 );
	bSizer402->Add( m_ComboGender, 1, wxALL, 5 );


	bSizer402->Add( 0, 0, 1, wxEXPAND, 5 );


	bRootSizer->Add( bSizer402, 0, wxEXPAND, 5 );


	bRootSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer1151;
	bSizer1151 = new wxBoxSizer( wxHORIZONTAL );


	bSizer1151->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtNumericTesterInfo = new wxStaticText( this, wxID_ANY, _("Please use the space below to check the numeric keypad\nEnable or Disable the Num Lock key if the numbers are not written as you type\n\nPlease enter the number 456"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER_HORIZONTAL );
	m_TxtNumericTesterInfo->Wrap( -1 );
	bSizer1151->Add( m_TxtNumericTesterInfo, 1, wxALL|wxEXPAND, 5 );


	bSizer1151->Add( 0, 0, 1, wxEXPAND, 5 );


	bRootSizer->Add( bSizer1151, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer403;
	bSizer403 = new wxBoxSizer( wxHORIZONTAL );


	bSizer403->Add( 0, 0, 1, wxEXPAND, 5 );

	m_InputNumTest = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputNumTest->SetToolTip( _("Test numeric keys are working here") );

	m_InputNumTest->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &m_ValTestNumId ) );

	bSizer403->Add( m_InputNumTest, 1, wxALL, 5 );


	bSizer403->Add( 0, 0, 1, wxEXPAND, 5 );


	bRootSizer->Add( bSizer403, 0, wxEXPAND, 5 );


	bRootSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer402111;
	bSizer402111 = new wxBoxSizer( wxHORIZONTAL );

	m_BtnBack = new wxButton( this, wxID_ANY, _("< Back"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer402111->Add( m_BtnBack, 0, wxALL, 5 );


	bSizer402111->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnStart = new wxButton( this, wxID_ANY, _("Start"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer402111->Add( m_BtnStart, 0, wxALL, 5 );


	bRootSizer->Add( bSizer402111, 0, wxEXPAND, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();
	bRootSizer->Fit( this );

	// Connect Events
	m_ComboPhase->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Participant::OnChangePhase ), NULL, this );
	m_InputId->Connect( wxEVT_KEY_UP, wxKeyEventHandler( Participant::InputIdKeyUp ), NULL, this );
	m_InputNumTest->Connect( wxEVT_KEY_UP, wxKeyEventHandler( Participant::NumTestKeyUp ), NULL, this );
	m_BtnBack->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Participant::BtnBackClick ), NULL, this );
	m_BtnStart->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Participant::BtnStartClick ), NULL, this );
}

Participant::~Participant()
{
	// Disconnect Events
	m_ComboPhase->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Participant::OnChangePhase ), NULL, this );
	m_InputId->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( Participant::InputIdKeyUp ), NULL, this );
	m_InputNumTest->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( Participant::NumTestKeyUp ), NULL, this );
	m_BtnBack->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Participant::BtnBackClick ), NULL, this );
	m_BtnStart->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Participant::BtnStartClick ), NULL, this );

}

AddEditBankDlg::AddEditBankDlg( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bRootSizer;
	bRootSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer16;
	bSizer16 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtWordOrSentence = new wxStaticText( this, wxID_ANY, _("Word or Sentence"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtWordOrSentence->Wrap( -1 );
	bSizer16->Add( m_TxtWordOrSentence, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputWordOrSentence = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer16->Add( m_InputWordOrSentence, 1, wxALL, 5 );


	bRootSizer->Add( bSizer16, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer161;
	bSizer161 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtFilter = new wxStaticText( this, wxID_ANY, _("Filter"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtFilter->Wrap( -1 );
	bSizer161->Add( m_TxtFilter, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	wxArrayString m_ComboFilterChoices;
	m_ComboFilter = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_ComboFilterChoices, 0 );
	m_ComboFilter->SetSelection( 0 );
	bSizer161->Add( m_ComboFilter, 1, wxALL, 5 );


	bRootSizer->Add( bSizer161, 0, wxEXPAND, 5 );

	bMessageSizer = new wxBoxSizer( wxVERTICAL );

	m_TxtMessage = new wxStaticText( this, wxID_ANY, _("---"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtMessage->Wrap( -1 );
	bMessageSizer->Add( m_TxtMessage, 1, wxALIGN_CENTER|wxALIGN_CENTER_VERTICAL|wxALL, 5 );


	bRootSizer->Add( bMessageSizer, 0, wxEXPAND, 5 );


	bRootSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer40;
	bSizer40 = new wxBoxSizer( wxHORIZONTAL );


	bSizer40->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnCancel = new wxButton( this, wxID_ANY, _("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer40->Add( m_BtnCancel, 0, wxALL, 5 );

	m_BtnAddEdit = new wxButton( this, wxID_ANY, _("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer40->Add( m_BtnAddEdit, 0, wxALL, 5 );


	bRootSizer->Add( bSizer40, 0, wxEXPAND, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_InputWordOrSentence->Connect( wxEVT_KEY_UP, wxKeyEventHandler( AddEditBankDlg::KeyUpEvent ), NULL, this );
	m_BtnCancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( AddEditBankDlg::BtnCancelClick ), NULL, this );
	m_BtnAddEdit->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( AddEditBankDlg::BtnAddEditClick ), NULL, this );
}

AddEditBankDlg::~AddEditBankDlg()
{
	// Disconnect Events
	m_InputWordOrSentence->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( AddEditBankDlg::KeyUpEvent ), NULL, this );
	m_BtnCancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( AddEditBankDlg::BtnCancelClick ), NULL, this );
	m_BtnAddEdit->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( AddEditBankDlg::BtnAddEditClick ), NULL, this );

}

AddEditWithChildrenBankDlg::AddEditWithChildrenBankDlg( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bRootSizer;
	bRootSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer161;
	bSizer161 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtFilter = new wxStaticText( this, wxID_ANY, _("Filter"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtFilter->Wrap( -1 );
	bSizer161->Add( m_TxtFilter, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	wxArrayString m_ComboFilterChoices;
	m_ComboFilter = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_ComboFilterChoices, 0 );
	m_ComboFilter->SetSelection( 0 );
	bSizer161->Add( m_ComboFilter, 1, wxALL, 5 );


	bRootSizer->Add( bSizer161, 0, wxEXPAND, 5 );

	bListSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bInitialSizer1;
	bInitialSizer1 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtWordOrSentence1 = new wxStaticText( this, wxID_ANY, _("Sentence (4 words)"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtWordOrSentence1->Wrap( -1 );
	bInitialSizer1->Add( m_TxtWordOrSentence1, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputWordOrSentence1 = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bInitialSizer1->Add( m_InputWordOrSentence1, 1, wxALL, 5 );

	bMessageSizer1 = new wxBoxSizer( wxVERTICAL );

	m_TxtMessage1 = new wxStaticText( this, wxID_ANY, _("---"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtMessage1->Wrap( -1 );
	bMessageSizer1->Add( m_TxtMessage1, 1, wxALIGN_CENTER|wxALIGN_CENTER_VERTICAL|wxALL, 5 );


	bInitialSizer1->Add( bMessageSizer1, 1, wxEXPAND, 5 );


	bListSizer->Add( bInitialSizer1, 1, wxEXPAND, 5 );

	wxBoxSizer* bInitialSizer2;
	bInitialSizer2 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtWordOrSentence2 = new wxStaticText( this, wxID_ANY, _("Sentence (5 words)"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtWordOrSentence2->Wrap( -1 );
	bInitialSizer2->Add( m_TxtWordOrSentence2, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputWordOrSentence2 = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bInitialSizer2->Add( m_InputWordOrSentence2, 1, wxALL, 5 );

	bMessageSizer2 = new wxBoxSizer( wxVERTICAL );

	m_TxtMessage2 = new wxStaticText( this, wxID_ANY, _("---"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtMessage2->Wrap( -1 );
	bMessageSizer2->Add( m_TxtMessage2, 1, wxALIGN_CENTER|wxALIGN_CENTER_VERTICAL|wxALL, 5 );


	bInitialSizer2->Add( bMessageSizer2, 1, wxEXPAND, 5 );


	bListSizer->Add( bInitialSizer2, 1, wxEXPAND, 5 );

	wxBoxSizer* bInitialSizer3;
	bInitialSizer3 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtWordOrSentence3 = new wxStaticText( this, wxID_ANY, _("Sentence (6 words)"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtWordOrSentence3->Wrap( -1 );
	bInitialSizer3->Add( m_TxtWordOrSentence3, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputWordOrSentence3 = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bInitialSizer3->Add( m_InputWordOrSentence3, 1, wxALL, 5 );

	bMessageSizer3 = new wxBoxSizer( wxVERTICAL );

	m_TxtMessage3 = new wxStaticText( this, wxID_ANY, _("---"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtMessage3->Wrap( -1 );
	bMessageSizer3->Add( m_TxtMessage3, 1, wxALIGN_CENTER|wxALIGN_CENTER_VERTICAL|wxALL, 5 );


	bInitialSizer3->Add( bMessageSizer3, 1, wxEXPAND, 5 );


	bListSizer->Add( bInitialSizer3, 1, wxEXPAND, 5 );


	bRootSizer->Add( bListSizer, 0, wxEXPAND, 5 );


	bRootSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer40;
	bSizer40 = new wxBoxSizer( wxHORIZONTAL );


	bSizer40->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnCancel = new wxButton( this, wxID_ANY, _("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer40->Add( m_BtnCancel, 0, wxALL, 5 );

	m_BtnAddEdit = new wxButton( this, wxID_ANY, _("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer40->Add( m_BtnAddEdit, 0, wxALL, 5 );


	bRootSizer->Add( bSizer40, 0, wxEXPAND, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_InputWordOrSentence1->Connect( wxEVT_KEY_UP, wxKeyEventHandler( AddEditWithChildrenBankDlg::KeyUpEvent ), NULL, this );
	m_InputWordOrSentence2->Connect( wxEVT_KEY_UP, wxKeyEventHandler( AddEditWithChildrenBankDlg::KeyUpEvent ), NULL, this );
	m_InputWordOrSentence3->Connect( wxEVT_KEY_UP, wxKeyEventHandler( AddEditWithChildrenBankDlg::KeyUpEvent ), NULL, this );
	m_BtnCancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( AddEditWithChildrenBankDlg::BtnCancelClick ), NULL, this );
	m_BtnAddEdit->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( AddEditWithChildrenBankDlg::BtnAddEditClick ), NULL, this );
}

AddEditWithChildrenBankDlg::~AddEditWithChildrenBankDlg()
{
	// Disconnect Events
	m_InputWordOrSentence1->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( AddEditWithChildrenBankDlg::KeyUpEvent ), NULL, this );
	m_InputWordOrSentence2->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( AddEditWithChildrenBankDlg::KeyUpEvent ), NULL, this );
	m_InputWordOrSentence3->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( AddEditWithChildrenBankDlg::KeyUpEvent ), NULL, this );
	m_BtnCancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( AddEditWithChildrenBankDlg::BtnCancelClick ), NULL, this );
	m_BtnAddEdit->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( AddEditWithChildrenBankDlg::BtnAddEditClick ), NULL, this );

}

AboutDlg::AboutDlg( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bRootSizer;
	bRootSizer = new wxBoxSizer( wxVERTICAL );

	m_AboutInfo = new wxRichTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY|wxVSCROLL|wxHSCROLL|wxWANTS_CHARS|wxBORDER_NONE );
	m_AboutInfo->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOW ) );

	bRootSizer->Add( m_AboutInfo, 1, wxEXPAND | wxALL, 0 );

	m_sdbSizer1 = new wxStdDialogButtonSizer();
	m_sdbSizer1OK = new wxButton( this, wxID_OK );
	m_sdbSizer1->AddButton( m_sdbSizer1OK );
	m_sdbSizer1->Realize();

	bRootSizer->Add( m_sdbSizer1, 0, wxEXPAND, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_AboutInfo->Connect( wxEVT_COMMAND_TEXT_URL, wxTextUrlEventHandler( AboutDlg::URLClick ), NULL, this );
	m_sdbSizer1OK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( AboutDlg::AboutDlgClickOK ), NULL, this );
}

AboutDlg::~AboutDlg()
{
	// Disconnect Events
	m_AboutInfo->Disconnect( wxEVT_COMMAND_TEXT_URL, wxTextUrlEventHandler( AboutDlg::URLClick ), NULL, this );
	m_sdbSizer1OK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( AboutDlg::AboutDlgClickOK ), NULL, this );

}

CreateEditProgramsDlg::CreateEditProgramsDlg( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bRootSizer;
	bRootSizer = new wxBoxSizer( wxVERTICAL );

	m_CreateEditTabs = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	m_SettingsPanel = new wxPanel( m_CreateEditTabs, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer76;
	bSizer76 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer422;
	bSizer422 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtProgramName = new wxStaticText( m_SettingsPanel, wxID_ANY, _("Program Name"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT|wxALIGN_CENTER_HORIZONTAL );
	m_TxtProgramName->Wrap( -1 );
	bSizer422->Add( m_TxtProgramName, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputProgramName = new wxTextCtrl( m_SettingsPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer422->Add( m_InputProgramName, 1, wxALL|wxEXPAND, 5 );


	bSizer76->Add( bSizer422, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer24;
	bSizer24 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtPhase = new wxStaticText( m_SettingsPanel, wxID_ANY, _("Select Phase"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtPhase->Wrap( -1 );
	bSizer24->Add( m_TxtPhase, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	wxArrayString m_ComboPhaseChoices;
	m_ComboPhase = new wxChoice( m_SettingsPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_ComboPhaseChoices, 0 );
	m_ComboPhase->SetSelection( 0 );
	bSizer24->Add( m_ComboPhase, 1, wxALL, 5 );


	bSizer76->Add( bSizer24, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer4221;
	bSizer4221 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtComment = new wxStaticText( m_SettingsPanel, wxID_ANY, _("Comment"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT|wxALIGN_CENTER_HORIZONTAL );
	m_TxtComment->Wrap( -1 );
	bSizer4221->Add( m_TxtComment, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputComments = new wxTextCtrl( m_SettingsPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer4221->Add( m_InputComments, 1, wxALL|wxEXPAND, 5 );


	bSizer76->Add( bSizer4221, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer754;
	bSizer754 = new wxBoxSizer( wxHORIZONTAL );


	bSizer754->Add( 0, 0, 1, wxEXPAND, 5 );

	m_CheckDefaults = new wxCheckBox( m_SettingsPanel, wxID_ANY, _("Use Defaults"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer754->Add( m_CheckDefaults, 0, wxALL, 5 );


	bSizer754->Add( 0, 0, 1, wxEXPAND, 5 );


	bSizer76->Add( bSizer754, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer108;
	bSizer108 = new wxBoxSizer( wxHORIZONTAL );

	wxBoxSizer* bSizer109;
	bSizer109 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer75;
	bSizer75 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtTimeToRespond = new wxStaticText( m_SettingsPanel, wxID_ANY, _("Time to Respond"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtTimeToRespond->Wrap( -1 );
	bSizer75->Add( m_TxtTimeToRespond, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputTimeToRespond = new wxTextCtrl( m_SettingsPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputTimeToRespond->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &m_ValTimeToRespond ) );

	bSizer75->Add( m_InputTimeToRespond, 1, wxALL, 5 );

	m_DefTimeToRespond = new wxStaticText( m_SettingsPanel, wxID_ANY, _("Default:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_DefTimeToRespond->Wrap( -1 );
	bSizer75->Add( m_DefTimeToRespond, 1, wxALL, 5 );


	bSizer109->Add( bSizer75, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer751;
	bSizer751 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtTimeout = new wxStaticText( m_SettingsPanel, wxID_ANY, _("Response Timeout"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtTimeout->Wrap( -1 );
	bSizer751->Add( m_TxtTimeout, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputTimeResponseTimeout = new wxTextCtrl( m_SettingsPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputTimeResponseTimeout->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &m_ValResponseTimeout ) );

	bSizer751->Add( m_InputTimeResponseTimeout, 1, wxALL, 5 );

	m_DefResponseTimeout = new wxStaticText( m_SettingsPanel, wxID_ANY, _("Default:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_DefResponseTimeout->Wrap( -1 );
	bSizer751->Add( m_DefResponseTimeout, 1, wxALL, 5 );


	bSizer109->Add( bSizer751, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer752;
	bSizer752 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtInterval = new wxStaticText( m_SettingsPanel, wxID_ANY, _("Interval"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtInterval->Wrap( -1 );
	bSizer752->Add( m_TxtInterval, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputTimeInterval = new wxTextCtrl( m_SettingsPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputTimeInterval->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &m_ValInterval ) );

	bSizer752->Add( m_InputTimeInterval, 1, wxALL, 5 );

	m_DefInterval = new wxStaticText( m_SettingsPanel, wxID_ANY, _("Default:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_DefInterval->Wrap( -1 );
	bSizer752->Add( m_DefInterval, 1, wxALL, 5 );


	bSizer109->Add( bSizer752, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer753;
	bSizer753 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtTimeBetweenChanges = new wxStaticText( m_SettingsPanel, wxID_ANY, _("Time Between Changes"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtTimeBetweenChanges->Wrap( -1 );
	bSizer753->Add( m_TxtTimeBetweenChanges, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputTimeBetweenChanges = new wxTextCtrl( m_SettingsPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputTimeBetweenChanges->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &m_ValTimeBetweenChanges ) );

	bSizer753->Add( m_InputTimeBetweenChanges, 1, wxALL, 5 );

	m_DefTimeBetweenChanges = new wxStaticText( m_SettingsPanel, wxID_ANY, _("Default:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_DefTimeBetweenChanges->Wrap( -1 );
	bSizer753->Add( m_DefTimeBetweenChanges, 1, wxALL, 5 );


	bSizer109->Add( bSizer753, 0, wxEXPAND, 5 );


	bSizer108->Add( bSizer109, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer110;
	bSizer110 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer75311;
	bSizer75311 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtWordMin = new wxStaticText( m_SettingsPanel, wxID_ANY, _("Words Min"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtWordMin->Wrap( -1 );
	bSizer75311->Add( m_TxtWordMin, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputWordMin = new wxTextCtrl( m_SettingsPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputWordMin->SetMaxSize( wxSize( 50,-1 ) );

	m_InputWordMin->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &m_ValWordsMin ) );

	bSizer75311->Add( m_InputWordMin, 0, wxALL, 5 );

	m_TxtWordMax = new wxStaticText( m_SettingsPanel, wxID_ANY, _("Max"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtWordMax->Wrap( -1 );
	bSizer75311->Add( m_TxtWordMax, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputWordMax = new wxTextCtrl( m_SettingsPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputWordMax->SetMaxSize( wxSize( 50,-1 ) );

	m_InputWordMax->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &m_ValWordsMax ) );

	bSizer75311->Add( m_InputWordMax, 1, wxALL, 5 );


	bSizer110->Add( bSizer75311, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer7531;
	bSizer7531 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtLinesMin = new wxStaticText( m_SettingsPanel, wxID_ANY, _("Lines Min"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtLinesMin->Wrap( -1 );
	bSizer7531->Add( m_TxtLinesMin, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputLinesMin = new wxTextCtrl( m_SettingsPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputLinesMin->SetMaxSize( wxSize( 50,-1 ) );

	m_InputLinesMin->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &m_ValLinesMin ) );

	bSizer7531->Add( m_InputLinesMin, 0, wxALL, 5 );

	m_TxtLinesMax = new wxStaticText( m_SettingsPanel, wxID_ANY, _("Max"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtLinesMax->Wrap( -1 );
	bSizer7531->Add( m_TxtLinesMax, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputLinesMax = new wxTextCtrl( m_SettingsPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputLinesMax->SetMaxSize( wxSize( 50,-1 ) );

	m_InputLinesMax->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &m_ValLinesMax ) );

	bSizer7531->Add( m_InputLinesMax, 1, wxALL, 5 );


	bSizer110->Add( bSizer7531, 0, wxEXPAND, 5 );


	bSizer108->Add( bSizer110, 1, wxEXPAND, 5 );


	bSizer76->Add( bSizer108, 0, wxEXPAND, 5 );

	wxBoxSizer* bSettingsFilterBox;
	bSettingsFilterBox = new wxBoxSizer( wxVERTICAL );

	m_SettingsFilterScroll = new wxScrolledWindow( m_SettingsPanel, wxID_ANY, wxDefaultPosition, wxSize( -1,-1 ), wxVSCROLL );
	m_SettingsFilterScroll->SetScrollRate( 5, 5 );
	m_SettingsFilterScroll->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_3DLIGHT ) );
	m_SettingsFilterScroll->SetMinSize( wxSize( -1,100 ) );

	bSettingsScrollBoxSizer = new wxBoxSizer( wxVERTICAL );


	m_SettingsFilterScroll->SetSizer( bSettingsScrollBoxSizer );
	m_SettingsFilterScroll->Layout();
	bSettingsScrollBoxSizer->Fit( m_SettingsFilterScroll );
	bSettingsFilterBox->Add( m_SettingsFilterScroll, 1, wxEXPAND | wxALL, 5 );


	bSizer76->Add( bSettingsFilterBox, 1, wxEXPAND, 5 );


	m_SettingsPanel->SetSizer( bSizer76 );
	m_SettingsPanel->Layout();
	bSizer76->Fit( m_SettingsPanel );
	m_CreateEditTabs->AddPage( m_SettingsPanel, _("Settings"), true );
	m_BankPanel = new wxPanel( m_CreateEditTabs, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	bBankPanelRootSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer241;
	bSizer241 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtBank = new wxStaticText( m_BankPanel, wxID_ANY, _("Select Bank"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtBank->Wrap( -1 );
	bSizer241->Add( m_TxtBank, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	wxArrayString m_ComboBankChoices;
	m_ComboBank = new wxChoice( m_BankPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_ComboBankChoices, 0 );
	m_ComboBank->SetSelection( 0 );
	bSizer241->Add( m_ComboBank, 1, wxALL, 5 );


	bBankPanelRootSizer->Add( bSizer241, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer64;
	bSizer64 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer42;
	bSizer42 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtFilter = new wxStaticText( m_BankPanel, wxID_ANY, _("Bank Filter"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT );
	m_TxtFilter->Wrap( -1 );
	m_TxtFilter->Hide();

	bSizer42->Add( m_TxtFilter, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	wxArrayString m_ComboFilterChoices;
	m_ComboFilter = new wxChoice( m_BankPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_ComboFilterChoices, 0 );
	m_ComboFilter->SetSelection( 0 );
	m_ComboFilter->Hide();

	bSizer42->Add( m_ComboFilter, 1, wxALL, 5 );


	bSizer64->Add( bSizer42, 0, wxEXPAND, 5 );

	bGridSizer = new wxBoxSizer( wxVERTICAL );

	m_WordBankGrid = new wxGrid( m_BankPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxVSCROLL );

	// Grid
	m_WordBankGrid->CreateGrid( 0, 4 );
	m_WordBankGrid->EnableEditing( true );
	m_WordBankGrid->EnableGridLines( true );
	m_WordBankGrid->EnableDragGridSize( false );
	m_WordBankGrid->SetMargins( 0, 0 );

	// Columns
	m_WordBankGrid->AutoSizeColumns();
	m_WordBankGrid->EnableDragColMove( false );
	m_WordBankGrid->EnableDragColSize( true );
	m_WordBankGrid->SetColLabelSize( 30 );
	m_WordBankGrid->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	m_WordBankGrid->AutoSizeRows();
	m_WordBankGrid->EnableDragRowSize( true );
	m_WordBankGrid->SetRowLabelSize( 80 );
	m_WordBankGrid->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	m_WordBankGrid->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	m_WordBankGrid->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOW ) );
	m_WordBankGrid->SetMinSize( wxSize( -1,100 ) );

	bGridSizer->Add( m_WordBankGrid, 1, wxALL|wxEXPAND, 5 );


	bSizer64->Add( bGridSizer, 1, wxEXPAND, 5 );

	bBankButtonSizer = new wxBoxSizer( wxHORIZONTAL );

	m_BtnAdd = new wxButton( m_BankPanel, wxID_ANY, _("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	bBankButtonSizer->Add( m_BtnAdd, 0, wxALL, 5 );

	m_BtnAddWithChildren = new wxButton( m_BankPanel, wxID_ANY, _("Add Related Sentences (v3)"), wxDefaultPosition, wxDefaultSize, 0 );
	bBankButtonSizer->Add( m_BtnAddWithChildren, 0, wxALL, 5 );

	m_BtnEdit = new wxButton( m_BankPanel, wxID_ANY, _("Edit"), wxDefaultPosition, wxDefaultSize, 0 );
	bBankButtonSizer->Add( m_BtnEdit, 0, wxALL, 5 );


	bBankButtonSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnDelete = new wxButton( m_BankPanel, wxID_ANY, _("Delete"), wxDefaultPosition, wxDefaultSize, 0 );
	bBankButtonSizer->Add( m_BtnDelete, 0, wxALL, 5 );


	bBankButtonSizer->Add( 0, 0, 1, wxEXPAND, 5 );


	bBankButtonSizer->Add( 0, 0, 1, wxEXPAND, 5 );


	bBankButtonSizer->Add( 0, 0, 1, wxEXPAND, 5 );


	bBankButtonSizer->Add( 0, 0, 1, wxEXPAND, 5 );


	bSizer64->Add( bBankButtonSizer, 0, wxEXPAND, 5 );


	bBankPanelRootSizer->Add( bSizer64, 1, wxEXPAND, 5 );

	bBankFilterBox = new wxBoxSizer( wxVERTICAL );

	m_TxtSelectCount = new wxStaticText( m_BankPanel, wxID_ANY, _("Required To Select"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtSelectCount->Wrap( -1 );
	bBankFilterBox->Add( m_TxtSelectCount, 0, wxALL|wxEXPAND, 5 );

	m_BankFilterScroll = new wxScrolledWindow( m_BankPanel, wxID_ANY, wxDefaultPosition, wxSize( -1,-1 ), wxVSCROLL );
	m_BankFilterScroll->SetScrollRate( 5, 5 );
	m_BankFilterScroll->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_3DLIGHT ) );
	m_BankFilterScroll->SetMinSize( wxSize( -1,100 ) );

	bBankScrollBoxSizer = new wxBoxSizer( wxVERTICAL );


	m_BankFilterScroll->SetSizer( bBankScrollBoxSizer );
	m_BankFilterScroll->Layout();
	bBankScrollBoxSizer->Fit( m_BankFilterScroll );
	bBankFilterBox->Add( m_BankFilterScroll, 1, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer63;
	bSizer63 = new wxBoxSizer( wxVERTICAL );

	m_BtnAutoSelect = new wxButton( m_BankPanel, wxID_ANY, _("Auto Select"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer63->Add( m_BtnAutoSelect, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );


	bBankFilterBox->Add( bSizer63, 0, wxEXPAND, 5 );


	bBankPanelRootSizer->Add( bBankFilterBox, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer86;
	bSizer86 = new wxBoxSizer( wxVERTICAL );

	m_TxtCtrlLog = new wxTextCtrl( m_BankPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY );
	bSizer86->Add( m_TxtCtrlLog, 1, wxALL|wxEXPAND, 5 );


	bBankPanelRootSizer->Add( bSizer86, 1, wxEXPAND, 5 );


	m_BankPanel->SetSizer( bBankPanelRootSizer );
	m_BankPanel->Layout();
	bBankPanelRootSizer->Fit( m_BankPanel );
	m_CreateEditTabs->AddPage( m_BankPanel, _("Bank"), false );
	m_OrderPanel = new wxPanel( m_CreateEditTabs, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer91;
	bSizer91 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer116;
	bSizer116 = new wxBoxSizer( wxHORIZONTAL );

	wxBoxSizer* bLSideSizer;
	bLSideSizer = new wxBoxSizer( wxVERTICAL );

	bOrderSizer = new wxBoxSizer( wxVERTICAL );

	m_OrderList = new wxGrid( m_OrderPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxVSCROLL );

	// Grid
	m_OrderList->CreateGrid( 0, 1 );
	m_OrderList->EnableEditing( false );
	m_OrderList->EnableGridLines( true );
	m_OrderList->EnableDragGridSize( false );
	m_OrderList->SetMargins( 0, 0 );

	// Columns
	m_OrderList->AutoSizeColumns();
	m_OrderList->EnableDragColMove( false );
	m_OrderList->EnableDragColSize( true );
	m_OrderList->SetColLabelSize( 30 );
	m_OrderList->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	m_OrderList->AutoSizeRows();
	m_OrderList->EnableDragRowSize( true );
	m_OrderList->SetRowLabelSize( 80 );
	m_OrderList->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	m_OrderList->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	bOrderSizer->Add( m_OrderList, 1, wxALL|wxEXPAND, 5 );


	bLSideSizer->Add( bOrderSizer, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer159;
	bSizer159 = new wxBoxSizer( wxHORIZONTAL );

	m_BtnRemove = new wxButton( m_OrderPanel, wxID_ANY, _("Remove Block"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer159->Add( m_BtnRemove, 0, wxALL, 5 );

	m_BtnMoveUp = new wxButton( m_OrderPanel, wxID_ANY, _("Move Up"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer159->Add( m_BtnMoveUp, 0, wxALL, 5 );

	m_BtnMoveDown = new wxButton( m_OrderPanel, wxID_ANY, _("Move Down"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer159->Add( m_BtnMoveDown, 0, wxALL, 5 );


	bLSideSizer->Add( bSizer159, 0, wxALIGN_CENTER|wxALL, 5 );


	bSizer116->Add( bLSideSizer, 1, wxEXPAND, 5 );

	wxBoxSizer* bRSideSizer;
	bRSideSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bRightRootSizer;
	bRightRootSizer = new wxBoxSizer( wxVERTICAL );

	m_BlockDetailScroll = new wxScrolledWindow( m_OrderPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxBORDER_SIMPLE );
	m_BlockDetailScroll->SetScrollRate( 5, 5 );
	bFilterWindowSizer = new wxBoxSizer( wxVERTICAL );


	m_BlockDetailScroll->SetSizer( bFilterWindowSizer );
	m_BlockDetailScroll->Layout();
	bFilterWindowSizer->Fit( m_BlockDetailScroll );
	bRightRootSizer->Add( m_BlockDetailScroll, 1, wxEXPAND | wxALL, 5 );


	bRSideSizer->Add( bRightRootSizer, 1, wxEXPAND, 5 );

	wxBoxSizer* bLockedSizer;
	bLockedSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bBreakType;
	bBreakType = new wxBoxSizer( wxHORIZONTAL );

	wxString m_BreakChoiceChoices[] = { _("Break per filter"), _("Break at end"), _("No break") };
	int m_BreakChoiceNChoices = sizeof( m_BreakChoiceChoices ) / sizeof( wxString );
	m_BreakChoice = new wxChoice( m_OrderPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_BreakChoiceNChoices, m_BreakChoiceChoices, 0 );
	m_BreakChoice->SetSelection( 0 );
	bBreakType->Add( m_BreakChoice, 1, wxALL, 5 );


	bLockedSizer->Add( bBreakType, 1, wxEXPAND, 5 );

	wxBoxSizer* bRandomSelectSizer;
	bRandomSelectSizer = new wxBoxSizer( wxHORIZONTAL );


	bRandomSelectSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_CheckRandom = new wxCheckBox( m_OrderPanel, wxID_ANY, _("Randomise"), wxDefaultPosition, wxDefaultSize, 0 );
	bRandomSelectSizer->Add( m_CheckRandom, 1, wxALL, 5 );


	bRandomSelectSizer->Add( 0, 0, 1, wxEXPAND, 5 );


	bLockedSizer->Add( bRandomSelectSizer, 0, wxEXPAND, 5 );

	m_BtnAddVal = new wxButton( m_OrderPanel, wxID_ANY, _("Add Block"), wxDefaultPosition, wxDefaultSize, 0 );
	bLockedSizer->Add( m_BtnAddVal, 0, wxALIGN_CENTER|wxALL, 5 );


	bRSideSizer->Add( bLockedSizer, 0, wxEXPAND, 5 );


	bSizer116->Add( bRSideSizer, 1, wxEXPAND, 5 );


	bSizer91->Add( bSizer116, 1, wxEXPAND, 5 );


	m_OrderPanel->SetSizer( bSizer91 );
	m_OrderPanel->Layout();
	bSizer91->Fit( m_OrderPanel );
	m_CreateEditTabs->AddPage( m_OrderPanel, _("Ordering"), false );

	bRootSizer->Add( m_CreateEditTabs, 1, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bButtonGrp;
	bButtonGrp = new wxBoxSizer( wxHORIZONTAL );

	m_BtnCancel = new wxButton( this, wxID_ANY, _("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	bButtonGrp->Add( m_BtnCancel, 0, wxALL, 5 );


	bButtonGrp->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnCreateEdit = new wxButton( this, wxID_ANY, _("Create"), wxDefaultPosition, wxDefaultSize, 0 );
	bButtonGrp->Add( m_BtnCreateEdit, 0, wxALL, 5 );


	bRootSizer->Add( bButtonGrp, 0, wxEXPAND, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	this->Connect( wxEVT_INIT_DIALOG, wxInitDialogEventHandler( CreateEditProgramsDlg::EventInitDialog ) );
	m_CreateEditTabs->Connect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGED, wxNotebookEventHandler( CreateEditProgramsDlg::PageChanged ), NULL, this );
	m_CreateEditTabs->Connect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGING, wxNotebookEventHandler( CreateEditProgramsDlg::PageChanging ), NULL, this );
	m_InputProgramName->Connect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_ComboPhase->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( CreateEditProgramsDlg::ComboPhaseChange ), NULL, this );
	m_InputComments->Connect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_CheckDefaults->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::ChangeDefaultCheckBox ), NULL, this );
	m_InputTimeToRespond->Connect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_InputTimeResponseTimeout->Connect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_InputTimeInterval->Connect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_InputTimeBetweenChanges->Connect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_InputWordMin->Connect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_InputWordMax->Connect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_InputLinesMin->Connect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_InputLinesMax->Connect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_ComboBank->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( CreateEditProgramsDlg::ComboBankChange ), NULL, this );
	m_ComboFilter->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( CreateEditProgramsDlg::BankFilterChange ), NULL, this );
	m_WordBankGrid->Connect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( CreateEditProgramsDlg::GridCellLeftClick ), NULL, this );
	m_WordBankGrid->Connect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( CreateEditProgramsDlg::GridCellLeftDoubleClick ), NULL, this );
	m_WordBankGrid->Connect( wxEVT_GRID_RANGE_SELECT, wxGridRangeSelectEventHandler( CreateEditProgramsDlg::GridRangeSelect ), NULL, this );
	m_BtnAdd->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnAddClick ), NULL, this );
	m_BtnAddWithChildren->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnAddWithChildrenClick ), NULL, this );
	m_BtnEdit->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnEditClick ), NULL, this );
	m_BtnDelete->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnDeleteClick ), NULL, this );
	m_OrderList->Connect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( CreateEditProgramsDlg::OrderGridCellLeftClick ), NULL, this );
	m_BtnRemove->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnRemoveOrderClick ), NULL, this );
	m_BtnMoveUp->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnMoveUpOrderClick ), NULL, this );
	m_BtnMoveDown->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnMoveDownOrderClick ), NULL, this );
	m_BreakChoice->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( CreateEditProgramsDlg::BlockBreakChoice ), NULL, this );
	m_CheckRandom->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::RandomCheck ), NULL, this );
	m_BtnAddVal->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::AddBlockClick ), NULL, this );
	m_BtnCancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnCancelClick ), NULL, this );
	m_BtnCreateEdit->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnCreateEditClick ), NULL, this );
}

CreateEditProgramsDlg::~CreateEditProgramsDlg()
{
	// Disconnect Events
	this->Disconnect( wxEVT_INIT_DIALOG, wxInitDialogEventHandler( CreateEditProgramsDlg::EventInitDialog ) );
	m_CreateEditTabs->Disconnect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGED, wxNotebookEventHandler( CreateEditProgramsDlg::PageChanged ), NULL, this );
	m_CreateEditTabs->Disconnect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGING, wxNotebookEventHandler( CreateEditProgramsDlg::PageChanging ), NULL, this );
	m_InputProgramName->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_ComboPhase->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( CreateEditProgramsDlg::ComboPhaseChange ), NULL, this );
	m_InputComments->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_CheckDefaults->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::ChangeDefaultCheckBox ), NULL, this );
	m_InputTimeToRespond->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_InputTimeResponseTimeout->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_InputTimeInterval->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_InputTimeBetweenChanges->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_InputWordMin->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_InputWordMax->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_InputLinesMin->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_InputLinesMax->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( CreateEditProgramsDlg::SettingsKeyUp ), NULL, this );
	m_ComboBank->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( CreateEditProgramsDlg::ComboBankChange ), NULL, this );
	m_ComboFilter->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( CreateEditProgramsDlg::BankFilterChange ), NULL, this );
	m_WordBankGrid->Disconnect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( CreateEditProgramsDlg::GridCellLeftClick ), NULL, this );
	m_WordBankGrid->Disconnect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( CreateEditProgramsDlg::GridCellLeftDoubleClick ), NULL, this );
	m_WordBankGrid->Disconnect( wxEVT_GRID_RANGE_SELECT, wxGridRangeSelectEventHandler( CreateEditProgramsDlg::GridRangeSelect ), NULL, this );
	m_BtnAdd->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnAddClick ), NULL, this );
	m_BtnAddWithChildren->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnAddWithChildrenClick ), NULL, this );
	m_BtnEdit->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnEditClick ), NULL, this );
	m_BtnDelete->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnDeleteClick ), NULL, this );
	m_OrderList->Disconnect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( CreateEditProgramsDlg::OrderGridCellLeftClick ), NULL, this );
	m_BtnRemove->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnRemoveOrderClick ), NULL, this );
	m_BtnMoveUp->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnMoveUpOrderClick ), NULL, this );
	m_BtnMoveDown->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnMoveDownOrderClick ), NULL, this );
	m_BreakChoice->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( CreateEditProgramsDlg::BlockBreakChoice ), NULL, this );
	m_CheckRandom->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::RandomCheck ), NULL, this );
	m_BtnAddVal->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::AddBlockClick ), NULL, this );
	m_BtnCancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnCancelClick ), NULL, this );
	m_BtnCreateEdit->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CreateEditProgramsDlg::BtnCreateEditClick ), NULL, this );

}

CustomEditDlg::CustomEditDlg( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer65;
	bSizer65 = new wxBoxSizer( wxVERTICAL );

	m_TxtDefault = new wxStaticText( this, wxID_ANY, _("Default"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtDefault->Wrap( -1 );
	bSizer65->Add( m_TxtDefault, 0, wxALL, 5 );

	m_DefaultGridList = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	// Grid
	m_DefaultGridList->CreateGrid( 0, 4 );
	m_DefaultGridList->EnableEditing( true );
	m_DefaultGridList->EnableGridLines( true );
	m_DefaultGridList->EnableDragGridSize( false );
	m_DefaultGridList->SetMargins( 0, 0 );

	// Columns
	m_DefaultGridList->EnableDragColMove( false );
	m_DefaultGridList->EnableDragColSize( true );
	m_DefaultGridList->SetColLabelSize( 30 );
	m_DefaultGridList->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	m_DefaultGridList->EnableDragRowSize( true );
	m_DefaultGridList->SetRowLabelSize( 80 );
	m_DefaultGridList->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	m_DefaultGridList->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	bSizer65->Add( m_DefaultGridList, 1, wxALL|wxEXPAND, 5 );


	bSizer65->Add( 0, 0, 0, wxEXPAND, 5 );

	m_TxtCustom = new wxStaticText( this, wxID_ANY, _("Custom"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtCustom->Wrap( -1 );
	bSizer65->Add( m_TxtCustom, 0, wxALL, 5 );

	wxBoxSizer* bSizer66;
	bSizer66 = new wxBoxSizer( wxHORIZONTAL );

	m_CustomGridList = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	// Grid
	m_CustomGridList->CreateGrid( 0, 4 );
	m_CustomGridList->EnableEditing( true );
	m_CustomGridList->EnableGridLines( true );
	m_CustomGridList->EnableDragGridSize( false );
	m_CustomGridList->SetMargins( 0, 0 );

	// Columns
	m_CustomGridList->EnableDragColMove( false );
	m_CustomGridList->EnableDragColSize( true );
	m_CustomGridList->SetColLabelSize( 30 );
	m_CustomGridList->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	m_CustomGridList->EnableDragRowSize( true );
	m_CustomGridList->SetRowLabelSize( 25 );
	m_CustomGridList->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	m_CustomGridList->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	bSizer66->Add( m_CustomGridList, 1, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer67;
	bSizer67 = new wxBoxSizer( wxVERTICAL );

	m_BtnAdd = new wxButton( this, wxID_ANY, _("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnAdd->SetToolTip( _("Click to add a new custom item") );

	bSizer67->Add( m_BtnAdd, 0, wxALL, 5 );


	bSizer67->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnRemove = new wxButton( this, wxID_ANY, _("Remove"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnRemove->SetToolTip( _("Click to remove the selected item") );

	bSizer67->Add( m_BtnRemove, 0, wxALL, 5 );


	bSizer66->Add( bSizer67, 0, wxEXPAND, 5 );


	bSizer65->Add( bSizer66, 1, wxEXPAND, 5 );


	bSizer65->Add( 0, 0, 0, wxEXPAND, 5 );


	bSizer65->Add( 0, 0, 0, wxEXPAND, 5 );

	m_BtnDone = new wxButton( this, wxID_ANY, _("Done"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer65->Add( m_BtnDone, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );


	this->SetSizer( bSizer65 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	this->Connect( wxEVT_INIT_DIALOG, wxInitDialogEventHandler( CustomEditDlg::InitDialog ) );
	m_DefaultGridList->Connect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( CustomEditDlg::GridCellLeftClick ), NULL, this );
	m_CustomGridList->Connect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( CustomEditDlg::GridCellLeftClick ), NULL, this );
	m_BtnAdd->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CustomEditDlg::BtnAddClick ), NULL, this );
	m_BtnRemove->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CustomEditDlg::BtnRemoveClick ), NULL, this );
	m_BtnDone->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CustomEditDlg::BtnDoneClick ), NULL, this );
}

CustomEditDlg::~CustomEditDlg()
{
	// Disconnect Events
	this->Disconnect( wxEVT_INIT_DIALOG, wxInitDialogEventHandler( CustomEditDlg::InitDialog ) );
	m_DefaultGridList->Disconnect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( CustomEditDlg::GridCellLeftClick ), NULL, this );
	m_CustomGridList->Disconnect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( CustomEditDlg::GridCellLeftClick ), NULL, this );
	m_BtnAdd->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CustomEditDlg::BtnAddClick ), NULL, this );
	m_BtnRemove->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CustomEditDlg::BtnRemoveClick ), NULL, this );
	m_BtnDone->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CustomEditDlg::BtnDoneClick ), NULL, this );

}

SettingsDlg::SettingsDlg( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bRootSizer;
	bRootSizer = new wxBoxSizer( wxVERTICAL );

	m_SettingsTabs = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	m_GeneralPanel = new wxPanel( m_SettingsTabs, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer1731;
	bSizer1731 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer174;
	bSizer174 = new wxBoxSizer( wxVERTICAL );

	m_CheckUseAdminPass = new wxCheckBox( m_GeneralPanel, wxID_ANY, _("Enable Admin Password"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer174->Add( m_CheckUseAdminPass, 0, wxALIGN_CENTER|wxALL, 5 );


	bSizer1731->Add( bSizer174, 0, wxEXPAND, 5 );


	m_GeneralPanel->SetSizer( bSizer1731 );
	m_GeneralPanel->Layout();
	bSizer1731->Fit( m_GeneralPanel );
	m_SettingsTabs->AddPage( m_GeneralPanel, _("General"), false );
	m_PhaseSettingsPanel = new wxPanel( m_SettingsTabs, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer120;
	bSizer120 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer74;
	bSizer74 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtPhase = new wxStaticText( m_PhaseSettingsPanel, wxID_ANY, _("Phase Select"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtPhase->Wrap( -1 );
	bSizer74->Add( m_TxtPhase, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxArrayString m_ComboPhaseChoices;
	m_ComboPhase = new wxChoice( m_PhaseSettingsPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_ComboPhaseChoices, 0 );
	m_ComboPhase->SetSelection( 0 );
	bSizer74->Add( m_ComboPhase, 1, wxALL, 5 );


	bSizer120->Add( bSizer74, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer75;
	bSizer75 = new wxBoxSizer( wxHORIZONTAL );


	bSizer75->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtTimeToRespond = new wxStaticText( m_PhaseSettingsPanel, wxID_ANY, _("Time to Respond"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtTimeToRespond->Wrap( -1 );
	bSizer75->Add( m_TxtTimeToRespond, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputTimeToRespond = new wxTextCtrl( m_PhaseSettingsPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputTimeToRespond->SetToolTip( _("Modify global time to respond for this phase version") );

	m_InputTimeToRespond->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &m_ValTimeToRespond ) );

	bSizer75->Add( m_InputTimeToRespond, 1, wxALL, 5 );

	m_DefTimeToRespond = new wxStaticText( m_PhaseSettingsPanel, wxID_ANY, _("Default:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_DefTimeToRespond->Wrap( -1 );
	bSizer75->Add( m_DefTimeToRespond, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );


	bSizer75->Add( 0, 0, 1, wxEXPAND, 5 );


	bSizer120->Add( bSizer75, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer751;
	bSizer751 = new wxBoxSizer( wxHORIZONTAL );


	bSizer751->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtTimeout = new wxStaticText( m_PhaseSettingsPanel, wxID_ANY, _("Response Timeout"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtTimeout->Wrap( -1 );
	bSizer751->Add( m_TxtTimeout, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputTimeResponseTimeout = new wxTextCtrl( m_PhaseSettingsPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputTimeResponseTimeout->SetToolTip( _("Modify global response timeout for this phase version") );

	m_InputTimeResponseTimeout->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &m_ValResponseTimeout ) );

	bSizer751->Add( m_InputTimeResponseTimeout, 1, wxALL, 5 );

	m_DefResponseTimeout = new wxStaticText( m_PhaseSettingsPanel, wxID_ANY, _("Default:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_DefResponseTimeout->Wrap( -1 );
	bSizer751->Add( m_DefResponseTimeout, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );


	bSizer751->Add( 0, 0, 1, wxEXPAND, 5 );


	bSizer120->Add( bSizer751, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer752;
	bSizer752 = new wxBoxSizer( wxHORIZONTAL );


	bSizer752->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtInterval = new wxStaticText( m_PhaseSettingsPanel, wxID_ANY, _("Interval"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtInterval->Wrap( -1 );
	bSizer752->Add( m_TxtInterval, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputTimeInterval = new wxTextCtrl( m_PhaseSettingsPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputTimeInterval->SetToolTip( _("Modify global rest interval for this phase version") );

	m_InputTimeInterval->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &m_ValInterval ) );

	bSizer752->Add( m_InputTimeInterval, 1, wxALL, 5 );

	m_DefInterval = new wxStaticText( m_PhaseSettingsPanel, wxID_ANY, _("Default:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_DefInterval->Wrap( -1 );
	bSizer752->Add( m_DefInterval, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );


	bSizer752->Add( 0, 0, 1, wxEXPAND, 5 );


	bSizer120->Add( bSizer752, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer753;
	bSizer753 = new wxBoxSizer( wxHORIZONTAL );


	bSizer753->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtTimeBetweenChanges = new wxStaticText( m_PhaseSettingsPanel, wxID_ANY, _("Time Between Changes"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtTimeBetweenChanges->Wrap( -1 );
	bSizer753->Add( m_TxtTimeBetweenChanges, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputTimeBetweenChanges = new wxTextCtrl( m_PhaseSettingsPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputTimeBetweenChanges->SetToolTip( _("Modify global time between changes for this phase version") );

	m_InputTimeBetweenChanges->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &m_ValTimeBetweenChanges ) );

	bSizer753->Add( m_InputTimeBetweenChanges, 1, wxALL, 5 );

	m_DefTimeBetweenChanges = new wxStaticText( m_PhaseSettingsPanel, wxID_ANY, _("Default:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_DefTimeBetweenChanges->Wrap( -1 );
	bSizer753->Add( m_DefTimeBetweenChanges, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );


	bSizer753->Add( 0, 0, 1, wxEXPAND, 5 );


	bSizer120->Add( bSizer753, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer131;
	bSizer131 = new wxBoxSizer( wxVERTICAL );

	m_BtnResetPhase = new wxButton( m_PhaseSettingsPanel, wxID_ANY, _("Reset To Defaults"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnResetPhase->SetToolTip( _("Resets all global defaults for this phase version") );

	bSizer131->Add( m_BtnResetPhase, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );


	bSizer120->Add( bSizer131, 0, wxEXPAND, 5 );

	wxBoxSizer* bSettingsFilterBox;
	bSettingsFilterBox = new wxBoxSizer( wxVERTICAL );

	m_SettingsFilterScroll = new wxScrolledWindow( m_PhaseSettingsPanel, wxID_ANY, wxDefaultPosition, wxSize( -1,-1 ), wxVSCROLL );
	m_SettingsFilterScroll->SetScrollRate( 5, 5 );
	m_SettingsFilterScroll->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_3DLIGHT ) );
	m_SettingsFilterScroll->Hide();
	m_SettingsFilterScroll->SetMinSize( wxSize( -1,100 ) );

	bSettingsScrollBoxSizer = new wxBoxSizer( wxVERTICAL );


	m_SettingsFilterScroll->SetSizer( bSettingsScrollBoxSizer );
	m_SettingsFilterScroll->Layout();
	bSettingsScrollBoxSizer->Fit( m_SettingsFilterScroll );
	bSettingsFilterBox->Add( m_SettingsFilterScroll, 1, wxEXPAND | wxALL, 5 );


	bSizer120->Add( bSettingsFilterBox, 1, wxEXPAND, 5 );


	m_PhaseSettingsPanel->SetSizer( bSizer120 );
	m_PhaseSettingsPanel->Layout();
	bSizer120->Fit( m_PhaseSettingsPanel );
	m_SettingsTabs->AddPage( m_PhaseSettingsPanel, _("Phases"), false );
	m_TextAndBackgroundPanel = new wxPanel( m_SettingsTabs, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer123;
	bSizer123 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer754;
	bSizer754 = new wxBoxSizer( wxHORIZONTAL );


	bSizer754->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtBackgroundColour = new wxStaticText( m_TextAndBackgroundPanel, wxID_ANY, _("Text Background"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtBackgroundColour->Wrap( -1 );
	bSizer754->Add( m_TxtBackgroundColour, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_PickerTextBG = new wxColourPickerCtrl( m_TextAndBackgroundPanel, wxID_ANY, *wxBLACK, wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	bSizer754->Add( m_PickerTextBG, 0, wxALL, 5 );


	bSizer754->Add( 0, 0, 1, wxEXPAND, 5 );

	m_ResetBG = new wxButton( m_TextAndBackgroundPanel, wxID_ANY, _("Reset"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer754->Add( m_ResetBG, 0, wxALL, 5 );


	bSizer123->Add( bSizer754, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer7541;
	bSizer7541 = new wxBoxSizer( wxHORIZONTAL );


	bSizer7541->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtTextColour = new wxStaticText( m_TextAndBackgroundPanel, wxID_ANY, _("Text Colour"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtTextColour->Wrap( -1 );
	bSizer7541->Add( m_TxtTextColour, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_PickerText = new wxColourPickerCtrl( m_TextAndBackgroundPanel, wxID_ANY, *wxBLACK, wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	bSizer7541->Add( m_PickerText, 0, wxALL, 5 );


	bSizer7541->Add( 0, 0, 1, wxEXPAND, 5 );

	m_ResetText = new wxButton( m_TextAndBackgroundPanel, wxID_ANY, _("Reset"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7541->Add( m_ResetText, 0, wxALL, 5 );


	bSizer123->Add( bSizer7541, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer754111;
	bSizer754111 = new wxBoxSizer( wxHORIZONTAL );


	bSizer754111->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtInfoText = new wxStaticText( m_TextAndBackgroundPanel, wxID_ANY, _("Info Text Colour"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtInfoText->Wrap( -1 );
	bSizer754111->Add( m_TxtInfoText, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_PickerInfoText = new wxColourPickerCtrl( m_TextAndBackgroundPanel, wxID_ANY, *wxBLACK, wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	bSizer754111->Add( m_PickerInfoText, 0, wxALL, 5 );


	bSizer754111->Add( 0, 0, 1, wxEXPAND, 5 );

	m_ResetInfo = new wxButton( m_TextAndBackgroundPanel, wxID_ANY, _("Reset"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer754111->Add( m_ResetInfo, 0, wxALL, 5 );


	bSizer123->Add( bSizer754111, 0, wxEXPAND, 5 );


	bSizer123->Add( 0, 20, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer75411;
	bSizer75411 = new wxBoxSizer( wxHORIZONTAL );


	bSizer75411->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtBorderColour = new wxStaticText( m_TextAndBackgroundPanel, wxID_ANY, _("Text Border Colour"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtBorderColour->Wrap( -1 );
	bSizer75411->Add( m_TxtBorderColour, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_PickerBorder = new wxColourPickerCtrl( m_TextAndBackgroundPanel, wxID_ANY, *wxBLACK, wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	bSizer75411->Add( m_PickerBorder, 0, wxALL, 5 );


	bSizer75411->Add( 0, 0, 1, wxEXPAND, 5 );

	m_ResetBorder = new wxButton( m_TextAndBackgroundPanel, wxID_ANY, _("Reset"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer75411->Add( m_ResetBorder, 0, wxALL, 5 );


	bSizer123->Add( bSizer75411, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer155;
	bSizer155 = new wxBoxSizer( wxHORIZONTAL );


	bSizer155->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtBorderThickness = new wxStaticText( m_TextAndBackgroundPanel, wxID_ANY, _("Border Thickness"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtBorderThickness->Wrap( -1 );
	bSizer155->Add( m_TxtBorderThickness, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_BorderThicknessCtrl = new wxSpinCtrl( m_TextAndBackgroundPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 10, 0 );
	bSizer155->Add( m_BorderThicknessCtrl, 0, wxALL, 5 );


	bSizer155->Add( 0, 0, 1, wxEXPAND, 5 );

	m_ResetBorderThickness = new wxButton( m_TextAndBackgroundPanel, wxID_ANY, _("Reset"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer155->Add( m_ResetBorderThickness, 0, wxALL, 5 );


	bSizer123->Add( bSizer155, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer1551;
	bSizer1551 = new wxBoxSizer( wxHORIZONTAL );


	bSizer1551->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtBorderWidth = new wxStaticText( m_TextAndBackgroundPanel, wxID_ANY, _("Border Width"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtBorderWidth->Wrap( -1 );
	bSizer1551->Add( m_TxtBorderWidth, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_BorderWidthCtrl = new wxSpinCtrl( m_TextAndBackgroundPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 10, 0 );
	bSizer1551->Add( m_BorderWidthCtrl, 0, wxALL, 5 );


	bSizer1551->Add( 0, 0, 1, wxEXPAND, 5 );

	m_ResetBorderWidth = new wxButton( m_TextAndBackgroundPanel, wxID_ANY, _("Reset"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1551->Add( m_ResetBorderWidth, 0, wxALL, 5 );


	bSizer123->Add( bSizer1551, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer15511;
	bSizer15511 = new wxBoxSizer( wxHORIZONTAL );


	bSizer15511->Add( 0, 0, 1, wxEXPAND, 5 );

	m_TxtBorderHeight = new wxStaticText( m_TextAndBackgroundPanel, wxID_ANY, _("Border Height"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtBorderHeight->Wrap( -1 );
	bSizer15511->Add( m_TxtBorderHeight, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_BorderHeightCtrl = new wxSpinCtrl( m_TextAndBackgroundPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 10, 0 );
	bSizer15511->Add( m_BorderHeightCtrl, 0, wxALL, 5 );


	bSizer15511->Add( 0, 0, 1, wxEXPAND, 5 );

	m_ResetBorderHeight = new wxButton( m_TextAndBackgroundPanel, wxID_ANY, _("Reset"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer15511->Add( m_ResetBorderHeight, 0, wxALL, 5 );


	bSizer123->Add( bSizer15511, 0, wxEXPAND, 5 );


	bSizer123->Add( 0, 0, 1, wxEXPAND, 5 );


	m_TextAndBackgroundPanel->SetSizer( bSizer123 );
	m_TextAndBackgroundPanel->Layout();
	bSizer123->Fit( m_TextAndBackgroundPanel );
	m_SettingsTabs->AddPage( m_TextAndBackgroundPanel, _("Text + Background"), false );
	m_TutorialInfoPanel = new wxPanel( m_SettingsTabs, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer143;
	bSizer143 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer741;
	bSizer741 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtTutPhase = new wxStaticText( m_TutorialInfoPanel, wxID_ANY, _("Phase Select"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtTutPhase->Wrap( -1 );
	bSizer741->Add( m_TxtTutPhase, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxArrayString m_ComboTutPhaseChoices;
	m_ComboTutPhase = new wxChoice( m_TutorialInfoPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_ComboTutPhaseChoices, 0 );
	m_ComboTutPhase->SetSelection( 0 );
	bSizer741->Add( m_ComboTutPhase, 1, wxALL, 5 );


	bSizer143->Add( bSizer741, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer161;
	bSizer161 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtFontSize = new wxStaticText( m_TutorialInfoPanel, wxID_ANY, _("Font Size"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtFontSize->Wrap( -1 );
	m_TxtFontSize->Hide();

	bSizer161->Add( m_TxtFontSize, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_FontSizeControl = new wxSpinCtrl( m_TutorialInfoPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 50, 0 );
	m_FontSizeControl->Enable( false );
	m_FontSizeControl->Hide();

	bSizer161->Add( m_FontSizeControl, 0, wxALL, 5 );


	bSizer143->Add( bSizer161, 0, wxALIGN_CENTER, 5 );

	wxBoxSizer* bSizer173;
	bSizer173 = new wxBoxSizer( wxHORIZONTAL );

	wxBoxSizer* bSizer147;
	bSizer147 = new wxBoxSizer( wxHORIZONTAL );

	wxBoxSizer* bSizer160;
	bSizer160 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtTutorial = new wxStaticText( m_TutorialInfoPanel, wxID_ANY, _("Tutorial Intro Text"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtTutorial->Wrap( -1 );
	bSizer160->Add( m_TxtTutorial, 0, wxALL, 5 );

	m_TextCtrlTutorial = new wxTextCtrl( m_TutorialInfoPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_WORDWRAP );
	bSizer160->Add( m_TextCtrlTutorial, 1, wxALL|wxEXPAND, 5 );


	bSizer147->Add( bSizer160, 1, wxEXPAND, 5 );


	bSizer173->Add( bSizer147, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer1472;
	bSizer1472 = new wxBoxSizer( wxHORIZONTAL );

	wxBoxSizer* bSizer1601;
	bSizer1601 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtTutorialEnd = new wxStaticText( m_TutorialInfoPanel, wxID_ANY, _("Tutorial End Text"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtTutorialEnd->Wrap( -1 );
	bSizer1601->Add( m_TxtTutorialEnd, 0, wxALL, 5 );

	m_TextCtrlTutorialEnd = new wxTextCtrl( m_TutorialInfoPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_WORDWRAP );
	bSizer1601->Add( m_TextCtrlTutorialEnd, 1, wxALL|wxEXPAND, 5 );


	bSizer1472->Add( bSizer1601, 1, wxEXPAND, 5 );


	bSizer173->Add( bSizer1472, 1, wxEXPAND, 5 );


	bSizer143->Add( bSizer173, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer1471;
	bSizer1471 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtTutorial1 = new wxStaticText( m_TutorialInfoPanel, wxID_ANY, _("First Example Text"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtTutorial1->Wrap( -1 );
	bSizer1471->Add( m_TxtTutorial1, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_TextControlTut1 = new wxTextCtrl( m_TutorialInfoPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1471->Add( m_TextControlTut1, 1, wxALL, 5 );


	bSizer143->Add( bSizer1471, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer14712;
	bSizer14712 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtEx1 = new wxStaticText( m_TutorialInfoPanel, wxID_ANY, _("1st example word/sentence"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtEx1->Wrap( -1 );
	bSizer14712->Add( m_TxtEx1, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_TextControlEx1 = new wxTextCtrl( m_TutorialInfoPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer14712->Add( m_TextControlEx1, 1, wxALL, 5 );


	bSizer143->Add( bSizer14712, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer14711;
	bSizer14711 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtTutorial2 = new wxStaticText( m_TutorialInfoPanel, wxID_ANY, _("Second Example Text"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtTutorial2->Wrap( -1 );
	bSizer14711->Add( m_TxtTutorial2, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_TextControlTut2 = new wxTextCtrl( m_TutorialInfoPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer14711->Add( m_TextControlTut2, 1, wxALL, 5 );


	bSizer143->Add( bSizer14711, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer147121;
	bSizer147121 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtEx2 = new wxStaticText( m_TutorialInfoPanel, wxID_ANY, _("2nd example word/sentence"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtEx2->Wrap( -1 );
	bSizer147121->Add( m_TxtEx2, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_TextControlEx2 = new wxTextCtrl( m_TutorialInfoPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer147121->Add( m_TextControlEx2, 1, wxALL, 5 );


	bSizer143->Add( bSizer147121, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer147111;
	bSizer147111 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtTutorial3 = new wxStaticText( m_TutorialInfoPanel, wxID_ANY, _("Third Example Text"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtTutorial3->Wrap( -1 );
	bSizer147111->Add( m_TxtTutorial3, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_TextControlTut3 = new wxTextCtrl( m_TutorialInfoPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer147111->Add( m_TextControlTut3, 1, wxALL, 5 );


	bSizer143->Add( bSizer147111, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer1471211;
	bSizer1471211 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtEx3 = new wxStaticText( m_TutorialInfoPanel, wxID_ANY, _("3rd example word/sentence"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtEx3->Wrap( -1 );
	bSizer1471211->Add( m_TxtEx3, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_TextControlEx3 = new wxTextCtrl( m_TutorialInfoPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1471211->Add( m_TextControlEx3, 1, wxALL, 5 );


	bSizer143->Add( bSizer1471211, 0, wxEXPAND, 5 );


	bSizer143->Add( 0, 20, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer14711111;
	bSizer14711111 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtCounts = new wxStaticText( m_TutorialInfoPanel, wxID_ANY, _("Example Counts"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtCounts->Wrap( -1 );
	bSizer14711111->Add( m_TxtCounts, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_TextControlCounts = new wxTextCtrl( m_TutorialInfoPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer14711111->Add( m_TextControlCounts, 1, wxALL, 5 );


	bSizer143->Add( bSizer14711111, 0, wxEXPAND, 5 );


	bSizer143->Add( 0, 20, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer1471111;
	bSizer1471111 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtWait = new wxStaticText( m_TutorialInfoPanel, wxID_ANY, _("Wait Text"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtWait->Wrap( -1 );
	bSizer1471111->Add( m_TxtWait, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_TextControlWait = new wxTextCtrl( m_TutorialInfoPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1471111->Add( m_TextControlWait, 1, wxALL, 5 );


	bSizer143->Add( bSizer1471111, 0, wxEXPAND, 5 );


	bSizer143->Add( 0, 20, 0, wxEXPAND, 5 );


	bSizer143->Add( 0, 0, 0, wxEXPAND, 5 );


	m_TutorialInfoPanel->SetSizer( bSizer143 );
	m_TutorialInfoPanel->Layout();
	bSizer143->Fit( m_TutorialInfoPanel );
	m_SettingsTabs->AddPage( m_TutorialInfoPanel, _("Tutorials"), true );

	bRootSizer->Add( m_SettingsTabs, 1, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer114;
	bSizer114 = new wxBoxSizer( wxHORIZONTAL );


	bSizer114->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnDone = new wxButton( this, wxID_ANY, _("Done"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer114->Add( m_BtnDone, 0, wxALL, 5 );


	bSizer114->Add( 0, 0, 1, wxEXPAND, 5 );


	bRootSizer->Add( bSizer114, 0, wxEXPAND, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	this->Connect( wxEVT_INIT_DIALOG, wxInitDialogEventHandler( SettingsDlg::EventInitDialog ) );
	m_SettingsTabs->Connect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGED, wxNotebookEventHandler( SettingsDlg::PageChanged ), NULL, this );
	m_SettingsTabs->Connect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGING, wxNotebookEventHandler( SettingsDlg::PageChanging ), NULL, this );
	m_CheckUseAdminPass->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SettingsDlg::CheckChangeAdminUse ), NULL, this );
	m_ComboPhase->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( SettingsDlg::OnChangePhase ), NULL, this );
	m_InputTimeToRespond->Connect( wxEVT_KEY_UP, wxKeyEventHandler( SettingsDlg::PhaseValChange ), NULL, this );
	m_InputTimeResponseTimeout->Connect( wxEVT_KEY_UP, wxKeyEventHandler( SettingsDlg::PhaseValChange ), NULL, this );
	m_InputTimeInterval->Connect( wxEVT_KEY_UP, wxKeyEventHandler( SettingsDlg::PhaseValChange ), NULL, this );
	m_InputTimeBetweenChanges->Connect( wxEVT_KEY_UP, wxKeyEventHandler( SettingsDlg::PhaseValChange ), NULL, this );
	m_BtnResetPhase->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnResetTimesClick ), NULL, this );
	m_PickerTextBG->Connect( wxEVT_COMMAND_COLOURPICKER_CHANGED, wxColourPickerEventHandler( SettingsDlg::OnBGColChange ), NULL, this );
	m_ResetBG->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnResetBGClick ), NULL, this );
	m_PickerText->Connect( wxEVT_COMMAND_COLOURPICKER_CHANGED, wxColourPickerEventHandler( SettingsDlg::OnTextColChange ), NULL, this );
	m_ResetText->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnResetTextClick ), NULL, this );
	m_PickerInfoText->Connect( wxEVT_COMMAND_COLOURPICKER_CHANGED, wxColourPickerEventHandler( SettingsDlg::OnTextInfoColChange ), NULL, this );
	m_ResetInfo->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnResetTextInfoClick ), NULL, this );
	m_PickerBorder->Connect( wxEVT_COMMAND_COLOURPICKER_CHANGED, wxColourPickerEventHandler( SettingsDlg::OnTextBorderColChange ), NULL, this );
	m_ResetBorder->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnResetTextBorderClick ), NULL, this );
	m_BorderThicknessCtrl->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( SettingsDlg::OnBorderThicknessChange ), NULL, this );
	m_BorderThicknessCtrl->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( SettingsDlg::OnBorderThicknessChangeTxt ), NULL, this );
	m_ResetBorderThickness->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnResetTextBorderThicknessClick ), NULL, this );
	m_BorderWidthCtrl->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( SettingsDlg::OnBorderWidthChange ), NULL, this );
	m_BorderWidthCtrl->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( SettingsDlg::OnBorderWidthChangeTxt ), NULL, this );
	m_ResetBorderWidth->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnResetTextBorderWidthClick ), NULL, this );
	m_BorderHeightCtrl->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( SettingsDlg::OnBorderHeightChange ), NULL, this );
	m_BorderHeightCtrl->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( SettingsDlg::OnBorderHeightChangeTxt ), NULL, this );
	m_ResetBorderHeight->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnResetTextBorderHeightClick ), NULL, this );
	m_ComboTutPhase->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( SettingsDlg::OnChangePhase ), NULL, this );
	m_FontSizeControl->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( SettingsDlg::OnChangeFontSize ), NULL, this );
	m_BtnDone->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnDoneClick ), NULL, this );
}

SettingsDlg::~SettingsDlg()
{
	// Disconnect Events
	this->Disconnect( wxEVT_INIT_DIALOG, wxInitDialogEventHandler( SettingsDlg::EventInitDialog ) );
	m_SettingsTabs->Disconnect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGED, wxNotebookEventHandler( SettingsDlg::PageChanged ), NULL, this );
	m_SettingsTabs->Disconnect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGING, wxNotebookEventHandler( SettingsDlg::PageChanging ), NULL, this );
	m_CheckUseAdminPass->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SettingsDlg::CheckChangeAdminUse ), NULL, this );
	m_ComboPhase->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( SettingsDlg::OnChangePhase ), NULL, this );
	m_InputTimeToRespond->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( SettingsDlg::PhaseValChange ), NULL, this );
	m_InputTimeResponseTimeout->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( SettingsDlg::PhaseValChange ), NULL, this );
	m_InputTimeInterval->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( SettingsDlg::PhaseValChange ), NULL, this );
	m_InputTimeBetweenChanges->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( SettingsDlg::PhaseValChange ), NULL, this );
	m_BtnResetPhase->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnResetTimesClick ), NULL, this );
	m_PickerTextBG->Disconnect( wxEVT_COMMAND_COLOURPICKER_CHANGED, wxColourPickerEventHandler( SettingsDlg::OnBGColChange ), NULL, this );
	m_ResetBG->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnResetBGClick ), NULL, this );
	m_PickerText->Disconnect( wxEVT_COMMAND_COLOURPICKER_CHANGED, wxColourPickerEventHandler( SettingsDlg::OnTextColChange ), NULL, this );
	m_ResetText->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnResetTextClick ), NULL, this );
	m_PickerInfoText->Disconnect( wxEVT_COMMAND_COLOURPICKER_CHANGED, wxColourPickerEventHandler( SettingsDlg::OnTextInfoColChange ), NULL, this );
	m_ResetInfo->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnResetTextInfoClick ), NULL, this );
	m_PickerBorder->Disconnect( wxEVT_COMMAND_COLOURPICKER_CHANGED, wxColourPickerEventHandler( SettingsDlg::OnTextBorderColChange ), NULL, this );
	m_ResetBorder->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnResetTextBorderClick ), NULL, this );
	m_BorderThicknessCtrl->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( SettingsDlg::OnBorderThicknessChange ), NULL, this );
	m_BorderThicknessCtrl->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( SettingsDlg::OnBorderThicknessChangeTxt ), NULL, this );
	m_ResetBorderThickness->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnResetTextBorderThicknessClick ), NULL, this );
	m_BorderWidthCtrl->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( SettingsDlg::OnBorderWidthChange ), NULL, this );
	m_BorderWidthCtrl->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( SettingsDlg::OnBorderWidthChangeTxt ), NULL, this );
	m_ResetBorderWidth->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnResetTextBorderWidthClick ), NULL, this );
	m_BorderHeightCtrl->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( SettingsDlg::OnBorderHeightChange ), NULL, this );
	m_BorderHeightCtrl->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( SettingsDlg::OnBorderHeightChangeTxt ), NULL, this );
	m_ResetBorderHeight->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnResetTextBorderHeightClick ), NULL, this );
	m_ComboTutPhase->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( SettingsDlg::OnChangePhase ), NULL, this );
	m_FontSizeControl->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( SettingsDlg::OnChangeFontSize ), NULL, this );
	m_BtnDone->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SettingsDlg::BtnDoneClick ), NULL, this );

}

ProgramViewDlg::ProgramViewDlg( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bRootSizer;
	bRootSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer93;
	bSizer93 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer126;
	bSizer126 = new wxBoxSizer( wxHORIZONTAL );

	wxBoxSizer* bSizer753111;
	bSizer753111 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtProgramName = new wxStaticText( this, wxID_ANY, _("Program Name"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtProgramName->Wrap( -1 );
	bSizer753111->Add( m_TxtProgramName, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputProgramName = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputProgramName->Enable( false );

	bSizer753111->Add( m_InputProgramName, 1, wxALL, 5 );


	bSizer126->Add( bSizer753111, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer7531111;
	bSizer7531111 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtVersion = new wxStaticText( this, wxID_ANY, _("Version"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtVersion->Wrap( -1 );
	bSizer7531111->Add( m_TxtVersion, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputVersion = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputVersion->Enable( false );

	bSizer7531111->Add( m_InputVersion, 1, wxALL, 5 );


	bSizer126->Add( bSizer7531111, 1, wxEXPAND, 5 );


	bSizer93->Add( bSizer126, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer171;
	bSizer171 = new wxBoxSizer( wxVERTICAL );

	m_ProgramSearch = new wxSearchCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	#ifndef __WXMAC__
	m_ProgramSearch->ShowSearchButton( false );
	#endif
	m_ProgramSearch->ShowCancelButton( true );
	m_ProgramSearch->SetToolTip( _("Filter program matching specific word(s) (note: case sensitive)") );

	bSizer171->Add( m_ProgramSearch, 0, wxALL|wxEXPAND, 2 );

	m_ProgramData = new wxListCtrl( this, wxID_ANY, wxDefaultPosition, wxSize( -1,-1 ), wxLC_HRULES|wxLC_REPORT|wxLC_SINGLE_SEL );
	bSizer171->Add( m_ProgramData, 1, wxALL|wxEXPAND, 5 );


	bSizer93->Add( bSizer171, 1, wxEXPAND, 5 );


	bRootSizer->Add( bSizer93, 1, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer172;
	bSizer172 = new wxBoxSizer( wxVERTICAL );

	m_DetailsTab = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	m_UserAnswersPage = new wxPanel( m_DetailsTab, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer95;
	bSizer95 = new wxBoxSizer( wxVERTICAL );

	m_InputUser = new wxRichTextCtrl( m_UserAnswersPage, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY|wxVSCROLL|wxHSCROLL|wxWANTS_CHARS|wxBORDER_NONE );
	m_InputUser->SetMinSize( wxSize( 300,-1 ) );

	bSizer95->Add( m_InputUser, 1, wxEXPAND | wxALL, 5 );


	m_UserAnswersPage->SetSizer( bSizer95 );
	m_UserAnswersPage->Layout();
	bSizer95->Fit( m_UserAnswersPage );
	m_DetailsTab->AddPage( m_UserAnswersPage, _("User Answers"), true );
	m_AnswerPage = new wxPanel( m_DetailsTab, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bCountersRootSizer;
	bCountersRootSizer = new wxBoxSizer( wxVERTICAL );

	bCountersRootSizer->SetMinSize( wxSize( 200,-1 ) );
	m_CorrectValuesScroll = new wxScrolledWindow( m_AnswerPage, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxBORDER_SIMPLE );
	m_CorrectValuesScroll->SetScrollRate( 5, 5 );
	bFilterWindowSizer = new wxBoxSizer( wxVERTICAL );


	m_CorrectValuesScroll->SetSizer( bFilterWindowSizer );
	m_CorrectValuesScroll->Layout();
	bFilterWindowSizer->Fit( m_CorrectValuesScroll );
	bCountersRootSizer->Add( m_CorrectValuesScroll, 1, wxEXPAND | wxALL, 0 );


	m_AnswerPage->SetSizer( bCountersRootSizer );
	m_AnswerPage->Layout();
	bCountersRootSizer->Fit( m_AnswerPage );
	m_DetailsTab->AddPage( m_AnswerPage, _("Answer Counts"), false );
	m_CommentsPage = new wxPanel( m_DetailsTab, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer103;
	bSizer103 = new wxBoxSizer( wxVERTICAL );

	m_InputMark = new wxRichTextCtrl( m_CommentsPage, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY|wxVSCROLL|wxHSCROLL|wxWANTS_CHARS|wxBORDER_NONE );
	bSizer103->Add( m_InputMark, 1, wxEXPAND | wxALL, 5 );


	m_CommentsPage->SetSizer( bSizer103 );
	m_CommentsPage->Layout();
	bSizer103->Fit( m_CommentsPage );
	m_DetailsTab->AddPage( m_CommentsPage, _("Comments"), false );
	m_ProgramTimePage = new wxPanel( m_DetailsTab, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer108;
	bSizer108 = new wxBoxSizer( wxHORIZONTAL );

	wxBoxSizer* bSizer109;
	bSizer109 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer75;
	bSizer75 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtTimeToRespond = new wxStaticText( m_ProgramTimePage, wxID_ANY, _("Time to Respond"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtTimeToRespond->Wrap( -1 );
	bSizer75->Add( m_TxtTimeToRespond, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputTimeToRespond = new wxTextCtrl( m_ProgramTimePage, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputTimeToRespond->Enable( false );

	bSizer75->Add( m_InputTimeToRespond, 1, wxALL, 5 );


	bSizer109->Add( bSizer75, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer751;
	bSizer751 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtTimeout = new wxStaticText( m_ProgramTimePage, wxID_ANY, _("Response Timeout"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtTimeout->Wrap( -1 );
	bSizer751->Add( m_TxtTimeout, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputTimeResponseTimeout = new wxTextCtrl( m_ProgramTimePage, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputTimeResponseTimeout->Enable( false );

	bSizer751->Add( m_InputTimeResponseTimeout, 1, wxALL, 5 );


	bSizer109->Add( bSizer751, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer752;
	bSizer752 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtInterval = new wxStaticText( m_ProgramTimePage, wxID_ANY, _("Interval"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtInterval->Wrap( -1 );
	bSizer752->Add( m_TxtInterval, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputTimeInterval = new wxTextCtrl( m_ProgramTimePage, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputTimeInterval->Enable( false );

	bSizer752->Add( m_InputTimeInterval, 1, wxALL, 5 );


	bSizer109->Add( bSizer752, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer753;
	bSizer753 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtTimeBetweenChanges = new wxStaticText( m_ProgramTimePage, wxID_ANY, _("Time Between Changes"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtTimeBetweenChanges->Wrap( -1 );
	bSizer753->Add( m_TxtTimeBetweenChanges, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputTimeBetweenChanges = new wxTextCtrl( m_ProgramTimePage, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputTimeBetweenChanges->Enable( false );

	bSizer753->Add( m_InputTimeBetweenChanges, 1, wxALL, 5 );


	bSizer109->Add( bSizer753, 0, wxEXPAND, 5 );


	bSizer108->Add( bSizer109, 1, wxALL|wxEXPAND, 0 );

	wxBoxSizer* bSizer110;
	bSizer110 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer75311;
	bSizer75311 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtWordMin = new wxStaticText( m_ProgramTimePage, wxID_ANY, _("Words Min"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtWordMin->Wrap( -1 );
	bSizer75311->Add( m_TxtWordMin, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputWordMin = new wxTextCtrl( m_ProgramTimePage, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputWordMin->Enable( false );
	m_InputWordMin->SetMaxSize( wxSize( 50,-1 ) );

	bSizer75311->Add( m_InputWordMin, 0, wxALL, 5 );

	m_TxtWordMax = new wxStaticText( m_ProgramTimePage, wxID_ANY, _("Max"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtWordMax->Wrap( -1 );
	bSizer75311->Add( m_TxtWordMax, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputWordMax = new wxTextCtrl( m_ProgramTimePage, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputWordMax->Enable( false );
	m_InputWordMax->SetMaxSize( wxSize( 50,-1 ) );

	bSizer75311->Add( m_InputWordMax, 0, wxALL, 5 );


	bSizer110->Add( bSizer75311, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer7531;
	bSizer7531 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtLinesMin = new wxStaticText( m_ProgramTimePage, wxID_ANY, _("Lines Min"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtLinesMin->Wrap( -1 );
	bSizer7531->Add( m_TxtLinesMin, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputLinesMin = new wxTextCtrl( m_ProgramTimePage, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputLinesMin->Enable( false );
	m_InputLinesMin->SetMaxSize( wxSize( 50,-1 ) );

	bSizer7531->Add( m_InputLinesMin, 0, wxALL, 5 );

	m_TxtLinesMax = new wxStaticText( m_ProgramTimePage, wxID_ANY, _("Max"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtLinesMax->Wrap( -1 );
	bSizer7531->Add( m_TxtLinesMax, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputLinesMax = new wxTextCtrl( m_ProgramTimePage, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_InputLinesMax->Enable( false );
	m_InputLinesMax->SetMaxSize( wxSize( 50,-1 ) );

	bSizer7531->Add( m_InputLinesMax, 0, wxALL, 5 );


	bSizer110->Add( bSizer7531, 0, wxEXPAND, 5 );


	bSizer108->Add( bSizer110, 1, wxALL|wxEXPAND, 0 );


	m_ProgramTimePage->SetSizer( bSizer108 );
	m_ProgramTimePage->Layout();
	bSizer108->Fit( m_ProgramTimePage );
	m_DetailsTab->AddPage( m_ProgramTimePage, _("Program Details"), false );

	bSizer172->Add( m_DetailsTab, 1, wxEXPAND | wxALL, 2 );

	wxBoxSizer* bSizer1721;
	bSizer1721 = new wxBoxSizer( wxHORIZONTAL );


	bSizer1721->Add( 0, 0, 1, wxEXPAND, 0 );

	m_BtnUnlock = new wxButton( this, wxID_ANY, _("Unlock"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnUnlock->SetToolTip( _("Allow/Disallow editing") );

	bSizer1721->Add( m_BtnUnlock, 0, wxALL, 0 );


	bSizer1721->Add( 0, 0, 1, wxEXPAND, 0 );


	bSizer172->Add( bSizer1721, 0, wxEXPAND, 0 );


	bRootSizer->Add( bSizer172, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer96;
	bSizer96 = new wxBoxSizer( wxHORIZONTAL );

	m_BtnExit = new wxButton( this, wxID_ANY, _("Exit"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer96->Add( m_BtnExit, 0, wxALL, 5 );


	bSizer96->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnGenerateHTML = new wxButton( this, wxID_ANY, _("Generate HTML"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnGenerateHTML->Hide();

	bSizer96->Add( m_BtnGenerateHTML, 0, wxALL, 5 );

	m_BtnGenerateExcel = new wxButton( this, wxID_ANY, _("Generate Excel"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer96->Add( m_BtnGenerateExcel, 0, wxALL, 5 );


	bSizer96->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnUpdate = new wxButton( this, wxID_ANY, _("Update"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer96->Add( m_BtnUpdate, 0, wxALL, 5 );


	bRootSizer->Add( bSizer96, 0, wxEXPAND, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_ProgramSearch->Connect( wxEVT_COMMAND_SEARCHCTRL_CANCEL_BTN, wxCommandEventHandler( ProgramViewDlg::ProgramSearchCancel ), NULL, this );
	m_ProgramSearch->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( ProgramViewDlg::ProgramSearch ), NULL, this );
	m_BtnUnlock->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ProgramViewDlg::BtnUnlockClick ), NULL, this );
	m_BtnExit->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ProgramViewDlg::BtnBackClick ), NULL, this );
	m_BtnGenerateHTML->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ProgramViewDlg::BtnGenHTMLClick ), NULL, this );
	m_BtnGenerateExcel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ProgramViewDlg::BtnGenExcelClick ), NULL, this );
	m_BtnUpdate->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ProgramViewDlg::BtnUpdateClick ), NULL, this );
}

ProgramViewDlg::~ProgramViewDlg()
{
	// Disconnect Events
	m_ProgramSearch->Disconnect( wxEVT_COMMAND_SEARCHCTRL_CANCEL_BTN, wxCommandEventHandler( ProgramViewDlg::ProgramSearchCancel ), NULL, this );
	m_ProgramSearch->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( ProgramViewDlg::ProgramSearch ), NULL, this );
	m_BtnUnlock->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ProgramViewDlg::BtnUnlockClick ), NULL, this );
	m_BtnExit->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ProgramViewDlg::BtnBackClick ), NULL, this );
	m_BtnGenerateHTML->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ProgramViewDlg::BtnGenHTMLClick ), NULL, this );
	m_BtnGenerateExcel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ProgramViewDlg::BtnGenExcelClick ), NULL, this );
	m_BtnUpdate->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ProgramViewDlg::BtnUpdateClick ), NULL, this );

}

BankImportDlg::BankImportDlg( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bRootSizer;
	bRootSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer112;
	bSizer112 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer161;
	bSizer161 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtFilter = new wxStaticText( this, wxID_ANY, _("Filter"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtFilter->Wrap( -1 );
	bSizer161->Add( m_TxtFilter, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	wxArrayString m_ComboFilterChoices;
	m_ComboFilter = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_ComboFilterChoices, 0 );
	m_ComboFilter->SetSelection( 0 );
	bSizer161->Add( m_ComboFilter, 1, wxALL, 5 );


	bSizer112->Add( bSizer161, 1, wxEXPAND, 5 );


	bRootSizer->Add( bSizer112, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer113;
	bSizer113 = new wxBoxSizer( wxVERTICAL );

	m_GridFound = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	// Grid
	m_GridFound->CreateGrid( 0, 3 );
	m_GridFound->EnableEditing( true );
	m_GridFound->EnableGridLines( true );
	m_GridFound->EnableDragGridSize( false );
	m_GridFound->SetMargins( 0, 0 );

	// Columns
	m_GridFound->EnableDragColMove( false );
	m_GridFound->EnableDragColSize( true );
	m_GridFound->SetColLabelSize( 30 );
	m_GridFound->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	m_GridFound->EnableDragRowSize( true );
	m_GridFound->SetRowLabelSize( 80 );
	m_GridFound->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	m_GridFound->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	bSizer113->Add( m_GridFound, 1, wxALL|wxEXPAND, 5 );


	bRootSizer->Add( bSizer113, 1, wxEXPAND, 5 );

	wxBoxSizer* bButtonGrp;
	bButtonGrp = new wxBoxSizer( wxHORIZONTAL );

	m_BtnCancel = new wxButton( this, wxID_ANY, _("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	bButtonGrp->Add( m_BtnCancel, 0, wxALL, 5 );


	bButtonGrp->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnDelete = new wxButton( this, wxID_ANY, _("Delete"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnDelete->SetToolTip( _("Remove selected word from the import") );

	bButtonGrp->Add( m_BtnDelete, 0, wxALL, 5 );


	bButtonGrp->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnAccept = new wxButton( this, wxID_ANY, _("Accept"), wxDefaultPosition, wxDefaultSize, 0 );
	m_BtnAccept->SetToolTip( _("Add all words to the bank") );

	bButtonGrp->Add( m_BtnAccept, 0, wxALL, 5 );


	bRootSizer->Add( bButtonGrp, 0, wxEXPAND, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_ComboFilter->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( BankImportDlg::ComboFilterChange ), NULL, this );
	m_GridFound->Connect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( BankImportDlg::GridCellLeftClick ), NULL, this );
	m_GridFound->Connect( wxEVT_GRID_EDITOR_HIDDEN, wxGridEventHandler( BankImportDlg::GridEditorHidden ), NULL, this );
	m_BtnCancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankImportDlg::BtnCancelClick ), NULL, this );
	m_BtnDelete->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankImportDlg::BtnDeleteClick ), NULL, this );
	m_BtnAccept->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankImportDlg::BtnAcceptClick ), NULL, this );
}

BankImportDlg::~BankImportDlg()
{
	// Disconnect Events
	m_ComboFilter->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( BankImportDlg::ComboFilterChange ), NULL, this );
	m_GridFound->Disconnect( wxEVT_GRID_CELL_LEFT_CLICK, wxGridEventHandler( BankImportDlg::GridCellLeftClick ), NULL, this );
	m_GridFound->Disconnect( wxEVT_GRID_EDITOR_HIDDEN, wxGridEventHandler( BankImportDlg::GridEditorHidden ), NULL, this );
	m_BtnCancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankImportDlg::BtnCancelClick ), NULL, this );
	m_BtnDelete->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankImportDlg::BtnDeleteClick ), NULL, this );
	m_BtnAccept->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BankImportDlg::BtnAcceptClick ), NULL, this );

}

PasswordDlg::PasswordDlg( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bRootSizer;
	bRootSizer = new wxBoxSizer( wxVERTICAL );

	m_TxtPassword = new wxStaticText( this, wxID_ANY, _("Enter administrator password"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TxtPassword->Wrap( -1 );
	bRootSizer->Add( m_TxtPassword, 0, wxALL, 5 );

	m_InputPassword = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD|wxTE_PROCESS_ENTER );
	m_InputPassword->SetToolTip( _("Enter administrator password (3 attempts allowed)") );

	m_InputPassword->SetValidator( wxTextValidator( wxFILTER_ALPHA, &m_ValPassword ) );

	bRootSizer->Add( m_InputPassword, 0, wxALL|wxEXPAND, 5 );


	bRootSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer168;
	bSizer168 = new wxBoxSizer( wxHORIZONTAL );


	bSizer168->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnOK = new wxButton( this, wxID_ANY, _("OK"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer168->Add( m_BtnOK, 0, wxALL, 5 );


	bRootSizer->Add( bSizer168, 0, wxEXPAND, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_InputPassword->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( PasswordDlg::InputTextEnterPress ), NULL, this );
	m_BtnOK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( PasswordDlg::BtnOKClick ), NULL, this );
}

PasswordDlg::~PasswordDlg()
{
	// Disconnect Events
	m_InputPassword->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( PasswordDlg::InputTextEnterPress ), NULL, this );
	m_BtnOK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( PasswordDlg::BtnOKClick ), NULL, this );

}

NewEditBankDlg::NewEditBankDlg( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bRootSizer;
	bRootSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer422;
	bSizer422 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtBankName = new wxStaticText( this, wxID_ANY, _("Bank Name"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT|wxALIGN_CENTER_HORIZONTAL );
	m_TxtBankName->Wrap( -1 );
	bSizer422->Add( m_TxtBankName, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputBankName = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer422->Add( m_InputBankName, 1, wxALL|wxEXPAND, 5 );


	bRootSizer->Add( bSizer422, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer4221;
	bSizer4221 = new wxBoxSizer( wxHORIZONTAL );

	m_TxtComment = new wxStaticText( this, wxID_ANY, _("Comment"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT|wxALIGN_CENTER_HORIZONTAL );
	m_TxtComment->Wrap( -1 );
	bSizer4221->Add( m_TxtComment, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_InputComments = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer4221->Add( m_InputComments, 1, wxALL|wxEXPAND, 5 );


	bRootSizer->Add( bSizer4221, 0, wxEXPAND, 5 );


	bRootSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer40;
	bSizer40 = new wxBoxSizer( wxHORIZONTAL );


	bSizer40->Add( 0, 0, 1, wxEXPAND, 5 );

	m_BtnCancel = new wxButton( this, wxID_ANY, _("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer40->Add( m_BtnCancel, 0, wxALL, 5 );

	m_BtnAddEdit = new wxButton( this, wxID_ANY, _("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer40->Add( m_BtnAddEdit, 0, wxALL, 5 );


	bRootSizer->Add( bSizer40, 0, wxEXPAND, 5 );


	this->SetSizer( bRootSizer );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_InputBankName->Connect( wxEVT_KEY_UP, wxKeyEventHandler( NewEditBankDlg::BankNewEditKeyUp ), NULL, this );
	m_InputComments->Connect( wxEVT_KEY_UP, wxKeyEventHandler( NewEditBankDlg::BankNewEditKeyUp ), NULL, this );
	m_BtnCancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( NewEditBankDlg::BtnCancelClick ), NULL, this );
	m_BtnAddEdit->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( NewEditBankDlg::BtnAddEditClick ), NULL, this );
}

NewEditBankDlg::~NewEditBankDlg()
{
	// Disconnect Events
	m_InputBankName->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( NewEditBankDlg::BankNewEditKeyUp ), NULL, this );
	m_InputComments->Disconnect( wxEVT_KEY_UP, wxKeyEventHandler( NewEditBankDlg::BankNewEditKeyUp ), NULL, this );
	m_BtnCancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( NewEditBankDlg::BtnCancelClick ), NULL, this );
	m_BtnAddEdit->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( NewEditBankDlg::BtnAddEditClick ), NULL, this );

}
