///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1-0-g8feb16b3)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/sizer.h>
#include <wx/statusbr.h>
#include <wx/frame.h>
#include <wx/stattext.h>
#include <wx/choice.h>
#include <wx/button.h>
#include <wx/grid.h>
#include <wx/panel.h>
#include <wx/textctrl.h>
#include <wx/valtext.h>
#include <wx/srchctrl.h>
#include <wx/richtext/richtextctrl.h>
#include <wx/listctrl.h>
#include <wx/scrolwin.h>
#include <wx/notebook.h>
#include <wx/datectrl.h>
#include <wx/dateevt.h>
#include <wx/dialog.h>
#include <wx/checkbox.h>
#include <wx/clrpicker.h>
#include <wx/spinctrl.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class MainFrame
///////////////////////////////////////////////////////////////////////////////
class MainFrame : public wxFrame
{
	private:

	protected:
		wxMenuBar* m_MenuBarMain;
		wxMenu* m_MenuSettings;
		wxMenu* m_MenuAbout;
		wxStatusBar* m_StatusBarMain;

		// Virtual event handlers, override them in your derived class
		virtual void CloseEvent( wxCloseEvent& event ) { event.Skip(); }
		virtual void IdleEvent( wxIdleEvent& event ) { event.Skip(); }
		virtual void SizeEvent( wxSizeEvent& event ) { event.Skip(); }
		virtual void MenuSettingsClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void MenuAboutClick( wxCommandEvent& event ) { event.Skip(); }


	public:
		wxBoxSizer* bMainFrameSizer;

		MainFrame( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("T E C S"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 800,600 ), long style = wxCAPTION|wxCLOSE_BOX|wxDEFAULT_FRAME_STYLE|wxSYSTEM_MENU|wxTAB_TRAVERSAL );

		~MainFrame();

};

///////////////////////////////////////////////////////////////////////////////
/// Class PhasePrograms
///////////////////////////////////////////////////////////////////////////////
class PhasePrograms : public wxPanel
{
	private:

	protected:
		wxStaticText* m_TxtPhaseFilter;
		wxChoice* m_ComboPhase;
		wxButton* m_BtnEditPhases;
		wxGrid* m_PhaseProgramList;
		wxButton* m_BtnCreate;
		wxButton* m_BtnEdit;
		wxButton* m_BtnCopy;
		wxButton* m_BtnDelete;
		wxButton* m_BtnBack;

		// Virtual event handlers, override them in your derived class
		virtual void PhaseFilterChange( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnEditPhasesClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void GridCellLeftDoubleClick( wxGridEvent& event ) { event.Skip(); }
		virtual void BtnCreateClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnEditClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnCopyClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnDeleteClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnBackClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		PhasePrograms( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,300 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~PhasePrograms();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Bank
///////////////////////////////////////////////////////////////////////////////
class Bank : public wxPanel
{
	private:

	protected:
		wxBoxSizer* bRootSizer;
		wxStaticText* m_TxtBankName;
		wxTextCtrl* m_BankName;
		wxStaticText* m_TxtFilter;
		wxChoice* m_ComboFilter;
		wxButton* m_BtnEditFilters;
		wxStaticText* m_TxtSearch;
		wxSearchCtrl* m_Search;
		wxStaticText* m_TxtFilterCount;
		wxTextCtrl* m_WordCount;
		wxGrid* m_WordBankGrid;
		wxButton* m_BtnAdd;
		wxButton* m_BtnAddWithChildren;
		wxButton* m_BtnEdit;
		wxButton* m_BtnDelete;
		wxButton* m_BtnExport;
		wxButton* m_BtnBack;

		// Virtual event handlers, override them in your derived class
		virtual void WordCountKeyUp( wxKeyEvent& event ) { event.Skip(); }
		virtual void BankFilterChange( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnEditFiltersClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BankSearchCancel( wxCommandEvent& event ) { event.Skip(); }
		virtual void BankSearch( wxCommandEvent& event ) { event.Skip(); }
		virtual void GridCellLeftClick( wxGridEvent& event ) { event.Skip(); }
		virtual void GridCellLeftDoubleClick( wxGridEvent& event ) { event.Skip(); }
		virtual void GridRangeSelect( wxGridRangeSelectEvent& event ) { event.Skip(); }
		virtual void BtnAddClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnAddWithChildrenClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnEditClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnDeleteClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnExportClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnBackClick( wxCommandEvent& event ) { event.Skip(); }


	public:
		wxString m_ValWordCount;

		Bank( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 706,520 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~Bank();

};

///////////////////////////////////////////////////////////////////////////////
/// Class BankSelection
///////////////////////////////////////////////////////////////////////////////
class BankSelection : public wxPanel
{
	private:

	protected:
		wxStaticText* m_TxtSelectBank;
		wxGrid* m_BankFileList;
		wxButton* m_BtnCreate;
		wxButton* m_BtnEdit;
		wxButton* m_BtnCopy;
		wxButton* m_BtnDelete;
		wxStaticText* m_TxtBankPreview;
		wxStaticText* m_TxtSearch;
		wxSearchCtrl* m_Search;
		wxGrid* m_BankPreview;
		wxButton* m_BtnBack;
		wxButton* m_BtnSelectBank;

		// Virtual event handlers, override them in your derived class
		virtual void FileGridCellLeftClick( wxGridEvent& event ) { event.Skip(); }
		virtual void FileGridCellLeftDoubleClick( wxGridEvent& event ) { event.Skip(); }
		virtual void BtnCreateClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnEditClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnCopyClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnDeleteClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BankSearchCancel( wxCommandEvent& event ) { event.Skip(); }
		virtual void BankSearch( wxCommandEvent& event ) { event.Skip(); }
		virtual void BankGridCellLeftClick( wxGridEvent& event ) { event.Skip(); }
		virtual void BtnBackClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnSelectClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		BankSelection( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,300 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~BankSelection();

};

///////////////////////////////////////////////////////////////////////////////
/// Class ParticipantInfo
///////////////////////////////////////////////////////////////////////////////
class ParticipantInfo : public wxPanel
{
	private:

	protected:
		wxStaticText* m_TxtFilter;
		wxChoice* m_ComboFilter;
		wxStaticText* m_TxtSearch;
		wxSearchCtrl* m_Search;
		wxGrid* m_ParticipantGrid;
		wxButton* m_BtnView;
		wxButton* m_BtnDelete;
		wxButton* m_BtnBack;

		// Virtual event handlers, override them in your derived class
		virtual void PhaseFilterChange( wxCommandEvent& event ) { event.Skip(); }
		virtual void IdSearchCancel( wxCommandEvent& event ) { event.Skip(); }
		virtual void IdSearch( wxCommandEvent& event ) { event.Skip(); }
		virtual void GridCellLeftClick( wxGridEvent& event ) { event.Skip(); }
		virtual void GridCellLeftDoubleClick( wxGridEvent& event ) { event.Skip(); }
		virtual void GridRangeSelect( wxGridRangeSelectEvent& event ) { event.Skip(); }
		virtual void BtnViewClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnDeleteClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnBackClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		ParticipantInfo( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 609,460 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~ParticipantInfo();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Tutorial
///////////////////////////////////////////////////////////////////////////////
class Tutorial : public wxPanel
{
	private:

	protected:
		wxBoxSizer* bRootSizer;
		wxRichTextCtrl* m_TxtTutorial;
		wxButton* m_BtnContinue;

		// Virtual event handlers, override them in your derived class
		virtual void KeyUpEvent( wxKeyEvent& event ) { event.Skip(); }
		virtual void ResizePanel( wxSizeEvent& event ) { event.Skip(); }
		virtual void BtnContinueClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		Tutorial( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 700,300 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~Tutorial();

};

///////////////////////////////////////////////////////////////////////////////
/// Class PhaseRun
///////////////////////////////////////////////////////////////////////////////
class PhaseRun : public wxPanel
{
	private:

	protected:
		wxBoxSizer* bRootSizer;
		wxRichTextCtrl* m_TxtTutorial;
		wxButton* m_BtnContinue;

		// Virtual event handlers, override them in your derived class
		virtual void KeyUpEvent( wxKeyEvent& event ) { event.Skip(); }
		virtual void ResizePanel( wxSizeEvent& event ) { event.Skip(); }
		virtual void OnTextAreaResize( wxSizeEvent& event ) { event.Skip(); }
		virtual void BtnContinueClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		PhaseRun( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 700,300 ), long style = 0, const wxString& name = wxEmptyString );

		~PhaseRun();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Entry
///////////////////////////////////////////////////////////////////////////////
class Entry : public wxPanel
{
	private:

	protected:
		wxButton* m_BtnAdmin;
		wxButton* m_BtnParticipant;

		// Virtual event handlers, override them in your derived class
		virtual void BtnAdminClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnParticipantClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		Entry( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,300 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~Entry();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Admin
///////////////////////////////////////////////////////////////////////////////
class Admin : public wxPanel
{
	private:

	protected:
		wxButton* m_BtnBank;
		wxButton* m_BtnPhasePrograms;
		wxButton* m_BtnParticipantInfo;
		wxButton* m_BtnBack;

		// Virtual event handlers, override them in your derived class
		virtual void BtnBankManageClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnPhaseProgramClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnParticipantInfoButtonClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnBackClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		Admin( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,300 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~Admin();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Questions
///////////////////////////////////////////////////////////////////////////////
class Questions : public wxPanel
{
	private:

	protected:
		wxStaticText* m_TxtQuestion;
		wxRichTextCtrl* m_InputText;
		wxPanel* m_ProgramPanel;
		wxSearchCtrl* m_ProgramSearch;
		wxStaticText* m_TxtAnswersTitle;
		wxListCtrl* m_ProgramData;
		wxBoxSizer* bMarkSizer;
		wxPanel* m_CheckPanel;
		wxNotebook* m_EvaluationTab;
		wxPanel* m_AnswerPage;
		wxScrolledWindow* m_CorrectValuesScroll;
		wxBoxSizer* bFilterWindowSizer;
		wxPanel* m_CommentsPage;
		wxRichTextCtrl* m_InputMark;
		wxButton* m_BtnEnd;

		// Virtual event handlers, override them in your derived class
		virtual void ProgramSearchCancel( wxCommandEvent& event ) { event.Skip(); }
		virtual void ProgramSearch( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnDoneClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		Questions( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~Questions();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Participant
///////////////////////////////////////////////////////////////////////////////
class Participant : public wxPanel
{
	private:

	protected:
		wxStaticText* m_TxtParticipantTitle;
		wxStaticText* m_TxtIDWarn;
		wxStaticText* m_TxtPhaseSelect;
		wxChoice* m_ComboPhase;
		wxStaticText* m_TxtPhaseProgram;
		wxChoice* m_ComboProgram;
		wxStaticText* m_TxtIdentifier;
		wxTextCtrl* m_InputId;
		wxStaticText* m_TxtDOB;
		wxDatePickerCtrl* m_DateDOB;
		wxStaticText* m_TxtGender;
		wxChoice* m_ComboGender;
		wxStaticText* m_TxtNumericTesterInfo;
		wxTextCtrl* m_InputNumTest;
		wxButton* m_BtnBack;
		wxButton* m_BtnStart;

		// Virtual event handlers, override them in your derived class
		virtual void OnChangePhase( wxCommandEvent& event ) { event.Skip(); }
		virtual void InputIdKeyUp( wxKeyEvent& event ) { event.Skip(); }
		virtual void NumTestKeyUp( wxKeyEvent& event ) { event.Skip(); }
		virtual void BtnBackClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnStartClick( wxCommandEvent& event ) { event.Skip(); }


	public:
		wxString m_ValInputId;
		wxString m_ValTestNumId;

		Participant( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~Participant();

};

///////////////////////////////////////////////////////////////////////////////
/// Class AddEditBankDlg
///////////////////////////////////////////////////////////////////////////////
class AddEditBankDlg : public wxDialog
{
	private:

	protected:
		wxStaticText* m_TxtWordOrSentence;
		wxTextCtrl* m_InputWordOrSentence;
		wxStaticText* m_TxtFilter;
		wxChoice* m_ComboFilter;
		wxBoxSizer* bMessageSizer;
		wxStaticText* m_TxtMessage;
		wxButton* m_BtnCancel;
		wxButton* m_BtnAddEdit;

		// Virtual event handlers, override them in your derived class
		virtual void KeyUpEvent( wxKeyEvent& event ) { event.Skip(); }
		virtual void BtnCancelClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnAddEditClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		AddEditBankDlg( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Add To Bank"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 513,213 ), long style = wxDEFAULT_DIALOG_STYLE );

		~AddEditBankDlg();

};

///////////////////////////////////////////////////////////////////////////////
/// Class AddEditWithChildrenBankDlg
///////////////////////////////////////////////////////////////////////////////
class AddEditWithChildrenBankDlg : public wxDialog
{
	private:

	protected:
		wxStaticText* m_TxtFilter;
		wxChoice* m_ComboFilter;
		wxBoxSizer* bListSizer;
		wxStaticText* m_TxtWordOrSentence1;
		wxTextCtrl* m_InputWordOrSentence1;
		wxBoxSizer* bMessageSizer1;
		wxStaticText* m_TxtMessage1;
		wxStaticText* m_TxtWordOrSentence2;
		wxTextCtrl* m_InputWordOrSentence2;
		wxBoxSizer* bMessageSizer2;
		wxStaticText* m_TxtMessage2;
		wxStaticText* m_TxtWordOrSentence3;
		wxTextCtrl* m_InputWordOrSentence3;
		wxBoxSizer* bMessageSizer3;
		wxStaticText* m_TxtMessage3;
		wxButton* m_BtnCancel;
		wxButton* m_BtnAddEdit;

		// Virtual event handlers, override them in your derived class
		virtual void KeyUpEvent( wxKeyEvent& event ) { event.Skip(); }
		virtual void BtnCancelClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnAddEditClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		AddEditWithChildrenBankDlg( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Add To Bank"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 650,210 ), long style = wxDEFAULT_DIALOG_STYLE );

		~AddEditWithChildrenBankDlg();

};

///////////////////////////////////////////////////////////////////////////////
/// Class AboutDlg
///////////////////////////////////////////////////////////////////////////////
class AboutDlg : public wxDialog
{
	private:

	protected:
		wxRichTextCtrl* m_AboutInfo;
		wxStdDialogButtonSizer* m_sdbSizer1;
		wxButton* m_sdbSizer1OK;

		// Virtual event handlers, override them in your derived class
		virtual void URLClick( wxTextUrlEvent& event ) { event.Skip(); }
		virtual void AboutDlgClickOK( wxCommandEvent& event ) { event.Skip(); }


	public:

		AboutDlg( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("About T E C S"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 312,392 ), long style = wxDEFAULT_DIALOG_STYLE );

		~AboutDlg();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CreateEditProgramsDlg
///////////////////////////////////////////////////////////////////////////////
class CreateEditProgramsDlg : public wxDialog
{
	private:

	protected:
		wxNotebook* m_CreateEditTabs;
		wxPanel* m_SettingsPanel;
		wxStaticText* m_TxtProgramName;
		wxTextCtrl* m_InputProgramName;
		wxStaticText* m_TxtPhase;
		wxChoice* m_ComboPhase;
		wxStaticText* m_TxtComment;
		wxTextCtrl* m_InputComments;
		wxCheckBox* m_CheckDefaults;
		wxStaticText* m_TxtTimeToRespond;
		wxTextCtrl* m_InputTimeToRespond;
		wxStaticText* m_DefTimeToRespond;
		wxStaticText* m_TxtTimeout;
		wxTextCtrl* m_InputTimeResponseTimeout;
		wxStaticText* m_DefResponseTimeout;
		wxStaticText* m_TxtInterval;
		wxTextCtrl* m_InputTimeInterval;
		wxStaticText* m_DefInterval;
		wxStaticText* m_TxtTimeBetweenChanges;
		wxTextCtrl* m_InputTimeBetweenChanges;
		wxStaticText* m_DefTimeBetweenChanges;
		wxStaticText* m_TxtWordMin;
		wxTextCtrl* m_InputWordMin;
		wxStaticText* m_TxtWordMax;
		wxTextCtrl* m_InputWordMax;
		wxStaticText* m_TxtLinesMin;
		wxTextCtrl* m_InputLinesMin;
		wxStaticText* m_TxtLinesMax;
		wxTextCtrl* m_InputLinesMax;
		wxScrolledWindow* m_SettingsFilterScroll;
		wxBoxSizer* bSettingsScrollBoxSizer;
		wxPanel* m_BankPanel;
		wxBoxSizer* bBankPanelRootSizer;
		wxStaticText* m_TxtBank;
		wxChoice* m_ComboBank;
		wxStaticText* m_TxtFilter;
		wxChoice* m_ComboFilter;
		wxBoxSizer* bGridSizer;
		wxGrid* m_WordBankGrid;
		wxBoxSizer* bBankButtonSizer;
		wxButton* m_BtnAdd;
		wxButton* m_BtnAddWithChildren;
		wxButton* m_BtnEdit;
		wxButton* m_BtnDelete;
		wxBoxSizer* bBankFilterBox;
		wxStaticText* m_TxtSelectCount;
		wxScrolledWindow* m_BankFilterScroll;
		wxBoxSizer* bBankScrollBoxSizer;
		wxButton* m_BtnAutoSelect;
		wxTextCtrl* m_TxtCtrlLog;
		wxPanel* m_OrderPanel;
		wxBoxSizer* bOrderSizer;
		wxGrid* m_OrderList;
		wxButton* m_BtnRemove;
		wxButton* m_BtnMoveUp;
		wxButton* m_BtnMoveDown;
		wxScrolledWindow* m_BlockDetailScroll;
		wxBoxSizer* bFilterWindowSizer;
		wxChoice* m_BreakChoice;
		wxCheckBox* m_CheckRandom;
		wxButton* m_BtnAddVal;
		wxButton* m_BtnCancel;
		wxButton* m_BtnCreateEdit;

		// Virtual event handlers, override them in your derived class
		virtual void EventInitDialog( wxInitDialogEvent& event ) { event.Skip(); }
		virtual void PageChanged( wxNotebookEvent& event ) { event.Skip(); }
		virtual void PageChanging( wxNotebookEvent& event ) { event.Skip(); }
		virtual void SettingsKeyUp( wxKeyEvent& event ) { event.Skip(); }
		virtual void ComboPhaseChange( wxCommandEvent& event ) { event.Skip(); }
		virtual void ChangeDefaultCheckBox( wxCommandEvent& event ) { event.Skip(); }
		virtual void ComboBankChange( wxCommandEvent& event ) { event.Skip(); }
		virtual void BankFilterChange( wxCommandEvent& event ) { event.Skip(); }
		virtual void GridCellLeftClick( wxGridEvent& event ) { event.Skip(); }
		virtual void GridCellLeftDoubleClick( wxGridEvent& event ) { event.Skip(); }
		virtual void GridRangeSelect( wxGridRangeSelectEvent& event ) { event.Skip(); }
		virtual void BtnAddClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnAddWithChildrenClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnEditClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnDeleteClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OrderGridCellLeftClick( wxGridEvent& event ) { event.Skip(); }
		virtual void BtnRemoveOrderClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnMoveUpOrderClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnMoveDownOrderClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BlockBreakChoice( wxCommandEvent& event ) { event.Skip(); }
		virtual void RandomCheck( wxCommandEvent& event ) { event.Skip(); }
		virtual void AddBlockClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnCancelClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnCreateEditClick( wxCommandEvent& event ) { event.Skip(); }


	public:
		wxString m_ValTimeToRespond;
		wxString m_ValResponseTimeout;
		wxString m_ValInterval;
		wxString m_ValTimeBetweenChanges;
		wxString m_ValWordsMin;
		wxString m_ValWordsMax;
		wxString m_ValLinesMin;
		wxString m_ValLinesMax;

		CreateEditProgramsDlg( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Create Program"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 700,600 ), long style = wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER );

		~CreateEditProgramsDlg();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CustomEditDlg
///////////////////////////////////////////////////////////////////////////////
class CustomEditDlg : public wxDialog
{
	private:

	protected:
		wxStaticText* m_TxtDefault;
		wxGrid* m_DefaultGridList;
		wxStaticText* m_TxtCustom;
		wxGrid* m_CustomGridList;
		wxButton* m_BtnAdd;
		wxButton* m_BtnRemove;
		wxButton* m_BtnDone;

		// Virtual event handlers, override them in your derived class
		virtual void InitDialog( wxInitDialogEvent& event ) { event.Skip(); }
		virtual void GridCellLeftClick( wxGridEvent& event ) { event.Skip(); }
		virtual void BtnAddClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnRemoveClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnDoneClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		CustomEditDlg( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Filter Edit"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 518,469 ), long style = wxDEFAULT_DIALOG_STYLE );

		~CustomEditDlg();

};

///////////////////////////////////////////////////////////////////////////////
/// Class SettingsDlg
///////////////////////////////////////////////////////////////////////////////
class SettingsDlg : public wxDialog
{
	private:

	protected:
		wxNotebook* m_SettingsTabs;
		wxPanel* m_GeneralPanel;
		wxCheckBox* m_CheckUseAdminPass;
		wxPanel* m_PhaseSettingsPanel;
		wxStaticText* m_TxtPhase;
		wxChoice* m_ComboPhase;
		wxStaticText* m_TxtTimeToRespond;
		wxTextCtrl* m_InputTimeToRespond;
		wxStaticText* m_DefTimeToRespond;
		wxStaticText* m_TxtTimeout;
		wxTextCtrl* m_InputTimeResponseTimeout;
		wxStaticText* m_DefResponseTimeout;
		wxStaticText* m_TxtInterval;
		wxTextCtrl* m_InputTimeInterval;
		wxStaticText* m_DefInterval;
		wxStaticText* m_TxtTimeBetweenChanges;
		wxTextCtrl* m_InputTimeBetweenChanges;
		wxStaticText* m_DefTimeBetweenChanges;
		wxButton* m_BtnResetPhase;
		wxScrolledWindow* m_SettingsFilterScroll;
		wxBoxSizer* bSettingsScrollBoxSizer;
		wxPanel* m_TextAndBackgroundPanel;
		wxStaticText* m_TxtBackgroundColour;
		wxColourPickerCtrl* m_PickerTextBG;
		wxButton* m_ResetBG;
		wxStaticText* m_TxtTextColour;
		wxColourPickerCtrl* m_PickerText;
		wxButton* m_ResetText;
		wxStaticText* m_TxtInfoText;
		wxColourPickerCtrl* m_PickerInfoText;
		wxButton* m_ResetInfo;
		wxStaticText* m_TxtBorderColour;
		wxColourPickerCtrl* m_PickerBorder;
		wxButton* m_ResetBorder;
		wxStaticText* m_TxtBorderThickness;
		wxSpinCtrl* m_BorderThicknessCtrl;
		wxButton* m_ResetBorderThickness;
		wxStaticText* m_TxtBorderWidth;
		wxSpinCtrl* m_BorderWidthCtrl;
		wxButton* m_ResetBorderWidth;
		wxStaticText* m_TxtBorderHeight;
		wxSpinCtrl* m_BorderHeightCtrl;
		wxButton* m_ResetBorderHeight;
		wxPanel* m_TutorialInfoPanel;
		wxStaticText* m_TxtTutPhase;
		wxChoice* m_ComboTutPhase;
		wxStaticText* m_TxtFontSize;
		wxSpinCtrl* m_FontSizeControl;
		wxStaticText* m_TxtTutorial;
		wxTextCtrl* m_TextCtrlTutorial;
		wxStaticText* m_TxtTutorialEnd;
		wxTextCtrl* m_TextCtrlTutorialEnd;
		wxStaticText* m_TxtTutorial1;
		wxTextCtrl* m_TextControlTut1;
		wxStaticText* m_TxtEx1;
		wxTextCtrl* m_TextControlEx1;
		wxStaticText* m_TxtTutorial2;
		wxTextCtrl* m_TextControlTut2;
		wxStaticText* m_TxtEx2;
		wxTextCtrl* m_TextControlEx2;
		wxStaticText* m_TxtTutorial3;
		wxTextCtrl* m_TextControlTut3;
		wxStaticText* m_TxtEx3;
		wxTextCtrl* m_TextControlEx3;
		wxStaticText* m_TxtCounts;
		wxTextCtrl* m_TextControlCounts;
		wxStaticText* m_TxtWait;
		wxTextCtrl* m_TextControlWait;
		wxButton* m_BtnDone;

		// Virtual event handlers, override them in your derived class
		virtual void EventInitDialog( wxInitDialogEvent& event ) { event.Skip(); }
		virtual void PageChanged( wxNotebookEvent& event ) { event.Skip(); }
		virtual void PageChanging( wxNotebookEvent& event ) { event.Skip(); }
		virtual void CheckChangeAdminUse( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnChangePhase( wxCommandEvent& event ) { event.Skip(); }
		virtual void PhaseValChange( wxKeyEvent& event ) { event.Skip(); }
		virtual void BtnResetTimesClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnBGColChange( wxColourPickerEvent& event ) { event.Skip(); }
		virtual void BtnResetBGClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextColChange( wxColourPickerEvent& event ) { event.Skip(); }
		virtual void BtnResetTextClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextInfoColChange( wxColourPickerEvent& event ) { event.Skip(); }
		virtual void BtnResetTextInfoClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextBorderColChange( wxColourPickerEvent& event ) { event.Skip(); }
		virtual void BtnResetTextBorderClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnBorderThicknessChange( wxSpinEvent& event ) { event.Skip(); }
		virtual void OnBorderThicknessChangeTxt( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnResetTextBorderThicknessClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnBorderWidthChange( wxSpinEvent& event ) { event.Skip(); }
		virtual void OnBorderWidthChangeTxt( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnResetTextBorderWidthClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnBorderHeightChange( wxSpinEvent& event ) { event.Skip(); }
		virtual void OnBorderHeightChangeTxt( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnResetTextBorderHeightClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnChangeFontSize( wxSpinEvent& event ) { event.Skip(); }
		virtual void BtnDoneClick( wxCommandEvent& event ) { event.Skip(); }


	public:
		wxString m_ValTimeToRespond;
		wxString m_ValResponseTimeout;
		wxString m_ValInterval;
		wxString m_ValTimeBetweenChanges;

		SettingsDlg( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Settings"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 625,600 ), long style = wxDEFAULT_DIALOG_STYLE );

		~SettingsDlg();

};

///////////////////////////////////////////////////////////////////////////////
/// Class ProgramViewDlg
///////////////////////////////////////////////////////////////////////////////
class ProgramViewDlg : public wxDialog
{
	private:

	protected:
		wxStaticText* m_TxtProgramName;
		wxTextCtrl* m_InputProgramName;
		wxStaticText* m_TxtVersion;
		wxTextCtrl* m_InputVersion;
		wxSearchCtrl* m_ProgramSearch;
		wxListCtrl* m_ProgramData;
		wxNotebook* m_DetailsTab;
		wxPanel* m_UserAnswersPage;
		wxRichTextCtrl* m_InputUser;
		wxPanel* m_AnswerPage;
		wxScrolledWindow* m_CorrectValuesScroll;
		wxBoxSizer* bFilterWindowSizer;
		wxPanel* m_CommentsPage;
		wxRichTextCtrl* m_InputMark;
		wxPanel* m_ProgramTimePage;
		wxStaticText* m_TxtTimeToRespond;
		wxTextCtrl* m_InputTimeToRespond;
		wxStaticText* m_TxtTimeout;
		wxTextCtrl* m_InputTimeResponseTimeout;
		wxStaticText* m_TxtInterval;
		wxTextCtrl* m_InputTimeInterval;
		wxStaticText* m_TxtTimeBetweenChanges;
		wxTextCtrl* m_InputTimeBetweenChanges;
		wxStaticText* m_TxtWordMin;
		wxTextCtrl* m_InputWordMin;
		wxStaticText* m_TxtWordMax;
		wxTextCtrl* m_InputWordMax;
		wxStaticText* m_TxtLinesMin;
		wxTextCtrl* m_InputLinesMin;
		wxStaticText* m_TxtLinesMax;
		wxTextCtrl* m_InputLinesMax;
		wxButton* m_BtnUnlock;
		wxButton* m_BtnExit;
		wxButton* m_BtnGenerateHTML;
		wxButton* m_BtnGenerateExcel;
		wxButton* m_BtnUpdate;

		// Virtual event handlers, override them in your derived class
		virtual void ProgramSearchCancel( wxCommandEvent& event ) { event.Skip(); }
		virtual void ProgramSearch( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnUnlockClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnBackClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnGenHTMLClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnGenExcelClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnUpdateClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		ProgramViewDlg( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Program Details"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 700,600 ), long style = wxDEFAULT_DIALOG_STYLE );

		~ProgramViewDlg();

};

///////////////////////////////////////////////////////////////////////////////
/// Class BankImportDlg
///////////////////////////////////////////////////////////////////////////////
class BankImportDlg : public wxDialog
{
	private:

	protected:
		wxStaticText* m_TxtFilter;
		wxChoice* m_ComboFilter;
		wxGrid* m_GridFound;
		wxButton* m_BtnCancel;
		wxButton* m_BtnDelete;
		wxButton* m_BtnAccept;

		// Virtual event handlers, override them in your derived class
		virtual void ComboFilterChange( wxCommandEvent& event ) { event.Skip(); }
		virtual void GridCellLeftClick( wxGridEvent& event ) { event.Skip(); }
		virtual void GridEditorHidden( wxGridEvent& event ) { event.Skip(); }
		virtual void BtnCancelClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnDeleteClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnAcceptClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		BankImportDlg( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Import"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 700,600 ), long style = wxDEFAULT_DIALOG_STYLE );

		~BankImportDlg();

};

///////////////////////////////////////////////////////////////////////////////
/// Class PasswordDlg
///////////////////////////////////////////////////////////////////////////////
class PasswordDlg : public wxDialog
{
	private:

	protected:
		wxStaticText* m_TxtPassword;
		wxTextCtrl* m_InputPassword;
		wxButton* m_BtnOK;

		// Virtual event handlers, override them in your derived class
		virtual void InputTextEnterPress( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnOKClick( wxCommandEvent& event ) { event.Skip(); }


	public:
		wxString m_ValPassword;

		PasswordDlg( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Password Required"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 532,145 ), long style = wxDEFAULT_DIALOG_STYLE );

		~PasswordDlg();

};

///////////////////////////////////////////////////////////////////////////////
/// Class NewEditBankDlg
///////////////////////////////////////////////////////////////////////////////
class NewEditBankDlg : public wxDialog
{
	private:

	protected:
		wxStaticText* m_TxtBankName;
		wxTextCtrl* m_InputBankName;
		wxStaticText* m_TxtComment;
		wxTextCtrl* m_InputComments;
		wxButton* m_BtnCancel;
		wxButton* m_BtnAddEdit;

		// Virtual event handlers, override them in your derived class
		virtual void BankNewEditKeyUp( wxKeyEvent& event ) { event.Skip(); }
		virtual void BtnCancelClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BtnAddEditClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		NewEditBankDlg( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 545,198 ), long style = wxDEFAULT_DIALOG_STYLE );

		~NewEditBankDlg();

};

