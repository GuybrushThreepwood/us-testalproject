/**
	TECS
	DlgPassword.cpp
    Purpose: Dialog for showing the administrator password input
*/

#include "DlgPassword.h"
#include "System.h"

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
DlgPassword::DlgPassword( wxWindow* parent )
:
PasswordDlg( parent )
{
	this->SetTitle(_("Password Required"));
	m_IncorrectCount = 1;
}

/////////////////////////////////////////////////////
/// InputTextEnterPress
/// Desc: Input event for enter key press on the password input containery
/////////////////////////////////////////////////////
void DlgPassword::InputTextEnterPress(wxCommandEvent& event)
{
	BtnOKClick(event);
}

/////////////////////////////////////////////////////
/// BtnOKClick
/// Desc: Button event for exiting the password dialog
/////////////////////////////////////////////////////
void DlgPassword::BtnOKClick(wxCommandEvent& WXUNUSED(event))
{
	wxString str = m_InputPassword->GetValue();

	// do the password test
	if (str.IsSameAs("udsevilla"))
	{
		this->EndModal(RESPONSE_OK);
	}
	else
	{
		// if wrong, just clear and wait?
		if(m_IncorrectCount >= 3)
			this->EndModal(RESPONSE_CANCEL);
		else
		{
			m_IncorrectCount++;
			m_InputPassword->Clear();
		}
	}
}