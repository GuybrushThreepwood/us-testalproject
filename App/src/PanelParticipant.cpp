/**
	TECS
	PanelParticipant.cpp
    Purpose: Panel shown for participant data entry and program selection
*/

#include "PanelParticipant.h"
#include "System.h"

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
PanelParticipant::PanelParticipant( wxWindow* parent )
:
Participant( parent )
{
	ParticipantData oldData = sys::AppData::Instance()->GetParticipantData();

	m_ValidProgram = true;

	m_FileMap.clear();

	// find all available files in the directory
	wxArrayString fileList;
	std::size_t totalFiles = wxDir::GetAllFiles(sys::AppData::Instance()->GetParticipantsDirectory(), &fileList);
	if (totalFiles > 0)
	{
		for (auto fileIter : fileList)
		{
			ParticipantFileInfo fileInfo;
			fileInfo.filename = fileIter;

			std::ifstream t(fileIter.ToStdString());
			std::stringstream buffer;
			buffer << t.rdbuf();

			std::string bufferStr = buffer.str();
			fileInfo.jsonData.loadFromString(bufferStr);
			//fileInfo.jsonData.loadFromFile(fileIter.ToStdString());

			if (!fileInfo.jsonData.isNull())
			{
				JsonBox::Value participantId = fileInfo.jsonData["ID"];

				if (participantId.isString())
				{
					fileInfo.participantId = participantId.getString();

					// do the insert
					m_FileMap.insert(std::make_pair(fileInfo.participantId, fileInfo));
				}
			}
		}
	}

	// gender
	m_ComboGender->Clear();
	m_ComboGender->Insert(_("Unspecified"), (int)GENDER_UNSPECIFIED);
	m_ComboGender->Insert(_("Male"), (int)GENDER_MALE);
	m_ComboGender->Insert(_("Female"), (int)GENDER_FEMALE);
	m_ComboGender->SetSelection(0);

	// phase list
	m_ComboPhase->Clear();

	// populate from the current map
	auto phaseMap = sys::AppData::Instance()->GetPhases();
	for (auto val : phaseMap)
	{
		int index = m_ComboPhase->GetCount();
		m_ComboPhase->Insert(val.second.name, index);

		// make this map match the selection box
		m_PhaseMap.insert(std::make_pair(index, val.second.val));
	}
	m_ComboPhase->SetSelection(0);

	wxCommandEvent empty;
	OnChangePhase(empty);

	std::size_t totalResponses = oldData.response.size();
	if (totalResponses == 0)
	{
		if (oldData.genderId != -1)
		{
			// refill data
			m_InputId->SetValue(oldData.strId);
			m_DateDOB->SetValue(oldData.dateOfBirth);

			for (auto comboVals : m_PhaseMap)
			{
				if (comboVals.second == oldData.phaseData.val)
				{
					m_ComboPhase->SetSelection(comboVals.first);
					break;
				}
			}

			OnChangePhase(empty);

			// set the original program name
			int programSel = 0;
			for (const auto iter : m_FilteredProgramFileMap)
			{
				if (iter.second.phaseId == oldData.phaseData.val )
				{
					programSel++;
					if (iter.second.name == oldData.programName)
						break;
				}
			}

			m_ComboProgram->SetSelection(programSel - 1);

			m_ComboGender->SetSelection(oldData.genderId);
		}
	}

	wxString str = m_InputId->GetValue();
	wxString err;

	if (!m_ValidProgram)
		err = _("Program is invalid or not selected");
	else
	if (str.Length() == 0)
		err = _("No ID entered");

	if (!m_ValidProgram ||
		str.Length() == 0)
	{
		m_BtnStart->Enable(false);
		m_TxtIDWarn->SetLabel(err);
		
		m_TxtIDWarn->Show();
		this->Layout();
	}

	//bool numLockState = wxGetKeyState(WXK_NUMLOCK);
	//wxLogDebug(wxString::Format("Numlock is %d", (numLockState != 0)));
}

/////////////////////////////////////////////////////
/// OnChangePhase
/// Desc: Combobox event when the phase is changed to update the available programs
/////////////////////////////////////////////////////
void PanelParticipant::OnChangePhase(wxCommandEvent& WXUNUSED(event))
{
	int sel = m_ComboPhase->GetSelection();

	m_ProgramFileMap.clear();

	// load all the files
	wxArrayString fileList;
	std::size_t totalFiles = wxDir::GetAllFiles(sys::AppData::Instance()->GetProgramsDirectory(), &fileList);
	if (totalFiles > 0)
	{
		for (auto fileIter : fileList)
		{
			ProgramFileInfo fileInfo;
			fileInfo.filename = fileIter;
			
			std::ifstream t(fileIter.ToStdString());
			std::stringstream buffer;
			buffer << t.rdbuf();

			std::string bufferStr = buffer.str();
			fileInfo.jsonData.loadFromString(bufferStr);
			//fileInfo.jsonData.loadFromFile(fileIter.ToStdString());

			if (!fileInfo.jsonData.isNull())
			{
				JsonBox::Value phaseId = fileInfo.jsonData["PhaseId"];
				JsonBox::Value bankGUID = fileInfo.jsonData["BankGUID"];

				if (phaseId.isInteger())
					fileInfo.phaseId = phaseId.getInteger();
				else
					fileInfo.phaseId = -1;

				if (bankGUID.isString())
					fileInfo.bankGUID = bankGUID.getString();
				else
					fileInfo.bankGUID = -1;

				if (!CheckProgramValidity(fileInfo))
					continue;

				JsonBox::Value name = fileInfo.jsonData["Name"];
				JsonBox::Value comment = fileInfo.jsonData["Comments"];
				JsonBox::Value dateTime = fileInfo.jsonData["Time"];
				

				if (name.isString() &&
					comment.isString() &&
					phaseId.isInteger()  )
				{
					fileInfo.name = name.getString();
					fileInfo.comment = comment.getString();
					fileInfo.phaseId = phaseId.getInteger();
					

					wxString str = dateTime.getString();
					long long val = -1;
					if (str.ToLongLong(&val))
					{
						fileInfo.dateTime = wxDateTime((wxLongLong)val);
					}

					// do the insert
					m_ProgramFileMap.insert(std::make_pair(fileIter, fileInfo));
				}
			}
		}
	}

	// find the available files for this version
	int totalSelections = 0;
	auto itemSelected = m_PhaseMap.find(sel);
	if (itemSelected != m_PhaseMap.end())
	{
		m_ComboProgram->Clear();

		const PhaseData pd = sys::AppData::Instance()->FindPhase(itemSelected->second);
		if (pd.val != -1)
		{
			// update the programs for this
			m_FilteredProgramFileMap.clear();

			m_FilteredProgramFileMap = m_ProgramFileMap;

			for (const auto iter : m_FilteredProgramFileMap)
			{
				if (iter.second.phaseId == pd.val)
				{
					m_ComboProgram->Insert(iter.second.name, m_ComboProgram->GetCount());
					totalSelections++;
				}
			}

			m_ComboProgram->SetSelection(0);
		}
	}

	// no available programs for this version
	if (totalSelections == 0)
	{
		m_ComboProgram->Insert(_("None"), m_ComboProgram->GetCount());
		m_ComboProgram->SetSelection(0);

		m_ValidProgram = false;
	}
	else
		m_ValidProgram = true;

	if (!m_ValidProgram)
		m_BtnStart->Enable(false);
	else
		m_BtnStart->Enable(true);

	wxKeyEvent empty;
	InputIdKeyUp(empty);
}

/////////////////////////////////////////////////////
/// CheckProgramValidity
/// Desc: Method to check is a program is still valid considering possible change to word/sentence availability
/////////////////////////////////////////////////////
bool PanelParticipant::CheckProgramValidity(ProgramFileInfo& pf)
{
	if (pf.jsonData["FilterCounts"].isArray())
	{
		if (!pf.jsonData["WordCountMin"].isNull() &&
			!pf.jsonData["WordCountMax"].isNull())
		{
			int wordMin = pf.jsonData["WordCountMin"].getInteger();
			int wordMax = pf.jsonData["WordCountMax"].getInteger();

			JsonBox::Array arr = pf.jsonData["FilterCounts"].getArray();
			auto iter = arr.begin();
			while (iter != arr.end())
			{
				if (iter->isObject())
				{
					JsonBox::Object filterItem = iter->getObject();
					if (!filterItem["FilterId"].isNull() &&
						!filterItem["Count"].isNull())
					{
						int filterId = filterItem["FilterId"].getInteger();
						int count = filterItem["Count"].getInteger();

						ProgramCreateEditLogInfo logData = sys::AppData::Instance()->CheckBankFilterCounts(pf.bankGUID, pf.phaseId, filterId, count, wordMin, wordMax);

						if (!logData.isValid)
							return false;
					}
				}
				// next
				++iter;
			}
		}
	}

	return true;
}

/////////////////////////////////////////////////////
/// InputIdKeyUp
/// Desc: Key event to check the value for the participant ID is valid
/////////////////////////////////////////////////////
void PanelParticipant::InputIdKeyUp(wxKeyEvent& WXUNUSED(event))
{
	wxString str = m_InputId->GetValue();
	wxString numTest = m_InputNumTest->GetValue();
	wxString err;

	if (!m_ValidProgram)
		err = _("Program is invalid or not selected");
	else
	if (str.Length() == 0)
		err = _("No ID entered");

	// test the program first
	if (!m_ValidProgram ||
		str.Length() == 0)
	{
		m_BtnStart->Enable(false);
		m_TxtIDWarn->SetLabel(err);
		
		m_TxtIDWarn->Show();
		this->Layout();
		return;
	}

	//long val = -1;
	//if (str.ToLong(&val))
	{
		// check it doesn't exist
		if (m_FileMap.find(str) != m_FileMap.end())
		{
			// show warning and disable start
			m_BtnStart->Enable(false);
			err = _("Participant ID already exists");
			m_TxtIDWarn->SetLabel(err);
			this->Layout();
			m_TxtIDWarn->Show();
			return;
		}
		else
		{
			// now test the num lock

			if (numTest.IsSameAs("456"))
			{
				m_BtnStart->Enable(true);
				m_TxtIDWarn->SetLabel(_("Valid"));
				//m_TxtIDWarn->Hide();
				this->Layout();
			}
			else
			{
				m_BtnStart->Enable(false);
				err = _("Please enter the Num Lock test");
				m_TxtIDWarn->SetLabel(err);
				this->Layout();
				m_TxtIDWarn->Show();
			}
		}
	}
}

/////////////////////////////////////////////////////
/// NumTestKeyUp
/// Desc: Key event to check the value for the numeric keys working with/without num lock
/////////////////////////////////////////////////////
void PanelParticipant::NumTestKeyUp(wxKeyEvent& WXUNUSED(event))
{
	wxKeyEvent empty;
	InputIdKeyUp(empty);
}

/////////////////////////////////////////////////////
/// BtnBackClick
/// Desc: Button event called to close the panel and return to the entry screen
/////////////////////////////////////////////////////
void PanelParticipant::BtnBackClick(wxCommandEvent& WXUNUSED(event))
{
	ParticipantData pd = sys::AppData::Instance()->GetParticipantData();

	pd.genderId = -1;
	pd.program.clear();
	pd.response.clear();
	sys::AppData::Instance()->SetParticipantData(pd);

	sys::AppData::Instance()->ChangePanel(SCREEN_ENTRY);
}

/////////////////////////////////////////////////////
/// BtnStartClick
/// Desc: Prepares the selected program to run and switches to the tutorial before the main program run
/////////////////////////////////////////////////////
void PanelParticipant::BtnStartClick(wxCommandEvent& WXUNUSED(event))
{
	// grab all the info
	ParticipantData data;
	//long idVal = -1;

	data.strId = m_InputId->GetValue();

	//if (m_InputId->GetValue().ToLong(&idVal))
	//	data.participantId = idVal;

	data.genderId = m_ComboGender->GetSelection();
	data.dateOfBirth = m_DateDOB->GetValue();

	wxLongLong value = data.dateOfBirth.GetValue();
	wxString numericStr = value.ToString();
	wxString str = data.dateOfBirth.FormatISODate();

	int sel = m_ComboPhase->GetSelection();
	auto itemSelected = m_PhaseMap.find(sel);
	if (itemSelected != m_PhaseMap.end())
	{
		const PhaseData pd = sys::AppData::Instance()->FindPhase(itemSelected->second);
		if (pd.val != -1)
		{
			data.selectedVersionId = pd.val;
		}
	}

	sel = m_ComboProgram->GetSelection();
	wxString programName = m_ComboProgram->GetStringSelection();

	// generate the program

	ProgramFileInfo pf;
	pf.phaseId = -1;
	for (const auto iter : m_FilteredProgramFileMap)
	{
		if (iter.second.name == programName)
		{
			pf = iter.second;
			break;
		}
	}

	data.programName = programName;

	if (pf.phaseId != -1)
	{
		int wordMin = 1;
		int wordMax = 1;
		int lineMin = 1;
		int lineMax = 1;

		if (!pf.jsonData["WordCountMin"].isNull() &&
			!pf.jsonData["WordCountMax"].isNull())
		{
			wordMin = pf.jsonData["WordCountMin"].getInteger();
			wordMax = pf.jsonData["WordCountMax"].getInteger();

			data.phaseData.wordCountMin = wordMin;
			data.phaseData.wordCountMax = wordMax;
		}

		if (!pf.jsonData["LinesMin"].isNull() &&
			!pf.jsonData["LinesMax"].isNull())
		{
			lineMin = pf.jsonData["LinesMin"].getInteger();
			lineMax = pf.jsonData["LinesMax"].getInteger();

			data.phaseData.linesCountMin = lineMin;
			data.phaseData.linesCountMax = lineMax;
		}

		// get defaults
		bool useDefaults = true;
		if (!pf.jsonData["UseTimeDefaults"].isNull())
		{
			useDefaults = pf.jsonData["UseTimeDefaults"].getBoolean();
		}
		
		// always get the default
		const PhaseData pd = sys::AppData::Instance()->FindPhase(pf.phaseId);
		if (pd.val != -1)
		{
			data.phaseData = pd;

			//
			if( pd.timeToRespond != pd.userTimeToRespond )
				data.phaseData.timeToRespond = (int)pd.userTimeToRespond;
			else
				data.phaseData.timeToRespond = (int)pd.timeToRespond;

			//
			if (pd.timeResponseTimeout != pd.userTimeResponseTimeout)
				data.phaseData.timeResponseTimeout = (int)pd.userTimeResponseTimeout;
			else
				data.phaseData.timeResponseTimeout = (int)pd.timeResponseTimeout;

			//
			if (pd.timeInterval != pd.userTimeInterval)
				data.phaseData.timeInterval = (int)pd.userTimeInterval;
			else
				data.phaseData.timeInterval = (int)pd.timeInterval;

			//
			if (pd.timeBetweenFilterChanges != pd.userTimeBetweenFilterChanges)
				data.phaseData.timeBetweenFilterChanges = (int)pd.userTimeBetweenFilterChanges;
			else
				data.phaseData.timeBetweenFilterChanges = (int)pd.timeBetweenFilterChanges;
		}

		if (!useDefaults)
		{
			if (!pf.jsonData["TimeToRespond"].isNull())
				data.phaseData.timeToRespond = pf.jsonData["TimeToRespond"].getInteger();

			if (!pf.jsonData["TimeResponseTimeout"].isNull())
				data.phaseData.timeResponseTimeout = pf.jsonData["TimeResponseTimeout"].getInteger();

			if (!pf.jsonData["TimeInterval"].isNull())
				data.phaseData.timeInterval = pf.jsonData["TimeInterval"].getInteger();

			if (!pf.jsonData["TimeBetweenFilterChanges"].isNull())
				data.phaseData.timeBetweenFilterChanges = pf.jsonData["TimeBetweenFilterChanges"].getInteger();
		}

		sys::AppData::Instance()->SetSelectedBankFromGUID(pf.bankGUID);

		std::map<wxString, BankData> bank = sys::AppData::Instance()->GetBank();

		// filter out unlinked words
		if (pf.phaseId == VERSION_THREE)
		{
			for (auto iter = bank.begin(); iter != bank.end();)
			{
				if (!iter->second.hasGUID)
				{
					iter = bank.erase(iter);
					continue;
				}

				// next
				iter++;
			}
		}

		// check word min/max count
		for (auto iter = bank.begin(); iter != bank.end();)
		{
			if (iter->second.wordCount < wordMin ||
				iter->second.wordCount > wordMax)
			{
				iter = bank.erase(iter);
				continue;
			}
			
			// next
			iter++;
		}

		// bank now has filter counts, now select
		std::map< int, std::vector<BankData> > filteredProgramMap; // map of lists

		if (pf.jsonData["FilterCounts"].isArray())
		{
			// bank now contains only valid values
			std::vector<std::vector<BankData>> allLists; // list of lists
			std::vector<BankData> finalProgramList; // final choices

			JsonBox::Array arr = pf.jsonData["FilterCounts"].getArray();
			auto iter = arr.begin();
			while (iter != arr.end())
			{
				if (iter->isObject())
				{
					JsonBox::Object filterItem = iter->getObject();
					if (!filterItem["FilterId"].isNull() &&
						!filterItem["Count"].isNull())
					{
						int filterId = filterItem["FilterId"].getInteger();
						int count = filterItem["Count"].getInteger();

						std::vector<BankData> newItem;

						for (auto innerIter = bank.begin(); innerIter != bank.end();)
						{
							if (innerIter->second.filterIndex == filterId)
							{
								newItem.push_back(innerIter->second);
							}

							// next
							++innerIter;
						}

						// add the list of this filter
						allLists.push_back(newItem);

						std::vector<BankData> selectedProgramItems;
						for (int i = 0; i < count; ++i)
						{
							std::size_t numElements = newItem.size();

							int selected = 0;
							if( numElements != 0 )
								selected = sys::RandInt(0, (numElements - 1));

							selectedProgramItems.push_back(newItem.at(selected));
							finalProgramList.push_back(newItem.at(selected));
					
							// remove so we don't selected it again
							newItem.erase(newItem.begin() + selected);
						}

						filteredProgramMap.insert( std::make_pair(filterId, selectedProgramItems) );
					}
				}
				// next
				++iter;
			}

			// there is a normal full block
			ProgramBlock defaultBlock;
			defaultBlock.blockId = 0;
			defaultBlock.breakType = BREAKTYPE_PERFILTER;
			defaultBlock.words = finalProgramList;

			// check for program blocks
			if (!pf.jsonData["OrderData"].isNull() &&
				pf.jsonData["OrderData"].isArray())
			{
				std::vector<OrderBlock> orderData;

				JsonBox::Value orderObj = pf.jsonData["OrderData"];
				JsonBox::Array orderArr = orderObj.getArray();

				auto iterOrder = orderArr.begin();
				while (iterOrder != orderArr.end())
				{
					OrderBlock newBlock;
					if (iterOrder->isObject())
					{
						JsonBox::Value blockObj = iterOrder->getObject();

						JsonBox::Value blockArray = blockObj["Blocks"];
						JsonBox::Value blockRandomVal = blockObj["Random"];
						JsonBox::Value blockBreakType = blockObj["BreakType"];
						JsonBox::Value blockIndex = blockObj["Index"];
						JsonBox::Array currBlock = blockArray.getArray();

						auto iterBlocks = currBlock.begin();

						std::vector<OrderValues> values;
						while (iterBlocks != currBlock.end())
						{
							if (iterBlocks->isObject())
							{
								JsonBox::Object blockItem = iterBlocks->getObject();
								if (!blockItem["FilterId"].isNull() &&
									!blockItem["Count"].isNull())
								{
									int filterId = blockItem["FilterId"].getInteger();
									int count = blockItem["Count"].getInteger();
									const FilterData fd = sys::AppData::Instance()->FindFilter(filterId);

									OrderValues val;
									val.filterId = filterId;
									val.count = count;
									val.filterName = fd.name;

									values.push_back(val);
								}
							}

							// next
							++iterBlocks;
						}

						newBlock.random = blockRandomVal.getBoolean();
						newBlock.breakType = (BreakType)blockBreakType.getInteger();
						newBlock.values = values;
					}

					// add
					orderData.push_back(newBlock);

					// next
					++iterOrder;
				}

				// now create better list based on the block ordering
				int index = 1;

				for (auto item : orderData)
				{
					ProgramBlock newBlock;
					newBlock.blockId = index++;
					newBlock.breakType = item.breakType;

					// first make sure the order count is realistic with the numbers in the filter map
					for (auto& val : item.values)
					{
						auto itemsInFilterList = filteredProgramMap.find(val.filterId);

						if (itemsInFilterList != filteredProgramMap.end())
						{
							// check for not enough to match the block and update the max
							if (itemsInFilterList->second.size() < (std::size_t)val.count)
								val.count = (int)itemsInFilterList->second.size();
						}
					}

					if (item.random) // choose randomly from the value array
					{
						while (item.values.size() > 0)
						{
							std::size_t numArrays = item.values.size();
							int selectedArray = sys::RandInt(0, (numArrays - 1));

							auto& val = item.values[selectedArray]; // pick a random array index

							auto itemsInFilterList = filteredProgramMap.find(val.filterId);
							if (itemsInFilterList != filteredProgramMap.end())
							{
								if (val.count > 0)
								{
									std::size_t numElements = itemsInFilterList->second.size();

									int selected = sys::RandInt(0, (numElements - 1)); // now pick a random bank item

									newBlock.words.push_back(itemsInFilterList->second.at(selected));

									// remove so we don't selected it again
									itemsInFilterList->second.erase(itemsInFilterList->second.begin() + selected);

									// decrease the count for this array element
									val.count -= 1;

									// check if this array is now empty and remove the array if there's no more to add
									if (val.count == 0)
									{
										item.values.erase(item.values.begin() + selectedArray);
									}
								}
							}
							else
								break;
						}
					}
					else
					{
						// add in sequence
						for (auto val : item.values)
						{
							auto itemsInFilterList = filteredProgramMap.find(val.filterId);
							std::size_t count = val.count;

							// go through the count of filters add to the final list and remove from the map
							for (std::size_t i = 0; i < count; ++i)
							{
								std::size_t numElements = itemsInFilterList->second.size();

								int selected = sys::RandInt(0, (numElements - 1));

								newBlock.words.push_back(itemsInFilterList->second.at(selected));

								// remove so we don't selected it again
								itemsInFilterList->second.erase(itemsInFilterList->second.begin() + selected);
							}
						}
					}

					// add to the program
					data.program.push_back(newBlock);
				}

				// finally add anything not in a block in sequence
				ProgramBlock extraBlock;
				extraBlock.blockId = index++;
				extraBlock.breakType = BREAKTYPE_PERFILTER;

				for (auto mapItem : filteredProgramMap)
				{
					if (mapItem.second.size() > 0)
					{
						for (auto bankItem : mapItem.second)
						{
							extraBlock.words.push_back(bankItem);
						}
					}
				}

				// if the extra block was filled, add it to the program
				if( extraBlock.words.size() > 0 )
					data.program.push_back(extraBlock);
			}

			// add the default block if there's no other blocks
			if(data.program.size() == 0 )
				data.program.push_back(defaultBlock);
		
		}
	}

	data.response.clear();
	sys::AppData::Instance()->SetParticipantData(data);

	sys::AppData::Instance()->ChangePanel(SCREEN_TUTORIAL);
}

