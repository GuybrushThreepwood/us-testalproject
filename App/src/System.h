
#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#include "Common.h"

#include "PanelEntry.h"
#include "PanelAdmin.h"
#include "PanelBankSelection.h"
#include "PanelBank.h"
#include "PanelPhasePrograms.h"
#include "PanelTutorial.h"
#include "PanelPhaseRun.h"
#include "PanelParticipant.h"
#include "PanelPhaseRun.h"
#include "PanelParticipantInfo.h"
#include "ProjectMainFrame.h"
#include "PanelQuestions.h"

namespace sys
{
	class AppData
	{
		public:
			AppData();
			virtual ~AppData();

			static void Initialise(void);
			static void Shutdown(void);

			static AppData *Instance(void)
			{
				wxASSERT_MSG((ms_Instance != 0), "AppData instance has not been created");

				return(ms_Instance);
			}
			static bool IsInitialised(void)
			{
				return(ms_Instance != 0);
			}

			/// SetMainFrame - set the main frame
			/// \param pMainFrame - the current main frame
			void SetMainFrame( ProjectMainFrame* pMainFrame);
			/// GetMainFrame - get the current frame
			/// \return ProjectMainFrame* - current frame
			ProjectMainFrame* GetMainFrame();

			/// LoadBank - load the bank file
			void LoadBank(const std::string& filename, bool expandPath=true);
			/// SaveBank - save the bank file
			void SaveBank(const std::string& filename);
			void SaveBank();

			/// LoadSettings - load the settings file
			void LoadSettings(const std::string& filename);
			/// SaveSettings - save the settings file
			void SaveSettings(const std::string& filename);

			/// ChangePanel - 
			/// \param newScreen - 
			bool ChangePanel(ScreenList newScreen);
			ScreenList GetPanelId() { return m_CurrentScreen;  }
			wxPanel* GetCurrentPanel() { return m_CurrentPanel;  }

			/// GetGUID
			Guid GetGUID()
			{
				return m_GUIDGenerator.newGuid();
			}

			// GetGUIDAsString
			const std::string GetGUIDAsString()
			{
				auto newGuid = m_GUIDGenerator.newGuid();

				std::stringstream stream;
				stream << newGuid;
				return stream.str();
			}
			/// GetBank
			const std::map<wxString, BankData>& GetBank()
			{
				return m_Bank;
			}

			/// AddToBank
			bool AddToBank(const BankData& bd);
			/// UpdateBank
			bool UpdateBank(const wxString& oldStr, const BankData& bd);
			/// RemoveFromBank
			bool RemoveFromBank(const wxString& str);
			/// GetFromBank
			const BankData& GetFromBank(const wxString& str);
			/// GetMatchingGUID
			const std::vector<BankData> GetMatchingGUID(const std::string& guid);

			/// GetFilters
			const std::map<int, FilterData>& GetFilters()
			{
				return m_Filters;
			}
			/// AddToFilters
			bool AddToFilters(const FilterData& fd);
			/// UpdateFilter
			bool UpdateFilter(const wxString& oldStr, const wxString& newStr);
			/// UpdateFilter
			bool UpdateFilter(const wxString& filterStr, const FilterData& newData);
			/// RemoveFromFilters
			bool RemoveFromFilters(const wxString& str, int filterId, bool updateBank=true, int updateBankFilter=FILTER_NEUTRAL);
			/// FindFilter
			const FilterData FindFilter(int index);
			const FilterData FindFilter(wxString str);


			/// GetPhases
			const std::map<int, PhaseData>& GetPhases() { return m_Phases; }

			/// AddToPhases
			bool AddToPhases(const PhaseData& pd);
			/// UpdatePhase
			bool UpdatePhase(const wxString& oldStr, const wxString& newStr);
			/// UpdatePhase
			bool UpdatePhase(const wxString& phaseStr, const PhaseData& newData);
			/// RemoveFromPhases
			//bool RemoveFromPhases(const wxString& str, int phaseId, bool updateProgram = true, int updateProgramPhase = VERSION_ONE);
			/// FindPhase
			const PhaseData FindPhase(int index);
			const PhaseData FindPhase(wxString str);

			/// SetSelectedBank
			void SetSelectedBankFromGUID(const std::string& bankGUID);
			void SetSelectedBank(const BankFileInfo& selectedBank);
			BankFileInfo& GetSelectedBank() { return m_SelectedBank; }

			/// GetBankFiles
			const std::map<wxString, BankFileInfo>& GetBankFileList();

			/// SetParticipantData - set the main frame
			/// \param pMainFrame - the current main frame
			void SetParticipantData(const ParticipantData& data) { m_ParticipantData = data; }
			/// GetParticipantData - get the current participant
			/// \return ParticipantData - current frame
			ParticipantData GetParticipantData()			{ return m_ParticipantData; }

			/// SetCurrentParticipantId 
			void SetCurrentParticipantId(int participantId) { m_CurrentParticipantId = participantId; }
			/// GetCurrentParticipantId 
			int GetCurrentParticipantId()					{ return m_CurrentParticipantId; }

			/// SetStoredData 
			void SetStoredData(StoredScaleData data)		{ m_StoredData = data; }
			/// GetStoredData 
			StoredScaleData GetStoredData()					{ return m_StoredData; }

			/// RequestNewFilterId
			int RequestNewFilterId( int count=0, int maxCount=1000 );

			/// RequestNewPhaseId
			int RequestNewPhaseId(int count=0, int maxCount=1000);

			/// CheckBankFilterCounts
			ProgramCreateEditLogInfo CheckBankFilterCounts(const std::string& bankGUID, int versionId, int filterId, int requiredCount, int requiredMinWord, int requiredMaxWord);

			/// GetBanksDirectory
			wxString GetBanksDirectory() { return m_AppBanksDirectory; }
			/// GetProgramsDirectory
			wxString GetProgramsDirectory()		{ return m_AppProgramsDirectory;  }
			/// GetParticipantsDirectory
			wxString GetParticipantsDirectory()	{ return m_AppParticipantsDirectory; }

			void SetAdminPasswordState(bool state) { m_UseAdminPassword = state; }
			bool GetAdminPasswordState() { return m_UseAdminPassword; }

			void SetBGColour(wxColour col) { m_BGColour = col; }
			wxColour GetBGColour() { return m_BGColour;  }

			void SetTextColour(wxColour col) { m_TextColour = col; }
			wxColour GetTextColour() { return m_TextColour; }


			void SetTextBorderColour(wxColour col) { m_TextBorderColour = col; }
			wxColour GetTextBorderColour() { return m_TextBorderColour; }

			void SetTextBorderThickness(int v) { m_BorderThickness = v; }
			int GetTextBorderThickness() { return m_BorderThickness; }

			void SetTextBorderWidth(int v) { m_BorderWidth = v; }
			int GetTextBorderWidth() { return m_BorderWidth; }

			void SetTextBorderHeight(int v) { m_BorderHeight = v; }
			int GetTextBorderHeight() { return m_BorderHeight; }

			void SetTextInfoColour(wxColour col) { m_TextInfoColour = col; }
			wxColour GetTextInfoColour() { return m_TextInfoColour; }

			void SetTextFontSize(int s) { m_FontSize = s; }
			int GetTextFontSize() { return m_FontSize; }

			void SetTutTxtVersion1(wxString s) { m_TutTxtVersion1 = s; }
			wxString GetTutTxtVersion1() { return m_TutTxtVersion1; }

			void SetTutTxtVersion2(wxString s) { m_TutTxtVersion2 = s; }
			wxString GetTutTxtVersion2() { return m_TutTxtVersion2; }

			void SetTutTxtVersion3And4(wxString s) { m_TutTxtVersion3And4 = s; }
			wxString GetTutTxtVersion3And4() { return m_TutTxtVersion3And4; }

			void SetTxtEx1(wxString s) { m_Ex1 = s; }
			wxString GetTxtEx1() { return m_Ex1; }

			void SetTxtEx2(wxString s) { m_Ex2 = s; }
			wxString GetTxtEx2() { return m_Ex2; }

			void SetTxtEx3(wxString s) { m_Ex3 = s; }
			wxString GetTxtEx3() { return m_Ex3; }

			// tut 1
			void SetTut1Ex1(wxString s) { m_Tut1Ex1 = s; }
			wxString GetTut1Ex1() { return m_Tut1Ex1; }

			void SetTut1Ex2(wxString s) { m_Tut1Ex2 = s; }
			wxString GetTut1Ex2() { return m_Tut1Ex2; }

			void SetTut1Ex3(wxString s) { m_Tut1Ex3 = s; }
			wxString GetTut1Ex3() { return m_Tut1Ex3; }

			// tut 2
			void SetTut2Ex1(wxString s) { m_Tut2Ex1 = s; }
			wxString GetTut2Ex1() { return m_Tut2Ex1; }

			void SetTut2Ex2(wxString s) { m_Tut2Ex2 = s; }
			wxString GetTut2Ex2() { return m_Tut2Ex2; }

			void SetTut2Ex3(wxString s) { m_Tut2Ex3 = s; }
			wxString GetTut2Ex3() { return m_Tut2Ex3; }

			// tut 3
			void SetTut3Ex1(wxString s) { m_Tut3Ex1 = s; }
			wxString GetTut3Ex1() { return m_Tut3Ex1; }

			void SetTut3Ex2(wxString s) { m_Tut3Ex2 = s; }
			wxString GetTut3Ex2() { return m_Tut3Ex2; }

			void SetTut3Ex3(wxString s) { m_Tut3Ex3 = s; }
			wxString GetTut3Ex3() { return m_Tut3Ex3; }

			// tut 4
			void SetTut4Ex1(wxString s) { m_Tut4Ex1 = s; }
			wxString GetTut4Ex1() { return m_Tut4Ex1; }

			void SetTut4Ex2(wxString s) { m_Tut4Ex2 = s; }
			wxString GetTut4Ex2() { return m_Tut4Ex2; }

			void SetTut4Ex3(wxString s) { m_Tut4Ex3 = s; }
			wxString GetTut4Ex3() { return m_Tut4Ex3; }


			void SetInputText1(wxString s) { m_InputText1 = s; }
			wxString GetInputText1() { return m_InputText1; }

			void SetInputText2(wxString s) { m_InputText2 = s; }
			wxString GetInputText2() { return m_InputText2; }

			void SetEndingText1(wxString s) { m_EndingText1 = s; }
			wxString GetEndingText1() { return m_EndingText1; }

			void SetEndingText2(wxString s) { m_EndingText2 = s; }
			wxString GetEndingText2() { return m_EndingText2; }

			void SetWaitText(wxString s) { m_WaitText = s; }
			wxString GetWaitText() { return m_WaitText; }

			std::vector<ExcelColourIndex> GetExcelColours() { return m_ExcelColours; }

			//
			std::string defaultBankFile;
			std::string defaultSettingsFile;

		private:
			static AppData* ms_Instance;

			wxString m_AppSaveDirectory;
			wxString m_AppBanksDirectory;
			wxString m_AppParticipantsDirectory;
			wxString m_AppProgramsDirectory;

			GuidGenerator m_GUIDGenerator;

			ProjectMainFrame* m_MainFrame = 0;
			ScreenList m_CurrentScreen = SCREEN_EMPTY;
			wxPanel* m_CurrentPanel = 0;

			StoredScaleData m_StoredData;

			bool m_UseAdminPassword;

			// word bank
			std::map<wxString, BankFileInfo> m_BankFileMap;
			std::map<wxString, BankData> m_Bank;		
			BankFileInfo m_SelectedBank;

			// filters
			std::map<int, FilterData> m_Filters;
			int m_NextFilterId;

			// versions
			std::map<int, PhaseData> m_Phases;
			int m_NextPhaseId;

			// current participant info
			ParticipantData m_ParticipantData;

			int m_CurrentPhaseId = -1;
			int m_CurrentParticipantId = -1;

			wxColour m_BGColour;
			wxColour m_TextColour;
			wxColour m_TextBorderColour;
			wxColour m_TextInfoColour;

			int m_FontSize;

			wxString m_TutTxtVersion1;
			wxString m_TutTxtVersion2;
			//wxString m_TutTxtVersion3;
			wxString m_TutTxtVersion3And4;

			wxString m_Ex1;
			wxString m_Ex2;
			wxString m_Ex3;

			wxString m_Tut1Ex1;
			wxString m_Tut1Ex2;
			wxString m_Tut1Ex3;

			wxString m_Tut2Ex1;
			wxString m_Tut2Ex2;
			wxString m_Tut2Ex3;

			wxString m_Tut3Ex1;
			wxString m_Tut3Ex2;
			wxString m_Tut3Ex3;

			wxString m_Tut4Ex1;
			wxString m_Tut4Ex2;
			wxString m_Tut4Ex3;

			wxString m_InputText1;
			wxString m_InputText2;

			wxString m_EndingText1;
			wxString m_EndingText2;

			wxString m_WaitText;

			int m_BorderThickness;
			int m_BorderWidth;
			int m_BorderHeight;

			std::vector<ExcelColourIndex> m_ExcelColours;
	};

	std::size_t WordCount(const std::wstring& str);
	int RandInt(int low, int high);
    
} // namespace sys

#endif // __SYSTEM_H__

