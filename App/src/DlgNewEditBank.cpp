
#include "DlgNewEditBank.h"
#include "System.h"

DlgNewEditBank::DlgNewEditBank( wxWindow* parent )
:
NewEditBankDlg( parent )
{

}

/////////////////////////////////////////////////////
/// Apply
/// Desc: Called manually after construction to populate or modify UI elements for either ADD-ing or EDIT-ing
/////////////////////////////////////////////////////
void DlgNewEditBank::Apply()
{
	if (m_Mode == MODE_ADD) // mode is set to add
	{
		// set initial data for ADD
		this->SetTitle(_("Create New Bank"));
		m_BtnAddEdit->SetLabelText(_("Create"));

		if (m_BankName.Length() == 0)
		{
			m_BankName = _("New Bank");
		}
		m_InputBankName->SetValue(m_BankName);

		if (m_Comments.Length() == 0)
		{
			m_Comments = "";
		}

		m_InputComments->SetValue(m_Comments);
	}
	else if (m_Mode == MODE_EDIT) // mode is set to edit
	{
		LoadFromFile();

		this->SetTitle(_("Edit Existing"));
		m_BtnAddEdit->SetLabelText(_("Update"));
	}
}

/////////////////////////////////////////////////////
/// BankNewEditKeyUp
/// Desc: Event for when a input value is changed
/////////////////////////////////////////////////////
void DlgNewEditBank::BankNewEditKeyUp( wxKeyEvent& WXUNUSED(event) )
{
	// program name
	if (m_InputBankName->GetValue().Length() != 0)
		m_BankName = m_InputBankName->GetValue();

	// comments
	if (m_InputComments->GetValue().Length() != 0)
		m_Comments = m_InputComments->GetValue();
}

/////////////////////////////////////////////////////
/// BtnCancelClick
/// Desc: Button callback for cancelling the dialog (nothing saved/changed)
/////////////////////////////////////////////////////
void DlgNewEditBank::BtnCancelClick( wxCommandEvent& WXUNUSED(event) )
{
	this->EndModal(RESPONSE_CANCEL);
}

/////////////////////////////////////////////////////
/// BtnAddEditClick
/// Desc: Button callback for adding to or updating the bank
/////////////////////////////////////////////////////
void DlgNewEditBank::BtnAddEditClick( wxCommandEvent& WXUNUSED(event) )
{
	std::string fullPath;
	wxLongLong value;
	std::string guid;

	if (m_Mode == MODE_ADD)
	{
		// creation date
		m_CreationDate = wxDateTime::Now();

		value = m_CreationDate.GetValue();
		wxString dateStr = m_CreationDate.FormatISODate();
		wxString timeStr = m_CreationDate.FormatISOTime();
		std::string convertedTime = timeStr.ToStdString();
		std::replace(convertedTime.begin(), convertedTime.end(), ':', '-');

		fullPath = sys::AppData::Instance()->GetBanksDirectory().ToStdString() + std::string("/") + std::string("Bank-") + std::string(dateStr.ToStdString() + "@" + convertedTime) + std::string(".json");

		guid = sys::AppData::Instance()->GetGUIDAsString();
		m_EditBank = JsonBox::Value();
	}
	else if (m_Mode == MODE_EDIT)
	{
		value = m_CreationDate.GetValue();

		fullPath = m_EditFilename.ToStdString();

		guid = m_EditGuid;
	}

	JsonBox::Object bankData;
	bankData["Name"] = JsonBox::Value(m_BankName.ToStdString());
	bankData["Comments"] = JsonBox::Value(m_Comments.ToStdString());
	bankData["GUID"] = JsonBox::Value(guid);
	bankData["Date"] = JsonBox::Value(value.ToString().ToStdString());
	bankData["Bank"] = m_EditBank;

	// save out to a value to write
	JsonBox::Value v(bankData);

	v.writeToFile(fullPath);

	this->EndModal(RESPONSE_OK);
}

void DlgNewEditBank::LoadFromFile()
{
	if (m_EditFilename.length() == 0)
		return;

	JsonBox::Value loadedProgram;
	
	std::ifstream t(m_EditFilename.ToStdString());
	std::stringstream buffer;
	buffer << t.rdbuf();

	std::string bufferStr = buffer.str();
	loadedProgram.loadFromString(bufferStr);
	//loadedProgram.loadFromFile(m_EditFilename.ToStdString());

	if (!loadedProgram.isNull())
	{
		if (loadedProgram["Name"].isString())
		{
			m_BankName = loadedProgram["Name"].getString();
			m_InputBankName->SetValue(m_BankName);
		}

		if (loadedProgram["Comments"].isString())
		{
			m_Comments = loadedProgram["Comments"].getString();
			m_InputComments->SetValue(m_Comments);
		}

		if (loadedProgram["Date"].isString())
		{
			wxString str = loadedProgram["Date"].getString();
			long long val = -1;
			if (str.ToLongLong(&val))
			{
				m_CreationDate = wxDateTime((wxLongLong)val);
			}
		}
	}
}