#ifndef __PanelTutorial__
#define __PanelTutorial__

/**
@file
Subclass of Tutorial, which is generated by wxFormBuilder.
*/

#include "Common.h"
#include "TECSApp.h"

//// end generated include
const int TUT_EXAMPLE_ID = 2000;
const int TUT_BREAK_ID = 2001;
const int TUT_PREPARESHOW_ID = 2002;

/** Implementing Tutorial */
class PanelTutorial : public Tutorial
{
	public:
		enum TutorialState
		{
			TUTORIAL_STATE_INITIAL = 0,
			TUTORIAL_EXAMPLE_1,
			TUTORIAL_EXAMPLE_2,
			TUTORIAL_EXAMPLE_3,
			TUTORIAL_END
		};

		/** Constructor */
		PanelTutorial( wxWindow* parent );
	//// end generated class members

		void SetVersion(int v)	{ m_Version = v; }

		void OnExampleTimer(wxTimerEvent& event);
		void OnBreakTimer(wxTimerEvent& event);
		void OnPrepareShowTimer(wxTimerEvent& event);

	private:
		void SetupState();
		void DoTutorialLayout1();
		void DoTutorialLayout2();
		void DoTutorialLayout3();
		void DoTutorialLayout4();

		void DoTutorialTextPrint();

		void DoLayoutVersion1(int example);
		void DoLayoutVersion2(int example);
		void DoLayoutVersion3(int example);
		void DoLayoutVersion4(int example);

		void DoPrepare();
		void DoBreakText();
		void DoEndTutorial();
		
	protected:
		virtual void ResizePanel(wxSizeEvent& event);
		virtual void BtnContinueClick(wxCommandEvent& event);
		virtual void KeyUpEvent(wxKeyEvent& event);

	private:
		bool m_ShowingWaitText;
		bool m_ShowExamplePrepare;

		bool m_TutorialComplete;
		wxTimer m_ExampleTimer;
		wxTimer m_BreakTimer;
		wxTimer m_PrepareShowTimer;

		int m_CurrentHeight;
		int m_CurrentWidth;

		float m_ScaleW;
		float m_ScaleH;

		int m_CurrentFontSize;
		int m_CurrentSmallFontSize;

		int m_Version;

		ParticipantData m_RunData;

		std::wstring m_LastTextLine;
		int m_NumLinePrints;
		int m_KeyCheckValue;

		TutorialState m_CurrentState;

		wxRichTextAttr m_BorderAttr;
		int m_BorderSpacing;

		int m_ExampleNumber;

		wxDECLARE_EVENT_TABLE();
};

#endif // __PanelTutorial__
