/**
	TECS
	Main.cpp
    Purpose: Main entry file for the application
*/

#include "System.h"
#include "PanelEntry.h"
#include "ProjectMainFrame.h"
#include "TECSApp.h"

namespace
{
	wxLocale* selectedLocale = nullptr;
	long language = wxLANGUAGE_DEFAULT;//wxLANGUAGE_SPANISH
}

// Define a new application type, each program should derive a class from wxApp
class MainApp : public wxApp
{
public:
    // override base class virtuals
    // ----------------------------

    // this one is called on application startup and is a good place for the app
    // initialization (doing it here and not in the ctor allows to have an error
    // return: if OnInit() returns false, the application terminates)
    virtual bool OnInit() wxOVERRIDE;

	virtual int OnExit() wxOVERRIDE;

private:
	sys::AppData* m_AppDataInstance = 0;
};


// Create a new application object: this macro will allow wxWidgets to create
// the application object during program execution (it's better than using a
// static object for many reasons) and also implements the accessor function
// wxGetApp() which will return the reference of the right type (i.e. MyApp and
// not wxApp)
wxIMPLEMENT_APP(MainApp);

// ----------------------------------------------------------------------------
// the application class
// ----------------------------------------------------------------------------

// 'Main program' equivalent: the program execution "starts" here
bool MainApp::OnInit()
{
    // call the base class initialization method, currently it only parses a
    // few common command-line options but it could be do more in the future
    if ( !wxApp::OnInit() )
        return false;

	// load language if possible, fall back to english otherwise
	if (wxLocale::IsAvailable(language))
	{
		selectedLocale = new wxLocale(language);

#ifdef __WXGTK__
		// add locale search paths
		locale->AddCatalogLookupPathPrefix(wxT("/usr"));
		locale->AddCatalogLookupPathPrefix(wxT("/usr/local"));
		wxStandardPaths* paths = (wxStandardPaths*)&wxStandardPaths::Get();
		wxString prefix = paths->GetInstallPrefix();
		locale->AddCatalogLookupPathPrefix(prefix);
#endif

		selectedLocale->AddCatalog("TECSApp");

		if (!selectedLocale->IsOk())
		{
			//std::cerr << "selected language is wrong" << std::endl;
			delete selectedLocale;
			selectedLocale = new wxLocale(wxLANGUAGE_ENGLISH);
			language = wxLANGUAGE_ENGLISH;
		}
	}
	else
	{
		//std::cout << "The selected language is not supported by your system."
		//	<< "Try installing support for this language." << std::endl;
		selectedLocale = new wxLocale(wxLANGUAGE_ENGLISH);
		language = wxLANGUAGE_ENGLISH;
	}

	sys::AppData::Initialise();
	sys::AppData::Instance()->LoadSettings(sys::AppData::Instance()->defaultSettingsFile);
	//sys::AppData::Instance()->LoadBank(sys::AppData::Instance()->defaultBankFile);

    // create the main application window
	ProjectMainFrame *frame = new ProjectMainFrame(0);
	sys::AppData::Instance()->SetMainFrame(frame);
	frame->SetIcons(wxNullIconBundle);

#ifdef _WIN32
	wxIcon appIcon;
	appIcon.LoadFile(wxT("#101"), wxBITMAP_TYPE_ICO_RESOURCE);
	frame->SetIcon(appIcon);
#endif
	// show a splash screen when not running in debug
#ifndef _DEBUG
#ifdef _WIN32
	wxBitmap bitmap;

	bool ok = bitmap.LoadFile(wxT("#105"), wxBITMAP_TYPE_BMP_RESOURCE /*wxBITMAP_TYPE_BMP wxBITMAP_TYPE_PNG*/);

	if (ok)
	{
		new wxSplashScreen(bitmap,
			wxSPLASH_CENTRE_ON_SCREEN | wxSPLASH_TIMEOUT,
			3000, frame, wxID_ANY, wxDefaultPosition, wxDefaultSize,
			wxSIMPLE_BORDER | wxSTAY_ON_TOP);
	}

	#if !defined(__WXGTK20__)
		// we don't need it at least on wxGTK with GTK+ 2.12.9
		wxYield();
	#endif
#endif // _WIN32
#endif //_DEBUG

    // and show it (the frames, unlike simple controls, are not shown when
    // created initially)
    frame->Show(true);

	sys::AppData::Instance()->ChangePanel(SCREEN_ENTRY);

    // success: wxApp::OnRun() will be called which will enter the main message
    // loop and the application will run. If we returned false here, the
    // application would exit immediately.
    return true;
}

int MainApp::OnExit()
{
	sys::AppData::Shutdown();

	if (selectedLocale != nullptr)
		delete selectedLocale;

	return 0;
}
