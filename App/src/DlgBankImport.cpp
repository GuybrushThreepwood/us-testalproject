
/**
	TECS
	DlgBankImport.cpp
    Purpose: Handles when a copy/paste has been called into the bank
*/

#include "DlgBankImport.h"
#include "System.h"

//----------------------------------------------------------------------------
// dxGridCellSizedChoiceEditor
//   Shows a wxGridCellChoiceEditor enough wide to see all choices
//----------------------------------------------------------------------------
dxGridCellSizedChoiceEditor::dxGridCellSizedChoiceEditor(const
	wxArrayString& choices,
	bool allowOthers,
	int max)
	:wxGridCellChoiceEditor(choices,
	allowOthers)
{
	m_maxWide = max;
}

dxGridCellSizedChoiceEditor::dxGridCellSizedChoiceEditor(size_t count,
	const wxString
	choices[],
	bool allowOthers,
	int max)
	:wxGridCellChoiceEditor(count,
	choices, allowOthers)
{
	m_maxWide = max;
}

wxGridCellEditor *dxGridCellSizedChoiceEditor::Clone() const
{
	dxGridCellSizedChoiceEditor *editor = new
		dxGridCellSizedChoiceEditor();
	editor->m_maxWide = m_maxWide;
	return editor;
}

void dxGridCellSizedChoiceEditor::Show(bool show, wxGridCellAttr *attr)
{
	if (show)
	{ //Search the widest string
		int h, w, widemax = -1;
		size_t i, n = m_choices.GetCount();
		wxFont font = m_control->GetFont();
		if (attr)
			font = attr->GetFont();
		for (i = 0; i<n; i++)
		{
			m_control->GetTextExtent(m_choices.Item(i), &w, &h, NULL,
				NULL, &font);
			if (w > widemax)
				widemax = w;
		}
		widemax += 8; //some more, because of control borders
		if (widemax > m_maxWide && m_maxWide >= 0)
			widemax = m_maxWide;
		//Sets the width
		m_control->SetSize(widemax, -1);
	}

	wxGridCellEditor::Show(show, attr);
}

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
DlgBankImport::DlgBankImport(wxWindow* parent, wxString str)
:
BankImportDlg( parent ),
m_ImportText(str)
{
	this->SetTitle(_("Import to Bank"));

	m_ComboFilter->Clear();
	m_FilterMap.clear();

	m_FilterList = sys::AppData::Instance()->GetFilters();
	auto currentBank = sys::AppData::Instance()->GetBank();

	for (auto val : m_FilterList)
	{
		int index = m_ComboFilter->GetCount();

		m_ComboFilter->Insert(val.second.name, index);

		// make this map match the selection box
		m_FilterMap.insert(std::make_pair(index, val.second.val));
	}

	m_ComboFilter->SetSelection(0);

	wxString curField;
	wxString curLine;
	// process to separate words by newline 
	do 
	{
		curLine = str.BeforeFirst('\n');
		str = str.AfterFirst('\n');

		do 
		{
			curField = curLine.BeforeFirst('\t');
			curLine = curLine.AfterFirst('\t');

			if (curField.Length() > 0)
			{
				// check if it exists already in the bank
				
				auto search = currentBank.find(curField);
				if (search == currentBank.end())
				{
					ListInfo info;
					info.textData = curField;
					info.isDefault = true;
					info.selectionId = 0;

					m_FilteredStringList.push_back(info);
				}
			}

		} while (curLine.IsEmpty() == false);

	} while (str.IsEmpty() == false);

	for (auto item : m_FilterList)
		m_StringArray.Insert(item.second.name, m_StringArray.size());

	// only allow row selection
	m_GridFound->SetSelectionMode(wxGrid::wxGridSelectRows);

	// labels
	m_GridFound->SetColLabelValue(0, _("Word/Sentence"));
	m_GridFound->SetColLabelValue(1, _("Filter"));
	m_GridFound->SetColLabelValue(2, _("Number of words"));

	// sort by filter
	m_GridFound->SetSortingColumn(1);

	// disable cell highlight
	m_GridFound->SetCellHighlightPenWidth(0);
	m_GridFound->SetCellHighlightROPenWidth(0);

	m_GridFound->Connect(wxEVT_GRID_CELL_CHANGED, wxGridEventHandler(DlgBankImport::GridCellChange), NULL, this);

	CreateGrid();
}

/////////////////////////////////////////////////////
/// CreateGrid
/// Desc: Cleans and populates the wxGrid with the word/sentence values 
/////////////////////////////////////////////////////
void DlgBankImport::CreateGrid()
{
	// How many Cols/Rows
	int rows = m_GridFound->GetNumberRows();

	// Delete all Cols/Rows
	if (rows > 0)
		m_GridFound->DeleteRows(0, rows, false);

	int sel = m_ComboFilter->GetSelection();

	int rowId = 0;
	if (m_FilteredStringList.size() > 0)
	{
		m_GridFound->InsertRows(0, m_FilteredStringList.size());
		std::size_t i = 0;
		for (i = 0; i < m_FilteredStringList.size(); ++i)
		{
			m_GridFound->SetCellValue(rowId, 0, m_FilteredStringList[i].textData);
			//m_GridFound->SetReadOnly(rowId, 0);

			m_GridFound->SetCellEditor(rowId, 1, new dxGridCellSizedChoiceEditor(m_StringArray, false)); // Add this to Cell

			if (m_FilteredStringList[i].isDefault)
			{
				auto lookup = m_FilterMap.find(sel);
				m_GridFound->SetCellValue(rowId, 1, m_StringArray[lookup->first]);
			}
			else
			{
				auto lookup = m_FilterMap.find(m_FilteredStringList[i].selectionId);
				m_GridFound->SetCellValue(rowId, 1, m_StringArray[lookup->first]);
			}

			m_GridFound->SetCellValue(rowId, 2, wxString::Format("%d", (int)sys::WordCount(m_FilteredStringList[i].textData.ToStdWstring())));
			m_GridFound->SetReadOnly(rowId, 2);

			rowId++;
		}
	}

	m_GridFound->AutoSize();
	this->Layout();
}

/////////////////////////////////////////////////////
/// GridCellLeftClick
/// Desc: Event callback on the word grid for basic selection
/////////////////////////////////////////////////////
void DlgBankImport::GridCellLeftClick(wxGridEvent& event)
{
	m_GridFound->ClearSelection();
	m_GridFound->SelectRow(event.GetRow());

	event.Skip();
}

/////////////////////////////////////////////////////
/// ComboFilterChange
/// Desc: Event callback for the root combo to apply the same filter to the imported words/sentences
/////////////////////////////////////////////////////
void DlgBankImport::ComboFilterChange(wxCommandEvent& WXUNUSED(event))
{
	int sel = m_ComboFilter->GetSelection();

	//auto lookup = m_FilterMap.find(sel);
	for (auto& item : m_FilteredStringList)
	{
		if (item.isDefault)
		{
			auto lookup = m_FilterMap.find(sel);

			item.selectionId = lookup->first;
		}
	}

	CreateGrid();
}

/////////////////////////////////////////////////////
/// GridCellChange
/// Desc: Event callback for changing the filter combo box per word/sentence
/////////////////////////////////////////////////////
void DlgBankImport::GridCellChange(wxGridEvent& event)
{
	int row = event.GetRow();
	int col = event.GetCol();

	auto& info = m_FilteredStringList[row];
	if (col == 0)
	{
		info.textData = m_GridFound->GetCellValue(row, col);
	}
	else
	if (col == 1)
	{
		wxString cellVal = m_GridFound->GetCellValue(row, col);

		for (auto val : m_FilterList)
		{
			if (val.second.name.IsSameAs(cellVal))
			{
				for (auto filter : m_FilterMap)
				{
					if (filter.second == val.second.val)
					{
						info.selectionId = filter.first;

						break;
					}
				}
			}
		}
		info.isDefault = false;
		CreateGrid();
	}
}


/////////////////////////////////////////////////////
/// BtnDeleteClick
/// Desc: Button event for deleting an import of a word/sentence in the list
/////////////////////////////////////////////////////
void DlgBankImport::BtnDeleteClick(wxCommandEvent& WXUNUSED(event))
{
	// get selected
	wxArrayInt arr = m_GridFound->GetSelectedRows();

	int numSelected = arr.Count();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId >= 0 &&
			rowId < m_GridFound->GetNumberRows())
		{
			//if (wxMessageBox(_("Are you sure you want to delete this from the list?"), _("DELETE WORD/SENTENCE"), wxYES_NO) == wxYES)
			{
				m_FilteredStringList.erase(m_FilteredStringList.begin()+rowId);
				CreateGrid();
			}
		}
	}
}

/////////////////////////////////////////////////////
/// BtnCancelClick
/// Desc: Button callback for cancelling the dialog (nothing saved/changed)
/////////////////////////////////////////////////////
void DlgBankImport::BtnCancelClick(wxCommandEvent& WXUNUSED(event))
{
	this->EndModal(RESPONSE_CANCEL);
}

/////////////////////////////////////////////////////
/// BtnAcceptClick
/// Desc: Button callback for accepting current word/sentence list to go into the bank
/////////////////////////////////////////////////////
void DlgBankImport::BtnAcceptClick(wxCommandEvent& WXUNUSED(event))
{
	// add the list to the bank
	for (auto newItem : m_FilteredStringList)
	{
		BankData bd;
		bd.str = newItem.textData;

		int filterId = 0;
		
		for (auto filter : m_FilterMap)
		{
			if (filter.first == newItem.selectionId)
			{
				filterId = filter.second;
				break;
			}
		}

		const FilterData& fd = sys::AppData::Instance()->FindFilter(filterId);

		if (fd.val != -1)
			filterId = fd.val;

		bd.filterIndex = filterId;

		bd.wordCount = sys::WordCount(newItem.textData.ToStdWstring());
		bd.hasGUID = false;
		bd.guid = "";

		sys::AppData::Instance()->AddToBank(bd);
	}

	sys::AppData::Instance()->SaveBank();
	
	this->EndModal(RESPONSE_OK);
}
