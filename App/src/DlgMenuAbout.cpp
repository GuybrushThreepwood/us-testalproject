/**
	TECS
	DlgMenuAbout.cpp
    Purpose: Dialog for showing the about information
*/

#include "DlgMenuAbout.h"
#include "System.h"

#include "xpm/useville.xpm"

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
DlgMenuAbout::DlgMenuAbout(wxWindow* parent)
:
AboutDlg( parent )
{
	this->SetTitle(_("About TECS"));

	wxString creditsTxt = _("Research by Rodr\u00edguez-Testal, J.F., Sen\u00edn-Calder\u00f3n, C. y Perona-Garcel\u00e1n, S.");

	wxImage::AddHandler(new wxPNGHandler);

	wxRichTextAttr urlStyle;
	urlStyle.SetTextColour(*wxBLUE);
	urlStyle.SetFontUnderlined(true);
	urlStyle.SetAlignment(wxTEXT_ALIGNMENT_CENTRE);

    m_AboutInfo->SetBackgroundColour(this->GetBackgroundColour());
	m_AboutInfo->BeginAlignment(wxTEXT_ALIGNMENT_CENTRE);
	m_AboutInfo->BeginBold();
	m_AboutInfo->BeginFontSize(14);
	m_AboutInfo->WriteText(_("TECS"));
	m_AboutInfo->Newline();
	m_AboutInfo->WriteText(_("T"));
	m_AboutInfo->EndBold();
	m_AboutInfo->WriteText(_("estal "));
	//m_AboutInfo->Newline();
	m_AboutInfo->BeginBold();
	m_AboutInfo->BeginFontSize(14);
	m_AboutInfo->WriteText(_("E"));
	m_AboutInfo->EndBold();
	m_AboutInfo->WriteText(_("motional "));
	//m_AboutInfo->Newline();
	m_AboutInfo->BeginBold();
	m_AboutInfo->BeginFontSize(14);
	m_AboutInfo->WriteText(_("C"));
	m_AboutInfo->EndBold();
	m_AboutInfo->WriteText(_("ounting "));
	//m_AboutInfo->Newline();
	m_AboutInfo->BeginBold();
	m_AboutInfo->BeginFontSize(14);
	m_AboutInfo->WriteText(_("S"));
	m_AboutInfo->EndBold();
	m_AboutInfo->WriteText(_("troop"));
	m_AboutInfo->Newline();
	m_AboutInfo->Newline();
	m_AboutInfo->EndBold();
    m_AboutInfo->WriteText(creditsTxt);
	m_AboutInfo->Newline();

	m_AboutInfo->BeginStyle(urlStyle);
	m_AboutInfo->BeginURL(wxT("http://www.us.es/"));
	m_AboutInfo->WriteText(_("University of Seville"));
	m_AboutInfo->EndURL();
	m_AboutInfo->EndStyle();

	m_AboutInfo->Newline();
	m_AboutInfo->WriteImage(wxBitmap(useville_xpm));

	m_AboutInfo->Newline();
	m_AboutInfo->Newline();
	m_AboutInfo->WriteText(_("Application developed by John Murray"));
	m_AboutInfo->Newline();

	/*m_AboutInfo->BeginStyle(urlStyle);
	m_AboutInfo->BeginURL(wxT("http://www.solocodo.com"));
	m_AboutInfo->WriteText(wxT("SOLOCODO Website"));
	m_AboutInfo->EndURL();
	m_AboutInfo->EndStyle();*/

	m_AboutInfo->Newline();
    
    //m_AboutInfo->Disable();
    m_AboutInfo->GetCaret()->Hide();
}

/////////////////////////////////////////////////////
/// URLClick
/// Desc: Window event for when a URL is clicked
/////////////////////////////////////////////////////
void DlgMenuAbout::URLClick(wxTextUrlEvent& event)
{
	wxLaunchDefaultBrowser(event.GetString());
}

/////////////////////////////////////////////////////
/// AboutDlgClickOK
/// Desc: Button event for exiting the about dialog
/////////////////////////////////////////////////////
void DlgMenuAbout::AboutDlgClickOK(wxCommandEvent& WXUNUSED(event))
{
	this->EndModal(RESPONSE_CANCEL);
}
