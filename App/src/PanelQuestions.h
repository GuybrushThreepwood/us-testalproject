#ifndef __PanelQuestions__
#define __PanelQuestions__

/**
@file
Subclass of Questions, which is generated by wxFormBuilder.
*/

#include "Common.h"
#include "TECSApp.h"

//// end generated include

/** Implementing Questions */
class PanelQuestions : public Questions
{
	public:
		/** Constructor */
		PanelQuestions( wxWindow* parent );
	//// end generated class members
	
	protected:
		void DynamicCountChange(wxSpinEvent& event);
		void DynamicCountChangeTxt(wxCommandEvent& event);

		virtual void ProgramSearchCancel(wxCommandEvent& event);
		virtual void ProgramSearch(wxCommandEvent& event);
		virtual void ProgramSearchEnter(wxCommandEvent& event);

		virtual void BtnDoneClick(wxCommandEvent& event);

	private:
		void CreateProgramDataList();

	private:
		bool m_UserInput;
		wxString m_StringFilter;

		ParticipantData m_CurrentData;

		// filter dynamic data
		std::vector<StoredCorrectData> m_AnswersDynamic;
		std::map<int, int> m_ProgramCounts;

		std::vector<wxString> m_AnswerStringList;
};

#endif // __PanelQuestions__
