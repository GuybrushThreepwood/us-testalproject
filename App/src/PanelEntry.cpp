/**
	TECS
	PanelEntry.cpp
    Purpose: Panel initially shown on start up for choosing between administration or running a program as a participant
*/

#include "PanelEntry.h"
#include "System.h"
#include "DlgPassword.h"

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
PanelEntry::PanelEntry(wxWindow* parent)
:
Entry( parent )
{

}

/////////////////////////////////////////////////////
/// BtnAdminClick
/// Desc: Button event for switching to admin section of the program
/////////////////////////////////////////////////////
void PanelEntry::BtnAdminClick(wxCommandEvent& WXUNUSED(event))
{
#ifdef _DEBUG
	sys::AppData::Instance()->ChangePanel(SCREEN_ADMIN);
#else
	if (sys::AppData::Instance()->GetAdminPasswordState())
	{
		DlgPassword* dlg = new DlgPassword(0);
		if (dlg->ShowModal() == RESPONSE_OK)
		{
			sys::AppData::Instance()->ChangePanel(SCREEN_ADMIN);
		}

		dlg->Destroy();
		delete dlg;
	}
	else
	{
		sys::AppData::Instance()->ChangePanel(SCREEN_ADMIN);
	}
#endif // _DEBUG
}

/////////////////////////////////////////////////////
/// BtnParticipantClick
/// Desc: Button event for switching to the participant detail view
/////////////////////////////////////////////////////
void PanelEntry::BtnParticipantClick(wxCommandEvent& WXUNUSED(event))
{
	sys::AppData::Instance()->ChangePanel(SCREEN_PARTICIPANT);
}
