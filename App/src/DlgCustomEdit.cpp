/**
	TECS
	DlgCustomEdit.cpp
    Purpose: Dialog for creating/editing new filters and phases
*/

#include "DlgCustomEdit.h"
#include "System.h"

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
DlgCustomEdit::DlgCustomEdit(wxWindow* parent, CustomEditMode mode)
:
CustomEditDlg( parent ),
m_Mode(mode)
{
	if (m_Mode == CUSTOMMODE_FILTER)
	{
		this->SetTitle(_("Filters"));

		// default filter list
		auto filerList = sys::AppData::Instance()->GetFilters();

		// DEFAULT GRID
		m_DefaultGridList->SetSelectionMode(wxGrid::wxGridSelectRows);

		m_DefaultGridList->SetColLabelValue(0, _("Name"));
		m_DefaultGridList->SetColLabelValue(1, _("Internal ID"));
		m_DefaultGridList->SetColLabelValue(2, _("Excel Colour Index"));
		m_DefaultGridList->SetColLabelValue(3, _("Excel Colour"));

		// disable cell highlight
		m_DefaultGridList->SetCellHighlightPenWidth(0);
		m_DefaultGridList->SetCellHighlightROPenWidth(0);


		// CUSTOM GRID
		m_CustomGridList->SetSelectionMode(wxGrid::wxGridSelectRows);

		m_CustomGridList->SetColLabelValue(0, _("Name"));
		m_CustomGridList->SetColLabelValue(1, _("Internal ID"));
		m_CustomGridList->SetColLabelValue(2, _("Excel Colour Index"));
		m_CustomGridList->SetColLabelValue(3, _("Excel Colour"));

		// disable cell highlight
		m_CustomGridList->SetCellHighlightPenWidth(0);
		m_CustomGridList->SetCellHighlightROPenWidth(0);

		int indexDefault = 0;
		int indexCustom = 0;

		for (auto filter : filerList)
		{
			if (filter.second.isDefault)
			{
				m_DefaultGridList->InsertRows(indexDefault);

				m_DefaultGridList->SetCellValue(indexDefault, 0, filter.second.name);
				m_DefaultGridList->SetCellValue(indexDefault, 1, wxString::Format("%d", filter.second.val));
				m_DefaultGridList->SetCellEditor(indexDefault, 2, new wxGridCellNumberEditor(0, 63));
				m_DefaultGridList->SetCellValue(indexDefault, 2, wxString::Format("%d", filter.second.colIndex));
				m_DefaultGridList->SetCellValue(indexDefault, 3, "");
				m_DefaultGridList->SetCellBackgroundColour(indexDefault, 3, sys::AppData::Instance()->GetExcelColours()[filter.second.colIndex].col);

				m_DefaultGridList->SetReadOnly(indexDefault, 0);
				m_DefaultGridList->SetReadOnly(indexDefault, 1);
				//m_DefaultGridList->SetReadOnly(indexDefault, 2);
				m_DefaultGridList->SetReadOnly(indexDefault, 3);

				indexDefault++;
			}
			else
			{
				m_CustomGridList->InsertRows(indexCustom);

				m_CustomGridList->SetCellValue(indexCustom, 0, filter.second.name);
				m_CustomGridList->SetCellValue(indexCustom, 1, wxString::Format("%d", filter.second.val));
				m_CustomGridList->SetCellEditor(indexCustom, 2, new wxGridCellNumberEditor(0, 63));
				m_CustomGridList->SetCellValue(indexCustom, 2, wxString::Format("%d", filter.second.colIndex));
				m_CustomGridList->SetCellValue(indexCustom, 3, "");
				m_CustomGridList->SetCellBackgroundColour(indexCustom, 3, sys::AppData::Instance()->GetExcelColours()[filter.second.colIndex].col);

				//m_CustomGridList->SetReadOnly(indexCustom, 0);
				m_CustomGridList->SetReadOnly(indexCustom, 1);
				//m_CustomGridList->SetReadOnly(indexDefault, 2);
				m_CustomGridList->SetReadOnly(indexCustom, 3);
			}
		}

		m_DefaultGridList->Connect(wxEVT_GRID_CELL_CHANGED, wxGridEventHandler(DlgCustomEdit::GridCellChange), NULL, this);
		m_CustomGridList->Connect(wxEVT_GRID_CELL_CHANGED, wxGridEventHandler(DlgCustomEdit::GridCellChange), NULL, this);
	}
	else if (m_Mode == CUSTOMMODE_PHASE)
	{
		this->SetTitle(_("Phases"));

	}

	DoCustomListRefresh();
}

/////////////////////////////////////////////////////
/// InitDialog
/// Desc: Event called when the dialog is ready to be set up
/////////////////////////////////////////////////////
void DlgCustomEdit::InitDialog(wxInitDialogEvent& WXUNUSED(event))
{
	// default list
	int numRows = m_DefaultGridList->GetNumberRows();
	int i = 0;

	for (i = 0; i < numRows; ++i)
	{
		m_DefaultGridList->SetGridCursor(i, 2);
		m_DefaultGridList->EnableCellEditControl(true);
		m_DefaultGridList->ShowCellEditControl();

		wxGridCellNumberEditor* editor = (wxGridCellNumberEditor*)m_DefaultGridList->GetCellEditor(i, 2);
		
		wxSpinCtrl *ctrl = (wxSpinCtrl *)editor->GetControl();
		if (ctrl)
		{
			ctrl->Connect(wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler(DlgCustomEdit::ColorChange), NULL, this);
			ctrl->Connect(wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler(DlgCustomEdit::ColorChangeTxt), NULL, this);

			int val = ctrl->GetValue();
			if( val >= 0 && val <= 63)
				m_DefaultGridList->SetCellBackgroundColour(i, 3, sys::AppData::Instance()->GetExcelColours()[ctrl->GetValue()].col);
		}
		else
			wxLogDebug("not valid");

		editor->DecRef();

		m_DefaultGridList->EnableCellEditControl(false);
		m_DefaultGridList->HideCellEditControl();
	}

	m_DefaultGridList->Refresh();

	// custom
	numRows = m_CustomGridList->GetNumberRows();

	for (i = 0; i < numRows; ++i)
	{
		m_CustomGridList->SetGridCursor(i, 2);
		m_CustomGridList->EnableCellEditControl(true);
		m_CustomGridList->ShowCellEditControl();

		wxGridCellNumberEditor* editor = (wxGridCellNumberEditor*)m_CustomGridList->GetCellEditor(i, 2);

		wxSpinCtrl *ctrl = (wxSpinCtrl *)editor->GetControl();
		if (ctrl)
		{
			ctrl->Connect(wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler(DlgCustomEdit::ColorChangeCustom), NULL, this);
			ctrl->Connect(wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler(DlgCustomEdit::ColorChangeCustomTxt), NULL, this);

			int val = ctrl->GetValue();
			if (val >= 0 && val <= 63)
				m_CustomGridList->SetCellBackgroundColour(i, 3, sys::AppData::Instance()->GetExcelColours()[ctrl->GetValue()].col);
		}
		else
			wxLogDebug("not valid");

		editor->DecRef();

		m_CustomGridList->EnableCellEditControl(false);
		m_CustomGridList->HideCellEditControl();
	}

	m_CustomGridList->Refresh();
}

/////////////////////////////////////////////////////
/// DoCustomListRefresh
/// Desc: Manual clean and remove of the custom list
/////////////////////////////////////////////////////
void DlgCustomEdit::DoCustomListRefresh()
{
	// How many Cols/Rows
	int rows = m_CustomGridList->GetNumberRows();

	// Delete all Cols/Rows
	if (rows > 0)
		m_CustomGridList->DeleteRows(0, rows, false);

	if (m_Mode == CUSTOMMODE_FILTER)
	{
		auto filerList = sys::AppData::Instance()->GetFilters();
		int indexCustom = 0;
		for (auto filter : filerList)
		{
			if (!filter.second.isDefault)
			{
				m_CustomGridList->InsertRows(indexCustom);

				m_CustomGridList->SetCellValue(indexCustom, 0, filter.second.name);
				m_CustomGridList->SetCellValue(indexCustom, 1, wxString::Format("%d", filter.second.val));
				m_CustomGridList->SetCellEditor(indexCustom, 2, new wxGridCellNumberEditor(0, 63));
				m_CustomGridList->SetCellValue(indexCustom, 2, wxString::Format("%d", filter.second.colIndex));
				m_CustomGridList->SetCellValue(indexCustom, 3, "");
				m_CustomGridList->SetCellBackgroundColour(indexCustom, 3, sys::AppData::Instance()->GetExcelColours()[filter.second.colIndex].col);

				m_CustomGridList->SetGridCursor(indexCustom, 2);
				m_CustomGridList->EnableCellEditControl(true);
				m_CustomGridList->ShowCellEditControl();

				wxGridCellNumberEditor* editor = (wxGridCellNumberEditor*)m_CustomGridList->GetCellEditor(indexCustom, 2);

				wxSpinCtrl *ctrl = (wxSpinCtrl *)editor->GetControl();
				if (ctrl)
				{
					ctrl->Connect(wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler(DlgCustomEdit::ColorChangeCustom), NULL, this);
					ctrl->Connect(wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler(DlgCustomEdit::ColorChangeCustomTxt), NULL, this);

					int val = ctrl->GetValue();
					if (val >= 0 && val <= 63)
						m_CustomGridList->SetCellBackgroundColour(indexCustom, 3, sys::AppData::Instance()->GetExcelColours()[ctrl->GetValue()].col);
				}
				else
					wxLogDebug("not valid");

				editor->DecRef();

				m_CustomGridList->EnableCellEditControl(false);
				m_CustomGridList->HideCellEditControl();

				//m_CustomGridList->SetReadOnly(indexCustom, 0);
				m_CustomGridList->SetReadOnly(indexCustom, 1);
				//m_CustomGridList->SetReadOnly(indexDefault, 2);
				m_CustomGridList->SetReadOnly(indexCustom, 3);

				indexCustom++;
			}
		}
	}
	else if (m_Mode == CUSTOMMODE_PHASE)
	{
		auto phaseList = sys::AppData::Instance()->GetPhases();

	}

	m_DefaultGridList->AutoSize();
	m_CustomGridList->AutoSize();
	this->Layout();
}

/////////////////////////////////////////////////////
/// GridCellLeftClick
/// Desc: Event callback on the word grid for basic selection
/////////////////////////////////////////////////////
void DlgCustomEdit::GridCellLeftClick(wxGridEvent& event)
{
	if (m_DefaultGridList == (wxGrid *)event.GetEventObject())
	{
		m_DefaultGridList->ClearSelection();
		m_DefaultGridList->SelectRow(event.GetRow());

		if (event.GetCol() == 2 ||
			event.GetCol() == 3)
		{
			m_DefaultGridList->SetGridCursor(event.GetRow(), 2);
			m_DefaultGridList->EnableCellEditControl(true);
			m_DefaultGridList->ShowCellEditControl();
		}
	}
	else
	if (m_CustomGridList == (wxGrid *)event.GetEventObject())
	{
		m_CustomGridList->ClearSelection();
		m_CustomGridList->SelectRow(event.GetRow());

		if (event.GetCol() == 2 ||
			event.GetCol() == 3)
		{
			m_CustomGridList->SetGridCursor(event.GetRow(), 2);
			m_CustomGridList->EnableCellEditControl(true);
			m_CustomGridList->ShowCellEditControl();
		}
	}
	event.Skip();
}

/////////////////////////////////////////////////////
/// GridCellChange
/// Desc: Event callback for when a cell gets changes
/////////////////////////////////////////////////////
void DlgCustomEdit::GridCellChange(wxGridEvent& WXUNUSED(event))
{
	/*int row = event.GetRow();
	int col = event.GetCol();*/
}

/////////////////////////////////////////////////////
/// ColorChangeTxt
/// Desc: SpinCtrl event for the excel colour index values
/////////////////////////////////////////////////////
void DlgCustomEdit::ColorChangeTxt(wxCommandEvent& event)
{
	wxSpinEvent e;
	e.SetEventObject(event.GetEventObject());
	ColorChange(e);
}

/////////////////////////////////////////////////////
/// ColorChange
/// Desc: SpinCtrl event for the excel colour index values
/////////////////////////////////////////////////////
void DlgCustomEdit::ColorChange(wxSpinEvent& event)
{
	wxSpinCtrl* pObj = reinterpret_cast<wxSpinCtrl *>(event.GetEventObject());
	if (pObj)
	{
		int curRow = m_DefaultGridList->GetGridCursorRow();
			
		m_DefaultGridList->SetCellBackgroundColour(curRow, 3, sys::AppData::Instance()->GetExcelColours()[pObj->GetValue()].col);
		m_DefaultGridList->Refresh();
	}
}

/////////////////////////////////////////////////////
/// ColorChangeCustomTxt
/// Desc: SpinCtrl event for the excel colour index values
/////////////////////////////////////////////////////
void DlgCustomEdit::ColorChangeCustomTxt(wxCommandEvent& event)
{
	wxSpinEvent e;
	e.SetEventObject(event.GetEventObject());
	ColorChangeCustom(e);
}

/////////////////////////////////////////////////////
/// ColorChangeCustom
/// Desc: SpinCtrl event for the excel colour index values
/////////////////////////////////////////////////////
void DlgCustomEdit::ColorChangeCustom(wxSpinEvent& event)
{
	wxSpinCtrl* pObj = reinterpret_cast<wxSpinCtrl *>(event.GetEventObject());
	if (pObj)
	{
		int curRow = m_CustomGridList->GetGridCursorRow();

		m_CustomGridList->SetCellBackgroundColour(curRow, 3, sys::AppData::Instance()->GetExcelColours()[pObj->GetValue()].col);
		m_CustomGridList->Refresh();
	}
}

/////////////////////////////////////////////////////
/// BtnAddClick
/// Desc: Event for button to add a new custom filter or phase
/////////////////////////////////////////////////////
void DlgCustomEdit::BtnAddClick(wxCommandEvent& WXUNUSED(event))
{

	if (m_Mode == CUSTOMMODE_FILTER)
	{
		int newFilterId = sys::AppData::Instance()->RequestNewFilterId();

		wxString str = wxString::Format("New Filter - %d", newFilterId);
		//long insertPos = m_CustomList->InsertItem(itemIndex++, str, -1);

		FilterData fd;
		fd.isDefault = false;
		fd.name = str;
		fd.val = newFilterId;
		fd.colIndex = 0;//sys::RandInt(0, 63);

		sys::AppData::Instance()->AddToFilters(fd);
	}
	else if (m_Mode == CUSTOMMODE_PHASE)
	{
		/*int newPhaseId = sys::AppData::Instance()->RequestNewPhaseId();

		wxString str = wxString::Format("New Phase - %d", newPhaseId);
		//long insertPos = m_CustomList->InsertItem(itemIndex++, str, -1);

		PhaseData pd;
		pd.isDefault = false;
		pd.name = str;
		pd.val = newPhaseId;

		sys::AppData::Instance()->AddToPhases(pd);*/
	}

	DoCustomListRefresh();
}

/////////////////////////////////////////////////////
/// BtnRemoveClick
/// Desc: Event for button to add a new custom filter or phase
/////////////////////////////////////////////////////
void DlgCustomEdit::BtnRemoveClick(wxCommandEvent& WXUNUSED(event))
{
	// get selected
	wxArrayInt arr = m_CustomGridList->GetSelectedRows();

	int numSelected = arr.Count();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId >= 0 &&
			rowId < m_CustomGridList->GetNumberRows())
		{
			if (m_Mode == CUSTOMMODE_FILTER)
			{
				wxString filterToRemoveStr = m_CustomGridList->GetCellValue(rowId, 0);
				if (filterToRemoveStr.Length() > 0)
				{
					// ask first
					if (wxMessageBox(_("Are you sure you want to delete this filter?\nAny word or sentence using this filter will be set to neutral"), _("DELETE Filter"), wxYES_NO) == wxYES)
					{
						const FilterData fd = sys::AppData::Instance()->FindFilter(filterToRemoveStr);

						if (fd.val != -1)
						{
							if (sys::AppData::Instance()->RemoveFromFilters(filterToRemoveStr, fd.val))
							{
								// done
								DoCustomListRefresh();
							}
						}
					}
				}
			}
			else if (m_Mode == CUSTOMMODE_PHASE)
			{
			}
		}
	}
}

/////////////////////////////////////////////////////
/// BtnDoneClick
/// Desc: Event for closing the dialog
/////////////////////////////////////////////////////
void DlgCustomEdit::BtnDoneClick(wxCommandEvent& WXUNUSED(event))
{
	// store colour changes
	int numRows = m_DefaultGridList->GetNumberRows();
	int i = 0;

	for (i = 0; i < numRows; ++i)
	{
		m_DefaultGridList->SetGridCursor(i, 2);
		m_DefaultGridList->EnableCellEditControl(true);
		m_DefaultGridList->ShowCellEditControl();

		wxGridCellNumberEditor* editor = (wxGridCellNumberEditor*)m_DefaultGridList->GetCellEditor(i, 2);

		wxSpinCtrl *ctrl = (wxSpinCtrl *)editor->GetControl();
		if (ctrl)
		{
			int val = ctrl->GetValue();
			if (val >= 0 && val <= 63)
			{
				FilterData fd = sys::AppData::Instance()->FindFilter(m_DefaultGridList->GetCellValue(i, 0));

				if (fd.val != -1)
				{
					fd.colIndex = val;
					sys::AppData::Instance()->UpdateFilter(fd.name, fd);
				}
			}
		}
		else
			wxLogDebug("not valid");

		editor->DecRef();

		m_DefaultGridList->EnableCellEditControl(false);
		m_DefaultGridList->HideCellEditControl();
	}

	// check all custom updates
	numRows = m_CustomGridList->GetNumberRows();
	for (i = 0; i < numRows; ++i)
	{
		m_CustomGridList->SetGridCursor(i, 2);
		m_CustomGridList->EnableCellEditControl(true);
		m_CustomGridList->ShowCellEditControl();

		wxGridCellNumberEditor* editor = (wxGridCellNumberEditor*)m_CustomGridList->GetCellEditor(i, 2);
		int val = 0;

		wxSpinCtrl *ctrl = (wxSpinCtrl *)editor->GetControl();
		if (ctrl)
		{
			val = ctrl->GetValue();
		}
		else
			wxLogDebug("not valid");

		editor->DecRef();

		long filterId = -1;
		if (m_CustomGridList->GetCellValue(i, 1).ToLong(&filterId))
		{
			if (m_CustomGridList->GetCellValue(i, 0).Length() > 0)
			{
				FilterData fd = sys::AppData::Instance()->FindFilter(filterId);
				if (fd.val != -1)
				{
					wxString origName = fd.name;
					fd.name = m_CustomGridList->GetCellValue(i, 0);
					fd.colIndex = val;
					sys::AppData::Instance()->UpdateFilter(origName, fd);
				}
			}
		}

		m_CustomGridList->EnableCellEditControl(false);
		m_CustomGridList->HideCellEditControl();
	}

	this->EndModal(RESPONSE_OK);
}
