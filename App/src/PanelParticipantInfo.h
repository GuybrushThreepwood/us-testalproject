#ifndef __PanelParticipantInfo__
#define __PanelParticipantInfo__

/**
@file
Subclass of ParticipantInfo, which is generated by wxFormBuilder.
*/

#include "Common.h"
#include "TECSApp.h"

//// end generated include

/** Implementing ParticipantInfo */
class PanelParticipantInfo : public ParticipantInfo
{
	private:
		void CreateGridFromPrograms();
		void DoRefreshPhases();
		void DoPhaseFilter(std::map<wxString, ParticipantFileInfo>& phases, int mode);

	protected:
		void PhaseFilterChange(wxCommandEvent& event);

		// Handlers for ParticipantInfo events.
		void IdSearchCancel(wxCommandEvent& event);
		void IdSearch(wxCommandEvent& event);
		void GridCellLeftClick( wxGridEvent& event );
		void GridCellLeftDoubleClick(wxGridEvent& event);
		void BtnViewClick( wxCommandEvent& event );
		void BtnDeleteClick( wxCommandEvent& event );
		void BtnBackClick( wxCommandEvent& event );

	public:
		/** Constructor */
		PanelParticipantInfo(wxWindow* parent);
	//// end generated class members
	
	private:
		int m_PhaseFilter;
		wxString m_StringFilter;

		std::map<int, PhaseData> m_PhaseMap;

		std::map<wxString, ParticipantFileInfo> m_FileMap;
		std::map<wxString, ParticipantFileInfo> m_FilteredFileMap;
};

#endif // __PanelParticipantInfo__
