/**
	TECS
	PanelPhasePrograms.cpp
    Purpose: Panel that shows all the currently saved programs found in the default (Docs) folder
*/

#include "PanelPhasePrograms.h"
#include "DlgCreateEditPrograms.h"
#include "DlgCustomEdit.h"
#include "System.h"

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
PanelPhasePrograms::PanelPhasePrograms( wxWindow* parent )
:
PhasePrograms( parent )
{
	// only allow row selection
	m_PhaseProgramList->SetSelectionMode(wxGrid::wxGridSelectRows);

	// labels
	m_PhaseProgramList->SetColLabelValue(0, _("Program Name"));
	m_PhaseProgramList->SetColLabelValue(1, _("Phase"));
	m_PhaseProgramList->SetColLabelValue(2, _("Comments"));
	m_PhaseProgramList->SetColLabelValue(3, _("Date / Time"));
	m_PhaseProgramList->SetColLabelValue(4, _("Valid"));

	// sort by filter
	m_PhaseProgramList->SetSortingColumn(1);

	// disable cell highlight
	m_PhaseProgramList->SetCellHighlightPenWidth(0);

	m_PhaseFilter = VERSION_ALL;

	CreateGridFromPrograms();

	DoRefreshPhases();

	m_PhaseProgramList->AutoSize();
}

/////////////////////////////////////////////////////
/// CreateGridFromPrograms
/// Desc: Clears and created the wxGrid list of programs
/////////////////////////////////////////////////////
void PanelPhasePrograms::CreateGridFromPrograms()
{
	m_FileMap.clear();

	// find the programs stored
	wxArrayString fileList;
	std::size_t totalFiles = wxDir::GetAllFiles(sys::AppData::Instance()->GetProgramsDirectory(), &fileList);
	if (totalFiles > 0)
	{
		for (auto fileIter : fileList)
		{
			ProgramFileInfo fileInfo;
			fileInfo.filename = fileIter;
			
			std::ifstream t(fileIter.ToStdString());
			std::stringstream buffer;
			buffer << t.rdbuf();

			std::string bufferStr = buffer.str();
			fileInfo.jsonData.loadFromString(bufferStr);
			//fileInfo.jsonData.loadFromFile(fileIter.ToStdString());

			if (!fileInfo.jsonData.isNull())
			{
				JsonBox::Value name = fileInfo.jsonData["Name"];
				JsonBox::Value comment = fileInfo.jsonData["Comments"];
				JsonBox::Value phaseId = fileInfo.jsonData["PhaseId"];
				JsonBox::Value dateTime = fileInfo.jsonData["Time"];
				JsonBox::Value bankGUID = fileInfo.jsonData["BankGUID"];

				if (name.isString() &&
					comment.isString() &&
					phaseId.isInteger() &&
					bankGUID.isString() )
				{
					fileInfo.name = name.getString();
					fileInfo.comment = comment.getString();
					fileInfo.phaseId = phaseId.getInteger();
					fileInfo.bankGUID = bankGUID.getString();

					wxString str = dateTime.getString();
					long long val = -1;
					if (str.ToLongLong(&val))
					{
						fileInfo.dateTime = wxDateTime((wxLongLong)val);
					}

					// do the insert
					m_FileMap.insert(std::make_pair(fileIter, fileInfo));
				}
			}
		}
	}

	// How many Cols/Rows
	int rows = m_PhaseProgramList->GetNumberRows();

	// Delete all Cols/Rows
	if (rows > 0)
		m_PhaseProgramList->DeleteRows(0, rows, true);

	// stored ready to filter
	m_FilteredFileMap.clear();

	m_FilteredFileMap = m_FileMap;

	DoPhaseFilter(m_FilteredFileMap, 0);

	m_PhaseProgramList->InsertRows(0, m_FilteredFileMap.size());

	// populate
	int rowId = 0;
	for (const auto iter : m_FilteredFileMap)
	{
		m_PhaseProgramList->SetCellValue(rowId, 0, iter.second.name);

		const PhaseData& pd = sys::AppData::Instance()->FindPhase((int)iter.second.phaseId);
		if ( pd.val == -1 )
			m_PhaseProgramList->SetCellValue(rowId, 1, wxString::Format("%d", (int)iter.second.phaseId) );
		else
			m_PhaseProgramList->SetCellValue(rowId, 1, pd.name);

		m_PhaseProgramList->SetCellValue(rowId, 2, iter.second.comment);

		wxString dateStr = iter.second.dateTime.FormatISODate();
		wxString timeStr = iter.second.dateTime.FormatISOTime();
		m_PhaseProgramList->SetCellValue(rowId, 3, wxString::Format("%s @ %s", dateStr, timeStr));

		auto otherIter = iter;
		if (CheckProgramValidity(otherIter.second))
		{
			m_PhaseProgramList->SetCellValue(rowId, 4, _("VALID"));
		}
		else
		{
			m_PhaseProgramList->SetCellValue(rowId, 4, _("INVALID"));
		}

		rowId++;
	}

	m_PhaseProgramList->AutoSize();
	this->Layout();
}

/////////////////////////////////////////////////////
/// DoRefreshPhases
/// Desc: Populates the phase combobox with all the current phases
/////////////////////////////////////////////////////
void PanelPhasePrograms::DoRefreshPhases()
{
	// populate the data
	m_ComboPhase->Clear();

	// populate from the current map
	m_PhaseMap = sys::AppData::Instance()->GetPhases();
	for (auto val : m_PhaseMap)
	{
		int index = m_ComboPhase->GetCount();
		m_ComboPhase->Insert(val.second.name, index);
	}
	// add ALL as first element
	m_ComboPhase->Insert(_("Show All"), 0);
	m_ComboPhase->SetSelection(0);
}

/////////////////////////////////////////////////////
/// DoPhaseFilter
/// Desc: Filters the shown phases 
/////////////////////////////////////////////////////
void PanelPhasePrograms::DoPhaseFilter(std::map<wxString, ProgramFileInfo>& phases, int mode)
{
	if (mode == 0) // filter by type
	{
		if (m_PhaseFilter != FILTER_ALL)
		{
			for (auto iter = phases.begin(); iter != phases.end();)
			{
				if (iter->second.phaseId != m_PhaseFilter)
				{
					iter = phases.erase(iter);
					continue;
				}
				
				// next
				iter++;
			}
		}
	}
}

/////////////////////////////////////////////////////
/// CheckProgramValidity
/// Desc: Checks the parameter program file to check if it's still valid or invalid
/////////////////////////////////////////////////////
bool PanelPhasePrograms::CheckProgramValidity(ProgramFileInfo& pf)
{
	if (pf.jsonData["FilterCounts"].isArray())
	{
		if (!pf.jsonData["WordCountMin"].isNull() &&
			!pf.jsonData["WordCountMax"].isNull())
		{
			int wordMin = pf.jsonData["WordCountMin"].getInteger();
			int wordMax = pf.jsonData["WordCountMax"].getInteger();

			JsonBox::Array arr = pf.jsonData["FilterCounts"].getArray();
			auto iter = arr.begin();
			while (iter != arr.end())
			{
				if (iter->isObject())
				{
					JsonBox::Object filterItem = iter->getObject();
					if (!filterItem["FilterId"].isNull() &&
						!filterItem["Count"].isNull())
					{
						int filterId = filterItem["FilterId"].getInteger();
						int count = filterItem["Count"].getInteger();

						ProgramCreateEditLogInfo logData = sys::AppData::Instance()->CheckBankFilterCounts(pf.bankGUID, pf.phaseId, filterId, count, wordMin, wordMax);

						if (!logData.isValid)
							return false;
					}
				}
				// next
				++iter;
			}
		}
	}

	return true;
}

/////////////////////////////////////////////////////
/// PhaseFilterChange
/// Desc: Event when the phase combo is changed to update the program grid
/////////////////////////////////////////////////////
void PanelPhasePrograms::PhaseFilterChange(wxCommandEvent& WXUNUSED(event))
{
	int phaseId = 0;

	int selectionIndex = m_ComboPhase->GetSelection();
	wxString str = m_ComboPhase->GetStringSelection();

	if (selectionIndex == 0 ||
		str.length() == 0)
		phaseId = 0;
	else
	{
		for (auto iter : m_PhaseMap)
		{
			if (iter.second.name.IsSameAs(str))
			{
				phaseId = iter.second.val;
				break;
			}
		}
	}

	if (phaseId != m_PhaseFilter)
	{
		m_PhaseFilter = phaseId;
		CreateGridFromPrograms();
	}
}

/////////////////////////////////////////////////////
/// GridCellLeftDoubleClick
/// Desc: Event for left clicking to select a row in the program grid
/////////////////////////////////////////////////////
void PanelPhasePrograms::GridCellLeftDoubleClick(wxGridEvent& WXUNUSED(event))
{
	wxCommandEvent empty;
	BtnEditClick(empty);
}

/////////////////////////////////////////////////////
/// BtnEditPhasesClick
/// Desc: Button event for the edit button to modify phases (not used)
/////////////////////////////////////////////////////
void PanelPhasePrograms::BtnEditPhasesClick(wxCommandEvent& WXUNUSED(event))
{
	DlgCustomEdit* dlg = new DlgCustomEdit(0, CUSTOMMODE_PHASE);
	if (dlg->ShowModal() == RESPONSE_OK)
	{
		CreateGridFromPrograms();

		sys::AppData::Instance()->SaveSettings(sys::AppData::Instance()->defaultSettingsFile);

		DoRefreshPhases();
	}

	dlg->Destroy();
	delete dlg;
}

/////////////////////////////////////////////////////
/// BtnCreateClick
/// Desc: Button event to launch the program creation dialog
/////////////////////////////////////////////////////
void PanelPhasePrograms::BtnCreateClick(wxCommandEvent& WXUNUSED(event))
{
	DlgCreateEditPrograms* dlg = new DlgCreateEditPrograms(0);

	dlg->SetMode(MODE_ADD);
	if (dlg->ShowModal() == RESPONSE_OK)
	{
		CreateGridFromPrograms();

		DoRefreshPhases();

		m_PhaseProgramList->AutoSize();
		this->Layout();
	}

	dlg->Destroy();
	delete dlg;
}

/////////////////////////////////////////////////////
/// BtnEditClick
/// Desc: Button event to launch the program creation/edit dialog
/////////////////////////////////////////////////////
void PanelPhasePrograms::BtnEditClick(wxCommandEvent& WXUNUSED(event))
{
	// get selected
	wxArrayInt arr = m_PhaseProgramList->GetSelectedRows();

	int numSelected = arr.Count();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId >= 0 && rowId <
			m_PhaseProgramList->GetNumberRows())
		{
			wxString cellName = m_PhaseProgramList->GetCellValue(rowId, 0);
			wxString cellDate = m_PhaseProgramList->GetCellValue(rowId, 3);

			for (const auto iter : m_FilteredFileMap)
			{
				wxString dateStr = iter.second.dateTime.FormatISODate();
				wxString timeStr = iter.second.dateTime.FormatISOTime();
				wxString dateTimeStr = wxString::Format("%s @ %s", dateStr, timeStr);

				if (iter.second.name == cellName.ToStdString() &&
					dateTimeStr.IsSameAs(cellDate))
				{ 
					DlgCreateEditPrograms* dlg = new DlgCreateEditPrograms(0);

					dlg->SetMode(MODE_EDIT);
					dlg->SetEditFile(iter.second.filename);
					if (dlg->ShowModal() == RESPONSE_OK)
					{
						CreateGridFromPrograms();

						DoRefreshPhases();

						m_PhaseProgramList->AutoSize();
						this->Layout();
					}

					dlg->Destroy();
					delete dlg;

					break;
				}
			}
		}
	}

}

/////////////////////////////////////////////////////
/// BtnCopyClick
/// Desc: Button event to make a copy of the selected program in the grid list
/////////////////////////////////////////////////////
void PanelPhasePrograms::BtnCopyClick(wxCommandEvent& WXUNUSED(event))
{
	// get selected
	wxArrayInt arr = m_PhaseProgramList->GetSelectedRows();

	int numSelected = arr.Count();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId >= 0 &&
			rowId < m_PhaseProgramList->GetNumberRows())
		{
			wxString name = m_PhaseProgramList->GetCellValue(rowId, 0);
			for (auto iter : m_FilteredFileMap)
			{
				if (name.IsSameAs(iter.second.name))
				{
					// copy this
					wxDateTime creationDate = wxDateTime::Now();

					wxLongLong value = creationDate.GetValue();
					wxString dateStr = creationDate.FormatISODate();
					wxString timeStr = creationDate.FormatISOTime();
					std::string convertedTime = timeStr.ToStdString();
					std::replace(convertedTime.begin(), convertedTime.end(), ':', '-');

					std::string fullPath = sys::AppData::Instance()->GetProgramsDirectory().ToStdString() + std::string("/") + std::string("Program-") + std::string(dateStr.ToStdString() + "@" + convertedTime) + std::string(".json");

					std::string newName = std::string("Copy of ") + name.ToStdString();
					iter.second.jsonData["Name"] = JsonBox::Value(newName);
					iter.second.jsonData["Time"] = JsonBox::Value(value.ToString().ToStdString());

					iter.second.jsonData.writeToFile(fullPath);

					CreateGridFromPrograms();

					DoRefreshPhases();

					m_PhaseProgramList->AutoSize();
					this->Layout();

					break;
				}
			}
		}
	}
}

/////////////////////////////////////////////////////
/// BtnDeleteClick
/// Desc: Button event to delete the selected program in the grid list 
/////////////////////////////////////////////////////
void PanelPhasePrograms::BtnDeleteClick(wxCommandEvent& WXUNUSED(event))
{
	// get selected
	wxArrayInt arr = m_PhaseProgramList->GetSelectedRows();

	int numSelected = arr.Count();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId >= 0 &&
			rowId < m_PhaseProgramList->GetNumberRows())
		{
			wxString name = m_PhaseProgramList->GetCellValue(rowId, 0);

			if (wxMessageBox(_("Are you sure you want to delete this program?"), _("DELETE PROGRAM"), wxYES_NO) == wxYES)
			{
				for (const auto iter : m_FilteredFileMap)
				{
					if (name.IsSameAs(iter.second.name))
					{
						// delete this
						if (wxRemoveFile(iter.second.filename))
						{
							CreateGridFromPrograms();

							DoRefreshPhases();

							m_PhaseProgramList->AutoSize();
							this->Layout();

							break;
						}
					}
				}
			}
		}
	}
}

/////////////////////////////////////////////////////
/// BtnBackClick
/// Desc: Button event to return to the admin panel
/////////////////////////////////////////////////////
void PanelPhasePrograms::BtnBackClick(wxCommandEvent& WXUNUSED(event))
{
	sys::AppData::Instance()->ChangePanel(SCREEN_ADMIN);
}
