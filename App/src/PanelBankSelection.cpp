
#include "PanelBankSelection.h"
#include "DlgNewEditBank.h"
#include "System.h"

PanelBankSelection::PanelBankSelection( wxWindow* parent )
:
BankSelection( parent )
{
	// only allow row selection
	m_BankFileList->SetSelectionMode(wxGrid::wxGridSelectRows);

	// labels
	m_BankFileList->SetColLabelValue(0, _("Bank Name"));
	m_BankFileList->SetColLabelValue(1, _("Comments"));
	m_BankFileList->SetColLabelValue(2, _("Date / Time"));
	m_BankFileList->SetColLabelValue(3, _("GUID"));

	// sort by filter
	m_BankFileList->SetSortingColumn(1);

	// disable cell highlight
	m_BankFileList->SetCellHighlightPenWidth(0);


	// only allow row selection
	m_BankPreview->SetSelectionMode(wxGrid::wxGridSelectRows);

	// labels
	m_BankPreview->SetColLabelValue(0, _("Word/Sentence"));
	m_BankPreview->SetColLabelValue(1, _("Filter"));
	m_BankPreview->SetColLabelValue(2, _("Number of words"));

	// sort by filter
	m_BankPreview->SetSortingColumn(1);

	// disable cell highlight
	m_BankPreview->SetCellHighlightPenWidth(0);

	m_StringFilter = "";

	RefreshFileList();
	RefreshBank();

	m_BtnSelectBank->Disable();
}

/////////////////////////////////////////////////////
/// RefreshFileList
/// Desc: Clear and refresh the bank file list
/////////////////////////////////////////////////////
void PanelBankSelection::RefreshFileList()
{
	m_FileMap.clear();

	m_BankFileList->ClearSelection();
	m_SelectedBankFile.jsonData = JsonBox::Value();
	m_SelectedBank = JsonBox::Value();

	// find all available files in the banks directory
	wxArrayString fileList;
	std::size_t totalFiles = wxDir::GetAllFiles(sys::AppData::Instance()->GetBanksDirectory(), &fileList);
	if (totalFiles > 0)
	{
		for (auto fileIter : fileList)
		{
			BankFileInfo fileInfo;
			fileInfo.filename = fileIter;

			std::ifstream t(fileInfo.filename.ToStdString());
			std::stringstream buffer;
			buffer << t.rdbuf();

			std::string bufferStr = buffer.str();
			fileInfo.jsonData.loadFromString(bufferStr);
			//fileInfo.jsonData.loadFromString(fileIter.ToStdString());

			if (!fileInfo.jsonData.isNull())
			{
				JsonBox::Value bankGUID = fileInfo.jsonData["GUID"];
				JsonBox::Value name = fileInfo.jsonData["Name"];
				JsonBox::Value comments = fileInfo.jsonData["Comments"];
				JsonBox::Value date = fileInfo.jsonData["Date"];

				if (name.isString() &&
					comments.isString() &&
					bankGUID.isString())
				{
					fileInfo.name = name.getString();
					fileInfo.comment = comments.getString();
					fileInfo.guid = bankGUID.getString();

					wxString str = date.getString();
					long long val = -1;
					if (str.ToLongLong(&val))
					{
						fileInfo.dateTime = wxDateTime((wxLongLong)val);
					}

					// do the insert
					m_FileMap.insert(std::make_pair(fileInfo.guid, fileInfo));
				}
			}
		}
	}

	// create grid list
	// How many Cols/Rows
	int rows = m_BankFileList->GetNumberRows();

	// Delete all Cols/Rows
	if (rows > 0)
		m_BankFileList->DeleteRows(0, rows, true);

	if (m_FileMap.size() > 0)
		m_BankFileList->InsertRows(0, m_FileMap.size());

	// populate
	int rowId = 0;
	for (const auto iter : m_FileMap)
	{
		m_BankFileList->SetCellValue(rowId, 0, iter.second.name);
		m_BankFileList->SetCellValue(rowId, 1, iter.second.comment);

		wxString dateStr = iter.second.dateTime.FormatISODate();
		wxString timeStr = iter.second.dateTime.FormatISOTime();
		m_BankFileList->SetCellValue(rowId, 2, wxString::Format("%s @ %s", dateStr, timeStr));

		m_BankFileList->SetCellValue(rowId, 3, iter.second.guid);

		rowId++;
	}

	m_BankFileList->AutoSize();

	RefreshBank();

	this->Layout();
}

/////////////////////////////////////////////////////
/// DoBankFilter
/// Desc: Recursive method to filter word/sentences from the current bank 
/////////////////////////////////////////////////////
void PanelBankSelection::DoBankFilter(std::map<wxString, BankData>& bank, int mode)
{
	if (mode == 0) // filter by search
	{
		if (m_StringFilter.Length() > 0)
		{
			for (auto iter = bank.begin(); iter != bank.end();)
			{
				if (!iter->second.str.Contains(m_StringFilter))
				{
					iter = bank.erase(iter);
					continue;
				}

				// next
				iter++;
			}
		}
	}
}

/////////////////////////////////////////////////////
/// BankSearchCancel
/// Desc: Cancel event for the search to clear the input box
/////////////////////////////////////////////////////
void PanelBankSelection::BankSearchCancel(wxCommandEvent& WXUNUSED(event))
{
	if (m_Search->GetValue().Length() > 0)
	{
		m_Search->Clear();
		m_StringFilter.Clear();

		RefreshBank();
	}
}

/////////////////////////////////////////////////////
/// BankSearchCancel
/// Desc: Button event to filter out containing words in the bank
/////////////////////////////////////////////////////
void PanelBankSelection::BankSearch(wxCommandEvent& WXUNUSED(event))
{
	wxString str = m_Search->GetValue();

	if (m_StringFilter != str)
	{
		m_StringFilter = str;
		RefreshBank();
	}
}

/////////////////////////////////////////////////////
/// RefreshBank
/// Desc: Clear and refresh the bank grid
/////////////////////////////////////////////////////
void PanelBankSelection::RefreshBank()
{
	m_Bank.clear();

	// How many Cols/Rows
	int rows = m_BankPreview->GetNumberRows();

	// Delete all Cols/Rows
	if (rows > 0)
		m_BankPreview->DeleteRows(0, rows, true);

	if (!m_SelectedBank.isNull())
	{
		JsonBox::Value rootObj = m_SelectedBank;

		if (rootObj.isArray() &&
			!rootObj.isNull())
		{
			JsonBox::Array arr = rootObj.getArray();
			auto iter = arr.begin();
			while (iter != arr.end())
			{
				if (iter->isObject())
				{
					JsonBox::Object bankItem = iter->getObject();
					if (!bankItem["StringData"].isNull() &&
						!bankItem["FilterType"].isNull())
					{
						wxString str = bankItem["StringData"].getString();
						int index = bankItem["FilterType"].getInteger();

						bool hasGUID = false;
						std::string guidVal = " ";

						if (!bankItem["HasGUID"].isNull())
							hasGUID = bankItem["HasGUID"].getBoolean();

						if (hasGUID &&
							!bankItem["GUID"].isNull())
						{
							wxString guidStr = bankItem["GUID"].getString();
							guidVal = guidStr.ToStdString();
						}
						else
							hasGUID = false;

						// loaded
						BankData data;
						data.str = str;
						data.filterIndex = index;
						data.wordCount = sys::WordCount(str.ToStdWstring());
						data.hasGUID = hasGUID;
						data.guid = guidVal;

						// add to the bank
						m_Bank.insert(std::make_pair(str, data));
					}
				}
				// next
				++iter;
			}
		}

		DoBankFilter(m_Bank, 0);

		if (m_Bank.size() > 0)
			m_BankPreview->InsertRows(0, m_Bank.size());

		// populate
		int rowId = 0;
		for (const auto iter : m_Bank)
		{
			m_BankPreview->SetCellValue(rowId, 0, iter.second.str);

			const FilterData& fd = sys::AppData::Instance()->FindFilter(iter.second.filterIndex);

			m_BankPreview->SetCellValue(rowId, 1, fd.name);
			m_BankPreview->SetCellValue(rowId, 2, wxString::Format("%d", (int)iter.second.wordCount));

			if (iter.second.hasGUID &&
				(iter.second.guid.size() > 0) &&
				(iter.second.guid != " "))
			{
				const int COLOUR_BG = 235;

				m_BankPreview->SetCellBackgroundColour(rowId, 0, wxColor(COLOUR_BG, COLOUR_BG, COLOUR_BG));
				m_BankPreview->SetCellBackgroundColour(rowId, 1, wxColor(COLOUR_BG, COLOUR_BG, COLOUR_BG));
				m_BankPreview->SetCellBackgroundColour(rowId, 2, wxColor(COLOUR_BG, COLOUR_BG, COLOUR_BG));
			}

			rowId++;
		}
	}

	m_BankPreview->AutoSize();
	this->Layout();
}

/////////////////////////////////////////////////////
/// FileGridCellLeftClick
/// Desc: Event for left click to select a row in the bank file list
/////////////////////////////////////////////////////
void PanelBankSelection::FileGridCellLeftClick(wxGridEvent& event)
{
	m_BankFileList->ClearSelection();
	m_BankFileList->SelectRow(event.GetRow());

	// get selected
	wxArrayInt arr = m_BankFileList->GetSelectedRows();

	int numSelected = arr.Count();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId >= 0 && rowId <
			m_BankFileList->GetNumberRows())
		{
			wxString cellName = m_BankFileList->GetCellValue(rowId, 0);
			wxString cellGUID = m_BankFileList->GetCellValue(rowId, 3);

			for (auto iter : m_FileMap)
			{
				wxString GUIDStr = iter.second.guid;

				if (iter.second.name == cellName.ToStdString() &&
					GUIDStr.IsSameAs(cellGUID))
				{
					m_SelectedBankFile = iter.second;
					m_SelectedBank = JsonBox::Value(iter.second.jsonData["Bank"]);

					m_BtnSelectBank->Enable();
					break;
				}
			}
		}
	}

	RefreshBank();
}

/////////////////////////////////////////////////////
/// FileGridCellLeftDoubleClick
/// Desc Event for double left click to edit a bank
/////////////////////////////////////////////////////
void PanelBankSelection::FileGridCellLeftDoubleClick(wxGridEvent& WXUNUSED(event))
{
	wxCommandEvent empty;
	BtnEditClick(empty);
}

/////////////////////////////////////////////////////
/// BankGridCellLeftClick
/// Desc: Event for left click to select a row in the word bank
/////////////////////////////////////////////////////
void PanelBankSelection::BankGridCellLeftClick(wxGridEvent& event)
{
	m_BankPreview->ClearSelection();
	m_BankPreview->SelectRow(event.GetRow());
}

/////////////////////////////////////////////////////
/// BtnCreateClick
/// Desc: Event for launching the create dialog for a new bank
/////////////////////////////////////////////////////
void PanelBankSelection::BtnCreateClick( wxCommandEvent& WXUNUSED(event) )
{
	DlgNewEditBank* dlg = new DlgNewEditBank(0);
	dlg->SetMode(MODE_ADD);
	dlg->Apply();
	if (dlg->ShowModal() == RESPONSE_OK)
	{
		RefreshFileList();
	}

	dlg->Destroy();
	delete dlg;
}

/////////////////////////////////////////////////////
/// BtnEditClick
/// Desc: Event for launching the edit dialog for an existing bank
/////////////////////////////////////////////////////
void PanelBankSelection::BtnEditClick( wxCommandEvent& WXUNUSED(event) )
{
	// get selected
	wxArrayInt arr = m_BankFileList->GetSelectedRows();

	int numSelected = arr.Count();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId >= 0 && rowId <
			m_BankFileList->GetNumberRows())
		{
			wxString cellName = m_BankFileList->GetCellValue(rowId, 0);
			wxString cellGUID = m_BankFileList->GetCellValue(rowId, 3);

			for (const auto iter : m_FileMap)
			{
				wxString GUIDStr = iter.second.guid;

				if (iter.second.name == cellName.ToStdString() &&
					GUIDStr.IsSameAs(cellGUID))
				{
					DlgNewEditBank* dlg = new DlgNewEditBank(0);
					dlg->SetMode(MODE_EDIT);
					dlg->SetEditFile(iter.second.filename);
					dlg->SetEditGUID(iter.second.guid);
					dlg->SetEditBank(m_SelectedBank);
					dlg->Apply();
					if (dlg->ShowModal() == RESPONSE_OK)
					{
						RefreshFileList();
					}

					dlg->Destroy();
					delete dlg;

					break;
				}
			}
		}
	}
}

/////////////////////////////////////////////////////
/// BtnCopyClick
/// Desc: Event for copy a selected bank
/////////////////////////////////////////////////////
void PanelBankSelection::BtnCopyClick( wxCommandEvent& WXUNUSED(event) )
{
	// get selected
	wxArrayInt arr = m_BankFileList->GetSelectedRows();

	int numSelected = arr.Count();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId >= 0 &&
			rowId < m_BankFileList->GetNumberRows())
		{
			wxString name = m_BankFileList->GetCellValue(rowId, 0);
			wxString guidStr = m_BankFileList->GetCellValue(rowId, 3);
			for (auto iter : m_FileMap)
			{
				if (guidStr.IsSameAs(iter.second.guid))
				{
					// copy this
					wxDateTime creationDate = wxDateTime::Now();

					wxLongLong value = creationDate.GetValue();
					wxString dateStr = creationDate.FormatISODate();
					wxString timeStr = creationDate.FormatISOTime();
					std::string convertedTime = timeStr.ToStdString();
					std::replace(convertedTime.begin(), convertedTime.end(), ':', '-');

					std::string fullPath = sys::AppData::Instance()->GetBanksDirectory().ToStdString() + std::string("/") + std::string("Bank-") + std::string(dateStr.ToStdString() + "@" + convertedTime) + std::string(".json");

					std::string newName = std::string("Copy of ") + name.ToStdString();
					iter.second.jsonData["Name"] = JsonBox::Value(newName);
					iter.second.jsonData["Date"] = JsonBox::Value(value.ToString().ToStdString());
					iter.second.jsonData["GUID"] = JsonBox::Value(sys::AppData::Instance()->GetGUIDAsString());
					iter.second.jsonData.writeToFile(fullPath);

					RefreshFileList();

					break;
				}
			}
		}
	}
}

/////////////////////////////////////////////////////
/// BtnDeleteClick
/// Desc: Event for deleting a selected bank
/////////////////////////////////////////////////////
void PanelBankSelection::BtnDeleteClick( wxCommandEvent& WXUNUSED(event) )
{
	// get selected
	wxArrayInt arr = m_BankFileList->GetSelectedRows();

	int numSelected = arr.Count();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId >= 0 &&
			rowId < m_BankFileList->GetNumberRows())
		{
			wxString name = m_BankFileList->GetCellValue(rowId, 0);
			wxString cellGUID = m_BankFileList->GetCellValue(rowId, 3);

			if (wxMessageBox(_("Are you sure you want to delete this bank?"), _("DELETE BANK"), wxYES_NO) == wxYES)
			{
				for (const auto iter : m_FileMap)
				{
					if (name.IsSameAs(iter.second.name) &&
						cellGUID.IsSameAs(iter.second.guid) )
					{
						// delete this
						if (wxRemoveFile(iter.second.filename))
						{
							RefreshFileList();

							break;
						}
					}
				}
			}
		}
	}
}

/////////////////////////////////////////////////////
/// BtnBackClick
/// Desc: Event for returning to admin panel
/////////////////////////////////////////////////////
void PanelBankSelection::BtnBackClick( wxCommandEvent& WXUNUSED(event) )
{
	sys::AppData::Instance()->ChangePanel(SCREEN_ADMIN);
}

/////////////////////////////////////////////////////
/// BtnSelectClick
/// Desc: Event for viewing the full bank panel
/////////////////////////////////////////////////////
void PanelBankSelection::BtnSelectClick( wxCommandEvent& WXUNUSED(event) )
{
	sys::AppData::Instance()->SetSelectedBank(m_SelectedBankFile);
	sys::AppData::Instance()->ChangePanel(SCREEN_BANK);
}
