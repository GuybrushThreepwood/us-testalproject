/**
	TECS
	PanelAdmin.cpp
    Purpose: Options panel for administrator to choose between managing the bank, managing programs or viewing previously run programs
*/

#include "PanelAdmin.h"
#include "System.h"

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
PanelAdmin::PanelAdmin( wxWindow* parent )
:
Admin( parent )
{

}

/////////////////////////////////////////////////////
/// BtnBankManageClick
/// Desc: Button event for switching to bank management
/////////////////////////////////////////////////////
void PanelAdmin::BtnBankManageClick(wxCommandEvent& WXUNUSED(event))
{
	sys::AppData::Instance()->ChangePanel(SCREEN_BANKSELECTION);
}

/////////////////////////////////////////////////////
/// BtnPhaseProgramClick
/// Desc: Button event for showing the manage programs panels
/////////////////////////////////////////////////////
void PanelAdmin::BtnPhaseProgramClick(wxCommandEvent& WXUNUSED(event))
{
	sys::AppData::Instance()->ChangePanel(SCREEN_PHASEPROGRAMS);
}

/////////////////////////////////////////////////////
/// BtnParticipantInfoButtonClick
/// Desc: Button event for switching to the participant event view
/////////////////////////////////////////////////////
void PanelAdmin::BtnParticipantInfoButtonClick(wxCommandEvent& WXUNUSED(event))
{
	sys::AppData::Instance()->ChangePanel(SCREEN_PARTICIPANTINFO);
}

/////////////////////////////////////////////////////
/// BtnBackClick
/// Desc: Button event to return to the entry panel
/////////////////////////////////////////////////////
void PanelAdmin::BtnBackClick(wxCommandEvent& WXUNUSED(event))
{
	sys::AppData::Instance()->ChangePanel(SCREEN_ENTRY);
}
