///////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////

#ifndef __COMMON_H__
#define __COMMON_H__

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

// for all others, include the necessary headers (this file is usually all you
// need because it includes almost all "standard" wxWidgets headers)
#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include <wx/dir.h>
#include <wx/richtext/richtextctrl.h>
#include <wx/xml/xml.h>
#include <wx/splash.h>
#include <wx/stdpaths.h>
#include <wx/timer.h>
#include <wx/stopwatch.h>
#include <wx/valnum.h>
#include <wx/filedlg.h>
#include <wx/wfstream.h>
//#include <wx/msw/ole/automtn.h>
#include <wx/clipbrd.h>
#include <wx/richtext/richtextformatdlg.h>
#include <wx/clrpicker.h>
#include <wx/utils.h> 
#include <wx/spinctrl.h> 

#include <map>
#include <array>
#include <chrono>
#include <cctype>
#include <random>
#include <sstream>
#include <iomanip>
#include <ctime>

#include "crossguid/guid.h"
#include <JsonBox.h>
#include "Excel/ExcelFormat.h"

#ifdef _WIN32

#else // _WIN32
	#define	FW_NORMAL	400
	#define	FW_BOLD		700
#endif // _WIN32

const int ORIGINAL_FONT_SIZE = 18;
const int ORIGINAL_BORDER_SPACE_W = 100;
const int ORIGINAL_BORDER_SPACE_H = 50;
const int ORIGINAL_BORDER_THICKNESS = 1;

// to make life clearer
enum ExcelColumnValues
{
	EX_COL_A = 0,
	EX_COL_B,
	EX_COL_C,
	EX_COL_D,
	EX_COL_E,
	EX_COL_F,
	EX_COL_G,
	EX_COL_H,
	EX_COL_I,
	EX_COL_J,
	EX_COL_K,
	EX_COL_L,
	EX_COL_M,
	EX_COL_N,
	EX_COL_O,
	EX_COL_P,
	EX_COL_Q,
	EX_COL_R,
	EX_COL_S,
	EX_COL_T,
	EX_COL_U,
	EX_COL_V,
	EX_COL_W,
	EX_COL_X,
	EX_COL_Y,
	EX_COL_Z
};

enum ScreenList
{
	SCREEN_EMPTY=-1,

	SCREEN_ENTRY=0,
	SCREEN_ADMIN,
	SCREEN_BANKSELECTION,
	SCREEN_BANK,
	SCREEN_PHASEPROGRAMS,

	SCREEN_PARTICIPANTINFO,
	SCREEN_PARTICIPANT,
	SCREEN_TUTORIAL,
	SCREEN_PHASERUN,
	SCREEN_QUESTIONS
};

enum DialogResponse
{
	RESPONSE_CANCEL=0,
	RESPONSE_OK
};

enum ModeType
{
	MODE_ADD=0,
	MODE_EDIT
};

enum CustomEditMode
{
	CUSTOMMODE_FILTER = 0,
	CUSTOMMODE_PHASE
};

enum WordFilter
{
	FILTER_ALL = 0,

	FILTER_NEUTRAL,
	//FILTER_NEUTRALWITHINT,
	FILTER_REF,
	//FILTER_REFWITHINT,

	CUSTOM_FILTER_OFFSET = 1000,
	CUSTOM_FILTER_MAX = 32000
};

enum VersionFilter
{
	VERSION_ALL = 0,

	VERSION_ONE,
	VERSION_TWO,
	VERSION_THREE,
	VERSION_FOUR,

	CUSTOM_VERSION_OFFSET = 1000,
	CUSTOM_VERSION_MAX = 32000
};

enum GenderFilter
{
	GENDER_UNSPECIFIED = 0,
	GENDER_MALE,
	GENDER_FEMALE
};

enum BreakType
{
	BREAKTYPE_PERFILTER=0,
	BREAKTYPE_BLOCKEND,
	BREAKTYPE_NOBREAK
};

struct BankFileInfo
{
	wxString filename;
	wxString name;
	wxString comment;
	wxDateTime dateTime;
	std::string guid;

	JsonBox::Value jsonData;
};

struct BankData
{
	wxString str;
	int filterIndex;
	int wordCount;

	bool hasGUID;
	std::string guid;
};

struct FilterData
{
	wxString name;
	int val;
	bool isDefault;
	int colIndex;
};

struct PhaseData
{
	bool isDefault;

	wxString name;
	int val;
	int timeToRespond;
	int userTimeToRespond;
	int timeResponseTimeout;
	int userTimeResponseTimeout;
	int timeInterval;
	int userTimeInterval;
	int timeBetweenFilterChanges;
	int userTimeBetweenFilterChanges;

	int wordCountMin;
	int wordCountMax;
	int linesCountMin;
	int linesCountMax;

//	int numNeutral;
//	int userNumNeutral;

};

struct ProgramFileInfo
{
	wxString filename;
	int phaseId;
	wxDateTime dateTime;
	std::string bankGUID;
	JsonBox::Value jsonData;
	std::string name;
	std::string comment;
};

struct ParticipantFileInfo
{
	wxString filename;
	wxString participantId;
	int versionId;
	wxString programName;
	wxDateTime dateTime;
	JsonBox::Value jsonData;
};

struct ParticipantResponse
{
	BankData data;
	bool didRespond;
	
	bool hasFirstRespose;
	int firstResponseTime;
	int firstResponseValue;

	bool hasSecondResponse;
	int secondResponseTime;
	int secondResponseValue;

	//int responseTime;
	int lineOrWordCount;
	
	//bool badAnswer;
	//int badResponseVal;
	//int badResponseTime;
};

struct ProgramBlock
{
	int blockId;
	BreakType breakType;
	std::vector<BankData> words;
};

struct ParticipantData
{
	wxString strId;
	//wxString participantId;
	wxDateTime dateOfBirth;
	int genderId;

	int selectedVersionId;
	wxString programName;

	PhaseData phaseData;

	std::vector<ProgramBlock> program;
	std::vector<ParticipantResponse> response;
};

struct ProgramCreateEditLogInfo
{
	bool isValid;
	wxString reasonNotValid;
};

struct StoredScaleData
{
	int currentHeight;
	int currentWidth;

	float scaleW;
	float scaleH;

	int currentFontSize;
	int currentSmallFontSize;
};

struct StoredInputData
{
	wxTextCtrl* input;
	FilterData fd;
	wxString validator;
	int val;
};

struct StoredCorrectData
{
	wxBoxSizer* filterboxSizer;
	wxStaticText* filterName;
	wxTextCtrl* shownVal;

	wxStaticText* rememberedCorrectly;
	wxSpinCtrl* spinValueControl;
	wxStaticText* percentage;
	FilterData fd;
};

struct OrderValues
{
	int filterId;
	wxString filterName;
	int count;
};

struct OrderBlock
{
	int index;

	bool random;
	BreakType breakType;
	std::vector<OrderValues> values;
};

struct ExcelColourIndex
{
	int idx;
	wxColor col;
};

#endif //__COMMON_H__
