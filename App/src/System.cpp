/**
	TECS
	System.cpp
    Purpose: Singleton class that is accessible anywhere to request the bank, programs and participant information 
*/

#include "System.h"

using namespace sys;

namespace
{
	const char* TECSDIR = "TECS";
	const char* PARTICIPANTSDIR = "Participants";
	const char* PROGRAMSDIR = "Programs";
	const char* BANKSDIR = "Banks";

	const char* BANKFILE = "tecs_bank.json";
	const char* SETTINGSFILE = "settings.json";

	// defaults
	const int VERSION_ONE_TIMETORESPOND = 1500;
	const int VERSION_ONE_TIMERESPONSETIMEOUT = 2000;
	const int VERSION_ONE_TIMEINTERVAL = 500;
	const int VERSION_ONE_TIMEBETWEENFILTERCHANGES = 4000;

	const int VERSION_TWO_TIMETORESPOND = 2000;
	const int VERSION_TWO_TIMERESPONSETIMEOUT = 2500;
	const int VERSION_TWO_TIMEINTERVAL = 500;
	const int VERSION_TWO_TIMEBETWEENFILTERCHANGES = 4000;

	const int VERSION_THREE_TIMETORESPOND = 2000;
	const int VERSION_THREE_TIMERESPONSETIMEOUT = 2500;
	const int VERSION_THREE_TIMEINTERVAL = 500;
	const int VERSION_THREE_TIMEBETWEENFILTERCHANGES = 4000;

	const int VERSION_FOUR_TIMETORESPOND = 2000;
	const int VERSION_FOUR_TIMERESPONSETIMEOUT = 2500;
	const int VERSION_FOUR_TIMEINTERVAL = 500;
	const int VERSION_FOUR_TIMEBETWEENFILTERCHANGES = 4000;
}

sys::AppData* sys::AppData::ms_Instance = 0;

/////////////////////////////////////////////////////
/// Method: Initialise
/// Params: None
///
/////////////////////////////////////////////////////
void AppData::Initialise(void)
{
	wxASSERT((ms_Instance == 0));


	ms_Instance = new AppData();

}

/////////////////////////////////////////////////////
/// Method: Shutdown
/// Params: None
///
/////////////////////////////////////////////////////
void AppData::Shutdown(void)
{
	if (ms_Instance)
	{
		delete ms_Instance;
		ms_Instance = 0;
	}
}

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
AppData::AppData()
{
	defaultBankFile = BANKFILE;
	defaultSettingsFile = SETTINGSFILE;

	m_Filters.clear();
	m_Phases.clear();
	m_ExcelColours.clear();

	// build the colour list for excel
	int colourCount = 64;
	wxString colourList[] = {
		"rgb(0,0,0)",
		"rgb(255, 255, 255)",
		"rgb(255, 0, 0)",
		"rgb(0, 255, 0)",
		"rgb(0, 0, 255)",
		"rgb(255, 255, 0)",
		"rgb(255, 0, 255)",
		"rgb(0, 255, 255)",
		"rgb(0,0,0)",
		"rgb(255, 255, 255)",
		"rgb(255, 0, 0)",
		"rgb(0, 255, 0)",
		"rgb(0, 0, 255)",
		"rgb(255, 255, 0)",
		"rgb(255, 0, 255)",
		"rgb(0, 255, 255)",
		"rgb(128, 0, 0)",
		"rgb(0, 128, 0)",
		"rgb(0, 0, 128)",
		"rgb(128, 128, 0)",
		"rgb(128, 0, 128)",
		"rgb(0, 128, 128)",
		"rgb(192, 192, 192)",
		"rgb(128, 128, 128)",
		"rgb(153, 153, 255)",
		"rgb(153, 51, 102)",
		"rgb(255, 255, 204)",
		"rgb(204, 255, 255)",
		"rgb(102, 0, 102)",
		"rgb(255, 128, 128)",
		"rgb(0, 102, 204)",
		"rgb(204, 204, 255)",
		"rgb(0, 0, 128)",
		"rgb(255, 0, 255)",
		"rgb(255, 255, 0)",
		"rgb(0, 255, 255)",
		"rgb(128, 0, 128)",
		"rgb(128, 0, 0)",
		"rgb(0, 128, 128)",
		"rgb(0, 0, 255)",
		"rgb(0, 204, 255)",
		"rgb(204, 255, 255)",
		"rgb(204, 255, 204)",
		"rgb(255, 255, 153)",
		"rgb(153, 204, 255)",
		"rgb(255, 153, 204)",
		"rgb(204, 153, 255)",
		"rgb(255, 204, 153)",
		"rgb(51, 102, 255)",
		"rgb(51, 204, 204)",
		"rgb(153, 204, 0)",
		"rgb(255, 204, 0)",
		"rgb(255, 153, 0)",
		"rgb(255, 102, 0)",
		"rgb(102, 102, 153)",
		"rgb(150, 150, 150)",
		"rgb(0, 51, 102)",
		"rgb(51, 153, 102)",
		"rgb(0, 51, 0)",
		"rgb(51, 51, 0)",
		"rgb(153, 51, 0)",
		"rgb(153, 51, 102)",
		"rgb(51, 51, 153)",
		"rgb(51, 51, 51)",
	};

	for (int i = 0; i < colourCount; ++i)
	{
		ExcelColourIndex col;
		col.idx = i;
		col.col = wxColor(colourList[i]);
		m_ExcelColours.push_back(col);
	}

	// default filter list
	FilterData fd;
	fd.name = _("Neutral");
	fd.val = FILTER_NEUTRAL;
	fd.isDefault = true;
	fd.colIndex = 3; // green
	m_Filters.insert(std::make_pair(FILTER_NEUTRAL, fd));
	//fd.name = _("Neutral with interference");
	//fd.val = FILTER_NEUTRALWITHINT;
	//m_Filters.insert(std::make_pair(FILTER_NEUTRALWITHINT, fd));
	fd.name = _("Referential");
	fd.val = FILTER_REF;
	fd.colIndex = 0; // black
	m_Filters.insert(std::make_pair(FILTER_REF, fd));
	//fd.name = _("Referential with interference");
	//fd.val = FILTER_REFWITHINT;
	//m_Filters.insert(std::make_pair(FILTER_REFWITHINT, fd));

	m_NextFilterId = CUSTOM_FILTER_OFFSET;

	// default phase list
	PhaseData pd;
	pd.isDefault = true;

	pd.name = _("Version 1");
	pd.val = VERSION_ONE;
	pd.timeToRespond = pd.userTimeToRespond = VERSION_ONE_TIMETORESPOND;
	pd.timeResponseTimeout = pd.userTimeResponseTimeout = VERSION_ONE_TIMERESPONSETIMEOUT;
	pd.timeInterval = pd.userTimeInterval = VERSION_ONE_TIMEINTERVAL;
	pd.timeBetweenFilterChanges = pd.userTimeBetweenFilterChanges = VERSION_ONE_TIMEBETWEENFILTERCHANGES;

	pd.wordCountMin = 1;
	pd.wordCountMax = 1;
	pd.linesCountMin = 4;
	pd.linesCountMax = 6;

	m_Phases.insert(std::make_pair(VERSION_ONE, pd));

	pd.name = _("Version 2");
	pd.val = VERSION_TWO;
	pd.timeToRespond = pd.userTimeToRespond = VERSION_TWO_TIMETORESPOND;
	pd.timeResponseTimeout = pd.userTimeResponseTimeout = VERSION_TWO_TIMERESPONSETIMEOUT;
	pd.timeInterval = pd.userTimeInterval = VERSION_TWO_TIMEINTERVAL;
	pd.timeBetweenFilterChanges = pd.userTimeBetweenFilterChanges = VERSION_TWO_TIMEBETWEENFILTERCHANGES;

	pd.wordCountMin = 3;
	pd.wordCountMax = 3;
	pd.linesCountMin = 4;
	pd.linesCountMax = 6;

	m_Phases.insert(std::make_pair(VERSION_TWO, pd));

	pd.name = _("Version 3");
	pd.val = VERSION_THREE;
	pd.timeToRespond = pd.userTimeToRespond = VERSION_THREE_TIMETORESPOND;
	pd.timeResponseTimeout = pd.userTimeResponseTimeout = VERSION_THREE_TIMERESPONSETIMEOUT;
	pd.timeInterval = pd.userTimeInterval = VERSION_THREE_TIMEINTERVAL;
	pd.timeBetweenFilterChanges = pd.userTimeBetweenFilterChanges = VERSION_THREE_TIMEBETWEENFILTERCHANGES;

	pd.wordCountMin = 4;
	pd.wordCountMax = 6;
	pd.linesCountMin = 1;
	pd.linesCountMax = 1;

	m_Phases.insert(std::make_pair(VERSION_THREE, pd));

	pd.name = _("Version 4");
	pd.val = VERSION_FOUR;
	pd.timeToRespond = pd.userTimeToRespond = VERSION_FOUR_TIMETORESPOND;
	pd.timeResponseTimeout = pd.userTimeResponseTimeout = VERSION_FOUR_TIMERESPONSETIMEOUT;
	pd.timeInterval = pd.userTimeInterval = VERSION_FOUR_TIMEINTERVAL;
	pd.timeBetweenFilterChanges = pd.userTimeBetweenFilterChanges = VERSION_FOUR_TIMEBETWEENFILTERCHANGES;

	pd.wordCountMin = 4;
	pd.wordCountMax = 6;
	pd.linesCountMin = 1;
	pd.linesCountMax = 1;

	m_Phases.insert(std::make_pair(VERSION_FOUR, pd));


	m_NextPhaseId = CUSTOM_VERSION_OFFSET;

	// create directories

	// default save dir
	m_AppSaveDirectory = wxString(wxStandardPaths::Get().GetDocumentsDir() + wxString("/") + wxString(TECSDIR));
	if (!wxDirExists(m_AppSaveDirectory))
	{
		bool result = wxMkdir(m_AppSaveDirectory);
		if (!result)
		{
			wxMessageBox(_("Could not create app save directory"));
		}
	}

	// banks dir
	m_AppBanksDirectory = m_AppSaveDirectory + wxString("/") + wxString(BANKSDIR);
	if (!wxDirExists(m_AppBanksDirectory))
	{
		bool result = wxMkdir(m_AppBanksDirectory);
		if (!result)
		{
			wxMessageBox(_("Could not create app banks directory"));
		}
	}

	// participants dir
	m_AppParticipantsDirectory = m_AppSaveDirectory + wxString("/") + wxString(PARTICIPANTSDIR);
	if (!wxDirExists(m_AppParticipantsDirectory))
	{
		bool result = wxMkdir(m_AppParticipantsDirectory);
		if (!result)
		{
			wxMessageBox(_("Could not create app participants directory"));
		}
	}

	// programs dir
	m_AppProgramsDirectory = m_AppSaveDirectory + wxString("/") + wxString(PROGRAMSDIR);
	if (!wxDirExists(m_AppProgramsDirectory))
	{
		bool result = wxMkdir(m_AppProgramsDirectory);
		if (!result)
		{
			wxMessageBox(_("Could not create app programs directory"));
		}
	}

	m_UseAdminPassword = false;

	m_BGColour.Set( 0, 0, 0);
	m_TextColour.Set( 255, 255, 255);
	m_TextBorderColour.Set(255, 255, 255);
	m_TextInfoColour.Set(255, 255, 255);

	m_BorderThickness = ORIGINAL_BORDER_THICKNESS;
	m_BorderWidth = ORIGINAL_BORDER_SPACE_W;
	m_BorderHeight = ORIGINAL_BORDER_SPACE_H;

	m_FontSize = ORIGINAL_FONT_SIZE;

	m_TutTxtVersion1 = _("This task is to count the number of words that appear on the screen and type the corresponding number on the keyboard.\n\n\n\nWork fast because words will disappear in a short time.\n\n\n\nAt the end of the task you will be asked to write as many words as you can remember.");
	m_TutTxtVersion2 = _("This task is to count the number of sentences that appear on the screen and type the corresponding number on the keyboard.\n\n\n\nWork fast because the sentences will disappear in a short time.\n\n\n\nAt the end of the task you will be asked to write as many complete sentences as you can remember.");
	m_TutTxtVersion3And4 = _("This task is to count the number of words in the sentences that appear on the screen and type the corresponding number of words on the keyboard.\n\n\n\nWork fast because the sentences will disappear in a short time.\n\n\n\nAt the end of the task you will be asked to write as many complete sentences as you can remember.");

	m_Ex1 = _("An example will follow");
	m_Ex2 = _("Another example will follow");
	m_Ex3 = _("One last example");

	m_Tut1Ex1 = _("computer");
	m_Tut1Ex2 = _("bookshop");
	m_Tut1Ex3 = _("meeting");

	m_Tut2Ex1 = _("they see me");
	m_Tut2Ex2 = _("they ignore me");
	m_Tut2Ex3 = _("they mock me");

	m_Tut3Ex1 = _("they look at me");
	m_Tut3Ex2 = _("they always look at me");
	m_Tut3Ex3 = _("they always look at me strangely");

	m_Tut4Ex1 = _("they speak about you");
	m_Tut4Ex2 = _("they laugh behind your back");
	m_Tut4Ex3 = _("the public have begun to arrive");

	m_InputText1 = _("The number of words that have appeared on the screen is");
	m_InputText2 = _("The number of sentences that have appeared on the screen is");

	m_EndingText1 = _("If you have any doubts, request help before continuing.\n\n\n\n\nRemember that at the end of the task you will be asked to write as many words as you can remember.\n\n\n\n\nIf you understand, click to continue.");
	m_EndingText2 = _("If you have any doubts, request help before continuing.\n\n\n\n\nRemember that at the end of the task you will be asked to write as many sentences as you can remember.\n\n\n\n\nIf you understand, click to continue.");

	m_WaitText = _("Please Wait");

	m_ParticipantData.strId = "";
	m_ParticipantData.genderId = -1;
	m_ParticipantData.selectedVersionId = -1;
	m_ParticipantData.programName = "";
	m_ParticipantData.program.clear();
	m_ParticipantData.response.clear();
}

/////////////////////////////////////////////////////
/// Destructor
/////////////////////////////////////////////////////
AppData::~AppData()
{

}

/////////////////////////////////////////////////////
/// SetMainFrame
/// Desc: Sets the initial frame of the app for access anywhere in code easily
/////////////////////////////////////////////////////
void AppData::SetMainFrame(ProjectMainFrame* pMainFrame)
{
	m_MainFrame = pMainFrame;
}

/////////////////////////////////////////////////////
/// GetMainFrame
/// Desc: Gets the app frame
/////////////////////////////////////////////////////
ProjectMainFrame* AppData::GetMainFrame()
{
	return m_MainFrame;
}

/////////////////////////////////////////////////////
/// LoadSettings
/// Desc: Loads a settings file
/////////////////////////////////////////////////////
void AppData::LoadSettings(const std::string& filename)
{
	if (filename.length() == 0)
		return;

	std::string fullPath = m_AppSaveDirectory.ToStdString() + std::string("/") + filename;

	m_Bank.clear();

	if (!wxFileExists(fullPath))
	{
		// no settings file
		JsonBox::Object settings;

		JsonBox::Array filterArr;
		JsonBox::Array phaseArr;

		for (auto iter : m_Filters)
		{
			JsonBox::Object filterItem;
			filterItem["FilterName"] = JsonBox::Value(iter.second.name.ToStdString());
			filterItem["FilterVal"] = JsonBox::Value(iter.second.val);
			filterItem["FilterDef"] = JsonBox::Value(iter.second.isDefault);
			filterItem["FilterColIdx"] = JsonBox::Value(iter.second.colIndex);

			// add default filters
			filterArr.push_back(filterItem);
		}

		for (auto iter : m_Phases)
		{
			JsonBox::Object phaseItem;
			phaseItem["PhaseName"] = JsonBox::Value(iter.second.name.ToStdString());
			phaseItem["PhaseVal"] = JsonBox::Value(iter.second.val);
			phaseItem["PhaseDef"] = JsonBox::Value(iter.second.isDefault);

			phaseItem["TimeToRespond"] = JsonBox::Value(iter.second.timeToRespond);
			phaseItem["UserTimeToRespond"] = JsonBox::Value(iter.second.timeToRespond);
			phaseItem["TimeResponseTimeout"] = JsonBox::Value(iter.second.timeResponseTimeout);
			phaseItem["UserTimeResponseTimeout"] = JsonBox::Value(iter.second.timeResponseTimeout);
			phaseItem["TimeInterval"] = JsonBox::Value(iter.second.timeInterval);
			phaseItem["UserTimeInterval"] = JsonBox::Value(iter.second.timeInterval);
			phaseItem["TimeBetweenFilterChanges"] = JsonBox::Value(iter.second.timeBetweenFilterChanges);
			phaseItem["UserTimeBetweenFilterChanges"] = JsonBox::Value(iter.second.timeBetweenFilterChanges);

			phaseItem["LinesMin"] = JsonBox::Value(iter.second.linesCountMin);
			phaseItem["LinesMax"] = JsonBox::Value(iter.second.linesCountMax);
			phaseItem["WordCountMin"] = JsonBox::Value(iter.second.wordCountMin);
			phaseItem["WordCountMax"] = JsonBox::Value(iter.second.wordCountMax);

			// add default phases
			phaseArr.push_back(phaseItem);
		}

		settings["NextFilterId"] = JsonBox::Value(m_NextFilterId);
		settings["NextPhaseId"] = JsonBox::Value(m_NextPhaseId);

		settings["UsePassword"] = JsonBox::Value(m_UseAdminPassword);

		wxString col = "#000000";
		settings["BGCol"] = JsonBox::Value(col.ToStdString());
		col = "#ffffff";
		settings["TextCol"] = JsonBox::Value(col.ToStdString());
		settings["TextBorderCol"] = JsonBox::Value(col.ToStdString());
		settings["TextInfoCol"] = JsonBox::Value(col.ToStdString());
		settings["TextFontSize"] = JsonBox::Value(m_FontSize);

		settings["TextBorderThickness"] = JsonBox::Value(m_BorderThickness);
		settings["TextBorderWidth"] = JsonBox::Value(m_BorderWidth);
		settings["TextBorderHeight"] = JsonBox::Value(m_BorderHeight);

		//
		settings["Tut1Str"] = JsonBox::Value(m_TutTxtVersion1.ToStdString());
		settings["Tut2Str"] = JsonBox::Value(m_TutTxtVersion2.ToStdString());
		settings["Tut3And4Str"] = JsonBox::Value(m_TutTxtVersion3And4.ToStdString());

		settings["Ex1"] = JsonBox::Value(m_Ex1.ToStdString());
		settings["Ex2"] = JsonBox::Value(m_Ex2.ToStdString());
		settings["Ex3"] = JsonBox::Value(m_Ex3.ToStdString());

		settings["Tut1Ex1"] = JsonBox::Value(m_Tut1Ex1.ToStdString());
		settings["Tut1Ex2"] = JsonBox::Value(m_Tut1Ex2.ToStdString());
		settings["Tut1Ex3"] = JsonBox::Value(m_Tut1Ex3.ToStdString());

		settings["Tut2Ex1"] = JsonBox::Value(m_Tut2Ex1.ToStdString());
		settings["Tut2Ex2"] = JsonBox::Value(m_Tut2Ex2.ToStdString());
		settings["Tut2Ex3"] = JsonBox::Value(m_Tut2Ex3.ToStdString());

		settings["Tut3Ex1"] = JsonBox::Value(m_Tut3Ex1.ToStdString());
		settings["Tut3Ex2"] = JsonBox::Value(m_Tut3Ex2.ToStdString());
		settings["Tut3Ex3"] = JsonBox::Value(m_Tut3Ex3.ToStdString());

		settings["Tut4Ex1"] = JsonBox::Value(m_Tut4Ex1.ToStdString());
		settings["Tut4Ex2"] = JsonBox::Value(m_Tut4Ex2.ToStdString());
		settings["Tut4Ex3"] = JsonBox::Value(m_Tut4Ex3.ToStdString());

		settings["InputText1"] = JsonBox::Value(m_InputText1.ToStdString());
		settings["InputText2"] = JsonBox::Value(m_InputText2.ToStdString());

		settings["EndingText1"] = JsonBox::Value(m_EndingText1.ToStdString());
		settings["EndingText2"] = JsonBox::Value(m_EndingText2.ToStdString());

		settings["WaitText"] = JsonBox::Value(m_WaitText.ToStdString());

		// save all to an object
		settings["Filters"] = JsonBox::Value(filterArr);
		settings["Phases"] = JsonBox::Value(phaseArr);
		JsonBox::Value v(settings);

		v.writeToFile(fullPath, true, true);
	}
	else
	{
		JsonBox::Value loadedSettings;

		std::ifstream t(fullPath);
		std::stringstream buffer;
		buffer << t.rdbuf();

		std::string bufferStr = buffer.str();
		loadedSettings.loadFromString(bufferStr);
		//loadedBank.loadFromFile(fullPath);

		if (!loadedSettings.isNull())
		{
			// filters list
			JsonBox::Value val = loadedSettings["Filters"];
			if (val.isArray() &&
				!val.isNull())
			{
				JsonBox::Array arr = val.getArray();
				auto iter = arr.begin();
				while (iter != arr.end())
				{
					if (iter->isObject())
					{
						JsonBox::Object filterItem = iter->getObject();
						if (!filterItem["FilterName"].isNull() &&
							!filterItem["FilterVal"].isNull() &&
							!filterItem["FilterDef"].isNull())
						{
							wxString str = filterItem["FilterName"].getString();
							int index = filterItem["FilterVal"].getInteger();
							bool isDefault = filterItem["FilterDef"].getBoolean();

							int colIdx = 0;
							if (!filterItem["FilterColIdx"].isNull())
								colIdx = filterItem["FilterColIdx"].getInteger();

							// loaded
							FilterData data;
							data.name = str;
							data.val = index;
							data.isDefault = isDefault;
							data.colIndex = colIdx;

							// add to the bank
							m_Filters[index] = data;
							//m_Filters.insert(std::make_pair(index, data));
						}
					}
					// next
					++iter;
				}
			}

			// phase list
			val = loadedSettings["Phases"];
			if (val.isArray() &&
				!val.isNull())
			{
				JsonBox::Array arr = val.getArray();
				auto iter = arr.begin();
				while (iter != arr.end())
				{
					if (iter->isObject())
					{
						JsonBox::Object phaseItem = iter->getObject();
						if (!phaseItem["PhaseName"].isNull() &&
							!phaseItem["PhaseVal"].isNull() &&
							!phaseItem["PhaseDef"].isNull())
						{
							wxString str = phaseItem["PhaseName"].getString();
							int index = phaseItem["PhaseVal"].getInteger();
							bool isDefault = phaseItem["PhaseDef"].getBoolean();

							int timeToRespond, userTimeToRespond;
							int timeResponseTimeout, userTimeResponseTimeout;
							int timeInterval, userTimeInterval;
							int timeBetweenFilterChanges, userTimeBetweenFilterChanges;

							timeToRespond = userTimeToRespond = phaseItem["TimeToRespond"].getInteger();
							timeResponseTimeout = userTimeResponseTimeout = phaseItem["TimeResponseTimeout"].getInteger();
							timeInterval = userTimeInterval = phaseItem["TimeInterval"].getInteger();
							timeBetweenFilterChanges = userTimeBetweenFilterChanges = phaseItem["TimeBetweenFilterChanges"].getInteger();

							// if user updates exist
							if (!phaseItem["UserTimeToRespond"].isNull())
								userTimeToRespond = phaseItem["UserTimeToRespond"].getInteger();

							if (!phaseItem["UserTimeResponseTimeout"].isNull())
								userTimeResponseTimeout = phaseItem["UserTimeResponseTimeout"].getInteger();

							if (!phaseItem["UserTimeInterval"].isNull())
								userTimeInterval = phaseItem["UserTimeInterval"].getInteger();

							if (!phaseItem["UserTimeBetweenFilterChanges"].isNull())
								userTimeBetweenFilterChanges = phaseItem["UserTimeBetweenFilterChanges"].getInteger();

							int linesCountMin = phaseItem["LinesMin"].getInteger();
							int linesCountMax = phaseItem["LinesMax"].getInteger();
							int wordCountMin = phaseItem["WordCountMin"].getInteger();
							int wordCountMax = phaseItem["WordCountMax"].getInteger();

							// loaded
							PhaseData data;
							data.name = str;
							data.val = index;
							data.isDefault = isDefault;

							data.timeToRespond = timeToRespond;
							data.timeResponseTimeout = timeResponseTimeout;
							data.timeInterval = timeInterval;
							data.timeBetweenFilterChanges = timeBetweenFilterChanges;

							data.userTimeToRespond = userTimeToRespond;
							data.userTimeResponseTimeout = userTimeResponseTimeout;
							data.userTimeInterval = userTimeInterval;
							data.userTimeBetweenFilterChanges = userTimeBetweenFilterChanges;

							data.linesCountMin = linesCountMin;
							data.linesCountMax = linesCountMax;
							data.wordCountMin = wordCountMin;
							data.wordCountMax = wordCountMax;

							// add to the bank
							m_Phases[index] = data;
							//m_Phases.insert(std::make_pair(index, data));
						}
					}
					// next
					++iter;
				}
			}

			// update filter ids
			val = loadedSettings["NextFilterId"];
			if (val.isInteger() &&
				!val.isNull())
			{
				m_NextFilterId = val.getInteger();
			}

			val = loadedSettings["NextPhaseId"];
			if (val.isInteger() &&
				!val.isNull())
			{
				m_NextPhaseId = val.getInteger();
			}

			val = loadedSettings["UsePassword"];
			if (val.isBoolean() &&
				!val.isNull())
			{
				m_UseAdminPassword = val.getBoolean();
			}

			// colours
			val = loadedSettings["BGCol"];
			if (val.isString() &&
				!val.isNull())
			{
				m_BGColour = wxColour(val.getString());
			}

			val = loadedSettings["TextCol"];
			if (val.isString() &&
				!val.isNull())
			{
				m_TextColour = wxColour(val.getString());
			}

			val = loadedSettings["TextBorderCol"];
			if (val.isString() &&
				!val.isNull())
			{
				m_TextBorderColour = wxColour(val.getString());
			}

			val = loadedSettings["TextInfoCol"];
			if (val.isString() &&
				!val.isNull())
			{
				m_TextInfoColour = wxColour(val.getString());
			}

			val = loadedSettings["TextFontSize"];
			if (val.isInteger() &&
				!val.isNull())
			{
				m_FontSize = val.getInteger();
			}

			val = loadedSettings["TextBorderThickness"];
			if (val.isInteger() &&
				!val.isNull())
			{
				m_BorderThickness = val.getInteger();
			}

			val = loadedSettings["TextBorderWidth"];
			if (val.isInteger() &&
				!val.isNull())
			{
				m_BorderWidth = val.getInteger();
			}

			val = loadedSettings["TextBorderHeight"];
			if (val.isInteger() &&
				!val.isNull())
			{
				m_BorderHeight = val.getInteger();
			}

			// text data
			val = loadedSettings["Tut1Str"];
			if (val.isString() &&
				!val.isNull())
			{
				m_TutTxtVersion1 = val.getString();
			}

			val = loadedSettings["Tut2Str"];
			if (val.isString() &&
				!val.isNull())
			{
				m_TutTxtVersion2 = val.getString();
			}

			val = loadedSettings["Tut3And4Str"];
			if (val.isString() &&
				!val.isNull())
			{
				m_TutTxtVersion3And4 = val.getString();
			}

			val = loadedSettings["Ex1"];
			if (val.isString() &&
				!val.isNull())
			{
				m_Ex1 = val.getString();
			}

			val = loadedSettings["Ex2"];
			if (val.isString() &&
				!val.isNull())
			{
				m_Ex2 = val.getString();
			}

			val = loadedSettings["Ex3"];
			if (val.isString() &&
				!val.isNull())
			{
				m_Ex3 = val.getString();
			}

			// tut 1
			val = loadedSettings["Tut1Ex1"];
			if (val.isString() &&
				!val.isNull())
			{
				m_Tut1Ex1 = val.getString();
			}

			val = loadedSettings["Tut1Ex2"];
			if (val.isString() &&
				!val.isNull())
			{
				m_Tut1Ex2 = val.getString();
			}

			val = loadedSettings["Tut1Ex3"];
			if (val.isString() &&
				!val.isNull())
			{
				m_Tut1Ex3 = val.getString();
			}

			// tut 2
			val = loadedSettings["Tut2Ex1"];
			if (val.isString() &&
				!val.isNull())
			{
				m_Tut2Ex1 = val.getString();
			}

			val = loadedSettings["Tut2Ex2"];
			if (val.isString() &&
				!val.isNull())
			{
				m_Tut2Ex2 = val.getString();
			}

			val = loadedSettings["Tut2Ex3"];
			if (val.isString() &&
				!val.isNull())
			{
				m_Tut2Ex3 = val.getString();
			}

			// tut 3
			val = loadedSettings["Tut3Ex1"];
			if (val.isString() &&
				!val.isNull())
			{
				m_Tut3Ex1 = val.getString();
			}

			val = loadedSettings["Tut3Ex2"];
			if (val.isString() &&
				!val.isNull())
			{
				m_Tut3Ex2 = val.getString();
			}

			val = loadedSettings["Tut3Ex3"];
			if (val.isString() &&
				!val.isNull())
			{
				m_Tut3Ex3 = val.getString();
			}

			// tut 4
			val = loadedSettings["Tut4Ex1"];
			if (val.isString() &&
				!val.isNull())
			{
				m_Tut4Ex1 = val.getString();
			}

			val = loadedSettings["Tut4Ex2"];
			if (val.isString() &&
				!val.isNull())
			{
				m_Tut4Ex2 = val.getString();
			}

			val = loadedSettings["Tut4Ex3"];
			if (val.isString() &&
				!val.isNull())
			{
				m_Tut4Ex3 = val.getString();
			}

			val = loadedSettings["InputText1"];
			if (val.isString() &&
				!val.isNull())
			{
				m_InputText1 = val.getString();
			}

			val = loadedSettings["InputText2"];
			if (val.isString() &&
				!val.isNull())
			{
				m_InputText2 = val.getString();
			}


			val = loadedSettings["EndingText1"];
			if (val.isString() &&
				!val.isNull())
			{
				m_EndingText1 = val.getString();
			}

			val = loadedSettings["EndingText2"];
			if (val.isString() &&
				!val.isNull())
			{
				m_EndingText2 = val.getString();
			}

			val = loadedSettings["WaitText"];
			if (val.isString() &&
				!val.isNull())
			{
				m_WaitText = val.getString();
			}

		}
	}
}

/////////////////////////////////////////////////////
/// SaveSettings
/// Desc: Saves a settings file
/////////////////////////////////////////////////////
void AppData::SaveSettings(const std::string& filename)
{
	if (filename.length() == 0)
		return;

	std::string fullPath = m_AppSaveDirectory.ToStdString() + std::string("/") + filename;

	JsonBox::Array filterArr;
	JsonBox::Array phaseArr;

	JsonBox::Object settings;

	for (auto iter : m_Filters)
	{
		JsonBox::Object filterItem;
		filterItem["FilterName"] = JsonBox::Value(iter.second.name.ToStdString());
		filterItem["FilterVal"] = JsonBox::Value(iter.second.val);
		filterItem["FilterDef"] = JsonBox::Value(iter.second.isDefault);
		filterItem["FilterColIdx"] = JsonBox::Value(iter.second.colIndex);

		// add default filters
		filterArr.push_back(filterItem);
	}

	for (auto iter : m_Phases)
	{
		JsonBox::Object phaseItem;
		phaseItem["PhaseName"] = JsonBox::Value(iter.second.name.ToStdString());
		phaseItem["PhaseVal"] = JsonBox::Value(iter.second.val);
		phaseItem["PhaseDef"] = JsonBox::Value(iter.second.isDefault);

		phaseItem["TimeToRespond"] = JsonBox::Value(iter.second.timeToRespond);
		phaseItem["TimeResponseTimeout"] = JsonBox::Value(iter.second.timeResponseTimeout);
		phaseItem["TimeInterval"] = JsonBox::Value(iter.second.timeInterval);
		phaseItem["TimeBetweenFilterChanges"] = JsonBox::Value(iter.second.timeBetweenFilterChanges);

		phaseItem["UserTimeToRespond"] = JsonBox::Value(iter.second.userTimeToRespond);
		phaseItem["UserTimeResponseTimeout"] = JsonBox::Value(iter.second.userTimeResponseTimeout);
		phaseItem["UserTimeInterval"] = JsonBox::Value(iter.second.userTimeInterval);
		phaseItem["UserTimeBetweenFilterChanges"] = JsonBox::Value(iter.second.userTimeBetweenFilterChanges);

		phaseItem["LinesMin"] = JsonBox::Value(iter.second.linesCountMin);
		phaseItem["LinesMax"] = JsonBox::Value(iter.second.linesCountMax);
		phaseItem["WordCountMin"] = JsonBox::Value(iter.second.wordCountMin);
		phaseItem["WordCountMax"] = JsonBox::Value(iter.second.wordCountMax);

		// add default phases
		phaseArr.push_back(phaseItem);
	}

	settings["NextFilterId"] = JsonBox::Value(m_NextFilterId);
	settings["NextPhaseId"] = JsonBox::Value(m_NextPhaseId);

	settings["UsePassword"] = JsonBox::Value(m_UseAdminPassword);

	settings["BGCol"] = JsonBox::Value(m_BGColour.GetAsString(wxC2S_HTML_SYNTAX).ToStdString());
	settings["TextCol"] = JsonBox::Value(m_TextColour.GetAsString(wxC2S_HTML_SYNTAX).ToStdString());
	settings["TextBorderCol"] = JsonBox::Value(m_TextBorderColour.GetAsString(wxC2S_HTML_SYNTAX).ToStdString());
	settings["TextInfoCol"] = JsonBox::Value(m_TextInfoColour.GetAsString(wxC2S_HTML_SYNTAX).ToStdString());
	settings["TextFontSize"] = JsonBox::Value(m_FontSize);

	settings["TextBorderThickness"] = JsonBox::Value(m_BorderThickness);
	settings["TextBorderWidth"] = JsonBox::Value(m_BorderWidth);
	settings["TextBorderHeight"] = JsonBox::Value(m_BorderHeight);

	settings["Tut1Str"] = JsonBox::Value(m_TutTxtVersion1.ToStdString());
	settings["Tut2Str"] = JsonBox::Value(m_TutTxtVersion2.ToStdString());
	settings["Tut3And4Str"] = JsonBox::Value(m_TutTxtVersion3And4.ToStdString());

	settings["Ex1"] = JsonBox::Value(m_Ex1.ToStdString());
	settings["Ex2"] = JsonBox::Value(m_Ex2.ToStdString());
	settings["Ex3"] = JsonBox::Value(m_Ex3.ToStdString());

	settings["Tut1Ex1"] = JsonBox::Value(m_Tut1Ex1.ToStdString());
	settings["Tut1Ex2"] = JsonBox::Value(m_Tut1Ex2.ToStdString());
	settings["Tut1Ex3"] = JsonBox::Value(m_Tut1Ex3.ToStdString());

	settings["Tut2Ex1"] = JsonBox::Value(m_Tut2Ex1.ToStdString());
	settings["Tut2Ex2"] = JsonBox::Value(m_Tut2Ex2.ToStdString());
	settings["Tut2Ex3"] = JsonBox::Value(m_Tut2Ex3.ToStdString());

	settings["Tut3Ex1"] = JsonBox::Value(m_Tut3Ex1.ToStdString());
	settings["Tut3Ex2"] = JsonBox::Value(m_Tut3Ex2.ToStdString());
	settings["Tut3Ex3"] = JsonBox::Value(m_Tut3Ex3.ToStdString());

	settings["Tut4Ex1"] = JsonBox::Value(m_Tut4Ex1.ToStdString());
	settings["Tut4Ex2"] = JsonBox::Value(m_Tut4Ex2.ToStdString());
	settings["Tut4Ex3"] = JsonBox::Value(m_Tut4Ex3.ToStdString());

	settings["InputText1"] = JsonBox::Value(m_InputText1.ToStdString());
	settings["InputText2"] = JsonBox::Value(m_InputText2.ToStdString());

	settings["EndingText1"] = JsonBox::Value(m_EndingText1.ToStdString());
	settings["EndingText2"] = JsonBox::Value(m_EndingText2.ToStdString());

	settings["WaitText"] = JsonBox::Value(m_WaitText.ToStdString());

	// save all to an object
	settings["Filters"] = JsonBox::Value(filterArr);
	settings["Phases"] = JsonBox::Value(phaseArr);

	JsonBox::Value v(settings);

	v.writeToFile(fullPath, true, true);
}

/////////////////////////////////////////////////////
/// LoadBank
/// Desc: Loads a bank file
/////////////////////////////////////////////////////
void AppData::LoadBank(const std::string& filename, bool expandPath)
{
	if (filename.length() == 0)
		return;

	std::string fullPath;
	if (expandPath)
		fullPath = m_AppSaveDirectory.ToStdString() + std::string("/") + filename;
	else
		fullPath = filename;

	m_Bank.clear();

	if (!wxFileExists(fullPath))
	{
		// no bank file
		JsonBox::Object bank;
		
		JsonBox::Array bankArr;

		// create a default file
		JsonBox::Object bankItem;
		wxString strVal = _("This is a default test string for the word/sentence bank");
		bankItem["StringData"] = JsonBox::Value(strVal.ToStdString());
		bankItem["FilterType"] = JsonBox::Value(FILTER_NEUTRAL);
		bankItem["HasGUID"] = JsonBox::Value(false);
		//bankItem["GUID"] = JsonBox::Value("-1");

		BankData data;
		data.str = strVal;
		data.filterIndex = FILTER_NEUTRAL;
		data.wordCount = sys::WordCount(strVal.ToStdWstring());
		data.hasGUID = false;
		data.guid = "";
		m_Bank.insert(std::make_pair(strVal, data));

		// add default
		bankArr.push_back(bankItem);

		// save all to an object
		bank["Bank"] = JsonBox::Value(bankArr);
		JsonBox::Value v(bank);

		v.writeToFile(fullPath, true, true);
	}
	else
	{
		JsonBox::Value loadedBank;
		
		std::ifstream t(fullPath);
		std::stringstream buffer;
		buffer << t.rdbuf();

		std::string bufferStr = buffer.str();
		loadedBank.loadFromString(bufferStr);
		//loadedBank.loadFromFile(fullPath);

		if (!loadedBank.isNull())
		{
			// filters list
			JsonBox::Value val;

			// word bank
			val = loadedBank["Bank"];
			if (val.isArray() &&
				!val.isNull())
			{
				JsonBox::Array arr = val.getArray();
				auto iter = arr.begin();
				while (iter != arr.end())
				{
					if (iter->isObject())
					{
						JsonBox::Object bankItem = iter->getObject();
						if (!bankItem["StringData"].isNull() &&
							!bankItem["FilterType"].isNull())
						{
							wxString str = bankItem["StringData"].getString();
							int index = bankItem["FilterType"].getInteger();

							bool hasGUID = false;
							std::string guidVal = " ";

							if (!bankItem["HasGUID"].isNull())
								hasGUID = bankItem["HasGUID"].getBoolean();

							if (hasGUID &&
								!bankItem["GUID"].isNull())
							{
								wxString guidStr = bankItem["GUID"].getString();
								guidVal = guidStr.ToStdString();
							}
							else
								hasGUID = false;

							// loaded
							BankData data;
							data.str = str;
							data.filterIndex = index;
							data.wordCount = sys::WordCount(str.ToStdWstring());
							data.hasGUID = hasGUID;
							data.guid = guidVal;

							// add to the bank
							m_Bank.insert(std::make_pair(str,data));
						}
					}
					// next
					++iter;
				}
			}
		}
	}
}

/////////////////////////////////////////////////////
/// SaveBank
/// Desc: Saves the selected bank file
/////////////////////////////////////////////////////
void AppData::SaveBank()
{
	if (!m_SelectedBank.jsonData.isNull())
	{
		JsonBox::Array bankArr;
		JsonBox::Object bank;

		for (auto item : m_Bank)
		{
			JsonBox::Object bankItem;

			std::string outData = item.second.str.ToStdString();

			bankItem["StringData"] = JsonBox::Value(outData.c_str());
			bankItem["FilterType"] = JsonBox::Value(item.second.filterIndex);
			bankItem["HasGUID"] = JsonBox::Value(item.second.hasGUID);

			if (item.second.hasGUID)
			{
				if (item.second.guid.length() == 0)
					bankItem["HasGUID"] = JsonBox::Value(false);
				else
					bankItem["GUID"] = JsonBox::Value(item.second.guid);
				//bankItem["GUID"] = JsonBox::Value(" ");
			}

			bankArr.push_back(bankItem);
		}

		// save all to an object
		m_SelectedBank.jsonData["Bank"] = JsonBox::Value(bankArr);

		// save out to a value to write
		JsonBox::Value v(m_SelectedBank.jsonData);

		v.writeToFile(m_SelectedBank.filename.ToStdString());
	}
}

/////////////////////////////////////////////////////
/// SaveBank
/// Desc: Saves a bank file
/////////////////////////////////////////////////////
void AppData::SaveBank(const std::string& filename)
{
	if (filename.length() == 0)
		return;

    std::string fullPath = m_AppSaveDirectory.ToStdString() + std::string("/") + filename;

	JsonBox::Array bankArr;
	JsonBox::Object bank;

	for (auto item : m_Bank)
	{
		JsonBox::Object bankItem;

		std::string outData = item.second.str.ToStdString();
		
		bankItem["StringData"] = JsonBox::Value(outData.c_str());
		bankItem["FilterType"] = JsonBox::Value(item.second.filterIndex);
		bankItem["HasGUID"] = JsonBox::Value(item.second.hasGUID);

		if (item.second.hasGUID)
		{
			if(item.second.guid.length() == 0)
				bankItem["HasGUID"] = JsonBox::Value(false);
			else
				bankItem["GUID"] = JsonBox::Value(item.second.guid);
			//bankItem["GUID"] = JsonBox::Value(" ");
		}	

		bankArr.push_back(bankItem);
	}

	// save all to an object
	bank["Bank"] = JsonBox::Value(bankArr);
	JsonBox::Value v(bank);

	v.writeToFile(fullPath, true, true);
}

/////////////////////////////////////////////////////
/// ChangePanel
/// Desc: Uses defined enums to switch the app panels easily
/////////////////////////////////////////////////////
bool AppData::ChangePanel(ScreenList newScreen)
{
	if (m_CurrentScreen != newScreen)
	{
		m_CurrentScreen = newScreen;
	}
	else
	{
		return false;
	}

	switch (newScreen)
	{
		case SCREEN_ENTRY:
		{
			if (m_CurrentPanel != 0)
			{
				m_MainFrame->bMainFrameSizer->Remove(0);
				m_CurrentPanel->Destroy();
			}

			PanelEntry *panel = new PanelEntry(m_MainFrame);
			m_MainFrame->bMainFrameSizer->Add(panel, 1, wxEXPAND);
			m_MainFrame->bMainFrameSizer->Layout();

			m_CurrentPanel = panel;

			m_CurrentPanel->Show();
		}break;
		case SCREEN_ADMIN:
		{
			if (m_CurrentPanel != 0)
			{
				m_MainFrame->bMainFrameSizer->Remove(0);
				m_CurrentPanel->Destroy();
			}

			PanelAdmin *panel = new PanelAdmin(m_MainFrame);
			m_MainFrame->bMainFrameSizer->Add(panel, 1, wxEXPAND);
			m_MainFrame->bMainFrameSizer->Layout();

			m_CurrentPanel = panel;

			m_CurrentPanel->Show();

		}break;

		case SCREEN_BANKSELECTION:
		{
			if (m_CurrentPanel != 0)
			{
				m_MainFrame->bMainFrameSizer->Remove(0);
				m_CurrentPanel->Destroy();
			}

			PanelBankSelection *panel = new PanelBankSelection(m_MainFrame);
			m_MainFrame->bMainFrameSizer->Add(panel, 1, wxEXPAND);
			m_MainFrame->bMainFrameSizer->Layout();

			m_CurrentPanel = panel;

			m_CurrentPanel->Show();
		}break;

		case SCREEN_BANK:
		{
			if (m_CurrentPanel != 0)
			{
				m_MainFrame->bMainFrameSizer->Remove(0);
				m_CurrentPanel->Destroy();
			}

			PanelBank *panel = new PanelBank(m_MainFrame);
			m_MainFrame->bMainFrameSizer->Add(panel, 1, wxEXPAND);
			m_MainFrame->bMainFrameSizer->Layout();

			m_CurrentPanel = panel;

			m_CurrentPanel->Show();
		}break;

		case SCREEN_PHASEPROGRAMS:
		{
			if (m_CurrentPanel != 0)
			{
				m_MainFrame->bMainFrameSizer->Remove(0);
				m_CurrentPanel->Destroy();
			}

			PanelPhasePrograms *panel = new PanelPhasePrograms(m_MainFrame);
			m_MainFrame->bMainFrameSizer->Add(panel, 1, wxEXPAND);
			m_MainFrame->bMainFrameSizer->Layout();

			m_CurrentPanel = panel;

			m_CurrentPanel->Show();
		}break;

		case SCREEN_PARTICIPANTINFO:
		{
			if (m_CurrentPanel != 0)
			{
				m_MainFrame->bMainFrameSizer->Remove(0);
				m_CurrentPanel->Destroy();
			}

			PanelParticipantInfo *panel = new PanelParticipantInfo(m_MainFrame);
			m_MainFrame->bMainFrameSizer->Add(panel, 1, wxEXPAND);
			m_MainFrame->bMainFrameSizer->Layout();

			m_CurrentPanel = panel;

			m_CurrentPanel->Show();
		}break;

		case SCREEN_PARTICIPANT:
		{
			if (m_CurrentPanel != 0)
			{
				m_MainFrame->bMainFrameSizer->Remove(0);
				m_CurrentPanel->Destroy();
			}

			PanelParticipant *panel = new PanelParticipant(m_MainFrame);
			m_MainFrame->bMainFrameSizer->Add(panel, 1, wxEXPAND);
			m_MainFrame->bMainFrameSizer->Layout();

			m_CurrentPanel = panel;

			m_CurrentPanel->Show();
		}break;

		case SCREEN_TUTORIAL:
		{
			if (m_CurrentPanel != 0)
			{
				m_MainFrame->bMainFrameSizer->Remove(0);
				m_CurrentPanel->Destroy();
			}

			PanelTutorial *panel = new PanelTutorial(m_MainFrame);
			m_MainFrame->bMainFrameSizer->Add(panel, 1, wxEXPAND);
			m_MainFrame->bMainFrameSizer->Layout();

			m_CurrentPanel = panel;

			m_CurrentPanel->Show();
			m_CurrentPanel->SetFocus();
		}break;

		case SCREEN_PHASERUN:
		{
			if (m_CurrentPanel != 0)
			{
				m_MainFrame->bMainFrameSizer->Remove(0);
				m_CurrentPanel->Destroy();
			}

			PanelPhaseRun *panel = new PanelPhaseRun(m_MainFrame);
			m_MainFrame->bMainFrameSizer->Add(panel, 1, wxEXPAND);
			m_MainFrame->bMainFrameSizer->Layout();

			m_CurrentPanel = panel;

			m_CurrentPanel->Show();
			m_CurrentPanel->SetFocus();

			//m_MainFrame->ShowFullScreen(true);

		}break;

		case SCREEN_QUESTIONS:
		{
			if (m_CurrentPanel != 0)
			{
				m_MainFrame->bMainFrameSizer->Remove(0);
				m_CurrentPanel->Destroy();
			}

			PanelQuestions *panel = new PanelQuestions(m_MainFrame);
			m_MainFrame->bMainFrameSizer->Add(panel, 1, wxEXPAND);
			m_MainFrame->bMainFrameSizer->Layout();

			m_CurrentPanel = panel;

			m_CurrentPanel->Show();
		}break;

		default:
			assert(0);
		break;
	}

	return true;
}

/////////////////////////////////////////////////////
/// AddToBank
/// Desc: Adds a BankData structure to the current bank (unless it already exists)
/////////////////////////////////////////////////////
bool AppData::AddToBank(const BankData& bd)
{
	auto search = m_Bank.find(bd.str);
	if (search != m_Bank.end())
	{
		// already exists
		return false;
	}

	m_Bank.insert(std::make_pair(bd.str, bd));

	return true;
}

/////////////////////////////////////////////////////
/// UpdateBank
/// Desc: Updates a BankData structure to the parameter passed structure
/////////////////////////////////////////////////////
bool AppData::UpdateBank(const wxString& oldStr, const BankData& bd)
{
	RemoveFromBank(oldStr);

	AddToBank(bd);

	return true;
}

/////////////////////////////////////////////////////
/// RemoveFromBank
/// Desc: Removes a word/sentence from the bank
/////////////////////////////////////////////////////
bool AppData::RemoveFromBank(const wxString& str)
{
	auto search = m_Bank.find(str);
	if (search == m_Bank.end())
	{
		// doesn't exist
		return false;
	}

	// need to check for a guid
	if (search->second.hasGUID)
	{
		// need to delete the others
		std::vector<BankData> removeList;
		if (search->second.guid.size() > 0)
		{
			for (auto item : m_Bank)
			{
				if (item.second.guid == search->second.guid)
				{
					removeList.push_back(item.second);
				}
			}
		}

		// go through the list and delete
		std::size_t i = 0;
		for (i = 0; i < removeList.size(); ++i)
		{
			BankData bd = removeList[i];

			search = m_Bank.find(bd.str);
			if (search != m_Bank.end())
				m_Bank.erase(search);
		}

	}
	else
	{
		// only single to remove
		m_Bank.erase(search);
	}
	return true;
}

/////////////////////////////////////////////////////
/// GetFromBank
/// Desc: Get the BankData structure for a word in the bank
/////////////////////////////////////////////////////
const BankData& AppData::GetFromBank(const wxString& str)
{
	const auto search = m_Bank.find(str);
	if (search == m_Bank.end())
	{
		wxASSERT(0);
	}
	
	// exists
	return search->second;
}

/////////////////////////////////////////////////////
/// GetMatchingGUID
/// Desc: requires a vector list of all related sentences matching the parameter guid
/////////////////////////////////////////////////////
const std::vector<BankData> AppData::GetMatchingGUID(const std::string& guid)
{
	std::vector<BankData> list;

	for ( auto item : m_Bank )
	{
		if (item.second.guid == guid)
			list.push_back(item.second);
	}

	return list;
}

/////////////////////////////////////////////////////
/// AddToFilters
/// Desc: Adds a FilterData structure to the internal list (if it doesn't already exist)
/////////////////////////////////////////////////////
bool AppData::AddToFilters(const FilterData& fd)
{
	auto search = m_Filters.find(fd.val);
	if (search != m_Filters.end())
	{
		// already exists
		return false;
	}

	m_Filters.insert(std::make_pair(fd.val, fd));

	return true;
}

/////////////////////////////////////////////////////
/// UpdateFilter
/// Desc: Updates a FilterData structure to the parameter passed string
/////////////////////////////////////////////////////
bool AppData::UpdateFilter(const wxString& oldStr, const wxString& newStr)
{
	bool valid = false;

	FilterData fd;
	for (auto search : m_Filters)
	{
		if (search.second.name.IsSameAs(oldStr))
		{
			// update
			fd = search.second;
			fd.name = newStr;
			valid = true;
			break;
		}
	}

	if (valid)
	{
		m_Filters[fd.val] = fd;
		return true;
	}

	return false;
}

/////////////////////////////////////////////////////
/// UpdateFilter
/// Desc: Updates a FilterData structure to the parameter passed FilterData 
/////////////////////////////////////////////////////
bool AppData::UpdateFilter(const wxString& filterStr, const FilterData& newData)
{
	bool valid = false;

	FilterData fd;
	for (auto search : m_Filters)
	{
		if (search.second.name.IsSameAs(filterStr))
		{
			// update
			fd.val = search.second.val;
			valid = true;
			break;
		}
	}

	if (valid)
	{
		m_Filters[fd.val] = newData;
		return true;
	}

	return false;
}

/////////////////////////////////////////////////////
/// RemoveFromFilters
/// Desc: Removes a filter from the internal list
/////////////////////////////////////////////////////
bool AppData::RemoveFromFilters(const wxString& WXUNUSED(str), int filterId, bool updateBank, int updateBankFilter)
{
	// first do the bank update
	if (updateBank)
	{
		BankData bd;
		for (auto search : m_Bank)
		{
			if (search.second.filterIndex == filterId)
			{
				// update
				bd = search.second;
				bd.filterIndex = updateBankFilter;
				m_Bank[search.first] = bd;
			}
		}
	}

	auto search = m_Filters.find(filterId);
	if (search == m_Filters.end())
	{
		// doesn't exist
		return false;
	}

	m_Filters.erase(search);

	return true;
}

/////////////////////////////////////////////////////
/// FindFilter
/// Desc: Finds a filter via its index
/////////////////////////////////////////////////////
const FilterData AppData::FindFilter(int index)
{
	const auto search = m_Filters.find(index);
	if (search == m_Filters.end())
	{
		FilterData fd;
		fd.name = _("BAD DATA");
		fd.isDefault = false;
		fd.val = -1;
		fd.colIndex = -1;

		return fd;
	}


	return search->second;
}

/////////////////////////////////////////////////////
/// FindFilter
/// Desc: Finds a filter via its name
/////////////////////////////////////////////////////
const FilterData AppData::FindFilter(wxString str)
{

	for (auto iter : m_Filters)
	{
		if (iter.second.name.IsSameAs(str))
		{
			return iter.second;
		}
	}

	FilterData fd;
	fd.name = _("BAD DATA");
	fd.isDefault = false;
	fd.val = -1;
	fd.colIndex = -1;

	return fd;
}

/////////////////////////////////////////////////////
/// AddToPhases
/// Desc: Adds another version to the internal list
/////////////////////////////////////////////////////
bool AppData::AddToPhases(const PhaseData& pd)
{
	auto search = m_Phases.find(pd.val);
	if (search != m_Phases.end())
	{
		// already exists
		return false;
	}

	m_Phases.insert(std::make_pair(pd.val, pd));

	return true;
}

/////////////////////////////////////////////////////
/// UpdatePhase
/// Desc: Updates a PhaseData structure to the parameter passed string
/////////////////////////////////////////////////////
bool AppData::UpdatePhase(const wxString& oldStr, const wxString& newStr)
{
	bool valid = false;

	PhaseData pd;
	for (auto search : m_Phases)
	{
		if (search.second.name.IsSameAs(oldStr))
		{
			// update
			pd.isDefault = search.second.isDefault;
			pd.name = newStr;
			pd.val = search.second.val;
			valid = true;
			break;
		}
	}

	if (valid)
	{
		m_Phases[pd.val] = pd;
		return true;
	}

	return false;
}

/////////////////////////////////////////////////////
/// UpdatePhase
/// Desc: Updates a PhaseData structure to the parameter passed PhaseData 
/////////////////////////////////////////////////////
bool AppData::UpdatePhase(const wxString& phaseStr, const PhaseData& newData)
{
	bool valid = false;

	PhaseData pd;
	for (auto search : m_Phases)
	{
		if (search.second.name.IsSameAs(phaseStr))
		{
			// update
			pd.val = search.second.val;
			valid = true;
			break;
		}
	}

	if (valid)
	{
		m_Phases[pd.val] = newData;
		return true;
	}

	return false;
}

/////////////////////////////////////////////////////
/// 
/////////////////////////////////////////////////////
/*bool AppData::RemoveFromPhases(const wxString& str, int phaseId, bool updateProgram, int updateProgramPhase)
{
	// first do the bank update
	if (updateBank)
	{
		bool valid = false;

		BankData bd;
		for (auto search : m_Bank)
		{
			if (search.second.filterIndex == filterId)
			{
				// update
				bd.str = search.second.str;
				bd.filterIndex = updateBankFilter;
				bd.wordCount = search.second.wordCount;

				m_Bank[search.first] = bd;
			}
		}
	}

	auto search = m_Phases.find(phaseId);
	if (search == m_Phases.end())
	{
		// doesn't exist
		return false;
	}

	m_Filters.erase(search);

	return true;
}*/

/////////////////////////////////////////////////////
/// FindPhase
/// Desc: Finds a phase via its index
/////////////////////////////////////////////////////
const PhaseData AppData::FindPhase(int index)
{
	const auto search = m_Phases.find(index);
	if (search == m_Phases.end())
	{
		PhaseData pd;
		pd.name = _("BAD DATA");
		pd.isDefault = false;
		pd.val = -1;

		return pd;
	}


	return search->second;
}

/////////////////////////////////////////////////////
/// FindPhase
/// Desc: Finds a phase via its name
/////////////////////////////////////////////////////
const PhaseData AppData::FindPhase(wxString str)
{
	for (auto iter : m_Phases)
	{
		if (iter.second.name.IsSameAs(str))
		{
			return iter.second;
		}
	}

	PhaseData pd;
	pd.name = _("BAD DATA");
	pd.isDefault = false;
	pd.val = -1;

	return pd;
}

/////////////////////////////////////////////////////
/// RequestNewFilterId
/// Desc: Requests to generate a new filter id that's not already used
/////////////////////////////////////////////////////
int AppData::RequestNewFilterId(int count, int maxCount)
{
	// increment the current default and check it doesn't clash
	m_NextFilterId++;

	if (m_NextFilterId > CUSTOM_FILTER_MAX)
		m_NextFilterId = CUSTOM_FILTER_OFFSET;

	auto searchResult = m_Filters.find(m_NextFilterId);
	if (searchResult == m_Filters.end())
	{
		// no matches, it's safe
		return m_NextFilterId;
	}

	// increment the count but stop if we're passing too deep
	count++;
	if (count >= maxCount)
		return -1;

	return RequestNewFilterId(count, maxCount);
}

/////////////////////////////////////////////////////
/// RequestNewPhaseId
/// Desc: Requests to generate a new phase id that's not already used
/////////////////////////////////////////////////////
int AppData::RequestNewPhaseId(int count, int maxCount)
{
	// increment the current default and check it doesn't clash
	m_NextPhaseId++;

	if (m_NextPhaseId > CUSTOM_VERSION_MAX)
		m_NextPhaseId = CUSTOM_VERSION_OFFSET;

	auto searchResult = m_Phases.find(m_NextPhaseId);
	if (searchResult == m_Phases.end())
	{
		// no matches, it's safe
		return m_NextPhaseId;
	}

	// increment the count but stop if we're passing too deep
	count++;
	if (count >= maxCount)
		return -1;

	return RequestNewPhaseId(count, maxCount);
}

/////////////////////////////////////////////////////
/// SetSelectedBank
/// Desc: Sets the current bank
/////////////////////////////////////////////////////
void AppData::SetSelectedBankFromGUID(const std::string& bankGUID)
{
	// use the bank guid to find the correct bank
	std::map<wxString, BankData> bank;
	GetBankFileList();

	auto bankFile = m_BankFileMap.find(bankGUID);
	if (bankFile != m_BankFileMap.end())
	{
		LoadBank(bankFile->second.filename.ToStdString(), false);
	}
	else
	{

	}
	bank = GetBank();
}

/////////////////////////////////////////////////////
/// SetSelectedBank
/// Desc: Sets the current bank
/////////////////////////////////////////////////////
void AppData::SetSelectedBank(const BankFileInfo& selectedBank)
{
	m_SelectedBank = selectedBank;
	m_Bank.clear();

	if (!m_SelectedBank.jsonData.isNull())
	{
		// bank list
		JsonBox::Value val;

		// word bank
		val = m_SelectedBank.jsonData["Bank"];
		if (val.isArray() &&
			!val.isNull())
		{
			JsonBox::Array arr = val.getArray();
			auto iter = arr.begin();
			while (iter != arr.end())
			{
				if (iter->isObject())
				{
					JsonBox::Object bankItem = iter->getObject();
					if (!bankItem["StringData"].isNull() &&
						!bankItem["FilterType"].isNull())
					{
						wxString str = bankItem["StringData"].getString();
						int index = bankItem["FilterType"].getInteger();

						bool hasGUID = false;
						std::string guidVal = " ";

						if (!bankItem["HasGUID"].isNull())
							hasGUID = bankItem["HasGUID"].getBoolean();

						if (hasGUID &&
							!bankItem["GUID"].isNull())
						{
							wxString guidStr = bankItem["GUID"].getString();
							guidVal = guidStr.ToStdString();
						}
						else
							hasGUID = false;

						// loaded
						BankData data;
						data.str = str;
						data.filterIndex = index;
						data.wordCount = sys::WordCount(str.ToStdWstring());
						data.hasGUID = hasGUID;
						data.guid = guidVal;

						// add to the bank
						m_Bank.insert(std::make_pair(str, data));
					}
				}
				// next
				++iter;
			}
		}
	}
}

/////////////////////////////////////////////////////
/// GetBankFileList
/// Desc: Gets a list of the current bank files on the system
/////////////////////////////////////////////////////
const std::map<wxString, BankFileInfo>& AppData::GetBankFileList()
{
	m_BankFileMap.clear();

	// find all available files in the banks directory
	wxArrayString fileList;
	std::size_t totalFiles = wxDir::GetAllFiles(sys::AppData::Instance()->GetBanksDirectory(), &fileList);
	if (totalFiles > 0)
	{
		for (auto fileIter : fileList)
		{
			BankFileInfo fileInfo;
			fileInfo.filename = fileIter;
			
			std::ifstream t(fileIter.ToStdString());
			std::stringstream buffer;
			buffer << t.rdbuf();

			std::string bufferStr = buffer.str();
			fileInfo.jsonData.loadFromString(bufferStr);
			//fileInfo.jsonData.loadFromFile(fileIter.ToStdString());

			if (!fileInfo.jsonData.isNull())
			{
				JsonBox::Value bankGUID = fileInfo.jsonData["GUID"];
				JsonBox::Value name = fileInfo.jsonData["Name"];
				JsonBox::Value comments = fileInfo.jsonData["Comments"];
				JsonBox::Value date = fileInfo.jsonData["Date"];

				if (name.isString() &&
					comments.isString() &&
					bankGUID.isString())
				{
					fileInfo.name = name.getString();
					fileInfo.comment = comments.getString();
					fileInfo.guid = bankGUID.getString();

					wxString str = date.getString();
					long long val = -1;
					if (str.ToLongLong(&val))
					{
						fileInfo.dateTime = wxDateTime((wxLongLong)val);
					}

					// do the insert
					m_BankFileMap.insert(std::make_pair(fileInfo.guid, fileInfo));
				}
			}
		}
	}

	return m_BankFileMap;
}

/////////////////////////////////////////////////////
/// CheckBankFilterCounts
/// Desc: Checks the parameter counts against the internal counts to see if the number of items are available for a program
/////////////////////////////////////////////////////
ProgramCreateEditLogInfo AppData::CheckBankFilterCounts(const std::string& bankGUID, int versionId, int filterId, int requiredCount, int requiredMinWord, int requiredMaxWord)
{
	ProgramCreateEditLogInfo logData;
	logData.isValid = true;

	// use the bank guid to find the correct bank
	std::map<wxString, BankData> bank;
	GetBankFileList();

	auto bankFile = m_BankFileMap.find(bankGUID);
	if (bankFile != m_BankFileMap.end())
	{
		LoadBank(bankFile->second.filename.ToStdString(), false);
	}
	else
	{
		logData.isValid = false;
		logData.reasonNotValid = wxString::Format(_("Bank '%s' does not exist on host system"), bankGUID);
		return logData;
	}
	bank = GetBank();

	// remove by filter
	for (auto iter = bank.begin(); iter != bank.end();)
	{
		if (versionId == VERSION_THREE)
		{
			if (!iter->second.hasGUID ||
				iter->second.filterIndex != filterId)
			{
				iter = bank.erase(iter);
				continue;
			}
		}
		else
		{
			if (iter->second.filterIndex != filterId)
			{
				iter = bank.erase(iter);
				continue;
			}		
		}

		// next
		iter++;
	}

	// check filter count
	if ((int)bank.size() < requiredCount)
	{
		const FilterData fd = sys::AppData::Instance()->FindFilter(filterId);

		logData.isValid = false;
		logData.reasonNotValid = wxString::Format(_("Required %d of type filter '%s' only found %d"), requiredCount, fd.name, (int)bank.size());
		return logData;
	}

	// check word min/max count
	for (auto iter = bank.begin(); iter != bank.end();)
	{
		if (iter->second.wordCount < requiredMinWord ||
			iter->second.wordCount > requiredMaxWord)
		{
			iter = bank.erase(iter);
			continue;
		}

		// next
		iter++;
	}

	// not enough
	if ((int)bank.size() < requiredCount)
	{
		const FilterData fd = sys::AppData::Instance()->FindFilter(filterId);

		logData.isValid = false;
		logData.reasonNotValid = wxString::Format(_("Need more words between '%d' and '%d' for filter '%s' require %d words but only found %d valid"), requiredMinWord, requiredMaxWord, fd.name, requiredCount, (int)bank.size());

		return logData;
	}

	return logData;
}

/////////////////////////////////////////////////////
/// WordCount
/// Desc: Counts the number of words in the parameter string
/////////////////////////////////////////////////////
std::size_t sys::WordCount(const std::wstring& str)
{
	std::wstring strLocal = str;

	if (str.length() == 0)
		return 0;

	std::size_t c = 0;
	auto pos = strLocal.begin();
	auto end = strLocal.end();

	while (pos != end)
	{
		while (pos != end && (*pos == L' ' || std::isspace(*pos)))
			++pos;
		c += (pos != end);
		while (pos != end && (*pos != L' ' || !std::isspace(*pos)))
			++pos;
	}

	return c;
}

/////////////////////////////////////////////////////
/// RandInt
/// Desc: Generates a random integer between the param low/high
/////////////////////////////////////////////////////
int sys::RandInt(int low, int high)
{
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	static std::default_random_engine re(seed);
	using Dist = std::uniform_int_distribution<int>;
	static Dist uid{};
	return uid(re, Dist::param_type{ low, high });
}
