/**
	TECS
	DlgProgramView.cpp
    Purpose: Dialog for reviewing a previously run program for a participant
*/

#include "DlgProgramView.h"
#include "System.h"

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
DlgProgramView::DlgProgramView( wxWindow* parent )
:
ProgramViewDlg( parent )
{
	this->SetTitle(_("Program Details"));

	m_IsLocked = true;

	m_ProgramData->InsertColumn(0, _("Word/Sentence")); // column A
	if (m_FileInfo.versionId == VERSION_ONE ||
		m_FileInfo.versionId == VERSION_TWO)
	{
		m_ProgramData->InsertColumn(1, _("Lines")); // column B
	}
	else
		m_ProgramData->InsertColumn(1, _("Word Count"));

	m_ProgramData->InsertColumn(2, _("Filter")); // column C
	m_ProgramData->InsertColumn(3, _("First Response")); // column D
	m_ProgramData->InsertColumn(4, _("Second Response")); // column E

	//long style = m_InputMark->GetWindowStyle();
	//style &= ~(wxTE_READONLY);
	m_InputUser->SetEditable(false);
	m_InputMark->SetEditable(false);

	m_StringFilter.Clear();
}

/////////////////////////////////////////////////////
/// Apply
/// Desc: Called manually after construction to populate or modify UI elements
/////////////////////////////////////////////////////
void DlgProgramView::Apply()
{
	JsonBox::Value programData = m_FileInfo.jsonData["ProgramInfo"];

	JsonBox::Value versionId = m_FileInfo.jsonData["VersionId"];
	JsonBox::Object programObj = programData.getObject();
	
	int index = 0;
	std::vector<ParticipantResponse> responseList;
	if (programData.isObject())
	{
		JsonBox::Value name = programObj["Name"];

		m_InputProgramName->SetValue( name.getString() );

		this->SetTitle(wxString::Format(_("Program Details - Participant %s"), m_FileInfo.jsonData["ID"].getString() ));

		const PhaseData pd = sys::AppData::Instance()->FindPhase(versionId.getInteger());

		m_InputVersion->SetValue(pd.name);

		if (programObj["Program"].isArray())
		{
			JsonBox::Array arr = programObj["Program"].getArray();

			auto iter = arr.begin();
			while (iter != arr.end())
			{
				if (iter->isObject())
				{
					JsonBox::Object programItem = iter->getObject();
					if (!programItem["StringData"].isNull() &&
						!programItem["FilterType"].isNull() &&
						!programItem["Index"].isNull() )
					{
						wxString strData = programItem["StringData"].getString();
						int filterId = programItem["FilterType"].getInteger();
						auto fd = sys::AppData::Instance()->FindFilter(filterId);
						int lineOrWordCount = programItem["LineOrWordCount"].getInteger();

						bool hasFirstResponse = programItem["HasFirstResponse"].getBoolean();
						//int firstResponseTime = programItem["FirstResponseTime"].getInteger();
						int firstResponseValue = programItem["FirstResponseValue"].getInteger();

						bool hasSecondResponse = programItem["HasSecondResponse"].getBoolean();
						//int secondResponseTime = programItem["SecondResponseTime"].getInteger();
						int secondResponseValue = programItem["SecondResponseValue"].getInteger();

						// create row and first column
						m_ProgramData->InsertItem(index, strData, -1); // column A

						m_ProgramData->SetItem(index, 1, wxString::Format("%d", lineOrWordCount), -1); // column B
						m_ProgramData->SetItem(index, 2, fd.name, -1); // column C

						// column D
						if (hasFirstResponse)
							m_ProgramData->SetItem(index, 3, wxString::Format("%d", firstResponseValue), -1);
						else
							m_ProgramData->SetItem(index, 3, _("OM"), -1);

						// column E
						if (hasSecondResponse)
							m_ProgramData->SetItem(index, 4, wxString::Format("%d", secondResponseValue), -1);
						else
							m_ProgramData->SetItem(index, 4, _(" "), -1);

						// need a list of filter ids shown
						ParticipantResponse resp;
						resp.data.str = strData;
						resp.data.filterIndex = filterId;
					
						responseList.push_back(resp);

						// next
						index++;
					}
				}
				// next
				++iter;
			}
		}

		if (programData["TimeToRespond"].isInteger())
			m_InputTimeToRespond->SetValue(wxString::Format("%d", programData["TimeToRespond"].getInteger()));

		if (programData["TimeResponseTimeout"].isInteger())
			m_InputTimeResponseTimeout->SetValue(wxString::Format("%d", programData["TimeResponseTimeout"].getInteger()));
		

		if (programData["TimeInterval"].isInteger())
			m_InputTimeInterval->SetValue(wxString::Format("%d", programData["TimeInterval"].getInteger()));
		

		if (programData["TimeBetweenFilterChanges"].isInteger())
			m_InputTimeBetweenChanges->SetValue(wxString::Format("%d", programData["TimeBetweenFilterChanges"].getInteger()));
		

		if (programData["LinesMin"].isInteger())
			m_InputLinesMin->SetValue(wxString::Format("%d", programData["LinesMin"].getInteger()));
		
		if (programData["LinesMax"].isInteger())
			m_InputLinesMax->SetValue(wxString::Format("%d", programData["LinesMax"].getInteger()));
		

		if (programData["WordCountMin"].isInteger())
			m_InputWordMin->SetValue(wxString::Format("%d", programData["WordCountMin"].getInteger()));

		if (programData["WordCountMax"].isInteger())
			m_InputWordMax->SetValue(wxString::Format("%d", programData["WordCountMax"].getInteger()));
		
		m_InputTimeToRespond->Disable();
		m_InputTimeResponseTimeout->Disable();
		m_InputTimeInterval->Disable();
		m_InputTimeBetweenChanges->Disable();

		m_InputWordMin->Disable();
		m_InputWordMax->Disable();

		m_InputLinesMin->Disable();
		m_InputLinesMax->Disable();
	}

	int i = 0;
	for (i = 0; i < m_ProgramData->GetColumnCount(); ++i)
	{
		m_ProgramData->SetColumnWidth(i, wxLIST_AUTOSIZE_USEHEADER);
		int wh = m_ProgramData->GetColumnWidth(i);
		m_ProgramData->SetColumnWidth(i, wxLIST_AUTOSIZE);
		int wc = m_ProgramData->GetColumnWidth(i);
		if (wh > wc)
			m_ProgramData->SetColumnWidth(i, wxLIST_AUTOSIZE_USEHEADER);
	}

	m_ProgramData->Layout();

	m_InputUser->Clear();
	if (m_FileInfo.jsonData["Answers"].isString())
		m_InputUser->SetValue(m_FileInfo.jsonData["Answers"].getString());

	// correct
	m_ProgramCounts.clear();
	for (auto resp : responseList)
	{
		const FilterData fd = sys::AppData::Instance()->FindFilter(resp.data.filterIndex);

		if (fd.val != -1)
		{
			auto mapFind = m_ProgramCounts.find(fd.val);
			if (mapFind == m_ProgramCounts.end())
			{
				// insert a new pair
				m_ProgramCounts.insert(std::make_pair(fd.val, 1));
			}
			else
			{
				// update
                int oldVal = m_ProgramCounts[fd.val];
                oldVal += 1;
				m_ProgramCounts[fd.val] = oldVal;
			}
		}
	}

	m_AnswersDynamic.clear();

	// dynamically create correct response counters
	auto filterMap = sys::AppData::Instance()->GetFilters();

	for (auto iter = filterMap.begin(); iter != filterMap.end();)
	{
		StoredCorrectData answerData;

		answerData.fd = iter->second;
		auto mapFilterCounts = m_ProgramCounts.find(answerData.fd.val);

		answerData.filterboxSizer = new wxBoxSizer(wxHORIZONTAL);

		answerData.filterName = new wxStaticText(m_CorrectValuesScroll, wxID_ANY, wxString::Format(_("%s shown"), answerData.fd.name), wxDefaultPosition, wxDefaultSize, 0);
		answerData.filterName->Wrap(-1);
		answerData.filterboxSizer->Add(answerData.filterName, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

		answerData.shownVal = new wxTextCtrl(m_CorrectValuesScroll, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0);
		answerData.shownVal->SetMaxSize(wxSize(55, -1));
		if (mapFilterCounts != m_ProgramCounts.end())
			answerData.shownVal->SetValue(wxString::Format("%d", mapFilterCounts->second));
		else
			answerData.shownVal->SetValue(wxString::Format("0"));
		answerData.shownVal->Disable();
		answerData.filterboxSizer->Add(answerData.shownVal, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

		answerData.rememberedCorrectly = new wxStaticText(m_CorrectValuesScroll, wxID_ANY, wxString::Format(_("remembered")), wxDefaultPosition, wxDefaultSize, 0);
		answerData.rememberedCorrectly->Wrap(-1);
		answerData.filterboxSizer->Add(answerData.rememberedCorrectly, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

		answerData.spinValueControl = new wxSpinCtrl(m_CorrectValuesScroll, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 100, 0);
		answerData.spinValueControl->SetMaxSize(wxSize(55, -1));

		if (mapFilterCounts != m_ProgramCounts.end())
			answerData.spinValueControl->SetRange(0, mapFilterCounts->second);
		else
			answerData.spinValueControl->SetRange(0, 0);

		answerData.spinValueControl->Disable();

		answerData.spinValueControl->Connect(wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler(DlgProgramView::DynamicCountChange), NULL, this);
		answerData.spinValueControl->Connect(wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler(DlgProgramView::DynamicCountChangeTxt), NULL, this);

		answerData.filterboxSizer->Add(answerData.spinValueControl, 1, wxALL | wxEXPAND, 5);

		answerData.percentage = new wxStaticText(m_CorrectValuesScroll, wxID_ANY, wxString("0%"), wxDefaultPosition, wxDefaultSize, 0);
		answerData.percentage->Wrap(-1);
		answerData.filterboxSizer->Add(answerData.percentage, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

		// add
		bFilterWindowSizer->Add(answerData.filterboxSizer, 0, wxEXPAND, 5);

		// add to the list
		m_AnswersDynamic.push_back(answerData);

		// next
		iter++;
	}

	bFilterWindowSizer->Layout();
	
	// answer percentages
	if (m_FileInfo.jsonData["AnswerCounts"].isArray())
	{
		JsonBox::Array arr = m_FileInfo.jsonData["AnswerCounts"].getArray();

		auto iter = arr.begin();
		while (iter != arr.end())
		{
			if (iter->isObject())
			{
				JsonBox::Object answerItem = iter->getObject();
				if (!answerItem["FilterType"].isNull() &&
					!answerItem["ShownCount"].isNull() &&
					!answerItem["RememberedCount"].isNull())
				{
					int filterId = answerItem["FilterType"].getInteger();
					int shownCount = answerItem["ShownCount"].getInteger();
					int rememberedCount = answerItem["RememberedCount"].getInteger();

					for (auto element : m_AnswersDynamic)
					{
						if (element.fd.val == filterId)
						{
							element.spinValueControl->SetValue(rememberedCount);

							if (shownCount == 0)
							{
								element.percentage->SetLabel(wxString("0%"));
							}
							else
							{
								float percent = (float)(std::abs(100.0f * ((float)rememberedCount / (float)shownCount)));
								element.percentage->SetLabel(wxString::Format("%.2f%%", percent));
							}

							// done
							break;
						}
					}

				}
			}
			// next
			++iter;
		}
	}

	m_InputMark->Clear();
	if (m_FileInfo.jsonData["Comments"].isString())
		m_InputMark->SetValue(m_FileInfo.jsonData["Comments"].getString());

	this->Layout();
}

/////////////////////////////////////////////////////
/// CreateProgramDataList
/// Desc: Clears and create the program data list 
/////////////////////////////////////////////////////
void DlgProgramView::CreateProgramDataList()
{
	JsonBox::Value programData = m_FileInfo.jsonData["ProgramInfo"];
	JsonBox::Object programObj = programData.getObject();

	int index = 0;
	m_ProgramData->DeleteAllItems();
	if (programData.isObject())
	{
		if (programObj["Program"].isArray())
		{
			JsonBox::Array arr = programObj["Program"].getArray();

			auto iter = arr.begin();
			while (iter != arr.end())
			{
				if (iter->isObject())
				{
					JsonBox::Object programItem = iter->getObject();
					if (!programItem["StringData"].isNull() &&
						!programItem["FilterType"].isNull() &&
						!programItem["Index"].isNull() )
					{
						wxString strData = programItem["StringData"].getString();
						int filterId = programItem["FilterType"].getInteger();
						auto fd = sys::AppData::Instance()->FindFilter(filterId);
						int lineOrWordCount = programItem["LineOrWordCount"].getInteger();
						//int time = programItem["ResponseTime"].getInteger();

						bool hasFirstResponse = programItem["HasFirstResponse"].getBoolean();
						//int firstResponseTime = programItem["FirstResponseTime"].getInteger();
						int firstResponseValue = programItem["FirstResponseValue"].getInteger();

						bool hasSecondResponse = programItem["HasSecondResponse"].getBoolean();
						//int secondResponseTime = programItem["SecondResponseTime"].getInteger();
						int secondResponseValue = programItem["SecondResponseValue"].getInteger();

						//int time = programItem["ResponseTime"].getInteger();

						if (m_StringFilter.Length() > 0)
						{
							if (strData.Contains(m_StringFilter)) // filter by string
							{
								// create row and first column
								m_ProgramData->InsertItem(index, strData, -1); // column A

								m_ProgramData->SetItem(index, 1, wxString::Format("%d", lineOrWordCount), -1); // column B
								m_ProgramData->SetItem(index, 2, fd.name, -1); // column C

								// column D
								if (hasFirstResponse)
									m_ProgramData->SetItem(index, 3, wxString::Format("%d", firstResponseValue), -1);
								else
									m_ProgramData->SetItem(index, 3, _("OM"), -1);

								// column E
								if (hasSecondResponse)
									m_ProgramData->SetItem(index, 4, wxString::Format("%d", secondResponseValue), -1);
								else
									m_ProgramData->SetItem(index, 4, _(" "), -1);

								// next
								index++;
							}
						}
						else
						{
							// create row and first column
							m_ProgramData->InsertItem(index, strData, -1); // column A

							m_ProgramData->SetItem(index, 1, wxString::Format("%d", lineOrWordCount), -1); // column B
							m_ProgramData->SetItem(index, 2, fd.name, -1); // column C

							// column D
							if (hasFirstResponse)
								m_ProgramData->SetItem(index, 3, wxString::Format("%d", firstResponseValue), -1);
							else
								m_ProgramData->SetItem(index, 3, _("OM"), -1);

							// column E
							if (hasSecondResponse)
								m_ProgramData->SetItem(index, 4, wxString::Format("%d", secondResponseValue), -1);
							else
								m_ProgramData->SetItem(index, 4, _(" "), -1);

							// next
							index++;
						}
					}
				}
				// next
				++iter;
			}
		}
	}
}

/////////////////////////////////////////////////////
/// ProgramSearchCancel
/// Desc: Clears the search filter
/////////////////////////////////////////////////////
void DlgProgramView::ProgramSearchCancel(wxCommandEvent& WXUNUSED(event))
{
	if (m_ProgramSearch->GetValue().Length() > 0)
	{
		m_ProgramSearch->Clear();
		m_StringFilter.Clear();

		CreateProgramDataList();
	}
}

/////////////////////////////////////////////////////
/// ProgramSearch
/// Desc: Recreated the list finding only containing parts in the search string
/////////////////////////////////////////////////////
void DlgProgramView::ProgramSearch(wxCommandEvent& WXUNUSED(event))
{
	wxString str = m_ProgramSearch->GetValue();

	if (m_StringFilter != str)
	{
		m_StringFilter = str;
		CreateProgramDataList();
	}
}

/////////////////////////////////////////////////////
/// BtnUnlockClick
/// Desc: Button callback for unlocking and locking editing of the program details
/////////////////////////////////////////////////////
void DlgProgramView::BtnUnlockClick(wxCommandEvent& WXUNUSED(event))
{
	if (m_IsLocked)
	{
		m_InputUser->SetEditable(true);
		m_InputMark->SetEditable(true);

		for (auto item : m_AnswersDynamic)
		{
			item.spinValueControl->Enable();
		}

		m_BtnUnlock->SetLabel(_("Lock"));

		m_IsLocked = false;
	}
	else
	{
		m_InputUser->SetEditable(false);
		m_InputMark->SetEditable(false);

		for (auto item : m_AnswersDynamic)
		{
			item.spinValueControl->Disable();
		}

		m_BtnUnlock->SetLabel(_("Unlock"));

		m_IsLocked = true;
	}
}

/////////////////////////////////////////////////////
/// DynamicCountChangeTxt
/// Desc: SpinCtrl event for the dynamic filter values
/////////////////////////////////////////////////////
void DlgProgramView::DynamicCountChangeTxt(wxCommandEvent& event)
{
	wxSpinEvent e;
	e.SetEventObject(event.GetEventObject());
	DynamicCountChange(e);
}

/////////////////////////////////////////////////////
/// DynamicCountChange
/// Desc: SpinCtrl event for the dynamic filter values
/////////////////////////////////////////////////////
void DlgProgramView::DynamicCountChange(wxSpinEvent& event)
{
	wxSpinCtrl* pObj = reinterpret_cast<wxSpinCtrl *>(event.GetEventObject());
	for (auto item : m_AnswersDynamic)
	{
		if (pObj == item.spinValueControl) // find the data
		{
			// get the values
			int valCorrect = item.spinValueControl->GetValue();
			wxString tmpStr = item.shownVal->GetValue();
			long tmpVal = -1;
			if (tmpStr.ToLong(&tmpVal))
			{
				int valShown = (int)tmpVal;

				if (valShown == 0)
				{
					item.percentage->SetLabel(wxString("0%"));
				}
				else
				{
					float percent = (float)(std::abs(100.0f * ((float)valCorrect / (float)valShown)));
					item.percentage->SetLabel(wxString::Format("%.2f%%", percent));
				}
			}
			else
			{
				item.percentage->SetLabel(wxString("0%"));
			}
			// done
			return;
		}
	}
}

/////////////////////////////////////////////////////
/// BtnBackClick
/// Desc: Button callback for canceling
/////////////////////////////////////////////////////
void DlgProgramView::BtnBackClick(wxCommandEvent& WXUNUSED(event))
{
	this->EndModal(RESPONSE_CANCEL);
}

/////////////////////////////////////////////////////
/// BtnGenHTMLClick
/// Desc: Button event to generate HTML (not used)
/////////////////////////////////////////////////////
void DlgProgramView::BtnGenHTMLClick(wxCommandEvent& WXUNUSED(event))
{

}

/////////////////////////////////////////////////////
/// BtnBackClick
/// Desc: Button callback for canceling
/////////////////////////////////////////////////////
void DlgProgramView::BtnUpdateClick(wxCommandEvent& WXUNUSED(event))
{
	m_FileInfo.jsonData["Answers"] = JsonBox::Value(m_InputUser->GetValue().ToStdString());
	m_FileInfo.jsonData["Comments"] = JsonBox::Value(m_InputMark->GetValue().ToStdString());

	JsonBox::Array answerArr;
	for (auto answeredCount : m_AnswersDynamic)
	{
		JsonBox::Object answerItem;
		// get the values
		int valCorrect = answeredCount.spinValueControl->GetValue();
		wxString tmpStr = answeredCount.shownVal->GetValue();
		long tmpVal = -1;
		if (tmpStr.ToLong(&tmpVal))
		{

		}
		else
			tmpVal = 0;

		answerItem["FilterType"] = JsonBox::Value(answeredCount.fd.val);
		answerItem["ShownCount"] = JsonBox::Value((int)tmpVal);
		answerItem["RememberedCount"] = JsonBox::Value(valCorrect);

		// add to answer count array
		answerArr.push_back(answerItem);
	}
	m_FileInfo.jsonData["AnswerCounts"] = JsonBox::Value(answerArr);

	JsonBox::Value v(m_FileInfo.jsonData);

	v.writeToFile(m_FileInfo.filename.ToStdString());

	this->EndModal(RESPONSE_OK);
}

/////////////////////////////////////////////////////
/// BtnGenExcelClick
/// Desc: Button event for generating excel for this program
/////////////////////////////////////////////////////
void DlgProgramView::BtnGenExcelClick(wxCommandEvent& WXUNUSED(event))
{
	wxFileDialog saveFileDialog(this, _("Save xls file"), "", "", "XLS files (*.xls)|*.xls", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveFileDialog.ShowModal() == wxID_CANCEL)
		return;   

	JsonBox::Value programData = m_FileInfo.jsonData["ProgramInfo"];

	ExcelFormat::BasicExcel xls;

	// create 2 sheets
	xls.New(2);
	ExcelFormat::BasicExcelWorksheet* sheet1 = xls.GetWorksheet(0);
	ExcelFormat::BasicExcelWorksheet* sheet2 = xls.GetWorksheet(1);
	//ExcelFormat::BasicExcelWorksheet* sheet3 = xls.GetWorksheet(2);

	sheet1->Rename(_("Participant Info").ToStdWstring().c_str());
	sheet2->Rename(_("Program").ToStdWstring().c_str());

	ExcelFormat::XLSFormatManager fmt_mgr(xls);

	// Create a table
	ExcelFormat::ExcelFont font_bold;
	ExcelFormat::ExcelFont font_normal;
	font_bold._weight = FW_BOLD; // 700
	font_normal._weight = FW_NORMAL;

	ExcelFormat::CellFormat fmtRed(fmt_mgr, font_normal.set_color_index(ExcelFormat::EGA_RED));

	ExcelFormat::CellFormat fmtBGYellow(fmt_mgr);
	fmtBGYellow.set_background(MAKE_COLOR2(ExcelFormat::EGA_YELLOW, 0));	// solid yellow background

	ExcelFormat::CellFormat fmtBGGray(fmt_mgr);
	fmtBGGray.set_background(MAKE_COLOR2(23, 0));	// solid grey background
	fmtBGGray.set_font(font_bold);

	int row = 0;

	// sheet 1
	wxString stringStore;

	ExcelFormat::BasicExcelCell* cell = sheet1->Cell(0, 0);

	fmtBGGray.set_alignment(ExcelFormat::EXCEL_HALIGN_RIGHT | ExcelFormat::EXCEL_VALIGN_CENTRED);

	cell->Set(_("ID:").ToStdWstring().c_str());
	cell->SetFormat(fmtBGGray);

	cell = sheet1->Cell(1, 0);
	cell->Set(_("DoB:").ToStdWstring().c_str());
	cell->SetFormat(fmtBGGray);

	cell = sheet1->Cell(2, 0);
	cell->Set(_("Date of Test:").ToStdWstring().c_str());
	cell->SetFormat(fmtBGGray);

	cell = sheet1->Cell(3, 0);
	cell->Set(_("Age During Test:").ToStdWstring().c_str());
	cell->SetFormat(fmtBGGray);

	cell = sheet1->Cell(4, 0);
	cell->Set(_("Gender:").ToStdWstring().c_str());
	cell->SetFormat(fmtBGGray);

	cell = sheet1->Cell(5, 0);
	cell->Set(_("Phase:").ToStdWstring().c_str());
	cell->SetFormat(fmtBGGray);

	cell = sheet1->Cell(6, 0);
	cell->Set(_("Program Name:").ToStdWstring().c_str());
	cell->SetFormat(fmtBGGray);

	fmtBGGray.set_alignment(ExcelFormat::EXCEL_HALIGN_LEFT | ExcelFormat::EXCEL_VALIGN_CENTRED);

	// next column
	auto pd = sys::AppData::Instance()->FindPhase(m_FileInfo.jsonData["VersionId"].getInteger());

	cell = sheet1->Cell(0, 1);
	cell->Set(wxString::Format("%s", m_FileInfo.jsonData["ID"].getString()).ToStdWstring().c_str());
	
	cell = sheet1->Cell(1, 1);
	stringStore = m_FileInfo.jsonData["DOB"].getString();
	// convert
	long long val = 0;
	stringStore.ToLongLong(&val);
	wxDateTime dateOfBirth = wxDateTime((wxLongLong)val);
	wxString dateStr = dateOfBirth.FormatISODate();
	wxDateTime currentDate = wxDateTime::Now();
	int ageYear = currentDate.GetYear() - dateOfBirth.GetYear();
	int ageMonth = currentDate.GetMonth() - dateOfBirth.GetMonth();
	int ageDay = currentDate.GetDay() - dateOfBirth.GetDay();

	// now make adjustments if we're ahead of it...
	if (ageDay < 0) { // the day hasn't happened in this month yet...
		int daysInLastMonth = wxDateTime::GetNumberOfDays(wxDateTime::Month(currentDate.GetMonth() == wxDateTime::Jan ?
			wxDateTime::Dec : currentDate.GetMonth() - 1), currentDate.GetYear());
		ageDay += daysInLastMonth;
		--ageMonth; // one month less older...
	}
	if (ageMonth < 0) { // one year less older..
		ageYear--;
		ageMonth += 12;
	}

	//dateStr += wxString::Format(_(" (%d years)"), ageYear);
	cell->Set(dateStr.ToStdWstring().c_str());

	cell = sheet1->Cell(2, 1);
	stringStore = m_FileInfo.jsonData["Date"].getString();
	stringStore.ToLongLong(&val);
	wxDateTime dateOfTest = wxDateTime((wxLongLong)val);
	dateStr = dateOfTest.FormatISODate();
	cell->Set(dateStr.ToStdWstring().c_str());

	cell = sheet1->Cell(3, 1);

	ageYear = dateOfTest.GetYear() - dateOfBirth.GetYear();
	ageMonth = dateOfTest.GetMonth() - dateOfBirth.GetMonth();
	ageDay = dateOfTest.GetDay() - dateOfBirth.GetDay();

	// now make adjustments if we're ahead of it...
	if (ageDay < 0) { // the day hasn't happened in this month yet...
		int daysInLastMonth = wxDateTime::GetNumberOfDays(wxDateTime::Month(dateOfTest.GetMonth() == wxDateTime::Jan ?
			wxDateTime::Dec : dateOfTest.GetMonth() - 1), dateOfTest.GetYear());
		ageDay += daysInLastMonth;
		--ageMonth; // one month less older...
	}
	if (ageMonth < 0) { // one year less older..
		ageYear--;
		ageMonth += 12;
	}

	dateStr = wxString::Format(_("%d years %d month(s) %d day(s)"), ageYear, ageMonth, ageDay);
	cell->Set(dateStr.ToStdWstring().c_str());

	cell = sheet1->Cell(4, 1);
	switch (m_FileInfo.jsonData["Gender"].getInteger())
	{
		case GENDER_UNSPECIFIED:
		{
			stringStore = _("Unspecified");
		}break;
		case GENDER_MALE:
		{
			stringStore = _("Male");
		}break;
		case GENDER_FEMALE:
		{
			stringStore = _("Female");
		}break;
		default:
			stringStore = _("Unspecified");
			break;
	}
	cell->Set(stringStore.ToStdWstring().c_str());

	cell = sheet1->Cell(5, 1);
	stringStore = pd.name;
	cell->Set(stringStore.ToStdWstring().c_str());

	cell = sheet1->Cell(6, 1);
	stringStore = programData["Name"].getString();
	cell->Set(stringStore.ToStdWstring().c_str());

	sheet1->SetColWidth(0, 10000);
	sheet1->SetColWidth(1, 10000);

	// sheet 2

	auto mainColourList = sys::AppData::Instance()->GetExcelColours();
	std::vector<ExcelFormat::CellFormat> excelFormatList;
	for (auto colVal : mainColourList)
	{
		ExcelFormat::CellFormat storedFilter(fmt_mgr, font_normal.set_color_index(colVal.idx));
		excelFormatList.push_back(storedFilter);
	}

	int COL_IDX = EX_COL_A;
	row = 0;

	// set widths
	int s = m_ProgramData->GetColumnWidth(0);
	sheet2->SetColWidth(0, s * 60);

	int i = 0;
	for( i=1; i < 100; ++i )
		sheet2->SetColWidth(i, 8000);


	// column A
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(_("Legend").ToStdWstring().c_str()); // 
	cell->SetFormat(fmtBGGray);
	
	// column B
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(_("Comment").ToStdWstring().c_str()); // 
	cell->SetFormat(fmtBGGray);

	row++;
	COL_IDX = EX_COL_A;

	// print OM
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(_("OM").ToStdWstring().c_str());
	cell->SetFormat(fmtRed);

	cell = sheet2->Cell(row, COL_IDX);
	cell->Set(_("Omitted - Red text means the participant either did not respond at all or tried to respond outside the response time").ToStdWstring().c_str());

	row++;
	COL_IDX = EX_COL_A;

	// print yellow
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(123);
	cell->SetFormat(fmtBGYellow);

	cell = sheet2->Cell(row, COL_IDX);
	cell->Set(_("Yellow background with a number means the participant answered in the response time but answered incorrectly").ToStdWstring().c_str());
	row++;

	const auto filterList = sys::AppData::Instance()->GetFilters();

	for (auto filter = filterList.rbegin(); filter != filterList.rend(); ++filter) // backwards through the list
	{
		COL_IDX = EX_COL_A;

		ExcelFormat::CellFormat fmtFilter = excelFormatList[filter->second.colIndex];
			
		cell = sheet2->Cell(row, COL_IDX++);
		cell->Set(wxString::Format("%s", filter->second.name).ToStdWstring().c_str()); // filter
		cell->SetFormat(fmtFilter);

		cell = sheet2->Cell(row, COL_IDX);
		cell->Set(wxString::Format(_("Colour for words/sentences of the %s filter"), filter->second.name).ToStdWstring().c_str());

		// next row
		row++;
	}
		
	
	// start the data
	row+=3;
	COL_IDX = EX_COL_A;

	// column A
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(_("Word/Sentence content").ToStdWstring().c_str()); // Oraciones y contenido
	cell->SetFormat(fmtBGGray);

	// column B
	cell = sheet2->Cell(row, COL_IDX++);
	if (m_FileInfo.versionId == VERSION_ONE ||
		m_FileInfo.versionId == VERSION_TWO)
	{
		cell->Set(_("Number of lines in the stimuli").ToStdWstring().c_str()); // N�mero de lineas de los est�mulos
		cell->SetFormat(fmtBGGray);
	}
	else
	{
		cell->Set(_("Number of words in the stimuli").ToStdWstring().c_str()); // N�mero de palabras de los est�mulos
		cell->SetFormat(fmtBGGray);
	}

	// column C
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(_("Category of word or sentence").ToStdWstring().c_str()); // Categor�as de cada palabra u oraci�n 
	cell->SetFormat(fmtBGGray);

	// column D
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(_("First answer (key)").ToStdWstring().c_str()); // Respuesta primera (tecla)
	cell->SetFormat(fmtBGGray);

	// column E
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(_("Second answer (key)").ToStdWstring().c_str()); // Respuesta segunda (tecla)
	cell->SetFormat(fmtBGGray);

	// column F
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(_("Correct in each sentence or word (count by first response categories)").ToStdWstring().c_str()); // Aciertos en cada oraci�n o palabra (contar por categor�as respuesta primera)
	cell->SetFormat(fmtBGGray);

	// column G
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(_("Omissions in each sentence or word").ToStdWstring().c_str()); // Omisiones en cada oraci�n o palabra (OM)
	cell->SetFormat(fmtBGGray);

	// column H
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(_("TOTAL Correct (1st)").ToStdWstring().c_str()); // TOT aciertos (1�)
	cell->SetFormat(fmtBGGray);

	// column I
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(_("TOTAL OM").ToStdWstring().c_str()); // TOT OM 
	cell->SetFormat(fmtBGGray);

	// column J
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(_("TOTAL Correct (2nd)").ToStdWstring().c_str()); // TOT aciertos (2�)
	cell->SetFormat(fmtBGGray);

	// column K
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(_("(%) Correct TOTAL").ToStdWstring().c_str()); // % aciertos TOT
	cell->SetFormat(fmtBGGray);

	// column L
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(_("(%) Correct (1st) without OM").ToStdWstring().c_str()); // % AC 1� sin OM
	cell->SetFormat(fmtBGGray);

	// column M
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(_("(%) Correct (2nd) without OM").ToStdWstring().c_str()); // % AC 2� sin OM
	cell->SetFormat(fmtBGGray);

	// column N
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(_("% OM").ToStdWstring().c_str()); // % OM
	cell->SetFormat(fmtBGGray);

	// column O
	cell = sheet2->Cell(row, COL_IDX++);
	cell->Set(_("Response Time for sentence or word").ToStdWstring().c_str()); // TR oraci�n o palabra
	cell->SetFormat(fmtBGGray);

	// rest of the columns are the average response time for each filter
	// get all filters
	//const auto filterList = sys::AppData::Instance()->GetFilters();

	COL_IDX = EX_COL_P;
	for (auto filter = filterList.rbegin(); filter != filterList.rend(); ++filter) // backwards through the list
	{
		cell = sheet2->Cell(row, COL_IDX);
		cell->Set(wxString::Format(_("Average Response Time for %s"), filter->second.name).ToStdWstring().c_str()); // TR media ref
		cell->SetFormat(fmtBGGray);

		// next column
		COL_IDX++;
	}

	row += 2;

	int index = 0;
	JsonBox::Object programObj;
	JsonBox::Array programItemArr;

	// averages
	struct ResultsData
	{
		FilterData fd;

		int filterId;
		int count;
		int totalCorrectFirstAnswer;
		int totalNoResponse;
		int totalCorrectSecondAnswer;
		int totalCorrectFirstOrSecondAnswerPercent;
		int totalCorrectFirstAnswerPercent;
		int totalCorrectSecondAnswerPercent;
		int totalNoResponsePercent;
		int totalResponseTimeForFilter;
		int totalResponsesForFilter;
		//std::vector<int> listOfCorrectTimes;
		//std::vector<int> listOfIncorrectTimes;
	};
	std::map< int, ResultsData > resultsMap;


	struct RawTimeData
	{
		int filterId;

		int lineOrWordCount;

		int firstResponseValue;
		int secondResponseValue;

		int firstResponseTime;
		int secondResponseTime;
	};
	std::vector<RawTimeData> rawTimeList;

	int programRowStart = row;

	if (programData.isObject())
	{
		programObj = programData.getObject();

		if (programObj["Program"].isArray())
		{
			JsonBox::Array arr = programObj["Program"].getArray();

			auto iter = arr.begin();
			while (iter != arr.end())
			{
				COL_IDX = EX_COL_A; // reset

				if (iter->isObject())
				{
					JsonBox::Object programItem = iter->getObject();
					if (!programItem["StringData"].isNull() &&
						!programItem["FilterType"].isNull() &&
						!programItem["Index"].isNull() )
					{
						wxString strData = programItem["StringData"].getString();
						int filterId = programItem["FilterType"].getInteger();
						int lineOrWordCount = programItem["LineOrWordCount"].getInteger();
						const auto fd = sys::AppData::Instance()->FindFilter(filterId);

						bool hasFirstResponse = programItem["HasFirstResponse"].getBoolean();
						int firstResponseTime = programItem["FirstResponseTime"].getInteger();
						int firstResponseValue = programItem["FirstResponseValue"].getInteger();

						bool hasSecondResponse = programItem["HasSecondResponse"].getBoolean();
						int secondResponseTime = programItem["SecondResponseTime"].getInteger();
						int secondResponseValue = programItem["SecondResponseValue"].getInteger();

						if (resultsMap.find(filterId) == resultsMap.end())
						{
							// not found

							// insert with val 1
							ResultsData rd;
							rd.filterId = filterId;
							rd.fd = sys::AppData::Instance()->FindFilter(filterId);
							rd.count = 1;
							rd.totalCorrectFirstAnswer = 0;
							rd.totalNoResponse = 0;
							rd.totalCorrectSecondAnswer = 0;
							rd.totalCorrectFirstOrSecondAnswerPercent = 0;
							rd.totalCorrectFirstAnswerPercent = 0;
							rd.totalCorrectSecondAnswerPercent = 0;
							rd.totalNoResponsePercent = 0;
							rd.totalResponseTimeForFilter = 0;
							rd.totalResponsesForFilter = 0;
							resultsMap.insert(std::make_pair(filterId, rd));
						}
						else
						{
							// found
							ResultsData rd = resultsMap[filterId];
							rd.count++;

							resultsMap[filterId] = rd;
						}
						ExcelFormat::CellFormat fmtFilter = excelFormatList[fd.colIndex];

						// column A - word or sentence
						if (!hasFirstResponse && !hasSecondResponse)
							sheet2->Cell(row, COL_IDX)->SetFormat(fmtRed);
						else
							sheet2->Cell(row, COL_IDX)->SetFormat(fmtFilter);
						sheet2->Cell(row, COL_IDX++)->Set(strData.ToStdWstring().c_str()); 

						// column B - line or word count
						if (!hasFirstResponse && !hasSecondResponse)
							sheet2->Cell(row, COL_IDX)->SetFormat(fmtRed);
						else
							sheet2->Cell(row, COL_IDX)->SetFormat(fmtFilter);

						sheet2->Cell(row, COL_IDX++)->Set(wxString::Format("%d", lineOrWordCount).ToStdWstring().c_str()); 

						// column C - filter
						if (!hasFirstResponse && !hasSecondResponse)
							sheet2->Cell(row, COL_IDX)->SetFormat(fmtRed);
						else
							sheet2->Cell(row, COL_IDX)->SetFormat(fmtFilter);

						sheet2->Cell(row, COL_IDX++)->Set(fd.name.ToStdWstring().c_str()); 

						// column D - first response (or OM) if wrong BG is yellow
						if (hasFirstResponse)
						{
							if( firstResponseValue != lineOrWordCount )
								sheet2->Cell(row, COL_IDX)->SetFormat(fmtBGYellow);
							else
							{
								sheet2->Cell(row, COL_IDX)->SetFormat(fmtFilter);
							}
							sheet2->Cell(row, COL_IDX++)->Set(firstResponseValue);
						}
						else
						{
							if (!hasSecondResponse)
							{
								sheet2->Cell(row, COL_IDX)->SetFormat(fmtRed);
								sheet2->Cell(row, COL_IDX++)->Set("OM");
							}
							else
								sheet2->Cell(row, COL_IDX++)->Set(" ");
						}

						// column E - second response if wrong BG is yellow
						if (hasSecondResponse)
						{
							if (secondResponseValue != lineOrWordCount)
								sheet2->Cell(row, COL_IDX)->SetFormat(fmtBGYellow);
							else
							{
								sheet2->Cell(row, COL_IDX)->SetFormat(fmtFilter);
							}
							sheet2->Cell(row, COL_IDX++)->Set(secondResponseValue);
						}
						else
							sheet2->Cell(row, COL_IDX++)->Set(" ");

						// column F - 1st correct
						if (hasFirstResponse &&
							(firstResponseValue == lineOrWordCount))
						{
							sheet2->Cell(row, COL_IDX++)->Set("1");
						}
						else
						{
							sheet2->Cell(row, COL_IDX++)->Set(" ");
						}

						// column G - omission
						if (!hasFirstResponse && !hasSecondResponse)
						{
							sheet2->Cell(row, COL_IDX)->SetFormat(fmtRed);
							sheet2->Cell(row, COL_IDX++)->Set("1");
						}
						else
						{
							sheet2->Cell(row, COL_IDX++)->Set(" ");
						}

						// column H - total correct (1st answer)
						if (resultsMap.find(filterId) != resultsMap.end())
						{
							// found
							ResultsData rd = resultsMap[filterId];

							if (hasFirstResponse &&
								(firstResponseValue == lineOrWordCount))
							{
								sheet2->Cell(row, COL_IDX)->SetFormat(fmtFilter);

								rd.totalCorrectFirstAnswer++;
								sheet2->Cell(row, COL_IDX++)->Set(wxString::Format("%d", rd.totalCorrectFirstAnswer).ToStdWstring().c_str());
							}
							else
								sheet2->Cell(row, COL_IDX++)->Set(" ");

							resultsMap[filterId] = rd;
						}

						// column I - total ommisions
						if (resultsMap.find(filterId) != resultsMap.end())
						{
							// found
							ResultsData rd = resultsMap[filterId];

							if (!hasFirstResponse)
							{
								sheet2->Cell(row, COL_IDX)->SetFormat(fmtFilter);

								rd.totalNoResponse++;
								sheet2->Cell(row, COL_IDX++)->Set(wxString::Format("%d", rd.totalNoResponse).ToStdWstring().c_str());
							}
							else
							{
								sheet2->Cell(row, COL_IDX++)->Set(" ");
							}

							resultsMap[filterId] = rd;
						}

						// column J - total correct (2nd answer)
						if (resultsMap.find(filterId) != resultsMap.end())
						{
							// found
							ResultsData rd = resultsMap[filterId];

							if (hasSecondResponse)
							{

								sheet2->Cell(row, COL_IDX)->SetFormat(fmtFilter);

								if (secondResponseValue == lineOrWordCount)
								{
									sheet2->Cell(row, COL_IDX++)->Set("1");
									rd.totalCorrectSecondAnswer++;
								}
								else
									sheet2->Cell(row, COL_IDX++)->Set("0");

								//sheet2->Cell(row, COL_IDX++)->Set(wxString::Format("%d", rd.totalCorrectSecondAnswer).ToStdWstring().c_str());
								//rd.totalCorrectSecondAnswer++;
							}
							else
							{
								sheet2->Cell(row, COL_IDX++)->Set(" ");
							}

							resultsMap[filterId] = rd;
						}

						// column K - percent correct (1st or 2nd) total
						if (resultsMap.find(filterId) != resultsMap.end())
						{
							// found
							ResultsData rd = resultsMap[filterId];
								
							sheet2->Cell(row, COL_IDX)->SetFormat(fmtFilter);

							if (hasFirstResponse)	
							{
								if (firstResponseValue == lineOrWordCount)
								{
									rd.totalCorrectFirstOrSecondAnswerPercent++;
									sheet2->Cell(row, COL_IDX++)->Set(wxString::Format("%d", rd.totalCorrectFirstOrSecondAnswerPercent).ToStdWstring().c_str());
								}
								else
								{
									sheet2->Cell(row, COL_IDX++)->Set("0");
								}
							}
							else
								sheet2->Cell(row, COL_IDX++)->Set("0");

							resultsMap[filterId] = rd;
						}

						// column L - percent correct 1st without omission
						if (resultsMap.find(filterId) != resultsMap.end())
						{
							// found
							ResultsData rd = resultsMap[filterId];

							if (hasFirstResponse)
							{
								sheet2->Cell(row, COL_IDX)->SetFormat(fmtFilter);

								if (firstResponseValue == lineOrWordCount)
								{
									rd.totalCorrectFirstAnswerPercent++;
									sheet2->Cell(row, COL_IDX++)->Set(wxString::Format("%d", rd.totalCorrectFirstAnswerPercent).ToStdWstring().c_str());
								}
								else
									sheet2->Cell(row, COL_IDX++)->Set("0");
							}
							else
								sheet2->Cell(row, COL_IDX++)->Set(" ");

							resultsMap[filterId] = rd;
						}

						// column M - percent correct 2nd without omission
						if (resultsMap.find(filterId) != resultsMap.end())
						{
							// found
							ResultsData rd = resultsMap[filterId];

							if (hasSecondResponse)
							{
								sheet2->Cell(row, COL_IDX)->SetFormat(fmtFilter);

								if (secondResponseValue == lineOrWordCount)
								{
									rd.totalCorrectSecondAnswerPercent++;
									sheet2->Cell(row, COL_IDX++)->Set(wxString::Format("%d", rd.totalCorrectSecondAnswerPercent).ToStdWstring().c_str());
								}
								else
									sheet2->Cell(row, COL_IDX++)->Set("0");
							}
							else
							{
								sheet2->Cell(row, COL_IDX++)->Set(" ");
							}

							resultsMap[filterId] = rd;
						}

						// column N - percent ommisions
						if (resultsMap.find(filterId) != resultsMap.end())
						{
							// found
							ResultsData rd = resultsMap[filterId];

							if (!hasFirstResponse)
							{
								sheet2->Cell(row, COL_IDX)->SetFormat(fmtFilter);

								rd.totalNoResponsePercent++;
								sheet2->Cell(row, COL_IDX++)->Set(wxString::Format("%d", rd.totalNoResponsePercent).ToStdWstring().c_str());
								
							}
							else
							{
								if( firstResponseValue != lineOrWordCount )
									sheet2->Cell(row, COL_IDX++)->Set("0");
								else
									sheet2->Cell(row, COL_IDX++)->Set(" ");
							}

							resultsMap[filterId] = rd;
						}

						// column O - response time
						if (resultsMap.find(filterId) != resultsMap.end())
						{
							// found
							ResultsData rd = resultsMap[filterId];

							if (hasFirstResponse || hasSecondResponse)
							{
								sheet2->Cell(row, COL_IDX)->SetFormat(fmtFilter);

								if (firstResponseValue == lineOrWordCount)
								{
									sheet2->Cell(row, COL_IDX++)->Set(wxString::Format("%d", firstResponseTime).ToStdWstring().c_str());
								}
								else if (secondResponseValue == lineOrWordCount)
								{
									sheet2->Cell(row, COL_IDX++)->Set(wxString::Format("%d", secondResponseTime).ToStdWstring().c_str());
								}
								else
								{
									sheet2->Cell(row, COL_IDX++)->Set(" ");
								}
							}
							else
							{
								sheet2->Cell(row, COL_IDX++)->Set(" ");
							}
						}

						RawTimeData rtd;
						rtd.filterId = filterId;
						rtd.firstResponseValue = firstResponseValue;
						rtd.secondResponseValue = secondResponseValue;
						rtd.firstResponseTime = firstResponseTime;
						rtd.secondResponseTime = secondResponseTime;
						rtd.lineOrWordCount = lineOrWordCount;
						rawTimeList.push_back(rtd);

						index++;
					}
				}
				// next
				++iter;

				row++;
			}
		}
	}

	// calc the averages
	COL_IDX = EX_COL_P; // column P
	for (auto filter = filterList.rbegin(); filter != filterList.rend(); ++filter)
	{
		int avRow = programRowStart;

		for (auto rawTimeInfo : rawTimeList)
		{
			if (rawTimeInfo.filterId == filter->second.val)
			{
				if (resultsMap.find(rawTimeInfo.filterId) != resultsMap.end())
				{
					// found
					ResultsData rd = resultsMap[rawTimeInfo.filterId];

					ExcelFormat::CellFormat fmtFilter = excelFormatList[filter->second.colIndex];

					if (rawTimeInfo.firstResponseValue == rawTimeInfo.lineOrWordCount)
					{
						rd.totalResponseTimeForFilter += rawTimeInfo.firstResponseTime;
						rd.totalResponsesForFilter++;
						sheet2->Cell(avRow, COL_IDX)->SetFormat(fmtFilter);
						sheet2->Cell(avRow, COL_IDX)->Set(wxString::Format("%d (+%d)", rd.totalResponseTimeForFilter, rawTimeInfo.firstResponseTime).ToStdWstring().c_str());
					}
					else if (rawTimeInfo.secondResponseValue == rawTimeInfo.lineOrWordCount)
					{
						rd.totalResponseTimeForFilter += rawTimeInfo.secondResponseTime;
						rd.totalResponsesForFilter++;
						sheet2->Cell(avRow, COL_IDX)->SetFormat(fmtFilter);
						sheet2->Cell(avRow, COL_IDX)->Set(wxString::Format("%d (+%d)", rd.totalResponseTimeForFilter, rawTimeInfo.secondResponseTime).ToStdWstring().c_str());
					}
					else
					{
						sheet2->Cell(avRow, COL_IDX)->SetFormat(fmtFilter);
						sheet2->Cell(avRow, COL_IDX)->Set(" ");
					}

					resultsMap[rawTimeInfo.filterId] = rd;
				}
			}

			avRow++;
		}

		COL_IDX++;
	}

	// now do full calcs 
	int rowForTotals = row;

	for (auto filter = filterList.rbegin(); filter != filterList.rend(); ++filter)
	{
		COL_IDX = EX_COL_H; // column H
		
		ExcelFormat::CellFormat fmtFilter = excelFormatList[filter->second.colIndex];

		// column H 
		if (resultsMap.find(filter->second.val) != resultsMap.end())
		{
			ResultsData rd = resultsMap[filter->second.val];

			sheet2->Cell(rowForTotals, COL_IDX)->SetFormat(fmtFilter);
			sheet2->Cell(rowForTotals, COL_IDX)->Set(wxString::Format("%s = %d", rd.fd.name, rd.totalCorrectFirstAnswer).ToStdWstring().c_str());
		}

		// column I
		COL_IDX++;
		if (resultsMap.find(filter->second.val) != resultsMap.end())
		{
			ResultsData rd = resultsMap[filter->second.val];

			sheet2->Cell(rowForTotals, COL_IDX)->SetFormat(fmtFilter);
			sheet2->Cell(rowForTotals, COL_IDX)->Set(wxString::Format("%s = %d", rd.fd.name, rd.totalNoResponse).ToStdWstring().c_str());
		}

		// column J
		COL_IDX++;
		if (resultsMap.find(filter->second.val) != resultsMap.end())
		{
			ResultsData rd = resultsMap[filter->second.val];

			sheet2->Cell(rowForTotals, COL_IDX)->SetFormat(fmtFilter);
			sheet2->Cell(rowForTotals, COL_IDX)->Set(wxString::Format("%s = %d", rd.fd.name, rd.totalCorrectSecondAnswer).ToStdWstring().c_str());
		}

		// column K
		COL_IDX++;
		if (resultsMap.find(filter->second.val) != resultsMap.end())
		{
			ResultsData rd = resultsMap[filter->second.val];
			float percent = 0.0f;
			if( rd.count != 0 )
				percent = (float)(std::abs(100.0f * ((float)rd.totalCorrectFirstOrSecondAnswerPercent / (float)rd.count)));

			sheet2->Cell(rowForTotals, COL_IDX)->SetFormat(fmtFilter);
			sheet2->Cell(rowForTotals, COL_IDX)->Set(wxString::Format("%s = %.2f%%", rd.fd.name, percent).ToStdWstring().c_str());
		}

		// column L
		COL_IDX++;
		if (resultsMap.find(filter->second.val) != resultsMap.end())
		{
			ResultsData rd = resultsMap[filter->second.val];
			float percent = 0.0f;
			if (rd.count != 0)
				percent = (float)(std::abs(100.0f * ((float)rd.totalCorrectFirstAnswerPercent / (float)rd.count)));

			sheet2->Cell(rowForTotals, COL_IDX)->SetFormat(fmtFilter);
			sheet2->Cell(rowForTotals, COL_IDX)->Set(wxString::Format("%s = %.2f%%", rd.fd.name, percent).ToStdWstring().c_str());
		}

		// column M
		COL_IDX++;
		if (resultsMap.find(filter->second.val) != resultsMap.end())
		{
			ResultsData rd = resultsMap[filter->second.val];
			float percent = 0.0f;
			if (rd.count != 0)
				percent = (float)(std::abs(100.0f * ((float)rd.totalCorrectSecondAnswerPercent / (float)rd.count)));

			sheet2->Cell(rowForTotals, COL_IDX)->SetFormat(fmtFilter);
			sheet2->Cell(rowForTotals, COL_IDX)->Set(wxString::Format("%s = %.2f%%", rd.fd.name, percent).ToStdWstring().c_str());
		}

		// column N
		COL_IDX++;
		if (resultsMap.find(filter->second.val) != resultsMap.end())
		{
			ResultsData rd = resultsMap[filter->second.val];
			float percent = 0.0f;
			if (rd.count != 0)
				percent = (float)(std::abs(100.0f * ((float)rd.totalNoResponsePercent / (float)rd.count)));

			sheet2->Cell(rowForTotals, COL_IDX)->SetFormat(fmtFilter);
			sheet2->Cell(rowForTotals, COL_IDX)->Set(wxString::Format("%s = %.2f%%", rd.fd.name, percent).ToStdWstring().c_str());
		}

		rowForTotals++;
	}

	// move back up the rows of each filter 
	rowForTotals = row;

	// now calc each filter 
	COL_IDX = EX_COL_P;
	for (auto filter = filterList.rbegin(); filter != filterList.rend(); ++filter) // backwards through the list
	{
		if (resultsMap.find(filter->second.val) != resultsMap.end()) 
		{
			ExcelFormat::CellFormat fmtFilter = excelFormatList[filter->second.colIndex];

			ResultsData rd = resultsMap[filter->second.val];
			float av = 0.0f;
			if (rd.totalResponsesForFilter != 0)
				av = (float)(std::abs(((float)rd.totalResponseTimeForFilter / (float)rd.totalResponsesForFilter)));

			sheet2->Cell(rowForTotals, COL_IDX)->SetFormat(fmtFilter);
			sheet2->Cell(rowForTotals, COL_IDX)->Set(wxString::Format("%s = %.2f", rd.fd.name, av).ToStdWstring().c_str());
		}

		// next column and next row
		COL_IDX++;
		rowForTotals++;
	}

	// store the row changes this time
	row = rowForTotals;

	// move down two rows to separate data
	row+=2;

	fmtBGGray.set_alignment(ExcelFormat::EXCEL_HALIGN_RIGHT| ExcelFormat::EXCEL_VALIGN_CENTRED);

	sheet2->Cell(row, 0)->Set(_("Time to Respond:").ToStdWstring().c_str());
	sheet2->Cell(row, 0)->SetFormat(fmtBGGray);
	sheet2->Cell(row, 1)->Set(programData["TimeToRespond"].getInteger());
	row++;
	sheet2->Cell(row, 0)->Set(_("Response Timeout:").ToStdWstring().c_str());
	sheet2->Cell(row, 0)->SetFormat(fmtBGGray);
	sheet2->Cell(row, 1)->Set(programData["TimeResponseTimeout"].getInteger());
	row++;
	sheet2->Cell(row, 0)->Set(_("Interval:").ToStdWstring().c_str());
	sheet2->Cell(row, 0)->SetFormat(fmtBGGray);
	sheet2->Cell(row, 1)->Set(programData["TimeInterval"].getInteger());
	row++;
	sheet2->Cell(row, 0)->Set(_("Time Between Filter Changes:").ToStdWstring().c_str());
	sheet2->Cell(row, 0)->SetFormat(fmtBGGray);
	sheet2->Cell(row, 1)->Set(programData["TimeBetweenFilterChanges"].getInteger());

	row += 2;

	// comments
	wxString commentStr;
	commentStr = m_FileInfo.jsonData["Answers"].getString();
	sheet2->Cell(row, 0)->Set(_("User Answers:").ToStdWstring().c_str());
	sheet2->Cell(row, 0)->SetFormat(fmtBGGray);
	sheet2->Cell(row, 1)->Set(commentStr.ToStdWstring().c_str());
	
	row += 2;

	fmtBGGray.set_alignment(ExcelFormat::EXCEL_HALIGN_LEFT | ExcelFormat::EXCEL_VALIGN_CENTRED);

	sheet2->Cell(row, 1)->Set(_("Category").ToStdWstring().c_str());
	sheet2->Cell(row, 1)->SetFormat(fmtBGGray);
	sheet2->Cell(row, 2)->Set(_("Shown").ToStdWstring().c_str());
	sheet2->Cell(row, 2)->SetFormat(fmtBGGray);
	sheet2->Cell(row, 3)->Set(_("Remembered").ToStdWstring().c_str());
	sheet2->Cell(row, 3)->SetFormat(fmtBGGray);
	sheet2->Cell(row, 4)->Set(_("Percent").ToStdWstring().c_str());
	sheet2->Cell(row, 4)->SetFormat(fmtBGGray);

	row+=2;

	// answer details
	if (!m_FileInfo.jsonData["AnswerCounts"].isNull() &&
		m_FileInfo.jsonData["AnswerCounts"].isArray())
	{
		JsonBox::Array arr = m_FileInfo.jsonData["AnswerCounts"].getArray();

		auto iter = arr.begin();
		while (iter != arr.end())
		{
			if (iter->isObject())
			{
				JsonBox::Object answerItem = iter->getObject();
				if (!answerItem["FilterType"].isNull() &&
					!answerItem["ShownCount"].isNull() &&
					!answerItem["RememberedCount"].isNull())
				{
					int filterId = answerItem["FilterType"].getInteger();
					int shownCount = answerItem["ShownCount"].getInteger();
					int rememberedCount = answerItem["RememberedCount"].getInteger();

					auto fd = sys::AppData::Instance()->FindFilter(filterId);

					ExcelFormat::CellFormat fmtFilter = excelFormatList[fd.colIndex];

					sheet2->Cell(row, 1)->SetFormat(fmtFilter);
					sheet2->Cell(row, 2)->SetFormat(fmtFilter);
					sheet2->Cell(row, 3)->SetFormat(fmtFilter);
					sheet2->Cell(row, 4)->SetFormat(fmtFilter);

					sheet2->Cell(row, 1)->Set(wxString::Format("%s", fd.name).ToStdWstring().c_str());
					sheet2->Cell(row, 2)->Set(wxString::Format("%d", shownCount).ToStdWstring().c_str());
					sheet2->Cell(row, 3)->Set(wxString::Format("%d", rememberedCount).ToStdWstring().c_str());

					if (shownCount == 0)
					{
						sheet2->Cell(row, 4)->Set(wxString::Format("0%%").ToStdWstring().c_str());
					}
					else
					{
						float percent = 0.0f;
						if( shownCount != 0 )
							percent = (float)(std::abs(100.0f * ((float)rememberedCount / (float)shownCount)));

						sheet2->Cell(row, 4)->Set(wxString::Format("%.2f%%", percent).ToStdWstring().c_str());
					}
					index++;
				}
			}
			// next
			++iter;
			row++;
		}
	}

	row += 2;

	fmtBGGray.set_alignment(ExcelFormat::EXCEL_HALIGN_RIGHT | ExcelFormat::EXCEL_VALIGN_CENTRED);

	commentStr = m_FileInfo.jsonData["Comments"].getString();
	sheet2->Cell(row, 0)->Set(_("Admin Comments:").ToStdWstring().c_str());
	sheet2->Cell(row, 0)->SetFormat(fmtBGGray);
	sheet2->Cell(row, 1)->Set(commentStr.ToStdWstring().c_str());
	row++;

	// SPSS data
	row += 2;

	// colour test
	//for (i = 0; i < 64; ++i)
	//{
	//	fmtBGGray.set_background(MAKE_COLOR2(i, 0));	// solid grey background
	//
	//	sheet2->Cell(row+i, 0)->SetFormat(fmtBGGray);
	//	sheet2->Cell(row+i, 0)->Set(wxString::Format("%d", i).ToStdWstring().c_str());
	//}
	//row+=64;

	COL_IDX = EX_COL_A;

	sheet2->Cell(row, COL_IDX)->SetFormat(fmtBGGray);
	sheet2->Cell(row, COL_IDX)->Set(_("SPSS").ToStdWstring().c_str());

	row += 1;

	fmtBGGray.set_alignment(ExcelFormat::EXCEL_HALIGN_LEFT | ExcelFormat::EXCEL_VALIGN_CENTRED);

	sheet2->Cell(row, COL_IDX)->SetFormat(fmtBGGray);
	sheet2->Cell(row, COL_IDX)->Set(_("Participant ID").ToStdWstring().c_str());
	sheet2->Cell(row+1, COL_IDX)->Set(wxString::Format("%s", m_FileInfo.jsonData["ID"].getString()).ToStdWstring().c_str());

	// now fill the titles
	int offsetVal = (int)filterList.size();

	COL_IDX = EX_COL_B;
	int filterOffset = 0;

	for (auto filter = filterList.rbegin(); filter != filterList.rend(); ++filter) // backwards through the list
	{
		bool validFilterData = false;
		ResultsData rd;
		float percent = 0.0f;
		
		if (resultsMap.find(filter->second.val) != resultsMap.end())
		{
			validFilterData = true;
			rd = resultsMap[filter->second.val];
		}
			
		sheet2->Cell(row, COL_IDX)->SetFormat(fmtBGGray);
		sheet2->Cell(row, COL_IDX)->Set(wxString::Format(_("Correct 1st Response %s"), filter->second.name).ToStdWstring().c_str());

		// H
		if(validFilterData)
			sheet2->Cell(row+1, COL_IDX)->Set(wxString::Format("%d", rd.totalCorrectFirstAnswer).ToStdWstring().c_str());

		// next column 
		COL_IDX += offsetVal;

		sheet2->Cell(row, COL_IDX)->SetFormat(fmtBGGray);
		sheet2->Cell(row, COL_IDX)->Set(wxString::Format(_("TOT OM %s"), filter->second.name).ToStdWstring().c_str());

		// I
		if (validFilterData)
			sheet2->Cell(row+1, COL_IDX)->Set(wxString::Format("%d", rd.totalNoResponse).ToStdWstring().c_str());

		// next column
		COL_IDX += offsetVal;

		sheet2->Cell(row, COL_IDX)->SetFormat(fmtBGGray);
		sheet2->Cell(row, COL_IDX)->Set(wxString::Format(_("Correct 2nd Response %s"), filter->second.name).ToStdWstring().c_str());

		// J
		if (validFilterData)
			sheet2->Cell(row+1, COL_IDX)->Set(wxString::Format("%d", rd.totalCorrectSecondAnswer).ToStdWstring().c_str());

		// next column
		COL_IDX += offsetVal;

		sheet2->Cell(row, COL_IDX)->SetFormat(fmtBGGray);
		sheet2->Cell(row, COL_IDX)->Set(wxString::Format(_("%% Correct Total %s"), filter->second.name).ToStdWstring().c_str());

		// K
		if (validFilterData)
		{
			if( rd.count != 0 )
				percent = (float)(std::abs(100.0f * ((float)rd.totalCorrectFirstOrSecondAnswerPercent / (float)rd.count)));

			sheet2->Cell(row + 1, COL_IDX)->Set(wxString::Format("%.2f%%", percent).ToStdWstring().c_str());
		}

		// next column
		COL_IDX += offsetVal;

		sheet2->Cell(row, COL_IDX)->SetFormat(fmtBGGray);
		sheet2->Cell(row, COL_IDX)->Set(wxString::Format(_("%% 1st Response no OM %s"), filter->second.name).ToStdWstring().c_str());

		// L
		if (validFilterData)
		{
			if (rd.count != 0)
				percent = (float)(std::abs(100.0f * ((float)rd.totalCorrectFirstAnswerPercent / (float)rd.count)));
			sheet2->Cell(row + 1, COL_IDX)->Set(wxString::Format("%.2f%%", percent).ToStdWstring().c_str());
		}

		// next column
		COL_IDX += offsetVal;

		sheet2->Cell(row, COL_IDX)->SetFormat(fmtBGGray);
		sheet2->Cell(row, COL_IDX)->Set(wxString::Format(_("%% 2nd Response no OM %s"), filter->second.name).ToStdWstring().c_str());

		// M
		if (validFilterData)
		{
			if (rd.count != 0)
				percent = (float)(std::abs(100.0f * ((float)rd.totalCorrectSecondAnswerPercent / (float)rd.count)));
			sheet2->Cell(row + 1, COL_IDX)->Set(wxString::Format("%.2f%%", percent).ToStdWstring().c_str());
		}

		// next column
		COL_IDX += offsetVal;

		sheet2->Cell(row, COL_IDX)->SetFormat(fmtBGGray);
		sheet2->Cell(row, COL_IDX)->Set(wxString::Format(_("%% OM %s"), filter->second.name).ToStdWstring().c_str());

		// N
		if (validFilterData)
		{
			if (rd.count != 0)
				percent = (float)(std::abs(100.0f * ((float)rd.totalNoResponsePercent / (float)rd.count)));
			sheet2->Cell(row + 1, COL_IDX)->Set(wxString::Format("%.2f%%", percent).ToStdWstring().c_str());
		}
		// next column
		COL_IDX += offsetVal;

		sheet2->Cell(row, COL_IDX)->SetFormat(fmtBGGray);
		sheet2->Cell(row, COL_IDX)->Set(wxString::Format(_("Mean Response Time %s"), filter->second.name).ToStdWstring().c_str());

		if (validFilterData)
		{
			float av = 0.0f;
			if( rd.totalResponsesForFilter != 0)
				av = (float)(std::abs(((float)rd.totalResponseTimeForFilter / (float)rd.totalResponsesForFilter)));
			sheet2->Cell(row + 1, COL_IDX)->Set(wxString::Format("%.2f", av).ToStdWstring().c_str());
		}

		filterOffset++;

		COL_IDX = EX_COL_B + filterOffset;
	}
	
	// aciertos 1�R REF	
	// aciertos 1�R NEU	
	// TOT OM REF	
	// TOT OM NEU	
	// aciertos 2�R REF	
	// aciertos 2�R NEU	
	// % aciertos totales REF	
	// % aciertos totales NEU	
	// % aciertos 1�R no OM REF	
	// % aciertos 1�R no OM NEU	
	// % aciertos 2�R no OM REF	
	// % aciertos 2�R no OM NEU	
	// % OM REF	
	// % OM NEU	
	// mean TR REF	
	// mean TR NEU

	// now save
	xls.SaveAs(saveFileDialog.GetPath().ToStdWstring().c_str());
}
