/**
	TECS
	DlgAddEditBank.cpp
    Purpose: Handles all the callbacks for adding or editing words/sentences in the bank
*/

#include "DlgAddEditBank.h"
#include "System.h"

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
DlgAddEditBank::DlgAddEditBank( wxWindow* parent )
:
AddEditBankDlg( parent )
{
	// make sure all variables are clear
	m_ComboFilter->Clear();
	m_FilterMap.clear();

	// add the current filters to the combo box
	const auto& filterMap = sys::AppData::Instance()->GetFilters();
	for (auto val : filterMap)
	{
		int index = m_ComboFilter->GetCount();

		m_ComboFilter->Insert(val.second.name, index);

		// make this map match the selection box
		m_FilterMap.insert(std::make_pair(val.second.val, index));
	}
		
	m_ComboFilter->SetSelection(0);
}

/////////////////////////////////////////////////////
/// Apply
/// Desc: Called manually after construction to populate or modify UI elements for either ADD-ing or EDIT-ing
/////////////////////////////////////////////////////
void DlgAddEditBank::Apply()
{
	if (m_Mode == MODE_ADD) // mode is set to add
	{
		// set initial data for ADD
		this->SetTitle(_("Add To Bank"));
		m_BtnAddEdit->SetLabelText(_("Add"));

		m_ComboFilter->SetSelection(0);
	}
	else if (m_Mode == MODE_EDIT) // mode is set to edit
	{
		// set all UI elements to the correct state to edit this word/sentence
		const BankData& bd = sys::AppData::Instance()->GetFromBank(m_String);

		m_InputWordOrSentence->SetValue(m_String);

		std::wstring str = m_String.ToStdWstring();

		// match the bank index to the correct
		auto properIndex = m_FilterMap.find(bd.filterIndex);
		if (properIndex != m_FilterMap.end())
			m_ComboFilter->SetSelection(properIndex->second);
		else
			wxASSERT(0); // filter doesn't exist

		std::size_t wordCount = sys::WordCount(str);

		if (m_String.size() == 0)
			m_TxtMessage->SetLabel("");
		else
			m_TxtMessage->SetLabel(wxString::Format(_("Word Count %d"), (int)wordCount));

		this->SetTitle(_("Edit Existing"));
		m_BtnAddEdit->SetLabelText(_("Update"));
	}

	CheckValues();
}

/////////////////////////////////////////////////////
/// CheckValues
/// Desc: Checks the bank for pre-existing word/sentence and updates the UI
/////////////////////////////////////////////////////
void DlgAddEditBank::CheckValues()
{
	// get the current word/sentence
	wxString str = m_InputWordOrSentence->GetValue();

	auto currentBank = sys::AppData::Instance()->GetBank();
	auto search = currentBank.find(str);

	if (m_Mode == MODE_ADD)
	{
		// we are adding this to the bank
		if (search == currentBank.end())
		{
			// doesn't exist, safe to be added
			std::wstring strCounter = str.ToStdWstring();
			std::size_t wordCount = sys::WordCount(strCounter);

			if (str.size() == 0)
			{
				m_TxtMessage->SetLabel("");

				m_BtnAddEdit->Disable();
			}
			else
			{
				m_TxtMessage->SetLabel(wxString::Format(_("Word Count %d"), (int)wordCount));

				m_BtnAddEdit->Enable();
			}
		}
		else 
		{
			// already exists
			m_TxtMessage->SetLabel(_("Word or Sentence already exists in the bank"));

			m_BtnAddEdit->Disable();
		}
	}
	else if (m_Mode == MODE_EDIT)
	{
		// editing a previous existing word/sentence
		if (search == currentBank.end())
		{
			// doesn't exist, safe to be added
			std::wstring strCounter = str.ToStdWstring();
			std::size_t wordCount = sys::WordCount(strCounter);

			if (str.size() == 0)
			{
				m_TxtMessage->SetLabel("");

				m_BtnAddEdit->Disable();
			}
			else
			{
				m_TxtMessage->SetLabel(wxString::Format(_("Word Count %d"), (int)wordCount));

				m_BtnAddEdit->Enable();
			}
		}
		else
		{
			// already exists
			if (m_OriginalString != str)
			{
				m_TxtMessage->SetLabel(_("Word or Sentence already exists in the bank"));

				m_BtnAddEdit->Disable();
			}
			else
			{
				std::wstring strCounter = str.ToStdWstring();
				std::size_t wordCount = sys::WordCount(strCounter);

				m_TxtMessage->SetLabel(wxString::Format(_("Word Count %d"), (int)wordCount));

				m_BtnAddEdit->Enable();
			}
		}
	}

	// refresh the info text
	bMessageSizer->Layout();
}

/////////////////////////////////////////////////////
/// KeyUpEvent
/// Desc: Key event for the input box (will check the bank for existing)
/////////////////////////////////////////////////////
void DlgAddEditBank::KeyUpEvent(wxKeyEvent& WXUNUSED(event))
{
	CheckValues();
}

/////////////////////////////////////////////////////
/// BtnCancelClick
/// Desc: Button callback for cancelling the dialog (nothing saved/changed)
/////////////////////////////////////////////////////
void DlgAddEditBank::BtnCancelClick(wxCommandEvent& WXUNUSED(event))
{
	this->EndModal(RESPONSE_CANCEL);
}

/////////////////////////////////////////////////////
/// BtnAddEditClick
/// Desc: Button callback for adding to or updating the bank
/////////////////////////////////////////////////////
void DlgAddEditBank::BtnAddEditClick(wxCommandEvent& WXUNUSED(event))
{
	if (m_Mode == MODE_ADD)
	{
		// the string will be added to the bank
		wxString str = m_InputWordOrSentence->GetValue();


		// get the type
		wxString selectionStr = m_ComboFilter->GetStringSelection();

		int filterId = 0;

		if (selectionStr.length() == 0)
			filterId = 0;
		else
		{
			const FilterData& fd = sys::AppData::Instance()->FindFilter(selectionStr);

			if( fd.val != -1)
				filterId = fd.val;
		}

		// create a new BankData object
		BankData bd;
		bd.str = str;
		bd.filterIndex = filterId;
		bd.wordCount = sys::WordCount(str.ToStdWstring());
		bd.hasGUID = false;
		bd.guid = "";

		// add
		sys::AppData::Instance()->AddToBank(bd);
	}
	else if (m_Mode == MODE_EDIT)
	{
		wxString str = m_InputWordOrSentence->GetValue();

		wxString selectionStr = m_ComboFilter->GetStringSelection();

		int filterId = 0;

		if (selectionStr.length() == 0)
			filterId = 0;
		else
		{
			const FilterData& fd = sys::AppData::Instance()->FindFilter(selectionStr);

			if (fd.val != -1)
				filterId = fd.val;
		}

		// create a new BankData object to replace the old value
		BankData bd;
		bd.str = str;
		bd.filterIndex = filterId;
		bd.wordCount = sys::WordCount(str.ToStdWstring());
		bd.hasGUID = false;
		bd.guid = "";

		// update the bank
		sys::AppData::Instance()->UpdateBank(m_OriginalString, bd);

		m_String = str;
	}

	// close the dialog
	this->EndModal(RESPONSE_OK);
}
