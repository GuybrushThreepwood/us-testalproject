
/**
	TECS
	DlgCreateEditPrograms.cpp
    Purpose: Dialog for managing the creation and editing of test programs
*/

#include "DlgCreateEditPrograms.h"
#include "DlgAddEditBank.h"
#include "DlgAddEditWithChildrenBank.h"
#include "DlgBankImport.h"
#include "System.h"

namespace
{
	const int DEFAULT_VAL = 12;
	const int SELECT_ACTIVE_COLUMN = 3;
	bool allowSelection = false;
}

/////////////////////////////////////////////////////
/// Constructor
/////////////////////////////////////////////////////
DlgCreateEditPrograms::DlgCreateEditPrograms(wxWindow* parent)
:
CreateEditProgramsDlg( parent )
{

}

/////////////////////////////////////////////////////
/// EventInitDialog
/// Desc: Called after construction to populate or modify UI elements
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::EventInitDialog(wxInitDialogEvent& WXUNUSED(event))
{
	m_PreviousSelection = -1;
	m_PreviousBankSelection = -1;
	m_UseTimeDefaults = true;

	m_TimeToRespond = -1;
	m_ResponseTimeout = -1;
	m_Interval = -1;
	m_TimeBetweenChanges = -1;

	m_WordCountMin = -1;
	m_WordCountMax = -1;
	m_LinesCountMin = -1;
	m_LinesCountMax = -1;

	//m_OrderList->InsertColumn(0, _("Block #"));
	m_OrderList->SetColLabelValue(0, _("Info"));

	// disable cell highlight
	m_OrderList->SetCellHighlightPenWidth(0);
	m_OrderList->SetCellHighlightROPenWidth(0);

	// only allow row selection
	m_OrderList->SetSelectionMode(wxGrid::wxGridSelectRows);
	m_OrderList->UnsetSortingColumn();

	CreateOrderData();

	m_BankSelectionMap.clear();

	if (!allowSelection)
	{
		m_WordBankGrid->DeleteCols(SELECT_ACTIVE_COLUMN, 1, true);

		bBankFilterBox->Clear(true);
		bBankPanelRootSizer->Remove(bBankFilterBox);
		bBankPanelRootSizer->Layout();
	}

	///// BANK

	// only allow row selection
	m_WordBankGrid->SetSelectionMode(wxGrid::wxGridSelectRows);

	// labels
	m_WordBankGrid->SetColLabelValue(0, _("Word/Sentence"));
	m_WordBankGrid->SetColLabelValue(1, _("Filter"));
	m_WordBankGrid->SetColLabelValue(2, _("Number of words"));

	if (allowSelection)
	{
		m_WordBankGrid->SetColLabelValue(3, _("Active"));
		m_WordBankGrid->SetColFormatBool(SELECT_ACTIVE_COLUMN);
	}

	// sort by filter
	m_WordBankGrid->SetSortingColumn(1);

	// disable cell highlight
	m_WordBankGrid->SetCellHighlightPenWidth(0);
	m_WordBankGrid->SetCellHighlightROPenWidth(0);

	m_CreateEditTabs->SetSelection(0);

	m_WordBankGrid->Connect(wxID_ANY, wxEVT_KEY_DOWN, wxKeyEventHandler(DlgCreateEditPrograms::CopyPasteData), (wxObject*)NULL, this);
	m_WordBankGrid->GetGridWindow()->Connect(wxEVT_RIGHT_UP, wxMouseEventHandler(DlgCreateEditPrograms::RightClick), NULL, this);

	m_OrderingDynamic.clear();

	// dynamically create block data for available filters in the ordering page
	auto filterMap = sys::AppData::Instance()->GetFilters();
	for (auto iter = filterMap.begin(); iter != filterMap.end();)
	{
		StoredBlockData blockData;

		blockData.fd = iter->second;

		blockData.filterboxSizer = new wxBoxSizer(wxHORIZONTAL);

		blockData.activeCheckbox = new wxCheckBox(m_BlockDetailScroll, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0);
		blockData.activeCheckbox->Connect(wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler(DlgCreateEditPrograms::BlockCheckBox), (wxObject*)NULL, this);

		blockData.filterboxSizer->Add(blockData.activeCheckbox, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

		blockData.filterName = new wxStaticText(m_BlockDetailScroll, wxID_ANY, blockData.fd.name, wxDefaultPosition, wxDefaultSize, 0);
		blockData.filterName->Wrap(-1);
		blockData.filterboxSizer->Add(blockData.filterName, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

		blockData.spinValueControl = new wxSpinCtrl(m_BlockDetailScroll, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 10, 0);
		blockData.spinValueControl->Connect(wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler(DlgCreateEditPrograms::BlockCountChange), NULL, this);
		blockData.spinValueControl->Connect(wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler(DlgCreateEditPrograms::BlockCountChangeTxt), NULL, this);

		blockData.filterboxSizer->Add(blockData.spinValueControl, 1, wxALL | wxEXPAND, 5);

		// add
		bFilterWindowSizer->Add(blockData.filterboxSizer, 0, wxEXPAND, 5);

		blockData.filterInfoboxSizer = new wxBoxSizer(wxHORIZONTAL);

		blockData.textInUse = new wxStaticText(m_BlockDetailScroll, wxID_ANY, _("In Use"), wxDefaultPosition, wxDefaultSize, 0);
		blockData.textInUse->Wrap(-1);
		blockData.textInUse->SetLabelText(_("In Use"));
		blockData.filterInfoboxSizer->Add(blockData.textInUse, 1, wxALL | wxLEFT, 5);


		blockData.filterInfoboxSizer->Add(0, 0, 1, wxEXPAND, 5);

		blockData.textRemaining = new wxStaticText(m_BlockDetailScroll, wxID_ANY, _("Remaining"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE);
		blockData.textRemaining->Wrap(-1);
		blockData.textInUse->SetLabelText(_("Remaining"));
		blockData.filterInfoboxSizer->Add(blockData.textRemaining, 1, wxALL | wxRIGHT, 5);

		// add this block to the window
		bFilterWindowSizer->Add(blockData.filterInfoboxSizer, 0, wxEXPAND, 5);

		// disable on start
		blockData.activeCheckbox->SetValue(false);
		blockData.spinValueControl->Disable();
		blockData.textInUse->Disable();
		blockData.textRemaining->Disable();
		blockData.filterName->Disable();

		// add to the list
		m_OrderingDynamic.push_back(blockData);

		// next
		iter++;
	}

	bFilterWindowSizer->Layout();

	// disable the checkbox which will update all values
	for (auto item : m_OrderingDynamic)
	{
		wxCommandEvent e;
		item.activeCheckbox->SetValue(false);
		item.spinValueControl->SetValue(0);
		e.SetEventObject(item.activeCheckbox);
		BlockCheckBox(e);
	}

	// set default to block end
	m_BreakChoice->SetSelection(BREAKTYPE_BLOCKEND);

	wxNotebookEvent empty;
	PageChanging(empty);

	if (m_Mode == MODE_ADD)
	{
		if (m_ProgramName.Length() == 0)
		{
			m_ProgramName = _("New Program");
		}
		m_InputProgramName->SetValue(m_ProgramName);

		if (m_Comments.Length() == 0)
		{
			m_Comments = "";
		}
		m_InputComments->SetValue(m_Comments);

		m_UseTimeDefaults = true;
		m_CheckDefaults->SetValue(m_UseTimeDefaults);

		this->SetTitle(_("Create Program"));
		m_BtnCreateEdit->SetLabelText(_("Create"));
	}
	else if (m_Mode == MODE_EDIT)
	{
		this->SetTitle(_("Edit Existing"));
		m_BtnCreateEdit->SetLabelText(_("Update"));

		LoadFromFile();

		CreateOrderData();
	}

	UpdateTimeValues();
	CheckValues();
}

/////////////////////////////////////////////////////
/// CreateGridFromBank
/// Desc: Clears and creates the current word bank
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::CreateGridFromBank()
{
	std::map<wxString, BankData> bank = sys::AppData::Instance()->GetBank();

	// remove by filter
	for (auto iter = bank.begin(); iter != bank.end();)
	{
		if (m_CurrentVersionSelected == VERSION_THREE)
		{
			if (!iter->second.hasGUID )
			{
				iter = bank.erase(iter);
				continue;
			}
		}

		// next
		iter++;
	}

	// How many Cols/Rows
	int rows = m_WordBankGrid->GetNumberRows();

	// Delete all Cols/Rows
	if (rows > 0)
		m_WordBankGrid->DeleteRows(0, rows, true);

	m_WordBankGrid->InsertRows(0, bank.size());

	// populate
	int rowId = 0;
	for (const auto iter : bank)
	{
		const FilterData& fd = sys::AppData::Instance()->FindFilter(iter.second.filterIndex);
		
		m_WordBankGrid->SetCellValue(rowId, 0, iter.second.str);
		m_WordBankGrid->SetCellValue(rowId, 1, fd.name);
		m_WordBankGrid->SetCellValue(rowId, 2, wxString::Format("%d", (int)iter.second.wordCount));

		if (allowSelection)
			m_WordBankGrid->SetCellValue(rowId, SELECT_ACTIVE_COLUMN, wxString::Format("%d", 0));

		if (iter.second.hasGUID &&
			(iter.second.guid.size() > 0) &&
			(iter.second.guid != " "))
		{
			const int COLOUR_BG = 235;

			m_WordBankGrid->SetCellBackgroundColour(rowId, 0, wxColor(COLOUR_BG, COLOUR_BG, COLOUR_BG));
			m_WordBankGrid->SetCellBackgroundColour(rowId, 1, wxColor(COLOUR_BG, COLOUR_BG, COLOUR_BG));
			m_WordBankGrid->SetCellBackgroundColour(rowId, 2, wxColor(COLOUR_BG, COLOUR_BG, COLOUR_BG));
		}

		m_WordBankGrid->SetReadOnly(rowId, 0);
		m_WordBankGrid->SetReadOnly(rowId, 1);
		m_WordBankGrid->SetReadOnly(rowId, 2);

		if (allowSelection)
			m_WordBankGrid->SetReadOnly(rowId, SELECT_ACTIVE_COLUMN, false);

		rowId++;
	}

	m_WordBankGrid->AutoSize();
	bGridSizer->RecalcSizes();
	this->Layout();
}

/////////////////////////////////////////////////////
/// DoRefreshFilters
/// Desc: Populate the filter combo box
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::DoRefreshFilters()
{
	// populate the data
	m_ComboFilter->Clear();

	// populate from the current map
	m_FilterMap = sys::AppData::Instance()->GetFilters();
	for (auto val : m_FilterMap)
	{
		int index = m_ComboFilter->GetCount();
		m_ComboFilter->Insert(val.second.name, index);
	}
	// add ALL as first element
	m_ComboFilter->Insert(_("Show All"), 0);
	m_ComboFilter->SetSelection(0);

	// bank
}

/////////////////////////////////////////////////////
/// UpdateTimeValues
/// Desc: Keeps the program time values up to date with either user defaults or app defaults
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::UpdateTimeValues()
{
	int sel = m_ComboPhase->GetSelection();

	// populate the current program time values with either the defaults or user settings for the current version
	auto itemSelected = m_PhaseMap.find(sel);
	if (itemSelected != m_PhaseMap.end())
	{
		const PhaseData pd = sys::AppData::Instance()->FindPhase(itemSelected->second);
		if (pd.val != -1)
		{
			wxString str;

			// time to respond
			if (m_UseTimeDefaults ||
				m_TimeToRespond == -1)
			{
				if (pd.userTimeToRespond != pd.timeToRespond)
					m_TimeToRespond = (int)pd.userTimeToRespond;
				else
					m_TimeToRespond = (int)pd.timeToRespond;
				m_InputTimeToRespond->SetValue(wxString::Format("%d", m_TimeToRespond));
			}
			else
			{
				m_InputTimeToRespond->SetValue(wxString::Format("%d", m_TimeToRespond));
			}

			// time response timeout
			if (m_UseTimeDefaults ||
				m_ResponseTimeout == -1)
			{
				if (pd.userTimeResponseTimeout != pd.timeResponseTimeout)
					m_ResponseTimeout = (int)pd.userTimeResponseTimeout;
				else
					m_ResponseTimeout = (int)pd.timeResponseTimeout;
				m_InputTimeResponseTimeout->SetValue(wxString::Format("%d", m_ResponseTimeout));
			}
			else
			{
				m_InputTimeResponseTimeout->SetValue(wxString::Format("%d", m_ResponseTimeout));
			}

			// time interval
			if (m_UseTimeDefaults ||
				m_Interval == -1)
			{
				if (pd.userTimeInterval != pd.timeInterval)
					m_Interval = (int)pd.userTimeInterval;
				else
					m_Interval = (int)pd.timeInterval;
				m_InputTimeInterval->SetValue(wxString::Format("%d", m_Interval));
			}
			else
			{
				m_InputTimeInterval->SetValue(wxString::Format("%d", m_Interval));
			}

			// time between filter changes
			if (m_UseTimeDefaults ||
				m_TimeBetweenChanges == -1)
			{
				if (pd.userTimeBetweenFilterChanges != pd.timeBetweenFilterChanges)
					m_TimeBetweenChanges = (int)pd.userTimeBetweenFilterChanges;
				else
					m_TimeBetweenChanges = (int)pd.timeBetweenFilterChanges;
				m_InputTimeBetweenChanges->SetValue(wxString::Format("%d", m_TimeBetweenChanges));
			}
			else
			{
				m_InputTimeBetweenChanges->SetValue(wxString::Format("%d", m_TimeBetweenChanges));
			}

			// word min
			if (m_UseTimeDefaults ||
				m_WordCountMin == -1)
			{
				m_WordCountMin = (int)pd.wordCountMin;
				m_InputWordMin->SetValue(wxString::Format("%d", m_WordCountMin));
			}
			else
			{
				m_InputWordMin->SetValue(wxString::Format("%d", m_WordCountMin));
			}

			// word max
			if (m_UseTimeDefaults ||
				m_WordCountMax == -1)
			{
				m_WordCountMax = (int)pd.wordCountMax;
				m_InputWordMax->SetValue(wxString::Format("%d", m_WordCountMax));
			}
			else
			{
				m_InputWordMax->SetValue(wxString::Format("%d", m_WordCountMax));
			}

			// line min
			if (m_UseTimeDefaults ||
				m_LinesCountMin == -1)
			{
				m_LinesCountMin = (int)pd.linesCountMin;
				m_InputLinesMin->SetValue(wxString::Format("%d", m_LinesCountMin));
			}
			else
			{
				m_InputLinesMin->SetValue(wxString::Format("%d", m_LinesCountMin));
			}

			// line max
			if (m_UseTimeDefaults ||
				m_LinesCountMax == -1)
			{
				m_LinesCountMax = (int)pd.linesCountMax;
				m_InputLinesMax->SetValue(wxString::Format("%d", m_LinesCountMax));
			}
			else
			{
				m_InputLinesMax->SetValue(wxString::Format("%d", m_LinesCountMax));
			}

			// control the enable/disable of changes
			if (m_UseTimeDefaults)
			{
				m_InputTimeToRespond->Disable();
				m_InputTimeResponseTimeout->Disable();
				m_InputTimeInterval->Disable();
				m_InputTimeBetweenChanges->Disable();

				m_InputWordMin->Disable();
				m_InputWordMax->Disable();

				m_InputLinesMin->Disable();
				m_InputLinesMax->Disable();
			}
			else
			{
				m_InputTimeToRespond->Enable();
				m_InputTimeResponseTimeout->Enable();
				m_InputTimeInterval->Enable();
				m_InputTimeBetweenChanges->Enable();

				m_InputWordMin->Disable();
				m_InputWordMax->Disable();

				m_InputLinesMin->Disable();
				m_InputLinesMax->Disable();
			}
		}
	}
}

/////////////////////////////////////////////////////
/// CheckValues
/// Desc: Checks if the program contains enough information to actually run and builds a list of issues in a log
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::CheckValues()
{
	// determine if the create/edit button should be disabled

	// get the counts from the settings
	bool allowContinue = true;
	m_TxtCtrlLog->Clear();

	for (auto item : m_SettingsCountList)
	{
		BankFileInfo bf = sys::AppData::Instance()->GetSelectedBank();

		if (!bf.jsonData.isNull())
		{
			ProgramCreateEditLogInfo logData = sys::AppData::Instance()->CheckBankFilterCounts(bf.guid, m_CurrentVersionSelected, item.fd.val, item.val, m_WordCountMin, m_WordCountMax);

			if (!logData.isValid)
			{
				m_TxtCtrlLog->SetValue(wxString::Format("%s\n%s", m_TxtCtrlLog->GetValue(), logData.reasonNotValid));

				if (allowContinue)
					allowContinue = !allowContinue;
			}
		}
		else
		{
			allowContinue = false;
			m_TxtCtrlLog->SetValue(wxString::Format(_("Invalid bank\n")));

		}
	}

	if (allowContinue)
		m_TxtCtrlLog->SetValue(_("Program Valid"));

	m_TxtCtrlLog->Disable();
	m_BtnCreateEdit->Enable(allowContinue);
}

/////////////////////////////////////////////////////
/// SetCurrentSelection
/// Desc: Determines if a word/sentence is selected for the program
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::SetCurrentSelection()
{
	if (!allowSelection)
		return;

	if (m_BankSelectionMap.size() > 0)
	{
		//int bankIdx = 0;
		int numRows = m_WordBankGrid->GetNumberRows();
		std::map<wxString, BankData> bank = sys::AppData::Instance()->GetBank();

		for (int i = 0; i < numRows; ++i)
		{
			wxString bankStr = m_WordBankGrid->GetCellValue(i, 0);

			// see if the filter id exists
			if (m_BankSelectionMap.find(bank[bankStr].filterIndex) == m_BankSelectionMap.end())
			{
				// filter not found
				m_WordBankGrid->SetCellValue(i, SELECT_ACTIVE_COLUMN, wxString::Format("%d", 0));
			}
			else
			{
				// need to check the list of words/sentences
				auto currentFilter = m_BankSelectionMap.find(bank[bankStr].filterIndex);
				if (currentFilter->second.list.size() > 0)
				{
					bool found = false;
					for (auto item : currentFilter->second.list)
					{
						if (item.str.IsSameAs(bankStr))
						{
							found = true;
							m_WordBankGrid->SetCellValue(i, SELECT_ACTIVE_COLUMN, wxString::Format("%d", 1));
						}
					}

					if ( !found)
						m_WordBankGrid->SetCellValue(i, SELECT_ACTIVE_COLUMN, wxString::Format("%d", 0));
				}
				else
					m_WordBankGrid->SetCellValue(i, SELECT_ACTIVE_COLUMN, wxString::Format("%d", 0));
			}
		}
	}

	m_BankSelectionMap.clear();
}

/////////////////////////////////////////////////////
/// CheckSelectionCounts
/// Desc: Checks is a word is selected that it matches the global program counts to run
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::CheckSelectionCounts()
{
	if (!allowSelection)
		return;

	int bankIdx = 0;
	int boolIdx = SELECT_ACTIVE_COLUMN;
	int numRows = m_WordBankGrid->GetNumberRows();

	std::map<wxString, BankData> bank = sys::AppData::Instance()->GetBank();

	m_BankSelectionMap.clear();

	for (int i = 0; i < numRows; ++i)
	{
		wxString bankStr = m_WordBankGrid->GetCellValue(i, bankIdx);
		wxString boolStr = m_WordBankGrid->GetCellValue(i, boolIdx);

		long val = -1;
		if (boolStr.Length() != 0)
		{
			if (boolStr.ToLong(&val))
			{
				if (val >= 1)
				{
					if (m_BankSelectionMap.find(bank[bankStr].filterIndex) == m_BankSelectionMap.end()) 
					{
						// not found

						// insert with val 1
						SelectionData sd;
						sd.filterId = bank[bankStr].filterIndex;
						sd.list.push_back(bank[bankStr]);

						m_BankSelectionMap.insert(std::make_pair(bank[bankStr].filterIndex, sd));
					}
					else 
					{
						// found
						SelectionData sd = m_BankSelectionMap[bank[bankStr].filterIndex];
						sd.list.push_back(bank[bankStr]);
						m_BankSelectionMap[bank[bankStr].filterIndex] = sd;	
					}
				}
			}
		}
	}

	// check requested values and update the current countdowns
	for (auto item : m_SelectionCountList)
	{
		//StoredInputData
		if (item.input)
		{
			if (m_BankSelectionMap.find(item.fd.val) != m_BankSelectionMap.end())
				item.input->SetValue(wxString::Format("%d", m_BankSelectionMap[item.fd.val].list.size()));
			else
				item.input->SetValue(wxString::Format("0"));
		}
	}
}

/////////////////////////////////////////////////////
/// CreateFilterData
/// Desc: Dynamically creates a box sizer containing any filters in the global list
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::CreateFilterData(wxBoxSizer* useSizer, wxScrolledWindow* scroller, std::vector<StoredInputData>& list, bool readOnly )
{
	bool isListRefresh = false;
	if (list.size() > 0)
		isListRefresh = true;

	// before clearing store previous values
	if (isListRefresh)
	{
		auto filterMap = sys::AppData::Instance()->GetFilters();
		for (auto iter = filterMap.begin(); iter != filterMap.end(); ++iter)
		{
			// find and replace
			for (auto& item : list)
			{
				// want to get the previous value and set it to the label
				if (item.fd.val == iter->second.val)
				{
					wxString lastVal = item.input->GetValue();
					long convert = -1;
					if (lastVal.ToLong(&convert))
						item.val = (int)convert;
				}
			}
		}
	}

	// now clear
	useSizer->Clear(true);

	auto filterMap = sys::AppData::Instance()->GetFilters();
	for (auto iter = filterMap.begin(); iter != filterMap.end();)
	{
		wxBoxSizer* bNewSizer;
		bNewSizer = new wxBoxSizer(wxHORIZONTAL);

		wxBoxSizer* bLeftSide;
		bLeftSide = new wxBoxSizer(wxHORIZONTAL);

		wxString label = wxString::Format(_("No. %s"), iter->second.name);
		wxStaticText* leftStaticText = new wxStaticText(scroller, wxID_ANY, label, wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
		leftStaticText->Wrap(-1);
		

		bLeftSide->Add(leftStaticText, 0, wxALIGN_CENTER | wxALL, 5);

		wxTextCtrl* leftTextCtrl = new wxTextCtrl(scroller, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0);
		leftTextCtrl->SetMaxSize(wxSize(75, -1));
		leftTextCtrl->Connect(wxEVT_KEY_UP, wxKeyEventHandler(DlgCreateEditPrograms::DynamicSettingsKeyUp), NULL, this);
		
		// store
		if (isListRefresh)
		{
			// find and replace
			for (auto& item : list)
			{
				// want to get the previous value and set it to the label
				if (item.fd.val == iter->second.val)
				{
					// update control ptr
					item.input = leftTextCtrl;
					leftTextCtrl->SetValue(wxString::Format("%d", item.val));
					break;
				}
			}
		}
		else
		{
			// create new
			StoredInputData inputStore;
			inputStore.input = leftTextCtrl;
			inputStore.fd = iter->second;
			if (inputStore.fd.val == FILTER_NEUTRAL ||
				inputStore.fd.val == FILTER_REF )
				inputStore.val = DEFAULT_VAL;
			else
				inputStore.val = 0;
			leftTextCtrl->SetValue(wxString::Format("%d", inputStore.val));
			leftTextCtrl->SetValidator(wxTextValidator(wxFILTER_NUMERIC, &inputStore.validator));
			//wxIntegerValidator<int> leftValCheck(&inputStore.val);
			//leftTextCtrl->SetValidator(leftValCheck);
			list.push_back(inputStore);
		}

		leftTextCtrl->Enable(!readOnly);
		bLeftSide->Add(leftTextCtrl, 1, wxALL, 5);

		bNewSizer->Add(bLeftSide, 1, wxEXPAND, 5);

		// next
		iter++;

		wxBoxSizer* bRightSide;
		bRightSide = new wxBoxSizer(wxHORIZONTAL);

		if (iter != filterMap.end())
		{
			label = wxString::Format(_("No. %s"), iter->second.name);
			wxStaticText* rightStaticText = new wxStaticText(scroller, wxID_ANY, label, wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
			rightStaticText->Wrap(-1);
			
			bRightSide->Add(rightStaticText, 0, wxALIGN_CENTER | wxALL, 5);

			wxTextCtrl* rightTextCtrl = new wxTextCtrl(scroller, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0);
			rightTextCtrl->SetMaxSize(wxSize(75, -1));
			rightTextCtrl->Connect(wxEVT_KEY_UP, wxKeyEventHandler(DlgCreateEditPrograms::DynamicSettingsKeyUp), NULL, this);

			// store
			if (isListRefresh)
			{
				// find and replace
				for (auto& item : list)
				{
					// want to get the previous value and set it to the label
					if (item.fd.val == iter->second.val)
					{
						// update control ptr
						item.input = rightTextCtrl;
						rightTextCtrl->SetValue(wxString::Format("%d", item.val));
						break;
					}
				}
			}
			else
			{
				// create new
				StoredInputData inputStore;
				inputStore.input = rightTextCtrl;
				inputStore.fd = iter->second;
				if (inputStore.fd.val == FILTER_NEUTRAL ||
					inputStore.fd.val == FILTER_REF)
					inputStore.val = DEFAULT_VAL;
				else
					inputStore.val = 0;
				rightTextCtrl->SetValue(wxString::Format("%d", inputStore.val));
				rightTextCtrl->SetValidator(wxTextValidator(wxFILTER_NUMERIC, &inputStore.validator));
				//wxIntegerValidator<int> rightValCheck(&inputStore.val);
				//rightTextCtrl->SetValidator(leftValCheck);
				list.push_back(inputStore);
			}

			rightTextCtrl->Enable(!readOnly);
			bRightSide->Add(rightTextCtrl, 1, wxALL, 5);

			// next
			iter++;
		}
		else
			bRightSide->Add(0, 0, 1, wxEXPAND, 5); // spacer

		bNewSizer->Add(bRightSide, 1, wxEXPAND, 5);


		// add to the scroll window
		useSizer->Add(bNewSizer, 0, wxEXPAND, 5);
	}
}

/////////////////////////////////////////////////////
/// CreateOrderData
/// Desc: Populates the ordering tab with the programs current information
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::CreateOrderData()
{
	// How many Cols/Rows
	int rows = m_OrderList->GetNumberRows();

	// Delete all Cols/Rows
	if (rows > 0)
		m_OrderList->DeleteRows(0, rows, true);

	if(m_OrderData.size() > 0 )
		m_OrderList->InsertRows(0, m_OrderData.size());

	int idx = 0;
	for (auto val : m_OrderData)
	{
		wxString combinedStr = "";

		//m_OrderList->InsertItem(idx, wxString::Format("%d", idx+1), -1);
		for (auto filter : val.values)
		{
			combinedStr += wxString::Format("%s : %d\n", filter.filterName, filter.count);
		}

		if (val.random)
		{
			combinedStr += _("Random\n");
		}
		else
		{
			combinedStr += _("In Sequence\n");
		}

		if (val.breakType == BREAKTYPE_PERFILTER)
		{
			combinedStr += _("Break per filter\n");
		}
		else
		if (val.breakType == BREAKTYPE_BLOCKEND)
		{
			combinedStr += _("Break at end\n");
		}
		else
		if (val.breakType == BREAKTYPE_NOBREAK)
		{
			combinedStr += _("No break\n");
		}

		//m_OrderList->SetItem(idx, 1, combinedStr, -1);
		m_OrderList->SetCellValue(idx, 0, combinedStr);
		m_OrderList->SetCellAlignment(idx, 0, wxALIGN_CENTRE, wxALIGN_CENTRE);
		
		idx++;
	}

	m_OrderList->AutoSize();
	bOrderSizer->RecalcSizes();

	this->Layout();
}

/////////////////////////////////////////////////////
/// SettingsKeyUp
/// Desc: Event for when a time/program setting is changed
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::SettingsKeyUp(wxKeyEvent& WXUNUSED(event))
{
	wxString in;
	long val = -1;

	// program name
	if (m_InputProgramName->GetValue().Length() != 0)
		m_ProgramName = m_InputProgramName->GetValue();

	// comments
	if (m_InputComments->GetValue().Length() != 0)
		m_Comments = m_InputComments->GetValue();

	// time to respond
	in = m_InputTimeToRespond->GetValue();
	val = -1;
	if (in.Length() != 0)
	{
		if (in.ToLong(&val))
		{
			m_TimeToRespond = (int)val;
		}
	}
	else
	{

	}

	// time response timeout
	in = m_InputTimeResponseTimeout->GetValue();
	val = -1;
	if (in.Length() != 0)
	{
		if (in.ToLong(&val))
		{
			m_ResponseTimeout = (int)val;
		}
	}
	else
	{

	}

	// time interval
	in = m_InputTimeInterval->GetValue();
	val = -1;
	if (in.Length() != 0)
	{
		if (in.ToLong(&val))
		{
			m_Interval = (int)val;
		}
	}
	else
	{

	}

	// time between changes
	in = m_InputTimeBetweenChanges->GetValue();
	val = -1;
	if (in.Length() != 0)
	{
		if (in.ToLong(&val))
		{
			m_TimeBetweenChanges = (int)val;
		}
	}
	else
	{

	}

	// word min
	in = m_InputWordMin->GetValue();
	val = -1;
	if (in.Length() != 0)
	{
		if (in.ToLong(&val))
		{
			m_WordCountMin = (int)val;
		}
	}
	else
	{

	}

	// word max
	in = m_InputWordMax->GetValue();
	val = -1;
	if (in.Length() != 0)
	{
		if (in.ToLong(&val))
		{
			m_WordCountMax = (int)val;
		}
	}
	else
	{

	}

	// line min
	in = m_InputLinesMin->GetValue();
	val = -1;
	if (in.Length() != 0)
	{
		if (in.ToLong(&val))
		{
			m_LinesCountMin = (int)val;
		}
	}
	else
	{

	}

	// line max
	in = m_InputLinesMax->GetValue();
	val = -1;
	if (in.Length() != 0)
	{
		if (in.ToLong(&val))
		{
			m_LinesCountMax = (int)val;
		}
	}
	else
	{

	}

	CheckValues();
}

/////////////////////////////////////////////////////
/// DynamicSettingsKeyUp
/// Desc: Event for when a filter count value is changed
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::DynamicSettingsKeyUp(wxKeyEvent& event)
{
	wxTextCtrl* ctrl = reinterpret_cast<wxTextCtrl*>(event.GetEventObject());
	for (auto& item : m_SettingsCountList)
	{
		// check the control matches and update the val
		if (item.input == ctrl)
		{
			wxString str = item.input->GetValue();
			long val = -1;
			if (str.Length() != 0)
			{
				if (str.ToLong(&val))
				{
					item.val = (int)val;
				}
			}
			else
			{
				
			}
		}
	}

	// now check
	CheckValues();
}

/////////////////////////////////////////////////////
/// PageChanged
/// Desc: Event callback when a tab page is changed
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::PageChanged(wxNotebookEvent& WXUNUSED(event))
{
	m_WordBankGrid->AutoSize();

	bGridSizer->RecalcSizes();

	for (auto item : m_SettingsCountList)
	{
		// want to get the previous value and set it to the label
		if (item.input)
			item.input->SetValue(wxString::Format("%d", item.val));
	}

	if ( allowSelection)
		bBankScrollBoxSizer->Layout();

	bSettingsScrollBoxSizer->Layout();

	SetCurrentSelection();
	CheckSelectionCounts();

	CheckValues();

	this->Layout();
}

/////////////////////////////////////////////////////
/// PageChanging
/// Desc: Event callback when a tab page is about to change
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::PageChanging(wxNotebookEvent& WXUNUSED(event))
{
	// phase set up
	m_ComboPhase->Clear();
	m_PhaseMap.clear();

	const auto& phaseMap = sys::AppData::Instance()->GetPhases();
	for (auto val : phaseMap)
	{
		int index = m_ComboPhase->GetCount();

		m_ComboPhase->Insert(val.second.name, index);

		// make this map match the selection box
		m_PhaseMap.insert(std::make_pair(index, val.second.val));
	}

	if( m_PreviousSelection != -1 )
		m_ComboPhase->SetSelection(m_PreviousSelection);
	else
	{
		m_PreviousSelection = 0;
		m_ComboPhase->SetSelection(m_PreviousSelection);
	}

	wxCommandEvent empty;
	ComboPhaseChange(empty);

	// populate the data
	m_ComboBank->Clear();

	// populate from the current map
	m_BankFileComboMap.clear();
	m_BankListMap = sys::AppData::Instance()->GetBankFileList();
	for (auto val : m_BankListMap)
	{
		int index = m_ComboBank->GetCount();
		m_ComboBank->Insert(val.second.name, index);

		m_BankFileComboMap.insert(std::make_pair(index, val.second));
	}

	if(m_PreviousBankSelection != -1)
		m_ComboBank->SetSelection(m_PreviousBankSelection);
	else
	{
		m_PreviousBankSelection = 0;
		m_ComboBank->SetSelection(m_PreviousBankSelection);
	}

	ComboBankChange(empty);

	//// filters
	if (allowSelection)
		CreateFilterData(bBankScrollBoxSizer, m_BankFilterScroll, m_SelectionCountList, true);

	CreateFilterData(bSettingsScrollBoxSizer, m_SettingsFilterScroll, m_SettingsCountList, false);

	m_BankFilter = FILTER_ALL;

	CreateGridFromBank();

	DoRefreshFilters();

	if ( allowSelection )
		bBankScrollBoxSizer->RecalcSizes();

	bSettingsScrollBoxSizer->RecalcSizes();

	// disable the checkbox 
	for (auto item : m_OrderingDynamic)
	{
		wxCommandEvent e;
		item.activeCheckbox->SetValue(false);
		item.spinValueControl->SetValue(0);
		e.SetEventObject(item.activeCheckbox);
		BlockCheckBox(e);
	}
}

/////////////////////////////////////////////////////
/// ComboBankChange
/// Desc: Event for changing the bank
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::ComboBankChange(wxCommandEvent& WXUNUSED(event))
{
	int selectionIndex = m_ComboBank->GetSelection();
	//wxString str = m_ComboBank->GetStringSelection();

	auto iter = m_BankFileComboMap.find(selectionIndex);
	if ( iter != m_BankFileComboMap.end() )
	{
		sys::AppData::Instance()->SetSelectedBank(iter->second);
		CreateGridFromBank();
		CheckValues();

		m_PreviousBankSelection = selectionIndex;
	}
	else
	{
		BankFileInfo empty;
		empty.jsonData = JsonBox::Value();
		sys::AppData::Instance()->SetSelectedBank(empty);
		CreateGridFromBank();
		CheckValues();
	}
}

/////////////////////////////////////////////////////
/// BankFilterChange
/// Desc: Event for when the filter combo box is changed and needs to filter the bank
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::BankFilterChange(wxCommandEvent& WXUNUSED(event))
{
	int filterId = 0;

	int selectionIndex = m_ComboFilter->GetSelection();
	wxString str = m_ComboFilter->GetStringSelection();

	if (selectionIndex == 0 ||
		str.length() == 0)
		filterId = 0;
	else
	{
		for (auto iter : m_FilterMap)
		{
			if (iter.second.name.IsSameAs(str))
			{
				filterId = iter.second.val;
				break;
			}
		}
	}

	if (filterId != m_BankFilter)
	{
		m_BankFilter = filterId;
		CreateGridFromBank();
	}
}

/////////////////////////////////////////////////////
/// ComboPhaseChange
/// Desc: Combo callback for when the version/phase is changed
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::ComboPhaseChange(wxCommandEvent& WXUNUSED(event))
{
	int sel = m_ComboPhase->GetSelection();
	
	auto itemSelected = m_PhaseMap.find(sel);
	if (itemSelected != m_PhaseMap.end())
	{
		const PhaseData pd = sys::AppData::Instance()->FindPhase(itemSelected->second);
		if (pd.val != -1)
		{
			// update the values
			m_DefTimeToRespond->SetLabel(wxString::Format(_("Default: %d"), (int)pd.timeToRespond));
			m_DefTimeToRespond->Refresh();
			m_DefResponseTimeout->SetLabel(wxString::Format(_("Default: %d"), (int)pd.timeResponseTimeout));
			m_DefResponseTimeout->Refresh();
			m_DefInterval->SetLabel(wxString::Format(_("Default: %d"), (int)pd.timeInterval));
			m_DefInterval->Refresh();
			m_DefTimeBetweenChanges->SetLabel(wxString::Format(_("Default: %d"), (int)pd.timeBetweenFilterChanges));
			m_DefTimeBetweenChanges->Refresh();

			m_PreviousSelection = sel;
		}

		m_CurrentVersionSelected = pd.val;

		if (pd.val == VERSION_THREE)
		{
			m_BtnAddWithChildren->Show();
			m_BtnAdd->Hide();
		}
		else
		{
			m_BtnAddWithChildren->Hide();
			m_BtnAdd->Show();
		}
	}

	bBankButtonSizer->RecalcSizes();

	UpdateTimeValues();
	CheckValues();

	this->Layout();
}

/////////////////////////////////////////////////////
/// ChangeDefaultCheckBox
/// Desc: Checkbox event for enable/disable the default or custom time values
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::ChangeDefaultCheckBox(wxCommandEvent& WXUNUSED(event))
{
	m_UseTimeDefaults = m_CheckDefaults->GetValue();
	UpdateTimeValues();
}

/////////////////////////////////////////////////////
/// CanModifyCell
/// Desc: Checks if a cell row can be changed (not used)
/////////////////////////////////////////////////////
bool DlgCreateEditPrograms::CanModifyCell( int row )
{
	wxString str = m_WordBankGrid->GetCellValue(row, 0);

	std::map<wxString, BankData> bank = sys::AppData::Instance()->GetBank();

	int filterId = -1;
	if (bank.find(str) != bank.end())
	{
		filterId = bank[str].filterIndex;
	}

	// if the value is 1, always allow deselect
	wxString deselectVal = m_WordBankGrid->GetCellValue(row, SELECT_ACTIVE_COLUMN);
	long val = -1;
	if (deselectVal.Length() != 0)
	{
		if (deselectVal.ToLong(&val))
		{
			if (val >= 1)
				return true;
		}
	}

	// need to compare the set settings
	if (filterId != -1)
	{
		if (m_BankSelectionMap.size() > 0 &&
			m_BankSelectionMap.find(filterId) != m_BankSelectionMap.end())
		{
			if (m_BankSelectionMap[filterId].list.size() > 0)
			{
				for (auto item : m_SettingsCountList)
				{
					if (item.fd.val == filterId)
					{
						// already at the limit of this filter
						if ((int)(m_BankSelectionMap[filterId].list.size() + 1) > item.val)
						{
							return false;
						}
					}
				}
			}
		}
		else
		{
			// filter doesn't exist yet, but check to see if the number in settings is > 0
			for (auto item : m_SettingsCountList)
			{
				if (item.fd.val == filterId)
				{
					if (item.val == 0)
					{
						return false;
					}
				}
			}
		}
	}
	return true;
}

/////////////////////////////////////////////////////
/// LoadFromFile
/// Desc: Loads the selected json program file and populates the dialog
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::LoadFromFile()
{
	if (m_EditFilename.length() == 0)
		return;

	JsonBox::Value loadedProgram;

	std::ifstream t(m_EditFilename.ToStdString());
	std::stringstream buffer;
	buffer << t.rdbuf();

	std::string bufferStr = buffer.str();
	loadedProgram.loadFromString(bufferStr);
	//loadedProgram.loadFromFile(m_EditFilename.ToStdString());

	if (!loadedProgram.isNull())
	{
		if (loadedProgram["Name"].isString())
		{
			m_ProgramName = loadedProgram["Name"].getString();
			m_InputProgramName->SetValue(m_ProgramName);
		}

		if (loadedProgram["Comments"].isString())
		{
			m_Comments = loadedProgram["Comments"].getString();
			m_InputComments->SetValue(m_Comments);
		}

		if (loadedProgram["Time"].isString())
		{
			wxString str = loadedProgram["Time"].getString();
			long long val = -1;
			if (str.ToLongLong(&val))
			{
				m_CreationDate = wxDateTime((wxLongLong)val);
			}
		}

		if (loadedProgram["BankGUID"].isString())
		{
			wxString str = loadedProgram["BankGUID"].getString();
			
			int index = 0;
			bool bankIsMissing = true;
			for (auto iter : m_BankFileComboMap)
			{
				if (iter.second.guid == str.ToStdString())
				{
					m_ComboBank->SetSelection(index);
					m_PreviousBankSelection = index;
					bankIsMissing = false;
					break;
				}
				index++;
			}

			if (bankIsMissing)
			{

			}
		}

		if (loadedProgram["PhaseId"].isInteger())
		{
			int phaseId = loadedProgram["PhaseId"].getInteger();

			// set selection box
			for (auto idx : m_PhaseMap)
			{
				if (idx.second == phaseId)
				{
					m_ComboPhase->SetSelection(idx.first);
					m_PreviousSelection = idx.first;
					break;
				}
			}
		}
		
		if (loadedProgram["UseTimeDefaults"].isBoolean())
		{
			bool val = loadedProgram["UseTimeDefaults"].getBoolean();
			m_UseTimeDefaults = val;
			m_CheckDefaults->SetValue(m_UseTimeDefaults);
		}

		if (!m_UseTimeDefaults)
		{
			if (loadedProgram["TimeToRespond"].isInteger())
			{
				m_TimeToRespond = loadedProgram["TimeToRespond"].getInteger();
				m_InputTimeToRespond->SetValue(wxString::Format("%d", m_TimeToRespond));
			}

			if (loadedProgram["TimeResponseTimeout"].isInteger())
			{
				m_ResponseTimeout = loadedProgram["TimeResponseTimeout"].getInteger();
				m_InputTimeResponseTimeout->SetValue(wxString::Format("%d", m_ResponseTimeout));
			}

			if (loadedProgram["TimeInterval"].isInteger())
			{
				m_Interval = loadedProgram["TimeInterval"].getInteger();
				m_InputTimeInterval->SetValue(wxString::Format("%d", m_Interval));
			}

			if (loadedProgram["TimeBetweenFilterChanges"].isInteger())
			{
				m_TimeBetweenChanges = loadedProgram["TimeBetweenFilterChanges"].getInteger();
				m_InputTimeBetweenChanges->SetValue(wxString::Format("%d", m_TimeBetweenChanges));
			}
		}

		if (loadedProgram["LinesMin"].isInteger())
		{
			m_LinesCountMin = loadedProgram["LinesMin"].getInteger();
			m_InputLinesMin->SetValue(wxString::Format("%d", m_LinesCountMin));
		}

		if (loadedProgram["LinesMax"].isInteger())
		{
			m_LinesCountMax = loadedProgram["LinesMax"].getInteger();
			m_InputLinesMax->SetValue(wxString::Format("%d", m_LinesCountMax));
		}

		if (loadedProgram["WordCountMin"].isInteger())
		{
			m_WordCountMin = loadedProgram["WordCountMin"].getInteger();
			m_InputWordMin->SetValue(wxString::Format("%d", m_WordCountMin));
		}

		if (loadedProgram["WordCountMax"].isInteger())
		{
			m_WordCountMax = loadedProgram["WordCountMax"].getInteger();
			m_InputWordMax->SetValue(wxString::Format("%d", m_WordCountMax));
		}

		if (loadedProgram["FilterCounts"].isArray())
		{
			JsonBox::Array arr = loadedProgram["FilterCounts"].getArray();
			auto iter = arr.begin();
			while (iter != arr.end())
			{
				if (iter->isObject())
				{
					JsonBox::Object filterItem = iter->getObject();
					if (!filterItem["FilterId"].isNull() &&
						!filterItem["Count"].isNull() )
					{
						int filterId = filterItem["FilterId"].getInteger();
						int count = filterItem["Count"].getInteger();

						for (auto& filterCount : m_SettingsCountList)
						{
							if (filterCount.fd.val == filterId)
							{
								if (filterCount.input)
								{
									filterCount.val = count;
									filterCount.input->SetValue(wxString::Format("%d", count));
								}
							}
						}
					}
				}
				// next
				++iter;
			}
		}

		if (!loadedProgram["OrderData"].isNull() &&
			loadedProgram["OrderData"].isArray())
		{
			m_OrderData.clear();

			JsonBox::Value orderObj = loadedProgram["OrderData"];
			JsonBox::Array arr = orderObj.getArray();
			
			auto iter = arr.begin();
			while (iter != arr.end())
			{
				OrderBlock newBlock;
				if (iter->isObject())
				{
					JsonBox::Value blockObj = iter->getObject();

					JsonBox::Value blockArray = blockObj["Blocks"];
					JsonBox::Value blockRandomVal = blockObj["Random"];
					JsonBox::Value blockIndex = blockObj["Index"];
					JsonBox::Value blockBreakType = blockObj["BreakType"];
					JsonBox::Array currBlock = blockArray.getArray();
					
					auto iterBlocks = currBlock.begin();

					std::vector<OrderValues> values;
					while (iterBlocks != currBlock.end())
					{
						if (iterBlocks->isObject())
						{
							JsonBox::Object blockItem = iterBlocks->getObject();
							if (!blockItem["FilterId"].isNull() &&
								!blockItem["Count"].isNull())
							{
								int filterId = blockItem["FilterId"].getInteger();
								int count = blockItem["Count"].getInteger();
								const FilterData fd = sys::AppData::Instance()->FindFilter(filterId);

								OrderValues val;
								val.filterId = filterId;
								val.count = count;
								val.filterName = fd.name;

								values.push_back(val);
							}
						}

						// next
						++iterBlocks;
					}

					newBlock.index = 0;
					newBlock.random = blockRandomVal.getBoolean();
					newBlock.breakType = (BreakType)blockBreakType.getInteger();
					newBlock.values = values;
				}
				
				// add
				m_OrderData.push_back(newBlock);

				// next
				++iter;
			}
		}
	}
}

/////////////////////////////////////////////////////
/// GridCellLeftClick
/// Desc: Event callback for when a word/sentence is selected in the bank view
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::GridCellLeftClick(wxGridEvent& event)
{
	m_WordBankGrid->ClearSelection();
	m_WordBankGrid->SelectRow(event.GetRow());

	if (event.GetCol() != SELECT_ACTIVE_COLUMN)
	{
		// don't care
		event.Skip();
	}
	else
	{
		bool canModify = CanModifyCell(event.GetRow());

		if (canModify)
		{
			int row = event.GetRow();

			wxString str = m_WordBankGrid->GetCellValue(row, event.GetCol());
			long val = -1;
			if (str.Length() != 0)
			{
				if (str.ToLong(&val))
				{
					if (val >= 1)
						m_WordBankGrid->SetCellValue(row, event.GetCol(), wxString::Format("%d", 0));
					else
						m_WordBankGrid->SetCellValue(row, event.GetCol(), wxString::Format("%d", 1));
				}
			}
			else
			{
				m_WordBankGrid->SetCellValue(row, event.GetCol(), wxString::Format("%d", 1));
			}

			// call refresh
			CheckSelectionCounts();
		}
		else
		{
			event.Veto();
		}
	}
}

/////////////////////////////////////////////////////
/// GridCellLeftDoubleClick
/// Desc: Event callback for left double click on word bank grid (goes to edit mode)
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::GridCellLeftDoubleClick(wxGridEvent& WXUNUSED(event))
{
	wxCommandEvent empty;
	BtnEditClick(empty);
}

/////////////////////////////////////////////////////
/// GridRangeSelect
/// Desc: Event callback to multiselect grid rows (veto'd event)
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::GridRangeSelect(wxGridRangeSelectEvent& event)
{
	event.Veto();
}

/////////////////////////////////////////////////////
/// BtnAddClick
/// Desc: Button event for adding a single word/sentence to the bank
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::BtnAddClick(wxCommandEvent& WXUNUSED(event))
{
	DlgAddEditBank* dlg = new DlgAddEditBank(0);
	dlg->SetMode(MODE_ADD);
	dlg->Apply();
	if (dlg->ShowModal() == RESPONSE_OK)
	{
		CreateGridFromBank();

		SetCurrentSelection();
		CheckSelectionCounts();

		sys::AppData::Instance()->SaveBank();
	}

	CheckValues();

	dlg->Destroy();
	delete dlg;
}

/////////////////////////////////////////////////////
/// BtnAddWithChildrenClick
/// Desc: Button event for adding a related word/sentence to the bank in version 3
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::BtnAddWithChildrenClick(wxCommandEvent& WXUNUSED(event))
{
	DlgAddEditWithChildrenBank* dlg = new DlgAddEditWithChildrenBank(0);
	dlg->SetMode(MODE_ADD);
	dlg->Apply();
	if (dlg->ShowModal() == RESPONSE_OK)
	{
		CreateGridFromBank();

		SetCurrentSelection();
		CheckSelectionCounts();

		sys::AppData::Instance()->SaveBank();
	}

	CheckValues();

	dlg->Destroy();
	delete dlg;
}

/////////////////////////////////////////////////////
/// BtnEditClick
/// Desc: Button event for editing a selected word/sentence
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::BtnEditClick(wxCommandEvent& WXUNUSED(event))
{
	// get selected
	wxArrayInt arr = m_WordBankGrid->GetSelectedRows();

	int numSelected = arr.Count();

	std::map<wxString, BankData> bank = sys::AppData::Instance()->GetBank();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId >= 0 && rowId <
			m_WordBankGrid->GetNumberRows())
		{
			// pass data and use for the comparison later
			BankData itemData;
			itemData.str = m_WordBankGrid->GetCellValue(rowId, 0);

			// find the item in the bank to check for a GUID
			const BankData& originalBankData = sys::AppData::Instance()->GetFromBank(itemData.str);

			int filterId = -1;
			if (bank.find(itemData.str) != bank.end())
			{
				filterId = bank[itemData.str].filterIndex;
			}

			if (!originalBankData.hasGUID)
			{
				DlgAddEditBank* dlg = new DlgAddEditBank(0);
				dlg->SetString(itemData.str);
				dlg->SetMode(MODE_EDIT);
				dlg->Apply();
				if (dlg->ShowModal() == RESPONSE_OK)
				{
					// if the name or filter has changed, deselect it in the selection list
					wxString newString = dlg->GetString();
					const BankData& bd = sys::AppData::Instance()->GetFromBank(newString);

					bool refreshSelected = false;

					if (!newString.IsSameAs(itemData.str) ||
						filterId != bd.filterIndex)
					{
						if (m_BankSelectionMap.size() > 0 &&
							m_BankSelectionMap.find(filterId) != m_BankSelectionMap.end())
						{
							if (m_BankSelectionMap[filterId].list.size() > 0)
							{
								// remove from the list
								for (auto iter = m_BankSelectionMap[filterId].list.begin(); iter != m_BankSelectionMap[filterId].list.end();)
								{
									if (iter->str.IsSameAs(itemData.str))
									{
										iter = m_BankSelectionMap[filterId].list.erase(iter);
										refreshSelected = true;
										break;
									}

									// next
									iter++;
								}
							}
						}
					}

					CreateGridFromBank();

					SetCurrentSelection();
					CheckSelectionCounts();

					sys::AppData::Instance()->SaveBank();
				}

				CheckValues();

				dlg->Destroy();
				delete dlg;
			}
			else
			{
				// handle related sentences
				std::vector<BankData> list = sys::AppData::Instance()->GetMatchingGUID(originalBankData.guid);

				if (list.size() == 3)
				{
					int validCount = 0;

					DlgAddEditWithChildrenBank* dlg = new DlgAddEditWithChildrenBank(0);

					for (std::size_t i = 0; i < list.size(); ++i)
					{
						int wordLength = sys::WordCount(list[i].str.ToStdWstring());

						if (wordLength == 4)
						{
							dlg->SetString(1, list[i].str);
							validCount++;
						}
						else
						if (wordLength == 5)
						{
							dlg->SetString(2, list[i].str);
							validCount++;
						}
						else
						if (wordLength == 6)
						{
							dlg->SetString(3, list[i].str);
							validCount++;
						}
					}

					if (validCount != 3)
					{
						dlg->Destroy();
						delete dlg;
					}
					else
					{
						dlg->SetMode(MODE_EDIT);
						dlg->Apply();
						if (dlg->ShowModal() == RESPONSE_OK)
						{				
							// if the name or filter has changed, deselect it in the selection list

							// SENTENCE 1
							{
								wxString newString = dlg->GetString(1);
								const BankData& bd = sys::AppData::Instance()->GetFromBank(newString);

								bool refreshSelected = false;

								if (!newString.IsSameAs(itemData.str) ||
									filterId != bd.filterIndex)
								{
									if (m_BankSelectionMap.size() > 0 &&
										m_BankSelectionMap.find(filterId) != m_BankSelectionMap.end())
									{
										if (m_BankSelectionMap[filterId].list.size() > 0)
										{
											// remove from the list
											for (auto iter = m_BankSelectionMap[filterId].list.begin(); iter != m_BankSelectionMap[filterId].list.end();)
											{
												if (iter->str.IsSameAs(itemData.str))
												{
													iter = m_BankSelectionMap[filterId].list.erase(iter);
													refreshSelected = true;
													break;
												}

												// next
												iter++;
											}
										}
									}
								}
							}

							// SENTENCE 2
							{
								wxString newString = dlg->GetString(2);
								const BankData& bd = sys::AppData::Instance()->GetFromBank(newString);

								bool refreshSelected = false;

								if (!newString.IsSameAs(itemData.str) ||
									filterId != bd.filterIndex)
								{
									if (m_BankSelectionMap.size() > 0 &&
										m_BankSelectionMap.find(filterId) != m_BankSelectionMap.end())
									{
										if (m_BankSelectionMap[filterId].list.size() > 0)
										{
											// remove from the list
											for (auto iter = m_BankSelectionMap[filterId].list.begin(); iter != m_BankSelectionMap[filterId].list.end();)
											{
												if (iter->str.IsSameAs(itemData.str))
												{
													iter = m_BankSelectionMap[filterId].list.erase(iter);
													refreshSelected = true;
													break;
												}

												// next
												iter++;
											}
										}
									}
								}
							}

							// SENTENCE 3
							{
								wxString newString = dlg->GetString(3);
								const BankData& bd = sys::AppData::Instance()->GetFromBank(newString);

								bool refreshSelected = false;

								if (!newString.IsSameAs(itemData.str) ||
									filterId != bd.filterIndex)
								{
									if (m_BankSelectionMap.size() > 0 &&
										m_BankSelectionMap.find(filterId) != m_BankSelectionMap.end())
									{
										if (m_BankSelectionMap[filterId].list.size() > 0)
										{
											// remove from the list
											for (auto iter = m_BankSelectionMap[filterId].list.begin(); iter != m_BankSelectionMap[filterId].list.end();)
											{
												if (iter->str.IsSameAs(itemData.str))
												{
													iter = m_BankSelectionMap[filterId].list.erase(iter);
													refreshSelected = true;
													break;
												}

												// next
												iter++;
											}
										}
									}
								}
							}

							CreateGridFromBank();

							SetCurrentSelection();
							CheckSelectionCounts();

							sys::AppData::Instance()->SaveBank();
						}

						CheckValues();

						dlg->Destroy();
						delete dlg;
					}
				}
			}
		}
	}
}

/////////////////////////////////////////////////////
/// BtnDeleteClick
/// Desc: Button event for deleting a selected word/sentence in the word bank
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::BtnDeleteClick(wxCommandEvent& WXUNUSED(event))
{
	// get selected
	wxArrayInt arr = m_WordBankGrid->GetSelectedRows();

	int numSelected = arr.Count();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId >= 0 &&
			rowId < m_WordBankGrid->GetNumberRows())
		{
			BankData itemData;
			itemData.str = m_WordBankGrid->GetCellValue(rowId, 0);

			std::map<wxString, BankData> bank = sys::AppData::Instance()->GetBank();

			int filterId = -1;
			bool hasGUID = false;
			std::string guid = "";
			if (bank.find(itemData.str) != bank.end())
			{
				filterId = bank[itemData.str].filterIndex;
				hasGUID = bank[itemData.str].hasGUID;
				guid = bank[itemData.str].guid;
			}

			// delete
			if (wxMessageBox(_("Are you sure you want to delete this from the bank?"), _("DELETE WORD/SENTENCE"), wxYES_NO) == wxYES)
			{
				if (m_BankSelectionMap.size() > 0 &&
					m_BankSelectionMap.find(filterId) != m_BankSelectionMap.end())
				{
					if (m_BankSelectionMap[filterId].list.size() > 0)
					{
						// remove from the list (include linked sentences)
						for (auto iter = m_BankSelectionMap[filterId].list.begin(); iter != m_BankSelectionMap[filterId].list.end();)
						{
							if (!hasGUID)
							{
								if (iter->str.IsSameAs(itemData.str))
								{
									iter = m_BankSelectionMap[filterId].list.erase(iter);
									break;
								}
								else
									// next
									iter++;
							}
							else
							{
								if (iter->guid == guid)
								{
									iter = m_BankSelectionMap[filterId].list.erase(iter);
									continue;
								}
								
								// next
								iter++;
							}
						}
					}
				}

				// remove from the bank
				if (sys::AppData::Instance()->RemoveFromBank(itemData.str))
				{
					CreateGridFromBank();

					SetCurrentSelection();
					CheckSelectionCounts();

					sys::AppData::Instance()->SaveBank();
				}

				CheckValues();
			}
		}
	}
}

/////////////////////////////////////////////////////
/// BtnCancelClick
/// Desc: Button callback for cancelling the dialog (nothing saved/changed)
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::BtnCancelClick(wxCommandEvent& WXUNUSED(event))
{
	this->EndModal(RESPONSE_CANCEL);
}

/////////////////////////////////////////////////////
/// BtnCreateEditClick
/// Desc: Saves a new or updates the program 
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::BtnCreateEditClick(wxCommandEvent& WXUNUSED(event))
{
	std::string fullPath;
	wxLongLong value;

	if (m_Mode == MODE_ADD)
	{
		// creation date
		m_CreationDate = wxDateTime::Now();

		value = m_CreationDate.GetValue();
		wxString dateStr = m_CreationDate.FormatISODate();
		wxString timeStr = m_CreationDate.FormatISOTime();
		std::string convertedTime = timeStr.ToStdString();
		std::replace(convertedTime.begin(), convertedTime.end(), ':', '-');

		fullPath = sys::AppData::Instance()->GetProgramsDirectory().ToStdString() + std::string("/") + std::string("Program-") + std::string(dateStr.ToStdString() + "@" + convertedTime) + std::string(".json");
	}
	else if (m_Mode == MODE_EDIT)
	{
		value = m_CreationDate.GetValue();

		fullPath = m_EditFilename.ToStdString();
	}
	
	JsonBox::Array settingsArr;
	JsonBox::Object finalObj;

	int sel = m_ComboPhase->GetSelection();
	int phaseId = -1;
	auto itemSelected = m_PhaseMap.find(sel);
	if (itemSelected != m_PhaseMap.end())
	{
		phaseId = itemSelected->second;
	}

	JsonBox::Object phaseItem;
	phaseItem["Name"] = JsonBox::Value(m_ProgramName.ToStdString());
	phaseItem["Comments"] = JsonBox::Value(m_Comments.ToStdString());
	phaseItem["PhaseId"] = JsonBox::Value(phaseId);
	phaseItem["Time"] = JsonBox::Value(value.ToString().ToStdString());
	
	int selectedBank = m_ComboBank->GetSelection();
	auto bankFile = m_BankFileComboMap.find(selectedBank);
	if (bankFile != m_BankFileComboMap.end())
	{
		phaseItem["BankGUID"] = JsonBox::Value(bankFile->second.guid);
	}

	phaseItem["UseTimeDefaults"] = JsonBox::Value(m_UseTimeDefaults);

	if (!m_UseTimeDefaults)
	{
		phaseItem["TimeToRespond"] = JsonBox::Value(m_TimeToRespond);
		phaseItem["TimeResponseTimeout"] = JsonBox::Value(m_ResponseTimeout);
		phaseItem["TimeInterval"] = JsonBox::Value(m_Interval);
		phaseItem["TimeBetweenFilterChanges"] = JsonBox::Value(m_TimeBetweenChanges);
	}
			
	phaseItem["LinesMin"] = JsonBox::Value(m_LinesCountMin);
	phaseItem["LinesMax"] = JsonBox::Value(m_LinesCountMax);
	phaseItem["WordCountMin"] = JsonBox::Value(m_WordCountMin);
	phaseItem["WordCountMax"] = JsonBox::Value(m_WordCountMax);

	// write out the settings counts
	JsonBox::Object settingsObj;
	for (auto item : m_SettingsCountList)
	{
		settingsObj["FilterId"] = JsonBox::Value(item.fd.val);
		settingsObj["Count"] = JsonBox::Value(item.val);

		settingsArr.push_back(settingsObj);
	}

	phaseItem["FilterCounts"] = JsonBox::Value(settingsArr);

	if (m_OrderData.size() > 0)
	{
		int idx = 0;
		JsonBox::Array blockArr;
		for (auto item : m_OrderData)
		{
			JsonBox::Object blockItem;
			blockItem["Random"] = JsonBox::Value(item.random);
			blockItem["BreakType"] = JsonBox::Value(item.breakType);
			blockItem["Index"] = JsonBox::Value(idx);
			JsonBox::Array valArr;
			JsonBox::Object filterObj;
			for (auto val : item.values)
			{
				filterObj["FilterId"] = JsonBox::Value(val.filterId);
				filterObj["Count"] = JsonBox::Value(val.count);

				valArr.push_back(filterObj);
			}
			blockItem["Blocks"] = JsonBox::Value(valArr);


			blockArr.push_back(blockItem);

			idx++;
		}
		phaseItem["OrderData"] = JsonBox::Value(blockArr);
	}

	// use values to build the program
	std::map<wxString, BankData> bank = sys::AppData::Instance()->GetBank();

	// check word min/max count
	for (auto iter = bank.begin(); iter != bank.end();)
	{
		if (iter->second.wordCount < m_WordCountMin ||
			iter->second.wordCount > m_WordCountMax)
		{
			iter = bank.erase(iter);
			continue;
		}
		
		// next
		iter++;
	}

	// bank now contains only valid values
	std::vector<std::vector<BankData>> allLists; // list of lists

	for (auto item : m_SettingsCountList)
	{
		std::vector<BankData> newItem;

		for (auto iter = bank.begin(); iter != bank.end();)
		{
			if (iter->second.filterIndex == item.fd.val)
			{
				newItem.push_back(iter->second);
			}

			// next
			++iter;
		}

		// add the list of this filter
		allLists.push_back(newItem);

		for (int i = 0; i < item.val; ++i)
		{
			std::size_t numElements = newItem.size();

			int selected = sys::RandInt(0, (numElements-1));

			newItem.erase(newItem.begin() + selected);
		}
	}

	// save out to a value to write
	JsonBox::Value v(phaseItem);

	v.writeToFile(fullPath);

	this->EndModal(RESPONSE_OK);
}

/////////////////////////////////////////////////////
/// BtnMoveUpOrderClick
/// Desc: Button event for moving a selected order block up the list
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::BtnMoveUpOrderClick(wxCommandEvent& WXUNUSED(event))
{
	// get selected
	wxArrayInt arr = m_OrderList->GetSelectedRows();

	int numSelected = arr.Count();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId == 0)
			return;

		int newSelected = -1;
		if (rowId >= 0 && rowId <
			m_OrderList->GetNumberRows())
		{

			std::swap(m_OrderData[rowId], m_OrderData[rowId-1]);

			newSelected = rowId - 1;
		}

		CreateOrderData();

		if (newSelected != -1)
		{
			m_OrderList->SelectRow(newSelected);
		}
	}
}

/////////////////////////////////////////////////////
/// BtnMoveDownOrderClick
/// Desc: Button event for moving a selected order block down the list
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::BtnMoveDownOrderClick(wxCommandEvent& WXUNUSED(event))
{
	// get selected
	wxArrayInt arr = m_OrderList->GetSelectedRows();

	int numSelected = arr.Count();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId == m_OrderList->GetNumberRows()-1)
			return;

		int newSelected = -1;
		if (rowId >= 0 && rowId <
			m_OrderList->GetNumberRows())
		{

			std::swap(m_OrderData[rowId], m_OrderData[rowId+1]);

			newSelected = rowId + 1;
		}

		CreateOrderData();

		if (newSelected != -1)
		{
			m_OrderList->SelectRow(newSelected);
		}
	}
}

/////////////////////////////////////////////////////
/// BtnRemoveOrderClick
/// Desc: Button event for removing a selected order block from the list
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::BtnRemoveOrderClick(wxCommandEvent& WXUNUSED(event))
{
	// get selected
	wxArrayInt arr = m_OrderList->GetSelectedRows();

	int numSelected = arr.Count();

	if (numSelected > 0)
	{
		// get only the first
		int rowId = arr[0];

		if (rowId >= 0 && rowId <
			m_OrderList->GetNumberRows())
		{
			m_OrderData.erase(m_OrderData.begin() + rowId);
		}

		CreateOrderData();
	}

	// call the spinner
	for (auto item : m_OrderingDynamic)
	{
		wxSpinEvent e;
		e.SetEventObject(item.spinValueControl);
		BlockCountChange(e);
	}
}

/////////////////////////////////////////////////////
/// OrderGridCellLeftClick
/// Desc: Event for selecting a block in the ordering tab grid
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::OrderGridCellLeftClick(wxGridEvent& event)
{
	m_OrderList->ClearSelection();
	m_OrderList->SelectRow(event.GetRow());

	// don't care
	event.Skip();
}

/////////////////////////////////////////////////////
/// AddBlockClick
/// Desc: Button event for adding a block to the ordering list
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::AddBlockClick(wxCommandEvent& WXUNUSED(event))
{
	std::vector<OrderValues> values;
	OrderBlock newBlock;
	for (auto item : m_OrderingDynamic)
	{
		if (item.activeCheckbox->GetValue())
		{
			const auto fd = sys::AppData::Instance()->FindFilter(item.fd.val);
			int val = item.spinValueControl->GetValue();

			if (val > 0 &&
				fd.val != -1)
			{
				OrderValues orderVal;
				orderVal.filterId = fd.val;
				orderVal.filterName = fd.name;
				orderVal.count = val;

				values.push_back(orderVal);
			}
		}
	}

	if (values.size() > 0)
	{
		newBlock.index = 0;
		newBlock.random = m_CheckRandom->GetValue();
		newBlock.breakType = (BreakType)m_BreakChoice->GetSelection();
		newBlock.values = values;
		m_OrderData.push_back(newBlock);
	}

	CreateOrderData();

	// disable the checkbox after adding
	for (auto item : m_OrderingDynamic)
	{
		wxCommandEvent e;
		item.activeCheckbox->SetValue(false);
		item.spinValueControl->SetValue(0);
		e.SetEventObject(item.activeCheckbox);
		BlockCheckBox(e);
	}

	this->Layout();
}

/////////////////////////////////////////////////////
/// BlockBreakChoice
/// Desc: Choice box event for the dynamic block filter data 
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::BlockBreakChoice(wxCommandEvent& WXUNUSED(event))
{

}

/////////////////////////////////////////////////////
/// BlockCheckBox
/// Desc: Checkbox event for the dynamic block filter data 
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::BlockCheckBox(wxCommandEvent& event)
{
	wxCheckBox* pObj = reinterpret_cast<wxCheckBox *>(event.GetEventObject());
	for (auto item : m_OrderingDynamic)
	{
		if (pObj == item.activeCheckbox)
		{
			if (pObj->GetValue())
			{
				item.spinValueControl->Enable();
				item.textInUse->Enable();
				item.textRemaining->Enable();
				item.filterName->Enable();
			}
			else
			{
				item.spinValueControl->Disable();
				item.textInUse->Disable();
				item.textRemaining->Disable();
				item.filterName->Disable();
			}

			// call the spinner
			wxSpinEvent e;
			e.SetEventObject(item.spinValueControl);
			BlockCountChange(e);
			return;
		}
	}
}

/////////////////////////////////////////////////////
/// BlockCountChangeTxt
/// Desc: SpinCtrl event for the dynamic filter values
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::BlockCountChangeTxt(wxCommandEvent& event)
{
	wxSpinEvent e;
	e.SetEventObject(event.GetEventObject());
	BlockCountChange(e);
}

/////////////////////////////////////////////////////
/// BlockCountChange
/// Desc: SpinCtrl event for the dynamic filter values
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::BlockCountChange(wxSpinEvent& event)
{
	wxSpinCtrl* pObj = reinterpret_cast<wxSpinCtrl *>(event.GetEventObject());
	for (auto item : m_OrderingDynamic)
	{
		if (pObj == item.spinValueControl) // find the data
		{
			const auto fd = sys::AppData::Instance()->FindFilter(item.fd.val);

			int currentCountForFilter = 0;
			for (auto currentItem : m_OrderData)
			{
				if (currentItem.values.size() > 0)
				{
					for (auto val : currentItem.values)
					{
						if (val.filterId == fd.val)
						{
							currentCountForFilter += val.count;
						}
					}
				}
			}

			// get the selection counts for this filter
			for (auto settingData : m_SettingsCountList)
			{
				// check the control matches and update the val
				if (settingData.fd.val == fd.val)
				{
					wxString str = settingData.input->GetValue();
					long inputVal = -1;
					if (str.Length() != 0)
					{
						if (str.ToLong(&inputVal))
						{
							int diff = (int)inputVal - currentCountForFilter;

							item.textInUse->SetLabel(wxString::Format(_("In Use: %d"), currentCountForFilter));
							item.textInUse->Layout();

							if (diff < 0)
								diff = 0;

							item.textRemaining->SetLabel(wxString::Format(_("Remaining: %d"), diff));
							item.textRemaining->Layout();

							//item.spinValueControl->SetValue(0);
							item.spinValueControl->SetRange(0,diff);

							if (item.spinValueControl->IsEnabled())
							{
								if (diff == 0)
									item.spinValueControl->Disable();
								else
									item.spinValueControl->Enable();
							}
						}
					}
					else
					{

					}
				}
			}

			// done
			return;
		}
	}
}

// m_list is an anyframe class variable pointing to wxListCtrl
// m_drag is an anyframe class variable containing the index value on drag start
// OnDragStart is driven from event table when user starts a drag (left button is down)
void DlgCreateEditPrograms::BeginDrag(wxListEvent &WXUNUSED(event))
{
/*	m_ListDragIndex = event.GetIndex();	// save the start index
	// do some checks here to make sure valid start
	//...
	// trigger when user releases left button (drop)
	m_OrderList->Connect(wxEVT_LEFT_UP, wxMouseEventHandler(DlgCreateEditPrograms::OnDragEnd), NULL, this);
	// trigger when user leaves window to abort drag
	m_OrderList->Connect(wxEVT_LEAVE_WINDOW, wxMouseEventHandler(DlgCreateEditPrograms::OnDragQuit), NULL, this);
	// give visual feedback that we are doing something
	m_OrderList->SetCursor(wxCursor(wxCURSOR_HAND));*/
}

// drop a list item (start row is in m_drag)
void DlgCreateEditPrograms::OnDragEnd(wxMouseEvent& WXUNUSED(event))
{
/*	wxPoint pos = event.GetPosition();  // must reference the event
	int flags = wxLIST_HITTEST_ONITEM;
	long index = m_OrderList->HitTest(pos, flags, NULL); // got to use it at last
	if (index >= 0 && index != m_ListDragIndex)
	{ // valid and not the same as start
		
		std::swap(m_OrderData[index], m_OrderData[m_ListDragIndex]);

		CreateOrderData();
	}			

	// restore cursor
	m_OrderList->SetCursor(wxCursor(*wxSTANDARD_CURSOR));
	// disconnect both functions
	m_OrderList->Disconnect(wxEVT_LEFT_UP, wxMouseEventHandler(DlgCreateEditPrograms::OnDragEnd));
	m_OrderList->Disconnect(wxEVT_LEAVE_WINDOW, wxMouseEventHandler(DlgCreateEditPrograms::OnDragQuit));*/

}
// abort dragging a list item because user has left window
void DlgCreateEditPrograms::OnDragQuit(wxMouseEvent& WXUNUSED(event))
{
	// restore cursor and disconnect unconditionally
/*	m_OrderList->SetCursor(wxCursor(*wxSTANDARD_CURSOR));
	m_OrderList->Disconnect(wxEVT_LEFT_UP, wxMouseEventHandler(DlgCreateEditPrograms::OnDragEnd));
	m_OrderList->Disconnect(wxEVT_LEAVE_WINDOW, wxMouseEventHandler(DlgCreateEditPrograms::OnDragQuit));*/
}


/////////////////////////////////////////////////////
/// CopyPasteData
/// Desc: Key event callback on the word bank grid for pasting new words/sentences from the clipboard
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::CopyPasteData(wxKeyEvent& event)
{
	BankFileInfo bf = sys::AppData::Instance()->GetSelectedBank();
	if (bf.jsonData.isNull())
		return;

	// handle ctrl+c or ctrl+v
	if ((event.GetUnicodeKey() == 'C') && (event.ControlDown() == true)) {

		//copy

	}
	else if ((event.GetUnicodeKey() == 'V') && (event.ControlDown() == true)) {
		//paste
		wxCommandEvent empty;
		Paste(empty);
	}
}

/////////////////////////////////////////////////////
/// Paste
/// Desc: Event callback for pasting from the clipboard
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::Paste(wxCommandEvent &WXUNUSED(event))
{
	if (wxTheClipboard->Open())
	{
		if (wxTheClipboard->IsSupported(wxDF_UNICODETEXT))
		{
			wxTextDataObject data;
			wxTheClipboard->GetData(data);

			wxString str = data.GetText();

			wxString curField;
			wxString curLine;

			bool validText = true;

			// go through the info in the clipboard and separate by newline
			auto currentBank = sys::AppData::Instance()->GetBank();
			do
			{
				curLine = str.BeforeFirst('\n');
				str = str.AfterFirst('\n');

				do
				{
					curField = curLine.BeforeFirst('\t');
					curLine = curLine.AfterFirst('\t');

					if (curField.Length() > 0)
					{
						// check if it exists already in the bank

						auto search = currentBank.find(curField);
						if (search != currentBank.end())
						{
							validText = false;
						}
					}

				} while (curLine.IsEmpty() == false);

			} while (str.IsEmpty() == false);

			// all valid we can show the import dialog
			if (validText)
			{
				DlgBankImport* dlg = new DlgBankImport(0, data.GetText());
				if (dlg->ShowModal() == RESPONSE_OK)
				{
					CreateGridFromBank();

					SetCurrentSelection();
					CheckSelectionCounts();

					sys::AppData::Instance()->SaveBank();

					CheckValues();
				}

				dlg->Destroy();
				delete dlg;
			}
		}
		wxTheClipboard->Close();
	}
}

/////////////////////////////////////////////////////
/// RightClick
/// Desc: Right click anywhere on the word grid event
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::RightClick(wxMouseEvent& WXUNUSED(event))
{
	BankFileInfo bf = sys::AppData::Instance()->GetSelectedBank();
	if (bf.jsonData.isNull())
		return;

	wxPoint point = wxGetMousePosition();//event.GetPosition();

	// If from keyboard
	if (point.x == -1 && point.y == -1)
	{
		wxSize size = GetSize();
		point.x = size.x / 2;
		point.y = size.y / 2;
	}
	else
	{
		//point = ScreenToClient(point);
		//point = ClientToScreen(point);
	}

	point.x = point.x - this->GetScreenPosition().x;
	point.y = point.y - this->GetScreenPosition().y;

	ShowContextMenu(point);
}

#define ID_CLIPBOARD_PASTE		100

/////////////////////////////////////////////////////
/// OnPopupClick
/// Desc: popup menu selection event
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::OnPopupClick(wxCommandEvent &event)
{
	//void *data = static_cast<wxMenu *>(event.GetEventObject())->GetClientData();
	switch (event.GetId())
	{
	case ID_CLIPBOARD_PASTE:
	{
		wxCommandEvent empty;
		Paste(empty);
	}break;
	default:
		break;
	}
}

/////////////////////////////////////////////////////
/// ShowContextMenu
/// Desc: Shows the paste context menu popup
/////////////////////////////////////////////////////
void DlgCreateEditPrograms::ShowContextMenu(const wxPoint& pos)
{
	wxMenu menu;

	menu.Append(ID_CLIPBOARD_PASTE, _("Paste From Clipboard"));
	menu.Connect(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DlgCreateEditPrograms::OnPopupClick), NULL, this);

	wxPoint p = pos;
	//p.y = p.y + 100;
	PopupMenu(&menu, p);
}
